Chnges:
-------
a) Themes.php helper in libaries folder
b) Added ui_main.offers translation string to english file in il8n directory
c) Added offers menu in nav helper
d) Edited main controller index function code - added session code to track user selected city from line 452 to 460
e) Removed class=\"content-block\" on line 21 & 25 of Blocks.php
f) Added arr::get to array helper in applications folder
g) Added twitter and google functions to login.php and their required vars (secrets and keys) to settings table
h) Added offers category to main.php view in geoplaces theme with category id of 'o' for offers
i) Edited cluster and index functions of json controller to accomodate offers requests
j) Edited fetch_offerincidents in offerreports helper line 627: Check for the offercategory param in url to accomodate the non-numeric 'o' value for the home page map offers category
k) Added variable for default offers category color for home page map to settings table and passed it to main page in index function
l) Edited ui_main.all_categories and added .all_offers
m) Edited offerreports controller view action to accomodate changed comment styling i.e. extra ratings field on comment form and average rating value calculation i.e. offerincident_rating
n) Added 'offerincident_id' and 'comment_' field to comments & ratings table
o) Edited offerreports_comments and offerreports_comments_form views to match new review style of commenting
p) Edited offerincident model's get_comments function to return comment data from both comment and ratings tables
q) added dynamic rating js to offerreports_view_js file
r) Edited all blocks again and main theme views

Changed files:
blog controller
ui.main - added .reviews .leave_a_review .review_btn_submit .similar_articles .tag_cloud keys for offer and article comment heading
article & article_comments view
articles model and table
style.css
added ckeditor js script tag to admin layout.php file

added article_tags model
added 