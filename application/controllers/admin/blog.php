<?php defined('SYSPATH') or die('No direct script access.');
/**
 * This controller handles frontend blog functionality
 *
 * @version 01 - Hezron Obuchele 2012-04-25
 *
 * PHP version 5
 * LICENSE: Not for reuse or modification without the express 
 * written authorization from BeeBuy Investments Ltd.
 *
 * Widget_Controller
 * @author     Hezron Obuchele
 * @package    CrowdPesa
 * @subpackage Controllers
 * @copyright  BeeBuy Investments Ltd. - http://www.beebuy.biz
 */

class Blog_Controller extends Admin_Controller{
	
	function __construct()
	{
		parent::__construct();
		$this->template->this_page = 'blog';
		$this->params = array('all_articles' => TRUE);
	}
	
	/**
	 * Show all articles in a list
	 */
	public function index()
	{
		if (!empty($_GET['status']))
        {
            $status = $_GET['status'];
            
           if (strtolower($status) == 'p')
            {
                $filter = 'published = 1 ';
            }
            elseif (strtolower($status) == 'u')
            {
                $filter = 'published = 0';
            }
         	elseif (strtolower($status) == 'd')
            {
                $filter = 'published = 2';
            }
            else
            {
                $status = "0";
                $filter = '1=1';
            }
        }
        else
        {
            $status = "0";
		    $filter = '1=1';
        }
		// Get Search Keywords (If Any)
		if (isset($_GET['k']))
		{
			//	Brute force input sanitization

			// Phase 1 - Strip the search string of all non-word characters
			$keyword_raw = (isset($_GET['k']))? preg_replace('#/\w+/#', '', $_GET['k']) : "";

			// Strip any HTML tags that may have been missed in Phase 1
			$keyword_raw = strip_tags($keyword_raw);

			// Phase 3 - Invoke Kohana's XSS cleaning mechanism just incase an outlier wasn't caught
			// in the first 2 steps
			$keyword_raw = $this->input->xss_clean($keyword_raw);

			$filter = " (".$this->_get_searchstring($keyword_raw).")";

			array_push($this->params, $filter);
		}
		else
		{
			$keyword_raw = "";
		}

		// check, has the form been submitted?
        $form_error = FALSE;
        $form_saved = FALSE;
        $form_action = "";
		if ($_POST)
		{
			$post = Validation::factory($_POST);

			 //	Add some filters
			$post->pre_filter('trim', TRUE);

			// Add some rules, the input field, followed by a list of checks, carried out in order
			$post->add_rules('action','required', 'alpha', 'length[1,1]');
			$post->add_rules('article_id.*','required','numeric');

			if ($post->validate())
			{
				// Publish Action
				if ($post->action == 'p')
				{
					foreach($post->article_id as $item)
					{
						
							$update = new Articles_Model($item);
							if ($update->loaded == TRUE)
							{
								$update->published ='1';

								$update->save();

								
								// Action::report_approve - Approve a Report
								Event::run('ushahidi_action.article_published', $update);
							}
						
						$form_action = strtoupper(Kohana::lang('ui_admin.published'));
					}

				}
				// Unpublish Action
				elseif ($post->action == 'u')
				{
					foreach ($post->article_id  as $item)
					{
						$update = new Articles_Model($item);
						if ($update->loaded == TRUE)
						{
							$update->published = '0';

							
							$update->save();

							
							// Action::article_unpublish - unpublish a Report
							Event::run('ushahidi_action.article_unpublished', $update);
						}
					}
					$form_action = strtoupper(Kohana::lang('ui_admin.unpublished'));
				}
				
				//Delete Action
				elseif ($post->action == 'd')
				{
					foreach($post->article_id as $item)
					{
						$update = new Articles_Model($item);
						if ($update->loaded == TRUE)
						{
							$article_id = $update->id;
							$update->delete();

							

							// Delete Photos From Directory
							foreach (ORM::factory('media')->where('article_id',$article_id)->where('media_type', 1) as $photo)
							{
								deletePhoto($photo->id);
							}

							// Delete Media
							ORM::factory('media')->where('article_id',$article_id)->delete_all();

							
							// Delete Comments
							ORM::factory('comment')->where('article_id',$article_id)->delete_all();

							

							// Action::report_delete - Deleted a Report
							Event::run('ushahidi_action.article_delete', $article_id);
						}
					}
					$form_action = strtoupper(Kohana::lang('ui_admin.deleted'));
				}
				$form_saved = TRUE;
			}
			else
			{
				$form_error = TRUE;
			}

		}
		// Pagination
        $pagination = new Pagination(array(
            'query_string'    => 'page',
            'items_per_page' => $this->items_per_page,
            'total_items'    => ORM::factory('articles')->where($filter)->count_all()
        ));
		
		$content = View::factory('blog/admin_default');
		$content->articles = ORM::factory('articles')
			->where($filter)
			->limit($this->items_per_page)
			->orderby('published_date', 'desc')
			->find_all();
		$content->comment_count = array();
		$db = Database::instance();
		foreach ($content->articles as $a) {
			$comment_count = $db->query("SELECT COUNT(*) 'total' FROM comment WHERE article_id = $a->id");
			$content->comment_count[$a->id] = $comment_count[0]->total;
		}
		//$content->tag_cloud = Article_Tags_Model::get_all_tags();
		$this->template->content = $content;
		$this->template->content->title = 'Blog';

        $this->template->content->pagination = $pagination;
        $this->template->content->form_error = $form_error;
        $this->template->content->form_saved = $form_saved;
        $this->template->content->form_action = $form_action;
        
        // Total offerreports
        $this->template->content->total_items = $pagination->total_items;
        
        // Status Tab
        $this->template->content->status = $status;
        
        // Javascript Header
        $this->template->js = new View('admin/blog_js');
		
		
	}
	
	public function edit($article_id = null){
		$db = new Database();

		// If user doesn't have access, redirect to dashboard
		if ( ! admin::permissions($this->user, "reports_edit"))
		{
			url::redirect(url::site().'admin/dashboard');
		}

		$this->template->content = new View('blog/admin_edit');
		$this->template->blog_edit = true;
	 	$this->template->js = new View('blog/admin_edit_js');

		$this->template->content->title = Kohana::lang('ui_admin.create_blogarticle');

		$form = array
		(
			'article_id' => '',
			'author_id' => '',
			'post_title' => '',
			'post_content' => '',
			'post_published' => '',
			'post_published_date' => date('Y-m-d H:i:s'),
			'post_meta_keywords' => '',
			'post_meta_description' => '',
			'article_photo' => array(),
			'post_tags' => '');
		
		// Get the incident media
		$article_media =  Articles_Model::is_valid_article($article_id)
			? ORM::factory('articles', $article_id)->media
			: FALSE;

		$this->template->content->article_media = $article_media;

		$errors = $form;
		$form_error = FALSE;
		$form_saved = FALSE;
		if ($_POST)
		{
			//print_r($_POST);exit;
			$post = Validation::factory($_POST);

			 //	 Add some filters
			//$post->pre_filter('trim', TRUE);

			//$post->add_rules('post_title','required','alpha_numeric');
			
			
			//print_r($_POST); exit;

			if ($post->validate())
			{

				

				$article = (isset($article_id) AND Articles_Model::is_valid_article($article_id))
				? ORM::factory('articles', $article_id)
				: new Articles_Model();
				
							
				$article->author_id = $this->user;
				$article->title = $post->post_title;
				$article->content = $post->post_content;
				$article->published = $post->post_published;
				$article->published_date = date('Y-m-d H:i:s');
				$article->meta_keywords = $post->post_meta_keywords;
				$article->meta_description = $post->post_meta_description;
				// TODO: check paths first before saving relative url to db
				//$article->image = $post->post_image;
				$article->save();					
				blog::save_media(array_merge($_POST,$_FILES), $article);
				
				// save tags to tag table
				
				$posted_tags = explode(',',arr::get($_POST, 'post_tags'));
				blog::save_tags($posted_tags,$article);

	 			$form_saved = TRUE;
	 			// redirect if "Save and Close" button was clicked
	 			if (isset($_POST['save_and_close'])){
	 				url::redirect(url::site() . 'admin/blog');}

				// repopulate the form fields
				$form = arr::overwrite($form, $post->as_array());
				
			}
			else
			{
				// repopulate the form fields
				$form = arr::overwrite($form, $post->as_array());

				// populate the error fields, if any
				$errors = arr::overwrite($errors, $post->errors('auth'));
				$form_error = TRUE;
			}

	}
	else
		{
			$article = ORM::factory('articles',$article_id);
			$tags =  Article_Tags_Model::get_article_tags($article_id);
					// Retrieve Media
					
			//$incident_video = array();
			$article_photo = array();
			foreach($article->media as $media)
			{
			
				//elseif ($media->media_type == 2)
				//{
					//$incident_video[] = $media->media_link;
				//}
				if ($media->media_type == 1)
				{
					$article_photo[] = $media->media_link;
				}
			}
			
			$form['article_id'] = $article->id;
			$form['author_id'] = $article->author_id;
			$form['post_title'] = $article->title;
			$form['post_content'] = $article->content;
			$form['post_published'] = $article->published;
			$form['post_published_date'] = $article->published_date;
			$form['article_photo'] = $article_photo;
			$form['post_meta_description'] = $article->meta_description;
			$form['post_meta_keywords'] = $article->meta_keywords;
			$form['post_tags'] =implode(',',$tags);
			
		}

		
		$this->template->content->form = $form;
		$this->template->content->errors = $errors;
		$this->template->content->form_error = $form_error;
		$this->template->content->form_saved = $form_saved;
		$this->template->content->published_array = array('0'=>strtoupper(Kohana::lang('ui_main.unpublish')),'1'=>strtoupper(Kohana::lang('ui_main.publish')),'2'=>strtoupper(Kohana::lang('ui_main.draft')));

		//$this->template->content->yesno_array = array('1'=>strtoupper(Kohana::lang('ui_main.yes')),'0'=>strtoupper(Kohana::lang('ui_main.no')));

		//// Javascript Header
		//$this->template->colorpicker_enabled = TRUE;
}
	
	/**
	* Delete Photo
	* @param int $id The unique id of the photo to be deleted
	*/
	public function deletePhoto ($id)
	{
		$this->auto_render = FALSE;
		$this->template = "";

		if ($id)
		{
			$photo = ORM::factory('media', $id);
			$photo_large = $photo->media_link;
			$photo_medium = $photo->media_medium;
			$photo_thumb = $photo->media_thumb;

			if (file_exists(Kohana::config('upload.directory', TRUE).$photo_large))
			{
				unlink(Kohana::config('upload.directory', TRUE).$photo_large);
			}
			elseif (Kohana::config("cdn.cdn_store_dynamic_content") AND valid::url($photo_large))
			{
				cdn::delete($photo_large);
			}

			if (file_exists(Kohana::config('upload.directory', TRUE).$photo_medium))
			{
				unlink(Kohana::config('upload.directory', TRUE).$photo_medium);
			}
			elseif (Kohana::config("cdn.cdn_store_dynamic_content") AND valid::url($photo_medium))
			{
				cdn::delete($photo_medium);
			}

			if (file_exists(Kohana::config('upload.directory', TRUE).$photo_thumb))
			{
				unlink(Kohana::config('upload.directory', TRUE).$photo_thumb);
			}
			elseif (Kohana::config("cdn.cdn_store_dynamic_content") AND valid::url($photo_thumb))
			{
				cdn::delete($photo_thumb);
			}

			// Finally Remove from DB
			$photo->delete();
		}
	}
	/**
	 * Creates a SQL string from search keywords
	 */
	private function _get_searchstring($keyword_raw)
	{
		$or = '';
		$where_string = '';

		/**
		 * NOTES: 2011-11-17 - John Etherton <john@ethertontech.com> I'm pretty sure this needs to be
		 * internationalized, seems rather biased towards English.
		 * */
		// Stop words that we won't search for
		// Add words as needed!!
		$stop_words = array('the', 'and', 'a', 'to', 'of', 'in', 'i', 'is', 'that', 'it',
		'on', 'you', 'this', 'for', 'but', 'with', 'are', 'have', 'be',
		'at', 'or', 'as', 'was', 'so', 'if', 'out', 'not');

		$keywords = explode(' ', $keyword_raw);

		if (is_array($keywords) AND !empty($keywords))
		{
			array_change_key_case($keywords, CASE_LOWER);
			$i = 0;

			foreach ($keywords as $value)
			{
				if (!in_array($value,$stop_words) AND !empty($value))
				{
					$chunk = $this->db->escape_str($value);
					if ($i > 0)
					{
						$or = ' OR ';
					}
					$where_string = $where_string
									.$or
									."title LIKE '%$chunk%' OR meta_keywords LIKE '%$chunk%' OR meta_description LIKE '%$chunk%' ";
					$i++;
				}
			}
		}

		// Return
		return (!empty($where_string)) ? $where_string :  "1=1";
	}

}