<?php defined('SYSPATH') or die('No direct script access.');
/**
 * Checkins Controller.
 *
 * PHP version 5
 * LICENSE: This source file is subject to LGPL license
 * that is available through the world-wide-web at the following URI:
 * http://www.gnu.org/copyleft/lesser.html
 * @author     Ushahidi Team <team@ushahidi.com>
 * @package    Ushahidi - http://source.ushahididev.com
 * @subpackage Admin
 * @copyright  Ushahidi - http://www.ushahidi.com
 * @license    http://www.gnu.org/copyleft/lesser.html GNU Lesser General Public License (LGPL)
 */

class Invites_Controller extends Admin_Controller
{
    function __construct()
    {
        parent::__construct();
        
        $this->template->this_page = 'invites';
        
        // If user doesn't have access, redirect to dashboard
        if ( ! admin::permissions($this->user, "users"))
        {
            url::redirect(url::site().'admin/dashboard');
        }
    }

    /**
    * Lists the checkins
    */
    function index()
    {
		$this->template->content = new View('admin/invites');
    	$this->template->content->title = Kohana::lang('ui_admin.invites');
    	
    	// check, has the form been submitted?
        $form_error = FALSE;
        $form_saved = FALSE;
        $form_action = '';
        $filter = '1=1';
        
        
        // Form submission wizardry
        // check, has the form been submitted, if so, setup validation
        if ($_POST)
        {
            // Instantiate Validation, use $post, so we don't overwrite $_POST fields with our own things
            $post = Validation::factory($_POST);

                //  Add some filters
            $post->pre_filter('trim', TRUE);

            // Add some rules, the input field, followed by a list of checks, carried out in order
            $post->add_rules('action','required', 'alpha', 'length[1,1]');
            $post->add_rules('invite_id.*','required','numeric');

            // Test to see if things passed the rule checks
            if ($post->validate())
            {
                if( $post->action == 'd' )              // Delete Action
                {
                    foreach($post->invite_id as $invite_id)
                    {
                        // Delete Invitee
                        ORM::factory('invite')->delete($invite_id);
                    }
                    
                    $form_saved = TRUE;
                    $form_action = strtoupper(Kohana::lang('ui_admin.deleted'));
                }else if($post->action == 's' ){

                	  //$success = true;
	                $message_class = 'info';
	                $invites =  $post->invite_id;	               
	               
	                
	                foreach($invites as $invite){
	                    // Send invitation email
	                    $user = ORM::factory('invite',$invite);

	                    $invite_code = $this->_generate_invite_code();
	                    $success = true;
	                    
	                    $email_sent = $this->_send_email_invite($user->email, $invite_code);
	                    if ($email_sent === null){
	                        $success = FALSE;
	                       // $failed_emails[] = $email;
	                    }else{
	                        $user->code = $invite_code;
	                        $user->save();

	                    }
	                    
                    $form_saved = TRUE;
                    $form_action = strtoupper(Kohana::lang('ui_admin.saved'));
                }
            }
            // No! We have validation errors, we need to show the form again, with the errors
            else
            {
                // repopulate the form fields
                $form = arr::overwrite($form, $post->as_array());

                // populate the error fields, if any
                $errors = arr::overwrite($errors, $post->errors('checkin'));
                $form_error = TRUE;
            }
        }
        }
        
        
        
        // Pagination
        $pagination = new Pagination(array(
            'query_string'   => 'page',
            'items_per_page' => $this->items_per_page,
            'total_items'    => ORM::factory('invite')
            					->where('code','')
            					->count_all()
        ));
        
        $invites = ORM::factory('invite')
        						->where('code','')
        						->orderby('email','desc')
                                ->find_all($this->items_per_page, $pagination->sql_offset);
        
        $this->template->content->invites = $invites;
        $this->template->content->pagination = $pagination;
        $this->template->content->form_error = $form_error;
        $this->template->content->form_saved = $form_saved;
        $this->template->content->form_action = $form_action;
        $this->template->content->total_items = $pagination->total_items;
        
        // Javascript Header
        $this->template->js = new View('admin/invites_js');
	    
   	}
   		private function _generate_invite_code () {
	    $chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";  
	    $length = 15;
	    $str = substr(str_shuffle($chars), 0, $length);
	    return $str;
    }
     
	/**
     * Sends an email confirmation
     */
    private function _send_email_invite($email, $invite_code)
    {
        $settings = kohana::config('settings');
        //die(var_dump($settings));
        // Check if we require users to go through this process
        if (isset($settings['require_email_invite']) && $settings['require_email_invite'] == 0)
        {
            return false;
        }

        $url = url::site()."customers/join/?ic=$invite_code";//&e=$email";

        $to = $email;
        $from = array($settings['site_email'], $settings['site_name']);
        $subject = $settings['site_name'].' '.Kohana::lang('ui_main.invite_confirmation_subject', array($settings['site_name']));
        $message = Kohana::lang('ui_main.invite_confirmation_message',
            array(Auth::instance()->get_user()->name, $settings['site_name'], $url));
        try{
            email::send($to, $from, $subject, $message, FALSE);

        }catch (Exception $e){
            return null;
            //Kohana::log('error', "email to $address could not be sent - ");
        }
        return true;
    }

}
