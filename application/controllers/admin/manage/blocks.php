<?php defined('SYSPATH') or die('No direct script access.');
/**
 * Blocks Controller
 * Add/Edit Ushahidi Instance Shares
 *
 * PHP version 5
 * LICENSE: This source file is subject to LGPL license 
 * that is available through the world-wide-web at the following URI:
 * http://www.gnu.org/copyleft/lesser.html
 * @author	   Ushahidi Team <team@ushahidi.com> 
 * @package	   Ushahidi - http://source.ushahididev.com
 * @subpackage Admin
 * @copyright  Ushahidi - http://www.ushahidi.com
 * @license	   http://www.gnu.org/copyleft/lesser.html GNU Lesser General Public License (LGPL) 
 */

class Blocks_Controller extends Admin_Controller
{
	private $_registered_blocks;

	function __construct()
	{
		parent::__construct();
		$this->template->this_page = 'settings';
		
		// If user doesn't have access, redirect to dashboard
		if ( ! admin::permissions($this->user, "manage"))
		{
			url::redirect(url::site().'admin/dashboard');
		}
		
		$this->_registered_blocks = Kohana::config("settings.blocks");
	}
	
	function index()
	{
		$this->template->content = new View('admin/blocks');
		$this->template->content->title = Kohana::lang('ui_admin.blocks');
		
		// Get Registered Blocks 
		if ( ! is_array($this->_registered_blocks) )
		{
			$this->_registered_blocks = array();
		}else{
			$registered_blocks = array();
			$content_blocks = ORM::factory('content_block')->select('name')->find_all()->as_array();
			
			foreach($this->_registered_blocks as $name=>$props){
				$registered_blocks[] = $name;
			}
			

		
			$blocks = array_diff($registered_blocks,$content_blocks);
			//print_r($blocks);exit;
			$i=1;
			foreach($blocks as $block){
				$temp = new Content_Block_Model();
				$temp->name=$block;
				$temp->ordering = $i;
				$temp->save();
				$i++;
			}
		}
		
		// Get Active Blocks
		$settings = ORM::factory('settings', 1);
		$active_blocks = $settings->blocks;
		$active_blocks = array_filter(explode("|", $active_blocks));
		
		
		// setup and initialize form field names
		$form = array
		(
			'action' => '',
			'block' => ''
		);
		//	copy the form as errors, so the errors will be stored with keys corresponding to the form field names
		$errors = $form;
		$form_error = FALSE;
		$form_saved = FALSE;
		$form_action = "";

		if ( $_POST )
		{
			$post = Validation::factory($_POST);

			 //	 Add some filters
			$post->pre_filter('trim', TRUE);

			// Add some rules, the input field, followed by a list of checks, carried out in order
			$post->add_rules('action','required', 'alpha', 'length[1,1]');
			$post->add_rules('block','required', 'alpha_dash');
			if ( ! array_key_exists($post->block, $this->_registered_blocks))
			{
				$post->add_error('block','exists');
			}
			
			if ($post->validate())
			{
				// Activate a block
				if ($post->action == 'a')
				{
					array_push($active_blocks, $post->block);
					$settings->blocks = implode("|", $active_blocks);
					$settings->save();
				}
				
				// Deactivate a block
				elseif ($post->action =='d')
				{
					$active_blocks = array_diff($active_blocks, array($post->block));
					$settings->blocks = implode("|", $active_blocks);
					$settings->save();
				}
			}
			else
			{
				$errors = arr::overwrite($errors, $post->errors('blocks'));
				$form_error = TRUE;
			}
		}
		
		// Sort the Blocks
		$sorted_blocks = blocks::sort($active_blocks, array_keys($this->_registered_blocks));
		
		$this->template->content->form = $form;
		$this->template->content->errors = $errors;
		$this->template->content->form_error = $form_error;
		$this->template->content->form_saved = $form_saved;
		$this->template->content->form_action = $form_action;
		$this->template->content->total_items = count($this->_registered_blocks);
		$this->template->content->registered_blocks = $this->_registered_blocks;
		$this->template->content->active_blocks = $active_blocks;
		$this->template->content->sorted_blocks = $sorted_blocks;
		
		// Javascript Header
		$this->template->tablerowsort_enabled = TRUE;
		$this->template->js = new View('admin/blocks_js');
	}
	
	public function sort()
	{
		$this->auto_render = FALSE;
		$this->template = "";
		
		if ($_POST)
		{
			if (isset($_POST['blocks'])
				AND ! empty($_POST['blocks'])
				)
			{
				$settings = ORM::factory('settings', 1);
				$active_blocks = $settings->blocks;
				$active_blocks = array_filter(explode("|", $active_blocks));
				
				$blocks = array_map('trim', explode(',', $_POST['blocks']));
				$block_check = array();
				$i = 1;
				foreach ($blocks as $block)
				{
					$temp = ORM::factory('content_block',$block);
					$temp->ordering = $i;
					$temp->save();
					$i++;
					if ( in_array($block, $active_blocks) )
					{
						$block_check[] = $block;
					}
				}
				
				$settings = ORM::factory('settings', 1);
				$settings->blocks = implode("|", $block_check);
				$settings->save();
			}
		}
	}
	/**/
	public function edit($block_name){
		

		$this->template->content = new View('admin/block_edit');
		
		$this->template->content->title = Kohana::lang('ui_admin.block_settings');

		$form = array
		(
			'block_name' => '',
			'position' => '',
			'pages' => '');
		$form_pages = array('home'=>'Home','search'=>'Search','places'=>'Places','offers'=>'Offers','blog'=>'Blog Page','article'=>'Article Page');
		

		$errors = $form;
		$form_error = FALSE;
		$form_saved = FALSE;
		$pages = array();
		if ($_POST)
		{
			//print_r($_POST);exit;
			$post = Validation::factory($_POST);

			

			if ($post->validate())
			{

				

				$block = (isset($block_name) AND Content_Block_Model::is_valid_content_block($block_name))
				? ORM::factory('content_block', $block_name)
				: new Content_Block_Model();
				
					
				$block->position = $post->position;
				$block->pages = (isset($_POST['pages']))?implode("|", $_POST['pages']):'';				
				$block->save();					
				

	 			$form_saved = TRUE;
	 			// redirect if "Save and Close" button was clicked
	 			if (isset($_POST['save_and_close'])){
	 				url::redirect(url::site() . 'admin/blog');}

				// repopulate the form fields
				$form = arr::overwrite($form, $post->as_array());
				
			}
			else
			{
				// repopulate the form fields
				$form = arr::overwrite($form, $post->as_array());

				// populate the error fields, if any
				$errors = arr::overwrite($errors, $post->errors('blocks'));
				$form_error = TRUE;
			}

	}
	else
		{
			$block = ORM::factory('content_block',$block_name);
			
			$pages = explode('|',$block->pages);
			$form['block_name'] = $block->name;
			$form['position'] = $block->position;
			
		}

		
		$this->template->content->form = $form;
		$this->template->content->errors = $errors;
		$this->template->content->form_error = $form_error;
		$this->template->content->form_saved = $form_saved;
		$this->template->content->position_array = array('body'=>'body','right'=>'right');
		$this->template->content->pages = $pages;
		$this->template->content->form_pages = $form_pages;
		//$this->template->content->yesno_array = array('1'=>strtoupper(Kohana::lang('ui_main.yes')),'0'=>strtoupper(Kohana::lang('ui_main.no')));

		//// Javascript Header
		//$this->template->colorpicker_enabled = TRUE;
} 
}
