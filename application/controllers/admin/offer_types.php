<?php defined('SYSPATH') or die('No direct script access.');
/**
 * Comments Controller.
 * This controller will take care of viewing and editing comments in the Admin section.
 *
 * PHP version 5
 * LICENSE: This source file is subject to LGPL license 
 * that is available through the world-wide-web at the following URI:
 * http://www.gnu.org/copyleft/lesser.html
 * @author     Ushahidi Team <team@ushahidi.com> 
 * @package    Ushahidi - http://source.ushahididev.com
 * @subpackage Admin
 * @copyright  Ushahidi - http://www.ushahidi.com
 * @license    http://www.gnu.org/copyleft/lesser.html GNU Lesser General Public License (LGPL) 
 */

class Offer_Types_Controller extends Admin_Controller
{
    function __construct()
    {
        parent::__construct();
    
        $this->template->this_page = 'offer_types';
        
        // If user doesn't have access, redirect to dashboard
       
    }
    
    
   public function index()
	{

		$this->template->content = new View('admin/offer_types');
		$this->template->content->title = Kohana::lang('ui_admin.offer_types');

		

		// Setup and initialize form field names
		$form = array
		(
			'action' => '',
			'offer_type_id' => '',
			'title' => '',
			'description' => '',
			'icon' => ''
		);

		
		// Copy the form as errors, so the errors will be stored with keys corresponding to the form field names
		$errors = $form;
		$form_error = FALSE;
		$form_saved = FALSE;
		$form_action = "";
		

		
		if($_POST){
				// Fetch the post data
			$post_data = array_merge($_POST, $_FILES);
			
			// Extract offertype-specific  information
			$offertype_data = arr::extract($post_data,  'title', 'description');
			
			// Extract offertype image and offertype languages for independent validation
			$secondary_data = arr::extract($post_data, 'icon', 'action');
			
			// Setup validation for the secondary data
			$post = Validation::factory($secondary_data)
						->pre_filter('trim', TRUE);
						
			// Add validation for the add/edit action
			if ($post->action == 'a')
			{
				$post->add_rules('icon', 'upload::valid', 'upload::type[gif,jpg,png]', 'upload::size[50K]');

				
			}
			$offertype = (! empty($_POST['offer_type_id']) AND Offer_Type_Model::is_valid_offer_type($_POST['offer_type_id']))
				? new Offer_Type_Model($_POST['offer_type_id'])
				: new Offer_Type_Model();
			if ($post->action == 'a')
			{
				// Test to see if things passed the rule checks
				if ($offertype->validate($offertype_data) AND $post->validate())
				{
					// Save the offertype
					$offertype->save();
					
					// Get the offertype localization
					
					
					// Upload Image/Icon
					$filename = upload::save('icon');
					if ($filename)
					{
						$new_filename = "offertype_".$offertype->id."_".time();
						
						// Name the files for the DB
						//$cat_img_file = $new_filename.".png";
						$cat_img_thumb_file = $new_filename."_t.png";

						// Resize Image to 32px if greater
						//Image::factory($filename)->resize(24,24,Image::HEIGHT)
						//	->save(Kohana::config('upload.directory', TRUE) . $cat_img_file);
						// Create a 16x16 version too
						Image::factory($filename)->resize(20,20,Image::WIDTH)
							->save(Kohana::config('upload.directory', TRUE) . $cat_img_thumb_file);
							
						// Okay, now we have these three different files on the server, now check to see
						//   if we should be dropping them on the CDN
						
						if(Kohana::config("cdn.cdn_store_dynamic_content"))
						{
							//$cat_img_file = cdn::upload($cat_img_file);
							$cat_img_thumb_file = cdn::upload($cat_img_thumb_file);
							
							// We no longer need the files we created on the server. Remove them.
							$local_directory = rtrim(Kohana::config('upload.directory', TRUE), '/').'/';
							unlink($local_directory.$new_filename.".png");
							unlink($local_directory.$new_filename."_t.png");
						}

						// Remove the temporary file
						unlink($filename);

						// Delete Old Image
						$offertype_old_image = $offertype->icon;
						if ( ! empty($offertype_old_image))
						{
							if(file_exists(Kohana::config('upload.directory', TRUE).$offertype_old_image))
							{
								unlink(Kohana::config('upload.directory', TRUE).$offertype_old_image);
							}elseif(Kohana::config("cdn.cdn_store_dynamic_content") AND valid::url($offertype_old_image)){
								cdn::delete($offertype_old_image);
							}
						}

						// Save
						//$offertype->offertype_image = $cat_img_file;
						$offertype->icon = $cat_img_thumb_file;
						$offertype->save();
					}

					$form_saved = TRUE;
					$form_action = strtoupper(Kohana::lang('ui_admin.added_edited'));

					// Empty $form array
					array_fill_keys($form, '');
				}
				else
				{
					// Validation failed

					// Repopulate the form fields
					$form = arr::overwrite($form, array_merge($offertype_data->as_array(), $post->as_array()));

					// populate the error fields, if any
					$errors = arr::overwrite($errors, 
						array_merge($offertype_data->errors('offertype'), $post->errors('offertype')));

					$form_error = TRUE;
				}
				
			}
			elseif ($post->action == 'd')
			{
				
					// @todo Delete the category image
					
					// Delete category itself - except if it is trusted
					ORM::factory('offer_Type')
						
						->delete($offertype->id);
						
					$form_saved = TRUE;
					$form_action = strtoupper(Kohana::lang('ui_admin.deleted'));
				
			}
			elseif( $post->action == 'i')
			{ 
				// Delete Image/Icon Action
				if ($offertype->loaded)
				{
					$icon = $offertype->icon;
					

					if ( ! empty($icon)
						 AND file_exists(Kohana::config('upload.directory', TRUE).$icon))
					{
						unlink(Kohana::config('upload.directory', TRUE) . $icon);
					}

					
					$offertype->icon = NULL;
					
					$offertype->save();
					
					$form_saved = TRUE;
					$form_action = strtoupper(Kohana::lang('ui_admin.modified'));
				}

			}
			
			
			
		}
		// Pagination
		$pagination = new Pagination(array(
			'query_string' => 'page',
			'items_per_page' => $this->items_per_page,
			'total_items' => ORM::factory('offer_type')->count_all()
		));

		$offertypes = ORM::factory('offer_type')
						->find_all($this->items_per_page, $pagination->sql_offset);

		

		$this->template->content->form = $form;
		$this->template->content->errors = $errors;
		$this->template->content->form_error = $form_error;
		$this->template->content->form_saved = $form_saved;
		$this->template->content->form_action = $form_action;
		$this->template->content->pagination = $pagination;
		$this->template->content->total_items = $pagination->total_items;
		$this->template->content->offertypes = $offertypes;

		//$this->template->content->parents_array = $parents_array;

		// Javascript Header
		$this->template->colorpicker_enabled = TRUE;
		$this->template->tablerowsort_enabled = TRUE;
		$this->template->js = new View('admin/offer_types_js');
		$this->template->form_error = $form_error;

		//$this->template->content->locale_array = $locales;
		//$this->template->js->locale_array = $locales;
	
	}
}
