<?php defined('SYSPATH') or die('No direct script access.');
/**
 * Offerreports Controller.
 * This controller will take care of adding and editing offerreports in the Admin section.
 *
 * PHP version 5
 * LICENSE: This source file is subject to LGPL license
 * that is available through the world-wide-web at the following URI:
 * http://www.gnu.org/copyleft/lesser.html
 * @author	   Ushahidi Team <team@ushahidi.com>
 * @package	   Ushahidi - http://source.ushahididev.com
 * @subpackage Admin
 * @copyright  Ushahidi - http://www.ushahidi.com
 * @license	   http://www.gnu.org/copyleft/lesser.html GNU Lesser General Public License (LGPL)
 */

class Offerreports_Controller extends Admin_Controller {
	public function __construct()
	{
		parent::__construct();

		$this->template->this_page = 'offerreports';
		$this->params = array('all_offerreports' => TRUE);
	}


	/**
	 * Lists the offerreports.
	 *
	 * @param int $page
	 */
	public function index($page = 1)
	{
		// If user doesn't have access, redirect to dashboard
		if ( ! admin::permissions($this->user, "reports_view"))
		{
			url::redirect(url::site().'admin/dashboard');
		}

		$this->template->content = new View('admin/offerreports');
		$this->template->content->title = Kohana::lang('ui_admin.offerreports');

		// Database table prefix
		$table_prefix = Kohana::config('database.default.table_prefix');

		// Hook into the event for the offerreports::fetch_offerincidents() method
		Event::add('ushahidi_filter.fetch_offerincidents_set_params', array($this,'_add_offerincident_filters'));


		$status = "0";

		if ( !empty($_GET['status']))
		{
			$status = $_GET['status'];

			if (strtolower($status) == 'a')
			{
				array_push($this->params, 'i.offerincident_active = 0');
			}
			elseif (strtolower($status) == 'v')
			{
				array_push($this->params, 'i.offerincident_verified = 0');
			}
			elseif (strtolower($status) == 'f')
			{
				array_push($this->params, 'i.offerincident_featured = 0');
			}
			elseif (strtolower($status) == 'o')
			{
				array_push($this->params, 'ic.offercategory_id = 5');
			}
			else
			{
				$status = "0";
			}
		}

		// Get Search Keywords (If Any)
		if (isset($_GET['k']))
		{
			//	Brute force input sanitization

			// Phase 1 - Strip the search string of all non-word characters
			$keyword_raw = (isset($_GET['k']))? preg_replace('#/\w+/#', '', $_GET['k']) : "";

			// Strip any HTML tags that may have been missed in Phase 1
			$keyword_raw = strip_tags($keyword_raw);

			// Phase 3 - Invoke Kohana's XSS cleaning mechanism just incase an outlier wasn't caught
			// in the first 2 steps
			$keyword_raw = $this->input->xss_clean($keyword_raw);

			$filter = " (".$this->_get_searchstring($keyword_raw).")";

			array_push($this->params, $filter);
		}
		else
		{
			$keyword_raw = "";
		}

		// Check, has the form been submitted?
		$form_error = FALSE;
		$form_saved = FALSE;
		$form_action = "";

		if ($_POST)
		{
			$post = Validation::factory($_POST);

			 //	Add some filters
			$post->pre_filter('trim', TRUE);

			// Add some rules, the input field, followed by a list of checks, carried out in order
			$post->add_rules('action','required', 'alpha', 'length[1,1]');
			$post->add_rules('offerincident_id.*','required','numeric');

			if ($post->validate())
			{
				// Approve Action
				if ($post->action == 'a')
				{
					foreach($post->offerincident_id as $item)
					{
						// Database instance
						$db = new Database();

						// Query to check if this offerreport is orphaned i.e offercategoryless
						$query = "SELECT ic.* FROM ".$table_prefix."offerincident_offercategory ic
								INNER JOIN ".$table_prefix."offercategory c ON c.id = ic.offercategory_id INNER JOIN ".$table_prefix."offerincident i ON i.id=ic.offerincident_id
								WHERE c.offercategory_title =\"NONE\" AND c.offercategory_trusted = '1' AND ic.offerincident_id = $item";
						$result = $db->query($query);

						// Only approve the offerreport IF it's not orphaned i.e the query returns a null set
						if(count($result) == 0)
						{
							$update = new Offerincident_Model($item);
							if ($update->loaded == TRUE)
							{
								$update->offerincident_active =($update->offerincident_active == 0) ? '1' : '0';

								// Tag this as a offerreport that needs to be sent out as an alert
								if ($update->offerincident_alert_status != '2')
								{
									// 2 = offerreport that has had an alert sent
									$update->offerincident_alert_status = '1';
								}

								$update->save();

								$verify = new Verify_Model();
								$verify->offerincident_id = $item;
								$verify->verified_status = '1';

								// Record 'Verified By' Action
								$verify->user_id = $_SESSION['auth_user']->id;
								$verify->verified_date = date("Y-m-d H:i:s",time());
								$verify->save();

								// Action::offerreport_approve - Approve a Offerreport
								Event::run('ushahidi_action.offerreport_approve', $update);
							}
						}
						$form_action = strtoupper(Kohana::lang('ui_admin.approved'));
					}

				}
				// Unapprove Action
				elseif ($post->action == 'u')
				{
					foreach ($post->offerincident_id as $item)
					{
						$update = new Offerincident_Model($item);
						if ($update->loaded == TRUE)
						{
							$update->offerincident_active = '0';

							// If Alert hasn't been sent yet, disable it
							if ($update->offerincident_alert_status == '1')
							{
								$update->offerincident_alert_status = '0';
							}

							$update->save();

							$verify = new Verify_Model();
							$verify->offerincident_id = $item;
							$verify->verified_status = '0';

							// Record 'Verified By' Action
							$verify->user_id = $_SESSION['auth_user']->id;
							$verify->verified_date = date("Y-m-d H:i:s",time());
							$verify->save();

							// Action::offerreport_unapprove - Unapprove a Offerreport
							Event::run('ushahidi_action.offerreport_unapprove', $update);
						}
					}
					$form_action = strtoupper(Kohana::lang('ui_admin.unapproved'));
				}
				// Verify Action
				elseif ($post->action == 'v')
				{
					foreach ($post->offerincident_id as $item)
					{
						$update = new Offerincident_Model($item);
						$verify = new Verify_Model();
						if ($update->loaded == TRUE)
						{
							if ($update->offerincident_verified == '1')
							{
								$update->offerincident_verified = '0';
								$verify->verified_status = '0';
							}
							else
							{
								$update->offerincident_verified = '1';
								$verify->verified_status = '2';
							}
							$update->save();

							$verify->offerincident_id = $item;
							// Record 'Verified By' Action
							$verify->user_id = $_SESSION['auth_user']->id;
							$verify->verified_date = date("Y-m-d H:i:s",time());
							$verify->save();
						}
					}

					// Set the form action
					$form_action = strtoupper(Kohana::lang('ui_admin.verified_unverified'));
				}
				// Feature Action
				elseif ($post->action == 'f')
				{
					foreach ($post->offerincident_id as $item)
					{
						$update = new Offerincident_Model($item);
						
						if ($update->loaded == TRUE)
						{
							if ($update->offerincident_featured == '1')
							{
								$update->offerincident_featured = '0';
								
							}
							else
							{
								$update->offerincident_featured = '1';
								
							}
							$update->save();

							
						}

						// Set the form action
						$form_action = strtoupper(Kohana::lang('ui_admin.featured_unfeatured'));
					}
				}

				//Delete Action
				elseif ($post->action == 'd')
				{
					foreach($post->offerincident_id as $item)
					{
						$update = new Offerincident_Model($item);
						if ($update->loaded == TRUE)
						{
							$offerincident_id = $update->id;
							$location_id = $update->location_id;
							$update->delete();

							// Delete Location
							ORM::factory('location')->where('id',$location_id)->delete_all();

							// Delete Offercategories
							ORM::factory('offerincident_offercategory')->where('offerincident_id',$offerincident_id)->delete_all();

							// Delete Translations
							ORM::factory('offerincident_lang')->where('offerincident_id',$offerincident_id)->delete_all();

							// Delete Photos From Directory
							foreach (ORM::factory('media')->where('offerincident_id',$offerincident_id)->where('media_type', 1) as $photo)
							{
								deletePhoto($photo->id);
							}

							// Delete Media
							ORM::factory('media')->where('offerincident_id',$offerincident_id)->delete_all();

							// Delete Sender
							ORM::factory('offerincident_person')->where('offerincident_id',$offerincident_id)->delete_all();

							// Delete relationship to SMS message
							$updatemessage = ORM::factory('message')->where('offerincident_id',$offerincident_id)->find();
							if ($updatemessage->loaded == TRUE)
							{
								$updatemessage->offerincident_id = 0;
								$updatemessage->save();
							}

							// Delete Comments
							ORM::factory('comment')->where('offerincident_id',$offerincident_id)->delete_all();

							// Delete form responses
							ORM::factory('form_response')->where('offerincident_id', $offerincident_id)->delete_all();

							// Action::offerreport_delete - Deleted a Offerreport
							Event::run('ushahidi_action.offerreport_delete', $offerincident_id);
						}
					}
					$form_action = strtoupper(Kohana::lang('ui_admin.deleted'));
				}
				$form_saved = TRUE;
			}
			else
			{
				$form_error = TRUE;
			}

		}


		// Fetch all offerincidents
		$all_offerincidents = offerreports::fetch_offerincidents();

		// Pagination
		$pagination = new Pagination(array(
				'style' => 'front-end-reports',
				'query_string' => 'page',
				'items_per_page' => (int) Kohana::config('settings.items_per_page'),
				'total_items' => $all_offerincidents->count()
				));

		Event::run('ushahidi_filter.pagination',$pagination);

		// Offerreports
		$offerincidents = Offerincident_Model::get_offerincidents(offerreports::$params, $pagination);


		Event::run('ushahidi_filter.filter_offerincidents',$offerincidents);
		$this->template->content->countries = Country_Model::get_countries_list();
		$this->template->content->offerincidents = $offerincidents;
		$this->template->content->pagination = $pagination;
		$this->template->content->form_error = $form_error;
		$this->template->content->form_saved = $form_saved;
		$this->template->content->form_action = $form_action;

		// Total Offerreports
		$this->template->content->total_items = $pagination->total_items;

		// Status Tab
		$this->template->content->status = $status;

		// Javascript Header
		$this->template->js = new View('admin/offerreports_js');
	}

	/**
	 * Edit a offerreport
	 * @param bool|int $id The id no. of the offerreport
	 * @param bool|string $saved
	 */
	public function edit($id = FALSE, $saved = FALSE)
	{
		$db = new Database();

		// If user doesn't have access, redirect to dashboard
		if ( ! admin::permissions($this->user, "reports_edit"))
		{
			url::redirect(url::site().'admin/dashboard');
		}

		$this->template->content = new View('admin/offerreports_edit');
		$this->template->content->title = Kohana::lang('ui_admin.create_offerreport');

		// setup and initialize form field names
		$form = array
		(
			'location_id' => '',
			'form_id' => '',
			'locale' => '',
			'offerincident_title' => '',
			'offerincident_description' => '',
			'offerincident_date' => '',
			'offerincident_hour' => '',
			'offerincident_minute' => '',
			'offerincident_ampm' => '',
			'offerincident_start_date' => '',
			'offerincident_start_hour' => '',
			'offerincident_start_minute' => '',
			'offerincident_start_ampm' => '',
			'offerincident_end_date' => '',
			'offerincident_end_hour' => '',
			'offerincident_end_minute' => '',
			'offerincident_end_ampm' => '',
			'latitude' => '',
			'longitude' => '',
			'geometry' => array(),
			'location_name' => '',
			'country_id' => '',
			'country_name' =>'',
			'offerincident_offercategory' => array(),
			'offerincident_news' => array(),
			'offerincident_video' => array(),
			'offerincident_photo' => array(),
			'person_first' => '',
			'person_last' => '',
			'person_email' => '',
			'custom_field' => array(),
			'offerincident_active' => '',
			'offerincident_featured' => '',
			'offerincident_price' => '',
			'offerincident_verified' => '',
			'offerincident_zoom' => ''
		);

		// Copy the form as errors, so the errors will be stored with keys corresponding to the form field names
		$errors = $form;
		$form_error = FALSE;
		$form_saved = ($saved == 'saved');

		// Initialize Default Values
		$form['locale'] = Kohana::config('locale.language');
		//$form['latitude'] = Kohana::config('settings.default_lat');
		//$form['longitude'] = Kohana::config('settings.default_lon');
		$form['offerincident_date'] = date("m/d/Y",time());
		$form['offerincident_hour'] = date('h');
		$form['offerincident_minute'] = date('i');
		$form['offerincident_ampm'] = date('a');
		$form['country_id'] = Kohana::config('settings.default_country');


		//get the form ID if relevant, kind of a hack
		//to just hit the database like this for one
		//tiny bit of info then throw away the DB model object,
		//but seems to be what everyone else does, so
		//why should I care. Just know that when your Ush system crashes
		//because you have 1000 concurrent users you'll need to do this
		//correctly. Etherton.
		$form_id = '';
		if($id && Offerincident_Model::is_valid_offerincident($id))
		{
			$form_id = ORM::factory('offerincident', $id)->form_id;
		}
		// initialize custom field array
        $form['custom_field'] = customforms::get_custom_form_fields($id,$form_id,true);

		// Locale (Language) Array
		$this->template->content->locale_array = Kohana::config('locale.all_languages');

		// Create Offercategories
		$this->template->content->offercategories = Offercategory_Model::get_offercategories(0, TRUE, FALSE);
		$this->template->content->new_offercategories_form = $this->_new_offercategories_form_arr();

		// Time formatting
		$this->template->content->hour_array = $this->_hour_array();
		$this->template->content->minute_array = $this->_minute_array();
		$this->template->content->ampm_array = $this->_ampm_array();

		$this->template->content->stroke_width_array = $this->_stroke_width_array();

		// Get Countries
		$countries = array();
		foreach (ORM::factory('country')->orderby('country')->find_all() as $country)
		{
			// Create a list of all countries
			$this_country = $country->country;
			if (strlen($this_country) > 35)
			{
				$this_country = substr($this_country, 0, 35) . "...";
			}
			$countries[$country->id] = $this_country;
		}

		// Initialize Default Value for Hidden Field Country Name, just incase Reverse Geo coding yields no result
		$form['country_name'] = $countries[$form['country_id']];

		$this->template->content->countries = $countries;


		//GET custom forms
		$forms = array();
		foreach (ORM::factory('form')->where('form_active',1)->find_all() as $custom_forms)
		{
			$forms[$custom_forms->id] = $custom_forms->form_title;
		}

		$this->template->content->forms = $forms;

		// Get the offerincident media
		$offerincident_media =  Offerincident_Model::is_valid_offerincident($id)
			? ORM::factory('offerincident', $id)->media
			: FALSE;

		$this->template->content->offerincident_media = $offerincident_media;

		// Are we creating this offerreport from SMS/Email/Twitter?
		// If so retrieve message
		if ( isset($_GET['mid']) AND intval($_GET['mid']) > 0 ) {

			$message_id = intval($_GET['mid']);
			$service_id = "";
			$message = ORM::factory('message', $message_id);

			if ($message->loaded AND $message->message_type == 1)
			{
				$service_id = $message->offerreporter->service_id;

				// Has a offerreport already been created for this Message?
				if ($message->offerincident_id != 0) {

					// Redirect to offerreport
					url::redirect('admin/offerreports/edit/'. $message->offerincident_id);
				}

				$this->template->content->show_messages = true;
				$offerincident_description = $message->message;
				if ( ! empty($message->message_detail))
				{
					$form['offerincident_title'] = $message->message;
					$offerincident_description = $message->message_detail;
				}

				$form['offerincident_description'] = $offerincident_description;
				$form['offerincident_date'] = date('m/d/Y', strtotime($message->message_date));
				$form['offerincident_hour'] = date('h', strtotime($message->message_date));
				$form['offerincident_minute'] = date('i', strtotime($message->message_date));
				$form['offerincident_ampm'] = date('a', strtotime($message->message_date));
				$form['person_first'] = $message->offerreporter->offerreporter_first;
				$form['person_last'] = $message->offerreporter->offerreporter_last;

				// Does the sender of this message have a location?
				if ($message->offerreporter->location->loaded)
				{
					$form['location_id'] = $message->offerreporter->location->id;
					$form['latitude'] = $message->offerreporter->location->latitude;
					$form['longitude'] = $message->offerreporter->location->longitude;
					$form['location_name'] = $message->offerreporter->location->location_name;
				}

				//Events to manipulate an already known location

				Event::run('ushahidi_action.location_from',$message_from = $message->message_from);
				//filter location name
				Event::run('ushahidi_filter.location_name',$form['location_name']);
				//filter //location find
				Event::run('ushahidi_filter.location_find',$form['location_find']);


				// Retrieve Last 5 Messages From this account
				$this->template->content->all_messages = ORM::factory('message')
					->where('offerreporter_id', $message->offerreporter_id)
					->orderby('message_date', 'desc')
					->limit(5)
					->find_all();
			}
			else
			{
				$message_id = "";
				$this->template->content->show_messages = false;
			}
		}
		else
		{
			$this->template->content->show_messages = false;
		}

		// Are we creating this offerreport from a Newsfeed?
		if ( isset($_GET['fid']) AND intval($_GET['fid']) > 0 )
		{
			$feed_item_id = intval($_GET['fid']);
			$feed_item = ORM::factory('feed_item', $feed_item_id);

			if ($feed_item->loaded)
			{
				// Has a offerreport already been created for this Feed item?
				if ($feed_item->offerincident_id != 0)
				{
					// Redirect to offerreport
					url::redirect('admin/offerreports/edit/'. $feed_item->offerincident_id);
				}

				$form['offerincident_title'] = $feed_item->item_title;
				$form['offerincident_description'] = $feed_item->item_description;
				$form['offerincident_date'] = date('m/d/Y', strtotime($feed_item->item_date));
				$form['offerincident_hour'] = date('h', strtotime($feed_item->item_date));
				$form['offerincident_minute'] = date('i', strtotime($feed_item->item_date));
				$form['offerincident_ampm'] = date('a', strtotime($feed_item->item_date));

				// News Link
				$form['offerincident_news'][0] = $feed_item->item_link;

				// Does this newsfeed have a geolocation?
				if ($feed_item->location_id)
				{
					$form['location_id'] = $feed_item->location_id;
					$form['latitude'] = $feed_item->location->latitude;
					$form['longitude'] = $feed_item->location->longitude;
					$form['location_name'] = $feed_item->location->location_name;
				}
			}
			else
			{
				$feed_item_id = "";
			}
		}

		// Check, has the form been submitted, if so, setup validation
		if ($_POST)
		{
			// Instantiate Validation, use $post, so we don't overwrite $_POST fields with our own things
			$post = array_merge($_POST, $_FILES);

			// Check if the service id exists
			if (isset($service_id) AND intval($service_id) > 0)
			{
				$post = array_merge($post, array('service_id' => $service_id));
			}

			// Check if the offerincident id is valid an add it to the post data
			if (Offerincident_Model::is_valid_offerincident($id))
			{
				$post = array_merge($post, array('offerincident_id' => $id));
			}

			/**
			 * NOTES - E.Kala July 27, 2011
			 *
			 * Previously, the $post parameter for this event was a Validation
			 * object. Now it's an array (i.e. the raw data without any validation rules applied to them).
			 * As such, all plugins making use of this event shall have to be updated
			 */

			// Action::offerreport_submit_admin - Offerreport Posted
			Event::run('ushahidi_action.offerreport_submit_admin', $post);

			// Validate
			if (offerreports::validate($post, TRUE))
			{
				// Yes! everything is valid
				$location_id = $post->location_id;

				// STEP 1: SAVE LOCATION
				$location = new Location_Model($location_id);
				offerreports::save_location($post, $location);

				// STEP 2: SAVE INCIDENT
				$offerincident = new Offerincident_Model($id);
				offerreports::save_offerreport($post, $offerincident, $location->id);

				// STEP 2b: Record Approval/Verification Action
				$verify = new Verify_Model();
				offerreports::verify_approve($post, $verify, $offerincident);

				// STEP 2c: SAVE INCIDENT GEOMETRIES
				offerreports::save_offerreport_geometry($post, $offerincident);

				// STEP 3: SAVE CATEGORIES
				offerreports::save_offercategory($post, $offerincident);

				// STEP 4: SAVE MEDIA
				offerreports::save_media($post, $offerincident);

				// STEP 5: SAVE PERSONAL INFORMATION
				offerreports::save_personal_info($post, $offerincident);

				// STEP 6a: SAVE LINK TO REPORTER MESSAGE
				// We're creating a offerreport from a message with this option
				if (isset($message_id) AND intval($message_id) > 0)
				{
					$savemessage = ORM::factory('message', $message_id);
					if ($savemessage->loaded)
					{
						$savemessage->offerincident_id = $offerincident->id;
						$savemessage->save();

						// Does Message Have Attachments?
						// Add Attachments
						$attachments = ORM::factory("media")
							->where("message_id", $savemessage->id)
							->find_all();
						foreach ($attachments AS $attachment)
						{
							$attachment->offerincident_id = $offerincident->id;
							$attachment->save();
						}
					}
				}

				// STEP 6b: SAVE LINK TO NEWS FEED
				// We're creating a offerreport from a newsfeed with this option
				if (isset($feed_item_id) AND intval($feed_item_id) > 0)
				{
					$savefeed = ORM::factory('feed_item', $feed_item_id);
					if ($savefeed->loaded)
					{
						$savefeed->offerincident_id = $offerincident->id;
						$savefeed->location_id = $location->id;
						$savefeed->save();
					}
				}

				// STEP 7: SAVE CUSTOM FORM FIELDS
				offerreports::save_custom_fields($post, $offerincident);

				// Action::offerreport_edit - Edited a Offerreport
				Event::run('ushahidi_action.offerreport_edit', $offerincident);

				// SAVE AND CLOSE?
				switch($post->save) {
					case 1:
					case 'dontclose':
						// Save but don't close
						url::redirect('admin/offerreports/edit/'. $offerincident->id .'/saved');
						break;
					case 'addnew':
						// Save and add new
						url::redirect('admin/offerreports/edit/0/saved');
						break;
					default:
						// Save and close
						url::redirect('admin/offerreports/');
				}

			}
			// No! We have validation errors, we need to show the form again, with the errors
			else
			{
				// Repopulate the form fields
				$form = arr::overwrite($form, $post->as_array());

				// Populate the error fields, if any
				$errors = arr::overwrite($errors, $post->errors('offerreport'));
				$form_error = TRUE;
			}
		}
		else
		{
			if (Offerincident_Model::is_valid_offerincident($id))
			{
				// Retrieve Current Offerincident
				$offerincident = ORM::factory('offerincident', $id);
				if ($offerincident->loaded == true)
				{
					// Retrieve Offercategories
					$offerincident_offercategory = array();
					foreach($offerincident->offerincident_offercategory as $offercategory)
					{
						$offerincident_offercategory[] = $offercategory->offercategory_id;
					}

					// Retrieve Media
					$offerincident_news = array();
					$offerincident_video = array();
					$offerincident_photo = array();
					foreach($offerincident->media as $media)
					{
						if ($media->media_type == 4)
						{
							$offerincident_news[] = $media->media_link;
						}
						elseif ($media->media_type == 2)
						{
							$offerincident_video[] = $media->media_link;
						}
						elseif ($media->media_type == 1)
						{
							$offerincident_photo[] = $media->media_link;
						}
					}

					// Get Geometries via SQL query as ORM can't handle Spatial Data
					$sql = "SELECT AsText(geometry) as geometry, geometry_label,
						geometry_comment, geometry_color, geometry_strokewidth
						FROM ".Kohana::config('database.default.table_prefix')."geometry
						WHERE offerincident_id=".$id;
					$query = $db->query($sql);
					foreach ( $query as $item )
					{
						$geometry = array(
								"geometry" => $item->geometry,
								"label" => $item->geometry_label,
								"comment" => $item->geometry_comment,
								"color" => $item->geometry_color,
								"strokewidth" => $item->geometry_strokewidth
							);
						$form['geometry'][] = json_encode($geometry);
					}

					// Combine Everything
					$offerincident_arr = array(
						'location_id' => $offerincident->location->id,
						'form_id' => $offerincident->form_id,
						'locale' => $offerincident->locale,
						'offerincident_title' => $offerincident->offerincident_title,
						'offerincident_description' => $offerincident->offerincident_description,
						'offerincident_date' => date('m/d/Y', strtotime($offerincident->offerincident_date)),
						'offerincident_hour' => date('h', strtotime($offerincident->offerincident_date)),
						'offerincident_minute' => date('i', strtotime($offerincident->offerincident_date)),
						'offerincident_ampm' => date('a', strtotime($offerincident->offerincident_date)),
						'offerincident_end_date' => date('m/d/Y', strtotime($offerincident->offerincident_end_date)),
						'offerincident_end_hour' => date('h', strtotime($offerincident->offerincident_end_date)),
						'offerincident_end_minute' => date('i', strtotime($offerincident->offerincident_end_date)),
						'offerincident_end_ampm' => date('a', strtotime($offerincident->offerincident_end_date)),
						'offerincident_start_date' => date('m/d/Y', strtotime($offerincident->offerincident_start_date)),
						'offerincident_start_hour' => date('h', strtotime($offerincident->offerincident_start_date)),
						'offerincident_start_minute' => date('i', strtotime($offerincident->offerincident_start_date)),
						'offerincident_start_ampm' => date('a', strtotime($offerincident->offerincident_start_date)),						
						'latitude' => $offerincident->location->latitude,
						'longitude' => $offerincident->location->longitude,
						'location_name' => $offerincident->location->location_name,
						'country_id' => $offerincident->location->country_id,
						'offerincident_offercategory' => $offerincident_offercategory,
						'offerincident_news' => $offerincident_news,
						'offerincident_video' => $offerincident_video,
						'offerincident_photo' => $offerincident_photo,
						'person_first' => $offerincident->offerincident_person->person_first,
						'person_last' => $offerincident->offerincident_person->person_last,
						'person_email' => $offerincident->offerincident_person->person_email,
						'custom_field' => customforms::get_custom_form_fields($id,$offerincident->form_id,true),
						'offerincident_active' => $offerincident->offerincident_active,
						'offerincident_verified' => $offerincident->offerincident_verified,
						'offerincident_featured' => $offerincident->offerincident_featured,
						'offerincident_price' => $offerincident->offerincident_price,
						'offerincident_zoom' => $offerincident->offerincident_zoom
					);

					// Merge To Form Array For Display
					$form = arr::overwrite($form, $offerincident_arr);
				}
				else
				{
					// Redirect
					url::redirect('admin/offerreports/');
				}

			}
		}

		$this->template->content->id = $id;
		$this->template->content->form = $form;
		$this->template->content->errors = $errors;
		$this->template->content->form_error = $form_error;
		$this->template->content->form_saved = $form_saved;

		// Retrieve Custom Form Fields Structure
		$this->template->content->custom_forms = new View('offerreports_submit_custom_forms');
		$disp_custom_fields = customforms::get_custom_form_fields($id, $form['form_id'], FALSE, "view");
		$custom_field_mismatch = customforms::get_edit_mismatch($form['form_id']);
        $this->template->content->custom_forms->disp_custom_fields = $disp_custom_fields;
		$this->template->content->custom_forms->custom_field_mismatch = $custom_field_mismatch;
		$this->template->content->custom_forms->form = $form;

		// Retrieve Previous & Next Records
		$previous = ORM::factory('offerincident')->where('id < ', $id)->orderby('id','desc')->find();
		$previous_url = ($previous->loaded ?
				url::base().'admin/offerreports/edit/'.$previous->id :
				url::base().'admin/offerreports/');
		$next = ORM::factory('offerincident')->where('id > ', $id)->orderby('id','desc')->find();
		$next_url = ($next->loaded ?
				url::base().'admin/offerreports/edit/'.$next->id :
				url::base().'admin/offerreports/');
		$this->template->content->previous_url = $previous_url;
		$this->template->content->next_url = $next_url;

		// Javascript Header
		$this->template->map_enabled = TRUE;
		$this->template->colorpicker_enabled = TRUE;
		$this->template->treeview_enabled = TRUE;
		$this->template->json2_enabled = TRUE;

		$this->template->js = new View('offerreports_submit_edit_js');
		$this->template->js->edit_mode = TRUE;
		$this->template->js->default_map = Kohana::config('settings.default_map');
		$this->template->js->default_zoom = Kohana::config('settings.default_zoom');

		if ( ! $form['latitude'] OR !$form['latitude'])
		{
			$this->template->js->latitude = Kohana::config('settings.default_lat');
			$this->template->js->longitude = Kohana::config('settings.default_lon');
		}
		else
		{
			$this->template->js->latitude = $form['latitude'];
			$this->template->js->longitude = $form['longitude'];
		}

		$this->template->js->offerincident_zoom = $form['offerincident_zoom'];
		$this->template->js->geometries = $form['geometry'];

		// Inline Javascript
		$this->template->content->date_picker_js = $this->_date_picker_js();
		$this->template->content->color_picker_js = $this->_color_picker_js();
		$this->template->content->new_offercategory_toggle_js = $this->_new_offercategory_toggle_js();

		// Pack Javascript
		$myPacker = new javascriptpacker($this->template->js , 'Normal', false, false);
		$this->template->js = $myPacker->pack();
	}


	/**
	 * Download Offerreports in CSV format
	 */
	public function download()
	{
		// If user doesn't have access, redirect to dashboard
		if ( ! admin::permissions($this->user, "reports_download"))
		{
			url::redirect(url::site().'admin/dashboard');
		}

		$this->template->content = new View('admin/offerreports_download');
		$this->template->content->title = Kohana::lang('ui_admin.download_offerreports');

		$form = array(
			'data_point'   => '',
			'data_include' => '',
			'from_date'	   => '',
			'to_date'	   => ''
		);

		$errors = $form;
		$form_error = FALSE;

		// Check, has the form been submitted, if so, setup validation
		if ($_POST)
		{
			// Instantiate Validation, use $post, so we don't overwrite $_POST fields with our own things
			$post = Validation::factory($_POST);

			 //	Add some filters
			$post->pre_filter('trim', TRUE);

			// Add some rules, the input field, followed by a list of checks, carried out in order
			$post->add_rules('data_point.*','required','numeric','between[1,4]');
			//$post->add_rules('data_include.*','numeric','between[1,5]');
			$post->add_rules('data_include.*','numeric','between[1,6]');
			$post->add_rules('from_date','date_mmddyyyy');
			$post->add_rules('to_date','date_mmddyyyy');

			// Validate the offerreport dates, if included in offerreport filter
			if (!empty($_POST['from_date']) OR !empty($_POST['to_date']))
			{
				// Valid FROM Date?
				if (empty($_POST['from_date']) OR (strtotime($_POST['from_date']) > strtotime("today")))
				{
					$post->add_error('from_date','range');
				}

				// Valid TO date?
				if (empty($_POST['to_date']) OR (strtotime($_POST['to_date']) > strtotime("today")))
				{
					$post->add_error('to_date','range');
				}

				// TO Date not greater than FROM Date?
				if (strtotime($_POST['from_date']) > strtotime($_POST['to_date']))
				{
					$post->add_error('to_date','range_greater');
				}
			}

			// Test to see if things passed the rule checks
			if ($post->validate())
			{
				// Add Filters
				$filter = " ( 1 !=1";

				// Offerreport Type Filter
				foreach($post->data_point as $item)
				{
					if ($item == 1)
					{
						$filter .= " OR offerincident_active = 1 ";
					}

					if ($item == 2)
					{
						$filter .= " OR offerincident_verified = 1 ";
					}

					if ($item == 3)
					{
						$filter .= " OR offerincident_active = 0 ";
					}

					if ($item == 4)
					{
						$filter .= " OR offerincident_verified = 0 ";
					}
				}
				$filter .= ") ";

				// Offerreport Date Filter
				if ( ! empty($post->from_date) AND !empty($post->to_date))
				{
					$filter .= " AND ( offerincident_date >= '" . date("Y-m-d H:i:s",strtotime($post->from_date))
							. "' AND offerincident_date <= '" . date("Y-m-d H:i:s",strtotime($post->to_date)) . "' ) ";
				}

				// Retrieve offerreports
				$offerincidents = ORM::factory('offerincident')->where($filter)->orderby('offerincident_dateadd', 'desc')->find_all();

				// Column Titles
				ob_start();
				echo "#,OFFER TITLE,OFFER DATE";
				foreach($post->data_include as $item)
				{
					if ($item == 1) {
						echo ",LOCATION";
					}

					if ($item == 2) {
						echo ",DESCRIPTION";
					}

					if ($item == 3) {
						echo ",CATEGORY";
					}

					if ($item == 4) {
						echo ",LATITUDE";
					}

					if($item == 5) {
						echo ",LONGITUDE";
					}
					if($item == 6)
					{
						$custom_titles = customforms::get_custom_form_fields('','',false);
						foreach($custom_titles as $field_name)
						{

							echo ",".$field_name['field_name'];
						}

					}

				}

				echo ",APPROVED,VERIFIED";

				//Incase a plugin would like to add some custom fields
				$custom_headers = "";
				Event::run('ushahidi_filter.offerreport_download_csv_header', $custom_headers);
				echo $custom_headers;

				echo "\n";

				foreach ($offerincidents as $offerincident)
				{
					echo '"'.$offerincident->id.'",';
					echo '"'.$this->_csv_text($offerincident->offerincident_title).'",';
					echo '"'.$offerincident->offerincident_date.'"';

					foreach($post->data_include as $item)
					{
						switch ($item)
						{
							case 1:
								echo ',"'.$this->_csv_text($offerincident->location->location_name).'"';
							break;

							case 2:
								echo ',"'.$this->_csv_text($offerincident->offerincident_description).'"';
							break;

							case 3:
								echo ',"';

								foreach($offerincident->offerincident_offercategory as $offercategory)
								{
									if ($offercategory->offercategory->offercategory_title)
									{
										echo $this->_csv_text($offercategory->offercategory->offercategory_title) . ", ";
									}
								}
								echo '"';
							break;

							case 4:
								echo ',"'.$this->_csv_text($offerincident->location->latitude).'"';
							break;

							case 5:
								echo ',"'.$this->_csv_text($offerincident->location->longitude).'"';
							break;

							case 6:
								$offerincident_id = $offerincident->id;
								$custom_fields = customforms::get_custom_form_fields($offerincident_id,'',false);
								if ( ! empty($custom_fields))
								{
									foreach($custom_fields as $custom_field)
									{
										echo',"'.$this->_csv_text($custom_field['field_response']).'"';
									}
								}
								else
								{
									$custom_field = customforms::get_custom_form_fields('','',false);
									foreach ($custom_field as $custom)
									{
										echo',"'.$this->_csv_text("").'"';
									}
								}
								break;

						}
					}

					if ($offerincident->offerincident_active)
					{
						echo ",YES";
					}
					else
					{
						echo ",NO";
					}

					if ($offerincident->offerincident_verified)
					{
						echo ",YES";
					}
					else
					{
						echo ",NO";
					}
					if ($offerincident->offerincident_featured)
					{
						echo ",YES";
					}
					else
					{
						echo ",NO";
					}

					//Incase a plugin would like to add some custom data for an offerincident
					$event_data = array("offerreport_csv" => "", "offerincident" => $offerincident);
					Event::run('ushahidi_filter.offerreport_download_csv_offerincident', $event_data);
					echo $event_data['offerreport_csv'];

					echo "\n";
				}
				$offerreport_csv = ob_get_clean();

				// Output to browser
				header("Content-type: text/x-csv");
				header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
				header("Content-Disposition: attachment; filename=" . time() . ".csv");
				header("Content-Length: " . strlen($offerreport_csv));
				echo $offerreport_csv;
				exit;

			}

			// No! We have validation errors, we need to show the form again, with the errors
			else
			{
				// Repopulate the form fields
				$form = arr::overwrite($form, $post->as_array());

				// Populate the error fields, if any
				$errors = arr::overwrite($errors, $post->errors('offerreport'));
				$form_error = TRUE;
			}
		}

		$this->template->content->form = $form;
		$this->template->content->errors = $errors;
		$this->template->content->form_error = $form_error;

		// Javascript Header
		$this->template->js = new View('admin/offerreports_download_js');
		$this->template->js->calendar_img = url::base() . "media/img/icon-calendar.gif";
	}

	public function upload()
	{
		// If user doesn't have access, redirect to dashboard
		if ( ! admin::permissions($this->user, "reports_upload"))
		{
			url::redirect(url::site().'admin/dashboard');
		}

		if ($_SERVER['REQUEST_METHOD'] == 'GET') {
			$this->template->content = new View('admin/offerreports_upload');
			$this->template->content->title = 'Upload Offerreports';
			$this->template->content->form_error = false;
		}

		if ($_SERVER['REQUEST_METHOD']=='POST')
		{
			$errors = array();
			$notices = array();

			if (!$_FILES['csvfile']['error'])
			{
				if (file_exists($_FILES['csvfile']['tmp_name']))
				{
					if($filehandle = fopen($_FILES['csvfile']['tmp_name'], 'r'))
					{
						$importer = new OfferreportsImporter;

						if ($importer->import($filehandle))
						{
							$this->template->content = new View('admin/offerreports_upload_success');
							$this->template->content->title = 'Upload Offers';
							$this->template->content->rowcount = $importer->totalrows;
							$this->template->content->imported = $importer->importedrows;
							$this->template->content->notices = $importer->notices;
						}
						else
						{
							$errors = $importer->errors;
						}
					}
					else
					{
						$errors[] = Kohana::lang('ui_admin.file_open_error');
					}
				}

				// File exists?
				else
				{
					$errors[] = Kohana::lang('ui_admin.file_not_found_upload');
				}
			}

			// Upload errors?
			else
			{
				$errors[] = $_FILES['csvfile']['error'];
			}

			if(count($errors))
			{
				$this->template->content = new View('admin/offerreports_upload');
				$this->template->content->title = Kohana::lang('ui_admin.upload_offerreports');
				$this->template->content->errors = $errors;
				$this->template->content->form_error = 1;
			}
		}
	}

	/**
	* Translate a offerreport
	* @param bool|int $id The id no. of the offerreport
	* @param bool|string $saved
	*/

	public function translate( $id = false, $saved = FALSE)
	{
		$this->template->content = new View('admin/offerreports_translate');
		$this->template->content->title = Kohana::lang('ui_admin.translate_offerreports');

		// Which offerincident are we adding this translation for?
		if (isset($_GET['iid']) && !empty($_GET['iid']))
		{
			$offerincident_id = (int) $_GET['iid'];
			$offerincident = ORM::factory('offerincident', $offerincident_id);

			if ($offerincident->loaded == true)
			{
				$orig_locale = $offerincident->locale;
				$this->template->content->orig_title = $offerincident->offerincident_title;
				$this->template->content->orig_description = $offerincident->offerincident_description;
			}
			else
			{
				// Redirect
				url::redirect('admin/offerreports/');
			}
		}
		else
		{
			// Redirect
			url::redirect('admin/offerreports/');
		}


		// Setup and initialize form field names
		$form = array(
			'locale'	  => '',
			'offerincident_title'	  => '',
			'offerincident_description'	  => ''
		);

		// Copy the form as errors, so the errors will be stored with keys corresponding to the form field names
		$errors = $form;
		$form_error = FALSE;

		$form_saved = ($saved == 'saved')? TRUE : FALSE;

		// Locale (Language) Array
		$this->template->content->locale_array = Kohana::config('locale.all_languages');

		// Check, has the form been submitted, if so, setup validation
		if ($_POST)
		{
			// Instantiate Validation, use $post, so we don't overwrite $_POST fields with our own things
			$post = Validation::factory($_POST);

			 //	Add some filters
			$post->pre_filter('trim', TRUE);

			// Add some rules, the input field, followed by a list of checks, carried out in order
			$post->add_rules('locale','required','alpha_dash','length[5]');
			$post->add_rules('offerincident_title','required', 'length[3,200]');
			$post->add_rules('offerincident_description','required');
			$post->add_callbacks('locale', array($this,'translate_exists_chk'));

			if ($orig_locale == $_POST['locale'])
			{
				// The original offerreport and the translation are the same language!
				$post->add_error('locale','locale');
			}

			// Test to see if things passed the rule checks
			if ($post->validate())
			{
				// SAVE INCIDENT TRANSLATION
				$offerincident_l = new Offerincident_Lang_Model($id);
				$offerincident_l->offerincident_id = $offerincident_id;
				$offerincident_l->locale = $post->locale;
				$offerincident_l->offerincident_title = $post->offerincident_title;
				$offerincident_l->offerincident_description = $post->offerincident_description;
				$offerincident_l->save();


				// SAVE AND CLOSE?
				// Save but don't close
				if ($post->save == 1)
				{
					url::redirect('admin/offerreports/translate/'. $offerincident_l->id .'/saved/?iid=' . $offerincident_id);
				}

				// Save and close
				else
				{
					url::redirect('admin/offerreports/');
				}
			}

			// No! We have validation errors, we need to show the form again, with the errors
			else
			{
				// Repopulate the form fields
				$form = arr::overwrite($form, $post->as_array());

				// Populate the error fields, if any
				$errors = arr::overwrite($errors, $post->errors('offerreport'));
				$form_error = TRUE;
			}
		}
		else
		{
			if ($id)
			{
				// Retrieve Current Offerincident
				$offerincident_l = ORM::factory('offerincident_lang', $id)->where('offerincident_id', $offerincident_id)->find();
				if ($offerincident_l->loaded == true)
				{
					$form['locale'] = $offerincident_l->locale;
					$form['offerincident_title'] = $offerincident_l->offerincident_title;
					$form['offerincident_description'] = $offerincident_l->offerincident_description;
				}
				else
				{
					// Redirect
					url::redirect('admin/offerreports/');
				}

			}
		}

		$this->template->content->form = $form;
		$this->template->content->errors = $errors;
		$this->template->content->form_error = $form_error;
		$this->template->content->form_saved = $form_saved;

		// Javascript Header
		$this->template->js = new View('admin/offerreports_translate_js');
	}




	/**
	* Save newly added dynamic offercategories
	*/
	public function save_offercategory()
	{
		$this->auto_render = FALSE;
		$this->template = "";

		// Check, has the form been submitted, if so, setup validation
		if ($_POST)
		{
			// Instantiate Validation, use $post, so we don't overwrite $_POST fields with our own things
			$post = Validation::factory($_POST);

			 //	Add some filters
			$post->pre_filter('trim', TRUE);

			// Add some rules, the input field, followed by a list of checks, carried out in order
			$post->add_rules('offercategory_title','required', 'length[3,200]');
			$post->add_rules('offercategory_description','required');
			$post->add_rules('offercategory_color','required', 'length[6,6]');


			// Test to see if things passed the rule checks
			if ($post->validate())
			{
				// SAVE Offercategory
				$offercategory = new Offercategory_Model();
				$offercategory->offercategory_title = $post->offercategory_title;
				$offercategory->offercategory_description = $post->offercategory_description;
				$offercategory->offercategory_color = $post->offercategory_color;
				$offercategory->save();
				$form_saved = TRUE;

				echo json_encode(array("status"=>"saved", "id"=>$offercategory->id));
			}
			else

			{
				echo json_encode(array("status"=>"error"));
			}
		}
		else
		{
			echo json_encode(array("status"=>"error"));
		}
	}

	/**
	* Delete Photo
	* @param int $id The unique id of the photo to be deleted
	*/
	public function deletePhoto ($id)
	{
		$this->auto_render = FALSE;
		$this->template = "";

		if ($id)
		{
			$photo = ORM::factory('media', $id);
			$photo_large = $photo->media_link;
			$photo_medium = $photo->media_medium;
			$photo_thumb = $photo->media_thumb;

			if (file_exists(Kohana::config('upload.directory', TRUE).$photo_large))
			{
				unlink(Kohana::config('upload.directory', TRUE).$photo_large);
			}
			elseif (Kohana::config("cdn.cdn_store_dynamic_content") AND valid::url($photo_large))
			{
				cdn::delete($photo_large);
			}

			if (file_exists(Kohana::config('upload.directory', TRUE).$photo_medium))
			{
				unlink(Kohana::config('upload.directory', TRUE).$photo_medium);
			}
			elseif (Kohana::config("cdn.cdn_store_dynamic_content") AND valid::url($photo_medium))
			{
				cdn::delete($photo_medium);
			}

			if (file_exists(Kohana::config('upload.directory', TRUE).$photo_thumb))
			{
				unlink(Kohana::config('upload.directory', TRUE).$photo_thumb);
			}
			elseif (Kohana::config("cdn.cdn_store_dynamic_content") AND valid::url($photo_thumb))
			{
				cdn::delete($photo_thumb);
			}

			// Finally Remove from DB
			$photo->delete();
		}
	}

	/* private functions */

	// Dynamic offercategories form fields
	private function _new_offercategories_form_arr()
	{
		return array(
			'offercategory_name' => '',
			'offercategory_description' => '',
			'offercategory_color' => '',
		);
	}

	// Time functions
	private function _hour_array()
	{
		for ($i=1; $i <= 12 ; $i++)
		{
			// Add Leading Zero
			$hour_array[sprintf("%02d", $i)] = sprintf("%02d", $i);
		}
		return $hour_array;
	}

	private function _minute_array()
	{
		for ($j=0; $j <= 59 ; $j++)
		{
			// Add Leading Zero
			$minute_array[sprintf("%02d", $j)] = sprintf("%02d", $j);
		}

		return $minute_array;
	}

	private function _ampm_array()
	{
		return $ampm_array = array('pm'=>Kohana::lang('ui_admin.pm'),'am'=>Kohana::lang('ui_admin.am'));
	}

	private function _stroke_width_array()
	{
		for ($i = 0.5; $i <= 8 ; $i += 0.5)
		{
			$stroke_width_array["$i"] = $i;
		}

		return $stroke_width_array;
	}

	// Javascript functions
	private function _color_picker_js()
	{
		 return "<script type=\"text/javascript\">
					$(document).ready(function() {
					$('#offercategory_color').ColorPicker({
							onSubmit: function(hsb, hex, rgb) {
								$('#offercategory_color').val(hex);
							},
							onChange: function(hsb, hex, rgb) {
								$('#offercategory_color').val(hex);
							},
							onBeforeShow: function () {
								$(this).ColorPickerSetColor(this.value);
							}
						})
					.bind('keyup', function(){
						$(this).ColorPickerSetColor(this.value);
					});
					});
				</script>";
	}

	private function _date_picker_js()
	{
		return "<script type=\"text/javascript\">
				$(document).ready(function() {
				$(\"#offerincident_start_date\").datepicker({
				showOn: \"both\",
				buttonImage: \"" . url::base() . "media/img/icon-calendar.gif\",
				buttonImageOnly: true
				});
				});
				$(document).ready(function() {
				$(\"#offerincident_end_date\").datepicker({
				showOn: \"both\",
				buttonImage: \"" . url::base() . "media/img/icon-calendar.gif\",
				buttonImageOnly: true
				});
				});
			</script>";
	}


	private function _new_offercategory_toggle_js()
	{
		return "<script type=\"text/javascript\">
				$(document).ready(function() {
				$('a#offercategory_toggle').click(function() {
				$('#offercategory_add').toggle(400);
				return false;
				});
				});
			</script>";
	}


	/**
	 * Checks if translation for this offerreport & locale exists
	 * @param Validation $post $_POST variable with validation rules
	 * @param int $iid The unique offerincident_id of the original offerreport
	 */
	public function translate_exists_chk(Validation $post)
	{
		// If add->rules validation found any errors, get me out of here!
		if (array_key_exists('locale', $post->errors()))
			return;

		$iid = (isset($_GET['iid']) AND intval($_GTE['iid'] > 0))? intval($_GET['iid']) : 0;

		// Load translation
		$translate = ORM::factory('offerincident_lang')
						->where('offerincident_id',$iid)
						->where('locale',$post->locale)
						->find();

		if ($translate->loaded)
		{
			$post->add_error( 'locale', 'exists');
		}
		else
		{
			// Not found
			return;
		}
	}

	/**
	 * Creates a SQL string from search keywords
	 */
	private function _get_searchstring($keyword_raw)
	{
		$or = '';
		$where_string = '';

		/**
		 * NOTES: 2011-11-17 - John Etherton <john@ethertontech.com> I'm pretty sure this needs to be
		 * internationalized, seems rather biased towards English.
		 * */
		// Stop words that we won't search for
		// Add words as needed!!
		$stop_words = array('the', 'and', 'a', 'to', 'of', 'in', 'i', 'is', 'that', 'it',
		'on', 'you', 'this', 'for', 'but', 'with', 'are', 'have', 'be',
		'at', 'or', 'as', 'was', 'so', 'if', 'out', 'not');

		$keywords = explode(' ', $keyword_raw);

		if (is_array($keywords) AND !empty($keywords))
		{
			array_change_key_case($keywords, CASE_LOWER);
			$i = 0;

			foreach ($keywords as $value)
			{
				if (!in_array($value,$stop_words) AND !empty($value))
				{
					$chunk = $this->db->escape_str($value);
					if ($i > 0)
					{
						$or = ' OR ';
					}
					$where_string = $where_string
									.$or
									."offerincident_title LIKE '%$chunk%' OR offerincident_description LIKE '%$chunk%' ";
					$i++;
				}
			}
		}

		// Return
		return (!empty($where_string)) ? $where_string :  "1=1";
	}

	private function _csv_text($text)
	{
		$text = stripslashes(htmlspecialchars($text));
		return $text;
	}


	/**
	 * Adds extra filter paramters to the offerreports::fetch_offerincidents()
	 * method. This way we can add 'all_offerreports=>true and other filters
	 * that don't come standard sinc we are on the backend.
	 * Works by simply adding in SQL conditions to the params
	 * array of the reprots::fetch_offerincidents() method
	 * @return none
	 */
	public function _add_offerincident_filters()
	{
		$params = Event::$data;
		$params = array_merge($params, $this->params);
		Event::$data = $params;
	}
}
