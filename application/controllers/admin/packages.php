<?php defined('SYSPATH') or die('No direct script access.');
/**
 * Comments Controller.
 * This controller will take care of viewing and editing comments in the Admin section.
 *
 * PHP version 5
 * LICENSE: This source file is subject to LGPL license 
 * that is available through the world-wide-web at the following URI:
 * http://www.gnu.org/copyleft/lesser.html
 * @author     Ushahidi Team <team@ushahidi.com> 
 * @package    Ushahidi - http://source.ushahididev.com
 * @subpackage Admin
 * @copyright  Ushahidi - http://www.ushahidi.com
 * @license    http://www.gnu.org/copyleft/lesser.html GNU Lesser General Public License (LGPL) 
 */

class Packages_Controller extends Admin_Controller
{
    function __construct()
    {
        parent::__construct();
    
        $this->template->this_page = 'packages';
        
        // If user doesn't have access, redirect to dashboard
       
    }
    
    
   public function index()
	{

		$this->template->content = new View('admin/packages');
		$this->template->content->title = Kohana::lang('ui_admin.packages');

		

		// Setup and initialize form field names
		$form = array
		(
			'action' => '',
			'package_id' => '',
			'package_name' => '',
			'description' => '',
			'map_places' => '',
			'map_offers' => '',
			'reviews' => '',
			'sales_analysis' => '',
			'offers_analysis' => '',
			'users_analysis' => '',
			'email_notifications' => '',
			'sms_notifications' => '',
			'blog' => '',
			'ticket' => '',
			'call' => '',
			'featured_offers' => '',
			'coupons' => '',
			'payment_fees' => '',
			'monthly_usd' => '',
			'quarterly_usd' => '',
			'bi_annual_usd' => '',
			'annual_usd' => ''
			
		);

		
		// Copy the form as errors, so the errors will be stored with keys corresponding to the form field names
		$errors = $form;
		$form_error = FALSE;
		$form_saved = FALSE;
		$form_action = "";
		

		
		if($_POST){
				// Fetch the post data
			$post_data = array_merge($_POST, $_FILES);
			
			//print_r($post_data);
			//exit;
			// Extract offertype-specific  information
			$package_data =  $post_data;//arr::extract($post_data,  'package_name', 'description','coupons','payment_fees','action');
			
			// Extract offertype image and offertype languages for independent validation
			//$secondary_data = arr::extract($post_data, 'icon', 'action');
			
			// Setup validation for the secondary data
			$post = Validation::factory($package_data)
						->pre_filter('trim', TRUE);
						
			// Add validation for the add/edit action
			//var_dump($_POST);
			//exit;
			$package = (! empty($_POST['package_id']) AND Package_Model::is_valid_package($_POST['package_id']))
				? new Package_Model($_POST['package_id'])
				: new Package_Model();
			//var_dump($package);
			//exit;
			if ($post->action == 'a')
			{
				// Test to see if things passed the rule checks
				if ($package->validate($package_data) AND $post->validate())
				{
					// Save the package
					//var_dump($package);exit;
					$package->map_offers=(!isset($package_data['map_offers']))?'0':$package->map_offers;
					$package->save();
					
					
					
					$form_saved = TRUE;
					$form_action = strtoupper(Kohana::lang('ui_admin.added_edited'));

					// Empty $form array
					array_fill_keys($form, '');
				}
				else
				{
					// Validation failed

					// Repopulate the form fields
					$form = arr::overwrite($form, array_merge($package_data->as_array(), $post->as_array()));

					// populate the error fields, if any
					$errors = arr::overwrite($errors, 
						array_merge($package_data->errors('offertype'), $post->errors('offertype')));

					$form_error = TRUE;
				}
				
			}
			elseif ($post->action == 'd')
			{
				
					// @todo Delete the category image
					
					// Delete category itself - except if it is trusted
					ORM::factory('package')
						
						->delete($package->id);
						
					$form_saved = TRUE;
					$form_action = strtoupper(Kohana::lang('ui_admin.deleted'));
				
			}
			
			
			
			
		}
		// Pagination
		$pagination = new Pagination(array(
			'query_string' => 'page',
			'items_per_page' => $this->items_per_page,
			'total_items' => ORM::factory('package')->count_all()
		));

		$packages = ORM::factory('package')
						->find_all($this->items_per_page, $pagination->sql_offset);

		

		$this->template->content->form = $form;
		$this->template->content->errors = $errors;
		$this->template->content->form_error = $form_error;
		$this->template->content->form_saved = $form_saved;
		$this->template->content->form_action = $form_action;
		$this->template->content->pagination = $pagination;
		$this->template->content->total_items = $pagination->total_items;
		$this->template->content->packages = $packages;
		$this->template->content->summaryfull_array = array('1'=>strtoupper(Kohana::lang('ui_main.full')),'0'=>strtoupper(Kohana::lang('ui_main.summary')));

		//$this->template->content->parents_array = $parents_array;

		// Javascript Header
		$this->template->colorpicker_enabled = TRUE;
		$this->template->tablerowsort_enabled = TRUE;
		$this->template->js = new View('admin/packages_js');
		$this->template->form_error = $form_error;

		//$this->template->content->locale_array = $locales;
		//$this->template->js->locale_array = $locales;
	
	}
}
