<?php defined('SYSPATH') or die('No direct script access.');
/**
 * This controller handles frontend blog functionality
 *
 * @version 01 - Hezron Obuchele 2012-04-25
 *
 * PHP version 5
 * LICENSE: Not for reuse or modification without the express 
 * written authorization from BeeBuy Investments Ltd.
 *
 * Widget_Controller
 * @author     Hezron Obuchele
 * @package    CrowdPesa
 * @subpackage Controllers
 * @copyright  BeeBuy Investments Ltd. - http://www.beebuy.biz
 */

class Blog_Controller extends Main_Controller{
	
	function __construct()
	{
		parent::__construct();
	}
	
	/**
	 * Show all articles in a list
	 */
	public function index()
	{
		$content = View::factory('blog/default');
		$content->articles = ORM::factory('articles')
			->where('published', '1')
			->limit('10')
			->orderby('published_date', 'desc')
			->find_all();
		$content->comment_count = array();
		$db = Database::instance();
		foreach ($content->articles as $a) {
			$comment_count = $db->query("SELECT COUNT(*) 'total' FROM comment WHERE article_id = $a->id");
			$content->comment_count[$a->id] = $comment_count[0]->total;
		}
		$content->tag_cloud = Article_Tags_Model::get_all_tags();
		$this->template->content = $content;
        $this->template->content->this_page = 'blog';
		$this->template->content->title = 'Blog';
		$this->template->header->page_title .= 'Blog' . Kohana::config('settings.title_delimiter');
		$this->template->header->header_block = $this->themes->header_block();
		$this->template->footer->footer_block = $this->themes->footer_block();
		
		/**
		$lang = Kohana::lang('ui_main');
		ksort($lang);
		$sorted_lang = '\t$lang = array(\n';
		foreach ($lang as $k => $v){
			//$k = substr($k, 1, -1);
			$sorted_lang .= "\t'$k' => '$v',\n";
		}
		$sorted_lang .= "\t);";
		*/
	}
	
	/**
	 * Show all articles with the specified tag
	 */
	public function tag($tag)
	{
		// TODO: sanitize $tag
		//if (!Article_Tags_Model::is_valid_tag($tag))
		//	url::redirect(url::site() . 'blog');
		$content = View::factory('blog/default');
		// get all ids of articles having the given tag
		$db = Database::instance();
		$result = $db->query("SELECT article_id FROM article_tags WHERE tag = '$tag'");
		$tag_articles = array();
		foreach ($result as $row)
			$tag_articles[] = $row->article_id;
		$tag_articles = "('" . implode("','", $tag_articles) . "')";
		$content->articles = $db->query("SELECT * FROM articles WHERE id IN $tag_articles AND published = 1 ORDER BY published_date DESC LIMIT 0, 10");
		$content->comment_count = array();
		$db = Database::instance();
		foreach ($content->articles as $a) {
			$comment_count = $db->query("SELECT COUNT(*) 'total' FROM comment WHERE article_id = $a->id");
			$content->comment_count[$a->id] = $comment_count[0]->total;
		}
		$content->tag_cloud = Article_Tags_Model::get_all_tags();
		$this->template->content = $content;
        $this->template->content->this_page = 'blog';
		$this->template->content->title = 'Articles tagged "' . $tag . '"';
		$this->template->header->page_title .= 'Blog' . Kohana::config('settings.title_delimiter');
		$this->template->header->header_block = $this->themes->header_block();
		$this->template->footer->footer_block = $this->themes->footer_block();
	}
	
	/**
	 * View a single article based on article id param passed
	 */
	public function article($article_id)
	{
		// Load Akismet API Key (Spam Blocker)
		$api_akismet = Kohana::config('settings.api_akismet');
		
		// Sanitize the offerreport id before proceeding
		$article_id = intval($article_id);
		
		if ($article_id > 0 AND Articles_Model::is_valid_article($article_id,FALSE))
		{
			$content = View::factory('blog/article');
			$article = ORM::factory('articles', $article_id);
			if (!$article->loaded || ($article->loaded && $article->published == 0))
				url::redirect(url::site());
			
			// Setup and initialize comment form field names
			$form = array(
					'comment_author' => '',
					'comment_description' => '',
					'comment_email' => '',
					'comment_ip' => '',
					'captcha' => '',
					'comment_rating' => 0,
					'comment_rating_scale' => array('No star','1 star','2 stars','3 stars','4 stars' ,'5 stars')
				);
	
			$captcha = Captcha::factory();
			$errors = $form;
			$form_error = FALSE;
			
			// Check, has the form been submitted, if so, setup validation

			if ($_POST AND Kohana::config('settings.allow_comments') )
			{
				// Instantiate Validation, use $post, so we don't overwrite $_POST fields with our own things

				$post = Validation::factory($_POST);

				// Add some filters

				$post->pre_filter('trim', TRUE);

				// Add some rules, the input field, followed by a list of checks, carried out in order

				if (!$this->user)
				{
					$post->add_rules('comment_author', 'required', 'length[3,100]');
					$post->add_rules('comment_email', 'required','email', 'length[4,100]');
				}
				$post->add_rules('comment_description', 'required');
				$post->add_rules('captcha', 'required', 'Captcha::valid');

				// Test to see if things passed the rule checks

				if ($post->validate())
				{
					// Yes! everything is valid

					if ($api_akismet != "")
					{
						// Run Akismet Spam Checker
						$akismet = new Akismet();

						// Comment data
						$comment = array(
							'website' => "",
							'body' => $post->comment_description,
							'user_ip' => $_SERVER['REMOTE_ADDR']
						);

						if ($this->user)
						{
							$comment['author'] = $this->user->name;
							$comment['email'] = $this->user->email;
						}
						else
						{
							$comment['author'] = $post->comment_author;
							$comment['email'] = $post->comment_email;
						}

						$config = array(
							'blog_url' => url::site(),
							'api_key' => $api_akismet,
							'comment' => $comment
						);

						$akismet->init($config);

						if ($akismet->errors_exist())
						{
							if ($akismet->is_error('AKISMET_INVALID_KEY'))
							{
								// throw new Kohana_Exception('akismet.api_key');
							}
							elseif ($akismet->is_error('AKISMET_RESPONSE_FAILED'))
							{
								// throw new Kohana_Exception('akismet.server_failed');
							}
							elseif ($akismet->is_error('AKISMET_SERVER_NOT_FOUND'))
							{
								// throw new Kohana_Exception('akismet.server_not_found');
							}

							// If the server is down, we have to post
							// the comment :(
							// $this->_post_comment($comment);

							$comment_spam = 0;
						}
						else
						{
							$comment_spam = ($akismet->is_spam()) ? 1 : 0;
						}
					}
					else
					{
						// No API Key!!
						$comment_spam = 0;
					}

					$comment = new Comment_Model();
					$comment->article_id = $article_id;
					$rating = new Rating_Model();
					$rating->article_id = $article_id;
					
					if ($this->user)
					{
						$comment->user_id = $this->user->id;
						$comment->comment_author = $this->user->name;
						$comment->comment_email = $this->user->email;
						$rating->user_id = $this->user->id;
					}
					else
					{
						$comment->comment_author = strip_tags($post->comment_author);
						$comment->comment_email = strip_tags($post->comment_email);
					}
					
					$comment->comment_description = strip_tags($post->comment_description);
					$comment->comment_ip = $_SERVER['REMOTE_ADDR'];
					$comment->comment_date = date("Y-m-d H:i:s",time());
					$rating->rating_date = date("Y-m-d H:i:s",time());
					$rating->rating = strip_tags($post->comment_rating);
					$rating->rating_ip = $_SERVER['REMOTE_ADDR'];

					// Activate comment for now
					if ($comment_spam == 1)
					{
						$comment->comment_spam = 1;
						$comment->comment_active = 0;
					}
					else
					{
						$comment->comment_spam = 0;
						$comment->comment_active = (Kohana::config('settings.allow_comments') == 1)? 1 : 0;
					}
					$comment->save();
					$rating->comment_id = $comment->id;
					$rating->save();

					// Event::comment_add - Added a New Comment
					Event::run('ushahidi_action.comment_add', $comment);

					// Notify Admin Of New Comment
					$send = notifications::notify_admins(
						"[".Kohana::config('settings.site_name')."] ".
							Kohana::lang('notifications.admin_new_comment.subject'),
							Kohana::lang('notifications.admin_new_comment.message')
							."\n\n'".strtoupper($article->title)."'"
							."\n".url::base().'blog/article/'.$article_id
						);

					// Redirect
					url::redirect('blog/article/'.$article_id);

				}
				else
				{
					// No! We have validation errors, we need to show the form again, with the errors
					// Repopulate the form fields
					$form = arr::overwrite($form, $post->as_array());

					// Populate the error fields, if any
					$errors = arr::overwrite($errors, $post->errors('comments'));
					$form_error = TRUE;
				}
			}
			
			$content->article = $article;
			$content->article_author = User_Model::get_user_by_id($article->author_id)->name;
			$tags = Article_Tags_Model::get_article_tags($article_id);
			$content->article_tags = ($tags) ? $tags : array('None');
			
			$this->template->header->page_title .= $article->title . ' - Blog ' . Kohana::config('settings.title_delimiter');
			$this->template->header->header_block = $this->themes->header_block();
			$this->template->content = $content;
            $this->template->content->this_page = 'article';
			$this->template->footer->footer_block = $this->themes->footer_block();
			
			// Get average rating
			$db = Database::instance();
			$average_rating = $db->query("SELECT AVG(rating) 'ave' FROM rating WHERE article_id = $article_id AND comment_id IS NOT NULL");
			$this->template->content->article_rating = round($average_rating[0]->ave);
			
			// Retrieve Comments
			$this->template->content->comments = "";
			if (Kohana::config('settings.allow_comments'))
			{
				$this->template->content->comments = new View('blog/article_comments');
				$article_comments = array();
				if ($article_id)
				{
					$article_comments = Articles_Model::get_comments($article_id);
				}
				$this->template->content->comments->article_comments = $article_comments;
			}
		
		}
		else
		{
			url::redirect(url::site());
		}
		
		// Add articles with similar tags
		$this->template->content->similar_articles = Articles_Model::get_similar_articles_with_tags($article_id, $content->article_tags);
		
		// Are we allowed to submit comments?
		$this->template->content->comments_form = "";
		if (Kohana::config('settings.allow_comments'))
		{
			$this->template->content->comments_form = new View('blog/article_comments_form');
			$this->template->content->comments_form->user = $this->user;
			$this->template->content->comments_form->form = $form;
			$this->template->content->comments_form->captcha = $captcha;
			$this->template->content->comments_form->errors = $errors;
			$this->template->content->comments_form->form_error = $form_error;
		}
	}
	
	/**
	 * Add/Edit articles depending on url param passed
	 */
	public function edit($article_id = null)
	{
		$page = View::factory('blog/admin_edit');
		// save this to database
		$posted_title = arr::get($_POST, 'post_title', 'No title');
		$posted_editor_data = arr::get($_POST, 'post_content', '<p><strong>Nothing</strong> was saved</p>');
		// convert to html entities before sending to editor
		$page->post_content = htmlentities($posted_editor_data);
		$page->post_title = $posted_title;
		echo $page;
	}
}