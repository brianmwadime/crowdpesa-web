<?php defined('SYSPATH') or die('No direct script access.');
/**
 * Checkins Controller.
 * This controller will take care of adding and editing reports in the Member section.
 *
 * PHP version 5
 * LICENSE: This source file is subject to LGPL license
 * that is available through the world-wide-web at the following URI:
 * http://www.gnu.org/copyleft/lesser.html
 * @author	   Ushahidi Team <team@ushahidi.com>
 * @package	   Ushahidi - http://source.ushahididev.com
 * @subpackage Members
 * @copyright  Ushahidi - http://www.ushahidi.com
 * @license	   http://www.gnu.org/copyleft/lesser.html GNU Lesser General Public License (LGPL)
 */

class Checkins_Controller extends Customers_Controller {
	
	function __construct()
	{
		parent::__construct();

		$this->template->this_page = 'checkins';
	}
	
	/**
	* Lists the checkins.
	* @param int $page
	*/
	function index($page = 1)
	{
		$this->template->content = new View('customers/checkins');
		$this->template->content->title = Kohana::lang('ui_admin.my_checkins');
		
		// check, has the form been submitted?

		$errors =array();
        $message = '';
        $message_class = '';
        $success = FALSE;
		$form_error = FALSE;
		$form_action = "";
		
		
		
		// Pagination
		$pagination = new Pagination(array(
			'query_string'	 => 'page',
			'items_per_page' => (int) Kohana::config('settings.items_per_page_admin'),
			'total_items'	 => ORM::factory('checkin')
				->where('user_id', $this->user->id)
				->count_all()
			));
		$db = Database::instance();
		$user_id = $this->user->id;
		$items_per_page = (int) Kohana::config('settings.items_per_page_admin');		
		$checkins =$db->query( "SELECT checkin.*,media.media_thumb,location.location_name,latitude,longitude
						FROM checkin 
						INNER JOIN location ON checkin.location_id = location.id
						LEFT JOIN media on media.checkin_id = checkin.id WHERE checkin.user_id = $user_id						
						 GROUP BY checkin.id ORDER BY checkin_date DESC LIMIT $pagination->sql_offset,$items_per_page");
		$media = $db->query("SELECT media_medium,media.media_title FROM media WHERE checkin_id IS NOT NULL");
	
		$this->template->content->media = $media; 	
		$this->template->content->checkins = $checkins;
		$this->template->content->pagination = $pagination;
		$this->template->content->form_error = $form_error;
		$this->template->content->message = $message;
		$this->template->content->message_class = $message_class;
		//$this->template->content->form_deleted = $form_deleted;
		//$this->template->content->form_action = $form_action;
			
		// Total Reports
		$this->template->content->total_items = $pagination->total_items;
		
		// Javascript Header
		$this->template->map_enabled = TRUE;
		$this->template->js = new View('customers/checkins_js');
	}
	public function delete($id){
		$this->template->content = new View('customers/checkins');
		$this->template->content->title = Kohana::lang('ui_admin.my_checkins');
 		$errors =array();
        $message = '';
        $message_class = '';
        //$success = FALSE;
		$form_error = FALSE;
		//$form_action = "";
		$update = ORM::factory('checkin')
			->where('user_id', $this->user->id)
			->find($id);
		if ($update->loaded == true)
		{
			$checkin_id = $update->id;
			$update->delete();
			// Delete Media
			ORM::factory('media')->where('checkin_id',$checkin_id)->delete_all();
			$message_class = 'info';
            $message = Kohana::lang('ui_main.checkin_deleted_successful');
			
		}else{
			$form_error = TRUE;
			
			$message_class = 'error';
            $message = Kohana::lang('ui_main.something_went_wrong');
		}
		$this->template->content->message = $message;
		$this->template->content->form_error = $form_error;
		$this->template->content->message_class = $message_class;
		
		url::redirect('customers/checkins');
	
		
	}
}
