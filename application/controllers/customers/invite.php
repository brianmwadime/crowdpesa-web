<?php defined('SYSPATH') or die('No direct script access.');
/**
 * Invite Controller.
 * This controller will take care of adding new members
 *
 * PHP version 5
 * LICENSE: This source file is subject to LGPL license
 * that is available through the world-wide-web at the following URI:
 * http://www.gnu.org/copyleft/lesser.html
 * @author	   Ushahidi Team <team@ushahidi.com>
 * @package	   Ushahidi - http://source.ushahididev.com
 * @subpackage Members
 * @copyright  Ushahidi - http://www.ushahidi.com
 * @license	   http://www.gnu.org/copyleft/lesser.html GNU Lesser General Public License (LGPL)
 */

class Invite_Controller extends Customers_Controller {
	
	function __construct()
	{
		parent::__construct();

		$this->template->this_page = 'join';
        $this->session = Session::instance();
	}
	
	/**
	* Show invitation form
	*/
	function index()
	{
		$this->template->content = new View('customers/invite');
		$this->template->content->title = Kohana::lang('ui_main.member_invite');
		
		// setup and initialize form field names
        $form = array
        (
            'emails' => '',
        );
        //  copy the form as errors, so the errors will be stored with keys corresponding to the form field names
        $errors = $form;
        $form_error = FALSE;
        $message = '';
        $message_class = '';
        $success = FALSE;
        $failed_emails = array();
		
        if ($_POST){
            $post = Validation::factory($_POST);
            
            //  Add some filters and rules
            $post->pre_filter('trim', TRUE);
            $post->add_rules('emails','required');

            if ($post->validate())
            {
                //$success = true;
                $message_class = 'info';
                $emails = str_ireplace(' ', '', $post->emails);
                $emails = explode(',', $emails);
                // generate codes for users
                $num_codes = count($emails);
                $codes = array('id' => '', 'code' => '');
                
                foreach($emails as $email){
                    // Send invitation email
                    $invite_code = $this->_generate_invite_code();
                    $success = true;
                    $codes['code'] =  $invite_code;
                    $email_sent = $this->_send_email_invite($email, $invite_code);
                    if ($email_sent === null){
                        $success = FALSE;
                        $failed_emails[] = $email;
                    }else{
                        // insert codes to db
                        $this->db->insert('invite_codes', $codes);
                    }
                }
                if (!empty($failed_emails)){
                    $message = 'Failed to send invitations to ' . implode(', ', $failed_emails);
                    $message_class = 'error';
                }
                if ($success)
                    $message = "Invitations sent successfully";
                //$this->db->query('INSERT INTO invite_codes (id, code) VALUES (\''.implode("', '", $codes).')');
            }
            else
            {
                // repopulate the form fields
                $form = arr::overwrite($form, $post->as_array());
                $message = "You haven't entered any valid email addresses";
                // populate the error fields, if any
                $errors = arr::overwrite($errors, $post->errors('auth'));
                $form_error = TRUE;
            }

        }
        
        $this->template->content->errors = $errors;
        $this->template->content->success = $success;
        $this->template->content->message = $message;
        $this->template->content->message_class = $message_class;
        //$this->template->content->change_pw_success = $change_pw_success;
        $this->template->content->form = $form;
        $this->template->content->form_error = $form_error;
        //$this->template->content->new_confirm_email_form = $new_confirm_email_form;
	}
    
    private function _generate_invite_code () {
        $chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";  
        $length = 15;
        $str = substr(str_shuffle($chars), 0, $length);
        return $str;
    }
    
	/**
     * Sends an email confirmation
     */
    private function _send_email_invite($email, $invite_code)
    {
        $settings = kohana::config('settings');
        //die(var_dump($settings));
        // Check if we require users to go through this process
        if (isset($settings['require_email_invite']) && $settings['require_email_invite'] == 0)
        {
            return false;
        }

        $url = url::site()."customers/join/?ic=$invite_code";//&e=$email";

        $to = $email;
        $from = array($settings['site_email'], $settings['site_name']);
        $subject = $settings['site_name'].' '.Kohana::lang('ui_main.invite_confirmation_subject', array($settings['site_name']));
        $message = Kohana::lang('ui_main.invite_confirmation_message',
            array(Auth::instance()->get_user()->name, $settings['site_name'], $url));
        
        if (email::send($to, $from, $subject, $message, FALSE) == 1)
        {
            return true;
        }
        return null;
    }
}
