<?php defined('SYSPATH') or die('No direct script access.');
/**
 * Join Controller.
 * This controller will take care of adding new members
 *
 * PHP version 5
 * LICENSE: This source file is subject to LGPL license
 * that is available through the world-wide-web at the following URI:
 * http://www.gnu.org/copyleft/lesser.html
 * @author	   Ushahidi Team <team@ushahidi.com>
 * @package	   Ushahidi - http://source.ushahididev.com
 * @subpackage Members
 * @copyright  Ushahidi - http://www.ushahidi.com
 * @license	   http://www.gnu.org/copyleft/lesser.html GNU Lesser General Public License (LGPL)
 */

class Join_Controller extends Main_Controller {
	
	function __construct()
	{
		parent::__construct();

		$this->template->this_page = 'join';
        $this->session = Session::instance();
        $this->db = Database::instance();
	}
	
	/**
	* Show sign up form
	*/
	function index()
	{
		$this->template->content = new View('customers/join');
		$this->template->content->title = Kohana::lang('ui_main.member_join');
		$invite_code = (isset($_GET['ic']) AND ! empty($_GET['ic'])) ? $_GET['ic'] : "";
        
		// setup and initialize form field names
        $form = array
        (
            'action'    => '',
            'username'  => '',
            'password'  => '',
            'password_again'  => '',
            'name'      => '',
            'email'     => '',
            'resetemail' => '',
            'confirmation_email' => '',
            'code' => $invite_code
        );
        //  copy the form as errors, so the errors will be stored with keys corresponding to the form field names
        $errors = $form;
        $form_error = FALSE;
        $message = '';
        $message_class = '';
        $success = FALSE;
        $this->session->set('invite_code', false);
		// Show that confirming the email address was a success
        if (isset($_GET["confirmation_success"]))
        {
            $message_class = 'info';
            $message = Kohana::lang('ui_main.confirm_email_successful');
        }
        // Check if invite code was sent from us
        if ($invite_code !== ''){
            if (count($this->db->query("SELECT code FROM invite_codes WHERE code = '$invite_code'"))){
                $this->session->set('invite_code', $invite_code);
            }else{
                $form['code'] = "";
                $this->session->set('invite_code', false);
            }
        }

        /*if ($this->session->get('invite_code') === false)
        {
            $message_class = 'error';
            $message = Kohana::lang('ui_main.need_invite');
        }*/
        // Invite request
        if ($_POST && isset($_POST["action"]) && $_POST["action"] == "invite"){
            $post = Validation::factory($_POST);
            $post->pre_filter('trim', TRUE);
            $post->add_rules('email','required','email','length[4,64]');
            if ($post->validate())
            {
                $invite = array('id' => '', 'code' => '', 'email' => $post->email);
                $this->db->insert('invite_codes', $invite);
                $message_class = 'info';
                $message = 'We have received your invite request.';
            }
            else
            {
                // repopulate the form fields
                $form = arr::overwrite($form, $post->as_array());
                $message_class = 'error';
                $message = 'You did not enter a valid email address';
                // populate the error fields, if any
                //$errors = arr::overwrite($errors, $post->errors());
                unset($errors['code']);
                $form_error = TRUE;
            }
        }
        // Show sign up form
        if ($_POST && isset($_POST["action"]) && $_POST["action"] == "new" && $this->session->get('invite_code')){
		    // START: New User Process
            $post = Validation::factory($_POST);
            //  Add some filters
            $post->pre_filter('trim', TRUE);

            $post->add_rules('password','required', 'length['.kohana::config('auth.password_length').']','alpha_numeric');
            $post->add_rules('name','required','length[3,100]');
            $post->add_rules('username','required','length[3,100]');
            $post->add_rules('email','required','email','length[4,64]');
            $post->add_callbacks('username', array($this,'username_exists_chk'));
            $post->add_callbacks('email', array($this,'email_exists_chk'));

            // If Password field is not blank
            if (!empty($post->password))
            {
                $post->add_rules('password','required','length['.kohana::config('auth.password_length').']'
                    ,'alpha_numeric','matches[password_again]');
            }

            if ($post->validate())
            {

                $riverid_id = false;
                $retailer = (isset($_POST['retailer'])); // ensures that retailers can only signup using their signup form
                if (kohana::config('riverid.enable') == true)
                {
                    $riverid = new RiverID;
                    $riverid->email = $post->email;
                    $riverid->password = $post->password;
                    $riverid->register();
                    $riverid_id = $riverid->user_id;
                }

                $user = User_Model::create_user($post->username, $post->email,$post->password,$riverid_id,$post->name, $retailer);

                // Send Confirmation email
                $email_sent = $this->_send_email_confirmation($user);
                // Log out any logged in users
                if (Auth::instance()->logged_in())
                        Auth::instance()->logout();
                if ($email_sent == true)
                {
                    $message_class = 'info';
                    $message = Kohana::lang('ui_main.login_confirmation_sent');
                }
                else
                {
                    $message_class = 'info';
                    $message = Kohana::lang('ui_main.login_account_creation_successful');
                    $this->template->content = new View('customers/login');
                }
                //delete invitation code from db
                $this->db->query("DELETE FROM invite_codes WHERE code = '$invite_code'");
                $success = TRUE;
                $action = "";
            }
            else
            {
                // repopulate the form fields
                $form = arr::overwrite($form, $post->as_array());

                // populate the error fields, if any
                $errors = arr::overwrite($errors, $post->errors('auth'));
                unset($errors['code']);
                $form_error = TRUE;
            }

            // END: New User Process
        }
        
        
        $this->template->content->errors = $errors;
        $this->template->content->success = $success;
        $this->template->content->message = $message;
        $this->template->content->message_class = $message_class;
        //$this->template->content->change_pw_success = $change_pw_success;
        $this->template->content->form = $form;
        $this->template->content->form_error = $form_error;
        //$this->template->content->new_confirm_email_form = $new_confirm_email_form;
	}

	/**
     * Sends an email confirmation
     */
    private function _send_email_confirmation($user)
    {
        $settings = kohana::config('settings');

        // Check if we require users to go through this process
        if ($settings['require_email_confirmation'] == 0)
        {
            return false;
        }

        $email = $user->email;
        $code = text::random('alnum', 20);
        $user->code = $code;
        $user->save();

        $url = url::site()."customers/join/verify/?c=$code&e=$email";

        $to = $email;
        $from = array($settings['site_email'], $settings['site_name']);
        $subject = $settings['site_name'].' '.Kohana::lang('ui_main.login_signup_confirmation_subject');
        $message = Kohana::lang('ui_main.login_signup_confirmation_message',
            array($settings['site_name'], $url));

        if (email::send($to, $from, $subject, $message, FALSE) == 1)
        {
            return true;
        }
        return false;
    }
    
    /**
     * Checks if username already exists.
     * @param Validation $post $_POST variable with validation rules
     */
    public function username_exists_chk(Validation $post)
    {
        $users = ORM::factory('user');
        // If add->rules validation found any errors, get me out of here!
        if (array_key_exists('username', $post->errors()))
            return;

        if ($users->username_exists($post->username))
            $post->add_error( 'username', 'exists');
    }

    /**
     * Checks if email address is associated with an account.
     * @param Validation $post $_POST variable with validation rules
     */
    public function email_exists_chk( Validation $post )
    {
        $users = ORM::factory('user');
        if ($post->action == "new")
        {
            if (array_key_exists('email',$post->errors()))
                return;

            if ($users->email_exists( $post->email ) )
                $post->add_error('email','exists');
        }
        elseif($post->action == "forgot")
        {
            if (array_key_exists('resetemail',$post->errors()))
                return;

            if ( ! $users->email_exists( $post->resetemail ) )
                $post->add_error('resetemail','invalid');
        }
    }
    
    /**
     * Confirms user registration
     */
    public function verify()
    {
        $auth = Auth::instance();

        $code = (isset($_GET['c']) AND ! empty($_GET['c'])) ? $_GET['c'] : "";
        $email = (isset($_GET['e']) AND ! empty($_GET['e'])) ? $_GET['e'] : "";

        $user = ORM::factory("user")
            ->where("code", $code)
            ->where("email", $email)
            ->where("confirmed != 1")
            ->find();

        if ($user->loaded)
        {
            $user->confirmed = 1;

            // Give the user the appropriate roles if the admin doesn't need to verify accounts
            //   and if they don't already have role assigned.
            if (Kohana::config('settings.manually_approve_users') == 0
                AND ! $user->has(ORM::factory('role', 'login')))
            {
                $user->add(ORM::factory('role', 'login'));
                $user->add(ORM::factory('role', 'member'));
                $user->add(ORM::factory('role', 'customer'));
            }

            $user->save();
            //TODO: delete invitation code from db
            
            // Log all other sessions out so they can log in nicely on the login page
            $auth->logout();

            // Redirect to login
            url::redirect("customers/join?confirmation_success");
        }
        else
        {
            // Redirect to Login which will log themin if they are already logged in
            url::redirect("customers/join");
        }
    }
}
