<?php defined('SYSPATH') or die('No direct script access.');
/**
 * Offerreports Controller.
 * This controller will take care of adding and editing offerreports in the Member section.
 *
 * PHP version 5
 * LICENSE: This source file is subject to LGPL license
 * that is available through the world-wide-web at the following URI:
 * http://www.gnu.org/copyleft/lesser.html
 * @author	   BeeBuy Team <info@beebuy.com>
 * @package	   CrowdPesa - http://crowdpesa.com
 * @subpackage Members
 * @copyright  CrowdPesa - http://crowdpesa.com
 * @license	   http://www.gnu.org/copyleft/lesser.html GNU Lesser General Public License (LGPL)
 */

class Offerreports_Controller extends Customers_Controller {
	
	function __construct()
	{
		parent::__construct();

		$this->template->this_page = 'offerreports';
	}


	/**
	* Lists the offerreports.
	* @param int $page
	*/
	function myoffers()
	{
		$this->template->content = new View('customers/offerreports');
		$this->template->content->title = Kohana::lang('ui_admin.offerreports');


		if (!empty($_GET['status']))
		{
			$status = $_GET['status'];

			if (strtolower($status) == 'p')
			{
				$filter = 'picked = 1';
				$status = 'p';
			}
			elseif (strtolower($status) == 'np')
			{
				$filter = 'picked = 0';
				$status = 'np';
			}
			else
			{
				$status = "a";
				$filter = '1=1';
			}
		}
		else
		{
			$status = "a";
			$filter = "1=1";
		}

		// Get Search Keywords (If Any)
		if (isset($_GET['k']))
		{
			//	Brute force input sanitization
			
			// Phase 1 - Strip the search string of all non-word characters 
			$keyword_raw = preg_replace('/[^\w+]\w*/', '', $_GET['k']);
			
			// Strip any HTML tags that may have been missed in Phase 1
			$keyword_raw = strip_tags($keyword_raw);
			
			// Phase 3 - Invoke Kohana's XSS cleaning mechanism just incase an outlier wasn't caught
			// in the first 2 steps
			$keyword_raw = $this->input->xss_clean($keyword_raw);
			
			$filter .= " AND (".$this->_get_searchstring($keyword_raw).")";
		}
		else
		{
			$keyword_raw = "";
		}

		// check, has the form been submitted?
		$form_error = FALSE;
		$form_saved = FALSE;
		$form_action = "";
		
		if ($_POST)
		{
			// Setup validation
			$post = Validation::factory($_POST);

			 //	 Add some filters
			$post->pre_filter('trim', TRUE);

			// Add some rules, the input field, followed by a list of checks, carried out in order
			$post->add_rules('action','required', 'alpha', 'length[1,1]');
			$post->add_rules('offerincident_id.*','required','numeric');

			if ($post->validate())
			{
				if ($post->action == 'd')	//Delete Action
				{
					foreach($post->offerincident_id as $item)
					{
						$update = ORM::factory('offerincident')
							->where('user_id', $this->user->id)
							->find($item);
						if ($update->loaded == true)
						{
							$offerincident_id = $update->id;
							$location_id = $update->location_id;
							$update->delete();

							// Delete Location
							ORM::factory('location')->where('id',$location_id)->delete_all();

							// Delete Offercategories
							ORM::factory('offerincident_offercategory')->where('offerincident_id',$offerincident_id)->delete_all();

							// Delete Translations
							ORM::factory('offerincident_lang')->where('offerincident_id',$offerincident_id)->delete_all();

							// Delete Photos From Directory
							foreach (ORM::factory('media')->where('offerincident_id',$offerincident_id)->where('media_type', 1) as $photo) {
								deletePhoto($photo->id);
							}

							// Delete Media
							ORM::factory('media')->where('offerincident_id',$offerincident_id)->delete_all();

							// Delete Sender
							ORM::factory('offerincident_person')->where('offerincident_id',$offerincident_id)->delete_all();

							// Delete relationship to SMS message
							$updatemessage = ORM::factory('message')->where('offerincident_id',$offerincident_id)->find();
							if ($updatemessage->loaded)
							{
								$updatemessage->offerincident_id = 0;
								$updatemessage->save();
							}

							// Delete Comments
							ORM::factory('comment')->where('offerincident_id',$offerincident_id)->delete_all();

							// Action::offerreport_delete - Deleted a Offerreport
							Event::run('ushahidi_action.offerreport_delete', $update);
						}
					}
					$form_action = strtoupper(Kohana::lang('ui_admin.deleted'));
				}
				$form_saved = TRUE;
			}
			else
			{
				$form_error = TRUE;
			}

		}

		// Pagination
		$pagination = new Pagination(array(
			'query_string'	 => 'page',
			'items_per_page' => (int) Kohana::config('settings.items_per_page_admin'),
			'total_items'	 => ORM::factory('offerincident')
				->join('orders', 'orders.offerincident_id', 'offerincident.id','INNER')
				->where('orders.customer_id', $this->user->id)
				//->where('transaction_history.collected')
				->count_all()
			));

		$offerincidents = Offerincident_Model::get_collected_offers($this->user->id,$pagination->sql_offset,(int) Kohana::config('settings.items_per_page_admin'),$filter);

		$location_ids = array();
		$country_ids = array();
		

		
		$this->template->content->offerincidents = $offerincidents;
		$this->template->content->pagination = $pagination;
		$this->template->content->form_error = $form_error;
		$this->template->content->form_saved = $form_saved;
		$this->template->content->status = $status;
		$this->template->content->form_action = $form_action;

		// Total Offerreports
		$this->template->content->total_items = $pagination->total_items;

		// Status Tab
		$this->template->content->status = $status;

		// Javascript Header
		$this->template->js = new View('admin/offerreports_js');
	}


	
	
}
