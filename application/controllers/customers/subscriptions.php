<?php defined('SYSPATH') or die('No direct script access.');
/**
 * Checkins Controller.
 * This controller will take care of adding and editing reports in the Member section.
 *
 * PHP version 5
 * LICENSE: This source file is subject to LGPL license
 * that is available through the world-wide-web at the following URI:
 * http://www.gnu.org/copyleft/lesser.html
 * @author	   Ushahidi Team <team@ushahidi.com>
 * @package	   Ushahidi - http://source.ushahididev.com
 * @subpackage Members
 * @copyright  Ushahidi - http://www.ushahidi.com
 * @license	   http://www.gnu.org/copyleft/lesser.html GNU Lesser General Public License (LGPL)
 */

class Subscriptions_Controller extends Customers_Controller {
	
	function __construct()
	{
		parent::__construct();

		$this->template->this_page = 'subscriptions';
	}
	
	/**
	* Lists the checkins.
	* @param int $page
	*/
	function index($page = 1)
	{
		$this->template->content = new View('customers/subscriptions');
		$this->template->content->title = Kohana::lang('ui_admin.subscriptions');
		
	
		
	$db = Database::instance();
	$userid=$this->user->id;
		// Pagination
		$pagination = new Pagination(array(
			'query_string'	 => 'page',
			'items_per_page' => (int) Kohana::config('settings.items_per_page_admin'),
			'total_items'	 => ORM::factory('offersubscription')
						->where('user_id',$this->user->id)
				->count_all())
				);
		
	
		$items_per_page = (int) Kohana::config('settings.items_per_page_admin');		
		$subscritpions =$db->query( "SELECT retailer_id,users.*,retailer_info.* FROM offersubscription INNER JOIN users ON retailer_id = users.id
			LEFT JOIN retailer_info ON retailer_info.user_id = retailer_id WHERE offersubscription.user_id = $userid");
	
		
		$this->template->content->subscritpions = $subscritpions;
		$this->template->content->pagination = $pagination;			
		// Total Reports
		$this->template->content->total_items = $pagination->total_items;
		
		
	
	}	
}
