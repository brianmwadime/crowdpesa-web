<?php defined('SYSPATH') or die('No direct script access.');

/**

 * This is the controller for the main site.

 *

 * PHP version 5

 * LICENSE: This source file is subject to LGPL license

 * that is available through the world-wide-web at the following URI:

 * http://www.gnu.org/copyleft/lesser.html

 * @author     Ushahidi Team <team@ushahidi.com>

 * @package    Ushahidi - http://source.ushahididev.com

 * @copyright  Ushahidi - http://www.ushahidi.com

 * @license    http://www.gnu.org/copyleft/lesser.html GNU Lesser General Public License (LGPL)

 */

class Main_Controller extends Template_Controller {

    /**

     * Automatically render the views loaded in this controller

     * @var bool

     */

    public $auto_render = TRUE;

    /**

     * Name of the template view

     * @var string

     */

    public $template = 'layout';

    /**

     * Cache object - to be used for caching content

     * @var Cache

     */

    protected $cache;

    /**

     * Whether the current controller is cacheable - defaults to FALSE

     * @var bool

     */

    public $is_cachable = FALSE;

    /**

     * Session object

     * @var Session

     */

    protected $session;

    /**

     * Prefix for the database tables

     * @var string

     */

    protected $table_prefix;

    /**

     * Themes helper library object

     * @var Themes

     */

    protected $themes;

    // User Object

    protected $user;

    public function __construct() {

        parent::__construct();

        // Load Session

        $this->session = Session::instance();

        // Load cache

        $this->cache = new Cache;

        // Load Header & Footer

        $this->template->header = new View('header');

        $this->template->footer = new View('footer');

        // Themes Helper

        $this->themes = new Themes();

        $this->themes->api_url = Kohana::config('settings.api_url');

        $this->template->header->submit_btn = $this->themes->submit_btn();

        $this->template->header->languages = $this->themes->languages();

        $this->template->header->search = $this->themes->search();

        $this->template->header->header_block = $this->themes->header_block();

        $this->template->footer->footer_block = $this->themes->footer_block();

        // Set Table Prefix

        $this->table_prefix = Kohana::config('database.default.table_prefix');

        // Retrieve Default Settings

        $site_name = Kohana::config('settings.site_name');

        // Get banner image and pass to the header

        if (Kohana::config('settings.site_banner_id') != NULL) {

            $banner = ORM::factory('media')->find(Kohana::config('settings.site_banner_id'));

            $this->template->header->banner = url::convert_uploaded_to_abs($banner->media_link);

        } else {

            $this->template->header->banner = NULL;

        }

        // Prevent Site Name From Breaking up if its too long

        // by reducing the size of the font

        $site_name_style = (strlen($site_name) > 20) ? " style=\"font-size:21px;\"" : "";

        $this->template->header->private_deployment = Kohana::config('settings.private_deployment');

        $this->template->header->site_name = $site_name;

        $this->template->header->site_name_style = $site_name_style;

        $this->template->header->site_tagline = Kohana::config('settings.site_tagline');

        // page_title is a special variable that will be overridden by other controllers to

        //    change the title bar contents

        $this->template->header->page_title = '';

        //pass the URI to the header so we can dynamically add css classes to the "body" tag

        $this->template->header->uri_segments = Router::$segments;

        $this->template->header->this_page = "";

        // add copyright info

        $this->template->footer->site_copyright_statement = '';

        $site_copyright_statement = trim(Kohana::config('settings.site_copyright_statement'));

        if ($site_copyright_statement != '') {

            $this->template->footer->site_copyright_statement = $site_copyright_statement;

        }

        // Display news feeds?

        $this->template->header->allow_feed = Kohana::config('settings.allow_feed');

        // Header Nav

        $header_nav = new View('header_nav');

        $this->template->header->header_nav = $header_nav;

        $this->template->header->header_nav->loggedin_user = FALSE;

        if (isset(Auth::instance()->get_user()->id)) {

            // Load User

            $this->template->header->header_nav->loggedin_role = (Auth::instance()->logged_in('member')) ? "members" : "admin";

            $this->template->header->header_nav->loggedin_user = Auth::instance()->get_user();

        }

        $this->template->header->header_nav->site_name = Kohana::config('settings.site_name');

        // Make the site location aware by setting user position in session

        if (isset($_POST['geolocate']) && strpos($_POST['geolocate'], ',') !== false) {

            // echo 'location: ' . $_POST['geolocate']; Hidden by John Karanja 04/06/2011

            $coords = explode(',', $_POST['geolocate']);

            if (count($coords) == 2) {

                $this->session->set('user_map_center', 2);

                $this->session->set('map_center_lat', $coords[0]);

                $this->session->set('map_center_lng', $coords[1]);

            }

        } elseif (isset($_POST['multi_city']) && strpos($_POST['multi_city'], ',') !== false) {

            $coords = explode(',', $_POST['multi_city']);

            if (count($coords) == 2) {

                $this->session->set('user_map_center', 1);

                $this->session->set('map_center_lat', $coords[0]);

                $this->session->set('map_center_lng', $coords[1]);

                //$this->session->set('map_zoom', $coords[2]);

            }

        } elseif ($this->session->get('user_map_center') != 1 && $this->session->get('user_map_center') != 2) {

            $this->session->set('user_map_center', 0);

            //$result = Database::instance()->query("SELECT incident_id, COUNT(incident_id) offers FROM offer_incidents GROUP BY incident_id ORDER BY incident_id DESC");
            //$incident_id = $result[0]->incident_id;
            //$result = Database::instance()->query("SELECT latitude, longitude FROM incident INNER JOIN location ON location.id = incident.location_id WHERE incident.id = {$incident_id}");
            
            $this->session->set('map_center_lat', -1.2988807);//$result[0]->latitude);//Kohana::config('settings.default_lat'));
            $this->session->set('map_center_lng', 36.790615);//$result[0]->longitude);//Kohana::config('settings.default_lon'));

            $this->session->set('map_zoom', Kohana::config('settings.default_zoom'));

        }

        $this->session->set('map_radius', 1);
        
        // Context
        $context = (isset($_GET['context']) && $_GET['context'] != '') ? $_GET['context'] : 'all';
        $this->template->header->context = $context;
        
        // Welcome message for logged in users
        if (Auth::instance()->logged_in('retailer')){
        	$db = Database::instance();
			// TODO: Replace NOW() with user time
			$merchant_offers = $db->query("SELECT COUNT(*) 'total' FROM offerincident WHERE user_id = " . Auth::instance()->get_user()->id . " AND offerincident_start_date < NOW() AND offerincident_end_date > NOW() AND offerincident_active = 1 ORDER BY offerincident_start_date DESC LIMIT 0, 5");
        	$this->template->header->welcome_msg = "You have " . $merchant_offers[0]->total . " active offers at the moment";
        }elseif (Auth::instance()->logged_in()){
            $this->template->header->welcome_msg = "There are " . Offerincident_Model::get_new_offers_count() . " new offers waiting for you..";
        }

        // Load profiler

        //$this->profiler = new Profiler;

    }

    /**

     * Retrieves Categories

     */

    protected function get_categories($selected_categories) {

        $categories = ORM::factory('category')->where('category_visible', '1')->where('parent_id', '0')->where('category_trusted != 1')->orderby('category_position', 'ASC')->orderby('category_title', 'ASC')->find_all();

        return $categories;

    }

    /**

     * Get Trusted Category Count

     */

    public function get_trusted_category_count($id) {

        $trusted = ORM::factory("incident")->join("incident_category", "incident.id", "incident_category.incident_id")->where("category_id", $id);

        return $trusted;

    }

    public function subscribe_retailer($id = 0) {

        if (!(intval($id) > 0))
            url::redirect($_SERVER['HTTP_REFERER']);

        if (!Auth::instance()->logged_in())
            url::redirect('login');

        $user = Auth::instance()->get_user();

        $user_id = $user->id;

        $sub = new Offersubscription_Model();

        $sub->user_id = $user_id;

        $sub->retailer_id = $id;

        $sub->save();

        url::redirect($_SERVER['HTTP_REFERER']);

    }

    public function unsubscribe_retailer($id = 0) {

        if (!(intval($id) > 0))
            url::redirect($_SERVER['HTTP_REFERER']);

        if (!Auth::instance()->logged_in())
            url::redirect('login');

        $user = Auth::instance()->get_user();

        $user_id = $user->id;

        ORM::factory('offersubscription')->where('user_id=' . $user_id)->where('retailer_id=' . $id)->delete_all();

        url::redirect($_SERVER['HTTP_REFERER']);

    }

    public function index() {
        $this->template->header->this_page = 'home';
        $category_id = (isset($_GET['category']) && $_GET['category'] != '') ? $_GET['category'] : 0;
        $context = (isset($_GET['context']) && $_GET['context'] != '') ? $_GET['context'] : 'all';
        //$category_id = ($context == 'offers') ? 0 : $category_id;
        $this->template->content = new View('main');
        $this->template->content->context = $context;
        $this->template->content->listings = new View('listings');
        $this->template->content->listings->context = $context;
        $this->template->content->listings->title = $context == 'places' ? 'reports' : $context;
		$this->template->content->category_title = '';
		$this->template->content->title = $this->template->content->listings->title;

        $db = Database::instance();
        $max_radius = $this->session->get('map_radius');
        $lat = Session::instance()->get('map_center_lat');
        $lng = Session::instance()->get('map_center_lng');
        $items_per_page = (int) Kohana::config('settings.items_per_page');

        $this->template->content->pagination = '';

        if ($context == 'offers') {
            $this->template->header->this_page = 'offers';
            if (intval($category_id) > 0) {
                $cat_id = $_GET['category'];
                $query = "SELECT offerincident.id, offerincident_title as title,  offerincident_featured as featured, offerincident_description as description, ( 6371 * acos( cos( radians( $lat ) ) * cos( radians( latitude ) ) * cos( radians( longitude ) - radians( $lng ) ) + sin( radians( $lat ) ) * sin( radians( latitude ) ) ) ) AS distance FROM offerincident_offercategory INNER JOIN offerincident ON offerincident.id = offerincident_offercategory.offerincident_id 
INNER JOIN offer_incidents oi ON oi.offerincident_id = offerincident.id
INNER JOIN incident ON incident.id = oi.incident_id 
INNER JOIN location ON incident.location_id = location.id WHERE offercategory_id = $cat_id AND offerincident_start_date < NOW() AND offerincident_end_date > NOW() AND offerincident_active = 1 AND longitude BETWEEN ( $lng -20 / abs( cos( radians( $lat ) ) *69 ) ) AND ( $lng +20 / abs( cos( radians( $lat ) ) *69 ) )
                    AND latitude BETWEEN ( $lat - ( $max_radius / 69 ) ) AND ( $lat + ( $max_radius / 69 ) ) GROUP BY offerincident.id HAVING distance < $max_radius ORDER BY featured DESC, offerincident_dateadd DESC";
                // Change title to match category title
                $title = $db->query("SELECT offercategory_title FROM offercategory WHERE id = {$cat_id}");
                $this->template->content->listings->category_title = $title[0]->offercategory_title;
				$this->template->content->category_title = $this->template->content->listings->category_title;
            } else {

                $query = "SELECT offerincident.id, offerincident_title as title,  offerincident_featured as featured, offerincident_description as description, ( 6371 * acos( cos( radians( $lat ) ) * cos( radians( latitude ) ) * cos( radians( longitude ) - radians( $lng ) ) + sin( radians( $lat ) ) * sin( radians( latitude ) ) ) ) AS distance FROM offerincident  
INNER JOIN offer_incidents oi ON oi.offerincident_id = offerincident.id
INNER JOIN incident ON incident.id = oi.incident_id 
INNER JOIN location ON incident.location_id = location.id WHERE offerincident_start_date < NOW() AND offerincident_end_date > NOW() AND offerincident_active = 1 AND longitude BETWEEN ( $lng -20 / abs( cos( radians( $lat ) ) *69 ) ) AND ( $lng +20 / abs( cos( radians( $lat ) ) *69 ) )
                    AND latitude BETWEEN ( $lat - ( $max_radius / 69 ) ) AND ( $lat + ( $max_radius / 69 ) ) GROUP BY offerincident.id HAVING distance < $max_radius ORDER BY featured DESC, offerincident_dateadd DESC";

            }

            // Pagination
            $pagination = new Pagination( array('query_string' => 'page', 'items_per_page' => $items_per_page, 'total_items' => count($db->query($query))));
            $this->template->content->pagination = $pagination;
            $this->template->content->listings->items = $db->query($query . ' LIMIT ' . $pagination->sql_offset . ',' . $items_per_page);

            // Retrieve comment counts and thumbnails for each item
            $this->template->content->listings->comment_count = array();
            $this->template->content->listings->thumbnails = array();

            foreach ($this->template->content->listings->items as $i) {
                $comment_count = $db->query("SELECT COUNT(*) 'total' FROM comment WHERE offerincident_id = $i->id");
                $media = $db->query("SELECT media_thumb FROM media WHERE offerincident_id = $i->id AND media_thumb IS NOT NULL");
                $image = (count($media) && url::exists(url::site() . Kohana::config('upload.relative_directory') . '/' . $media[0]->media_thumb)) ? $media[0]->media_thumb : '';
                $this->template->content->listings->thumbnails[$i->id] = $image;
                $this->template->content->listings->comment_count[$i->id] = $comment_count[0]->total;

            }

        } elseif ($context == 'places') {
            $this->template->header->this_page = 'places';
            if (intval($category_id) > 0) {
                $cat_id = $_GET['category'];
                $query = "SELECT incident.id, incident_title as title, incident_description as description, (0) as featured, ( 6371 * acos( cos( radians( $lat ) ) * cos( radians( latitude ) ) * cos( radians( longitude ) - radians( $lng ) ) + sin( radians( $lat ) ) * sin( radians( latitude ) ) ) ) AS distance FROM incident_category INNER JOIN incident ON incident.id = incident_id INNER JOIN location ON incident.location_id = location.id WHERE category_id = $cat_id AND incident_active = 1 AND longitude BETWEEN ( $lng -20 / abs( cos( radians( $lat ) ) *69 ) ) AND ( $lng +20 / abs( cos( radians( $lat ) ) *69 ) )

                    AND latitude BETWEEN ( $lat - ( $max_radius / 69 ) ) AND ( $lat + ( $max_radius / 69 ) ) GROUP BY incident.id HAVING distance < $max_radius ORDER BY incident_dateadd DESC";

                //Change title to match category title
                $title = $db->query("SELECT category_title FROM category WHERE id = {$cat_id}");
                $this->template->content->listings->category_title = $title[0]->category_title;
                $this->template->content->category_title = $this->template->content->listings->category_title;
            } else {
                $query = "SELECT incident.id, incident_title as title, incident_description as description, (0) as featured, ( 6371 * acos( cos( radians( $lat ) ) * cos( radians( latitude ) ) * cos( radians( longitude ) - radians( $lng ) ) + sin( radians( $lat ) ) * sin( radians( latitude ) ) ) ) AS distance FROM incident INNER JOIN location ON incident.location_id = location.id WHERE incident_active = 1 AND longitude BETWEEN ( $lng -20 / abs( cos( radians( $lat ) ) *69 ) ) AND ( $lng +20 / abs( cos( radians( $lat ) ) *69 ) )

                    AND latitude BETWEEN ( $lat - ( $max_radius / 69 ) ) AND ( $lat + ( $max_radius / 69 ) ) GROUP BY incident.id HAVING distance < $max_radius ORDER BY incident_dateadd DESC";

            }

            // Pagination
            $pagination = new Pagination( array('query_string' => 'page', 'items_per_page' => $items_per_page, 'total_items' => count($db->query($query))));
            $this->template->content->pagination = $pagination;
            $this->template->content->listings->items = $db->query($query . ' LIMIT ' . $pagination->sql_offset . ',' . $items_per_page);
            // Retrieve comment counts and thumbnails for each item

            $this->template->content->listings->comment_count = array();
            $this->template->content->listings->thumbnails = array();
            foreach ($this->template->content->listings->items as $i) {
                $comment_count = $db->query("SELECT COUNT(*) 'total' FROM comment WHERE incident_id = $i->id");
                $media = $db->query("SELECT media_thumb FROM media WHERE incident_id = $i->id AND media_thumb IS NOT NULL");
                $image = (count($media) && url::exists(url::site() . Kohana::config('upload.relative_directory') . '/' . $media[0]->media_thumb)) ? $media[0]->media_thumb : '';
                $this->template->content->listings->thumbnails[$i->id] = $image;
                $this->template->content->listings->comment_count[$i->id] = $comment_count[0]->total;
            }

        }

        // Cacheable Main Controller

        $this->is_cachable = TRUE;
        // Map and Slider Blocks
        $div_map = new View('main_map');
        $div_timeline = new View('main_timeline');
        // Filter::map_main - Modify Main Map Block
        Event::run('ushahidi_filter.map_main', $div_map);
        // Filter::map_timeline - Modify Main Map Block
        Event::run('ushahidi_filter.map_timeline', $div_timeline);
        $this->template->content->div_map = $div_map;
        $this->template->content->div_timeline = $div_timeline;

        // Check if there is a site message
        $this->template->content->site_message = '';
        $site_message = trim(Kohana::config('settings.site_message'));

        if ($site_message != '') {
            $this->template->content->site_message = $site_message;
        }

        // Get locale

        $l = Kohana::config('locale.language.0');

        // Get all active top level categories

        $parent_categories = array();

        /*foreach (ORM::factory('category')
        ->where('category_visible', '1')
        ->where('id != 5')
        ->where('parent_id', '0')
        ->find_all() as $category) {*/
        foreach ($db->query("SELECT `category`.* FROM `category` WHERE `category_visible` = '1' AND id != 5 AND `parent_id` = '0' AND id IN (SELECT category_id FROM incident_category) ORDER BY `category`.`category_position` ASC") as $category){
            // Get The Children

            /*$children = array();
            foreach ($category->children as $child) {

                $child_visible = $child->category_visible;

                if ($child_visible) {

                    // Check for localization of child category

                    $display_title = Category_Lang_Model::category_title($child->id, $l);

                    $ca_img = ($child->category_image != NULL) ? url::convert_uploaded_to_abs($child->category_image) : NULL;

                    $children[$child->id] = array($display_title, $child->category_color, $ca_img);

                }

            }*/

            // Check for localization of parent category

            $display_title = Category_Lang_Model::category_title($category->id, $l);

            // Put it all together

            $ca_img = ($category->category_image != NULL) ? url::convert_uploaded_to_abs($category->category_image) : NULL;
            $parent_categories[$category->id] = array($display_title, $category->category_color, $ca_img);//, $children);

        }
        
        $this->template->content->categories = $parent_categories;

        // Get all active Layers (KMZ/KML)

        $layers = array();

        $config_layers = Kohana::config('map.layers');
        // use config/map layers if set

        if ($config_layers == $layers) {

            foreach (ORM::factory('layer')

            ->where('layer_visible', 1)

            ->find_all() as $layer) {

                $layers[$layer->id] = array($layer->layer_name, $layer->layer_color, $layer->layer_url, $layer->layer_file);

            }

        } else {

            $layers = $config_layers;

        }

        $this->template->content->layers = $layers;

        // Get Default Color

        $this->template->content->default_map_all = Kohana::config('settings.default_map_all');

        $this->template->content->default_map_offers = Kohana::config('settings.default_map_offers');

        // Get Twitter Hashtags

        $this->template->content->twitter_hashtag_array = array_filter(array_map('trim', explode(',', Kohana::config('settings.twitter_hashtags'))));

        // Get Report-To-Email

        $this->template->content->report_email = Kohana::config('settings.site_email');

        // Get SMS Numbers

        $phone_array = array();

        $sms_no1 = Kohana::config('settings.sms_no1');

        $sms_no2 = Kohana::config('settings.sms_no2');

        $sms_no3 = Kohana::config('settings.sms_no3');

        if (!empty($sms_no1)) {

            $phone_array[] = $sms_no1;

        }

        if (!empty($sms_no2)) {

            $phone_array[] = $sms_no2;

        }

        if (!empty($sms_no3)) {

            $phone_array[] = $sms_no3;

        }

        $this->template->content->phone_array = $phone_array;

        // Get external apps

        $external_apps = array();

        $external_apps = ORM::factory('externalapp')->find_all();

        $this->template->content->external_apps = $external_apps;

        // Get The START, END and Incident Dates

        $startDate = "";

        $endDate = "";

        $display_startDate = 0;

        $display_endDate = 0;

        $db = new Database();

        // Next, Get the Range of Years

        $query = $db->query('SELECT DATE_FORMAT(incident_date, \'%Y-%c\') AS dates FROM ' . $this->table_prefix . 'incident WHERE incident_active = 1 GROUP BY DATE_FORMAT(incident_date, \'%Y-%c\') ORDER BY incident_date');

        $first_year = date('Y');

        $last_year = date('Y');

        $first_month = 1;

        $last_month = 12;

        $i = 0;

        foreach ($query as $data) {

            $date = explode('-', $data->dates);

            $year = $date[0];

            $month = $date[1];

            // Set first year

            if ($i == 0) {

                $first_year = $year;

                $first_month = $month;

            }

            // Set last dates

            $last_year = $year;

            $last_month = $month;

            $i++;

        }

        $show_year = $first_year;

        $selected_start_flag = TRUE;

        while ($show_year <= $last_year) {

            $startDate .= "<optgroup label=\"" . $show_year . "\">";

            $s_m = 1;

            if ($show_year == $first_year) {

                // If we are showing the first year, the starting month may not be January

                $s_m = $first_month;

            }

            $l_m = 12;

            if ($show_year == $last_year) {

                // If we are showing the last year, the ending month may not be December

                $l_m = $last_month;

            }

            for ($i = $s_m; $i <= $l_m; $i++) {

                if ($i < 10) {

                    // All months need to be two digits

                    $i = "0" . $i;

                }

                $startDate .= "<option value=\"" . strtotime($show_year . "-" . $i . "-01") . "\"";

                if ($selected_start_flag == TRUE) {

                    $display_startDate = strtotime($show_year . "-" . $i . "-01");

                    $startDate .= " selected=\"selected\" ";

                    $selected_start_flag = FALSE;

                }

                $startDate .= ">" . date('M', mktime(0, 0, 0, $i, 1)) . " " . $show_year . "</option>";

            }

            $startDate .= "</optgroup>";

            $endDate .= "<optgroup label=\"" . $show_year . "\">";

            for ($i = $s_m; $i <= $l_m; $i++) {

                if ($i < 10) {

                    // All months need to be two digits

                    $i = "0" . $i;

                }

                $endDate .= "<option value=\"" . strtotime($show_year . "-" . $i . "-" . date('t', mktime(0, 0, 0, $i, 1)) . " 23:59:59") . "\"";

                if ($i == $l_m AND $show_year == $last_year) {

                    $display_endDate = strtotime($show_year . "-" . $i . "-" . date('t', mktime(0, 0, 0, $i, 1)) . " 23:59:59");

                    $endDate .= " selected=\"selected\" ";

                }

                $endDate .= ">" . date('M', mktime(0, 0, 0, $i, 1)) . " " . $show_year . "</option>";

            }

            $endDate .= "</optgroup>";

            // Show next year

            $show_year++;

        }

        Event::run('ushahidi_filter.active_startDate', $display_startDate);

        Event::run('ushahidi_filter.active_endDate', $display_endDate);

        Event::run('ushahidi_filter.startDate', $startDate);

        Event::run('ushahidi_filter.endDate', $endDate);

        $this->template->content->div_timeline->startDate = $startDate;

        $this->template->content->div_timeline->endDate = $endDate;

        // Javascript Header

        $this->themes->map_enabled = TRUE;

        $this->themes->main_page = TRUE;

        // Map Settings

        $clustering = Kohana::config('settings.allow_clustering');

        $marker_radius = Kohana::config('map.marker_radius');

        $marker_opacity = Kohana::config('map.marker_opacity');

        $marker_stroke_width = Kohana::config('map.marker_stroke_width');

        $marker_stroke_opacity = Kohana::config('map.marker_stroke_opacity');

        // pdestefanis - allows to restrict the number of zoomlevels available

        $numZoomLevels = Kohana::config('map.numZoomLevels');

        $minZoomLevel = Kohana::config('map.minZoomLevel');

        $maxZoomLevel = Kohana::config('map.maxZoomLevel');

        // pdestefanis - allows to limit the extents of the map

        $lonFrom = Kohana::config('map.lonFrom');

        $latFrom = Kohana::config('map.latFrom');

        $lonTo = Kohana::config('map.lonTo');

        $latTo = Kohana::config('map.latTo');

        $this->themes->js = new View('main_js');

        $this->themes->js->json_url = ($clustering == 1) ? "json/cluster" : "json";

        $this->themes->js->marker_radius = ($marker_radius >= 1 && $marker_radius <= 10) ? $marker_radius : 5;

        $this->themes->js->marker_opacity = ($marker_opacity >= 1 && $marker_opacity <= 10) ? $marker_opacity * 0.1 : 0.9;

        $this->themes->js->marker_stroke_width = ($marker_stroke_width >= 1 && $marker_stroke_width <= 5) ? $marker_stroke_width : 2;

        $this->themes->js->marker_stroke_opacity = ($marker_stroke_opacity >= 1 && $marker_stroke_opacity <= 10) ? $marker_stroke_opacity * 0.1 : 0.9;

        // pdestefanis - allows to restrict the number of zoomlevels available

        $this->themes->js->numZoomLevels = $numZoomLevels;

        $this->themes->js->minZoomLevel = $minZoomLevel;

        $this->themes->js->maxZoomLevel = $maxZoomLevel;

        // pdestefanis - allows to limit the extents of the map

        $this->themes->js->lonFrom = $lonFrom;

        $this->themes->js->latFrom = $latFrom;

        $this->themes->js->lonTo = $lonTo;

        $this->themes->js->latTo = $latTo;

        $this->themes->js->default_map = Kohana::config('settings.default_map');

        $this->themes->js->default_zoom = $this->session->get('map_zoom');
        //Kohana::config('settings.default_zoom');

        $this->themes->js->radius = $this->session->get('radius');

        $this->themes->js->context = $context;

        $this->themes->js->category_id = $category_id;

        $this->themes->js->latitude = $this->session->get('map_center_lat', Kohana::config('settings.default_lat'));

        $this->themes->js->longitude = $this->session->get('map_center_lng', Kohana::config('settings.default_lon'));

        $this->themes->js->default_map_all = Kohana::config('settings.default_map_all');

        $this->themes->js->active_startDate = $display_startDate;

        $this->themes->js->active_endDate = $display_endDate;

        $this->themes->js->blocks_per_row = Kohana::config('settings.blocks_per_row');

        //$myPacker = new javascriptpacker($js , 'Normal', false, false);

        //$js = $myPacker->pack();

        // Build Header and Footer Blocks
        $this->template->header->header_block = $this->themes->header_block();
        $this->template->footer->footer_block = $this->themes->footer_block();
        $this->template->content->bind('this_page', $this->template->header->this_page);
    }

} // End Main
