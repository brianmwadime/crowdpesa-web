<?php defined('SYSPATH') or die('No direct script access.');
/**
 * This controller handles frontend offer functionality
 *
 * @version 01 - Hezron Obuchele 2012-05-25
 *
 * PHP version 5
 * LICENSE: Not for reuse or modification without the express 
 * written authorization from BeeBuy Investments Ltd.
 *
 * Offer_Controller
 * @author     Hezron Obuchele
 * @package    CrowdPesa
 * @subpackage Controllers
 * @copyright  BeeBuy Investments Ltd. - http://www.beebuy.biz
 */

class Offer_Controller extends Main_Controller{
	
	function __construct()
	{
		parent::__construct();
        
        $this->themes->validator_enabled = TRUE;
	}
    
    private function _generate_customer_token () {
        $chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";  
        $length = 5;
        $str = substr(str_shuffle($chars), 0, $length);
        return $str;
    }
    
    public function coupon($incident_id){
        header("Content-type: image/png");
        //$orange = imagecolorallocate($im, 220, 210, 60);
        $offer = ORM::factory('offerincident', $incident_id);
        if ($offer->loaded){
            $im     = imagecreatefrompng(MEDIAPATH."coupon_bg.png");
            $white = imagecolorallocate($im, 0xFF, 0xFF, 0xFF);
            $px     = (imagesx($im) - 7.5 * strlen($offer->offer_coupon)) / 2;
            $font = imageloadfont(MEDIAPATH.'trebuchetms.gdf');
            imagestring($im, $font, $px-14, 6, $offer->offer_coupon, $white);
            imagepng($im);
            imagedestroy($im);
        }
    }
    
    public function token($incident_id){
        header("Content-type: image/png");
        $string = Session::instance()->get('offer_' . $incident_id . '_token');
        $im     = imagecreatefrompng(MEDIAPATH."coupon_bg.png");
        $orange = imagecolorallocate($im, 0xFF, 0xFF, 0xFF);
        $px     = (imagesx($im) - 7.5 * strlen($string)) / 2;
        $font = imageloadfont(MEDIAPATH.'trebuchetms.gdf');
        imagestring($im, $font, $px-14, 6, $string, $orange);
        imagepng($im);
        imagedestroy($im);
    }
    
    public function collect($incident_id){
        if(!(intval($incident_id) > 0)) url::redirect($_SERVER['HTTP_REFERER']);
        
        if(!Auth::instance()->logged_in()) url::redirect('login');
        $user = Auth::instance()->get_user();
        $user_id = $user->id;
        
        $offer = ORM::factory('offerincident', $incident_id);
        if ($offer->max_coupons !== 0){
            $order = new Order_Model();
            $order->customer_id = $user_id;
            $order->offerincident_id = $incident_id;
            $order->date = date('Y-m-d H:i:s');
            $order->total_price = $offer->offerincident_price;
            $order->customer_token = $this->_generate_customer_token();
            if($order->save() && $offer->max_coupons !== null){
                $offer->max_coupons = $offer->max_coupons - 1;
                $offer->save();
            }
            Session::instance()->set('offer_' . $incident_id . '_token', $order->customer_token);
        }else{
            // someone already beat you to the last coupon
        }
        url::redirect($_SERVER['HTTP_REFERER']);
    }
	
	/**
	 * Show offer details
	 */
	public function details($incident_id)
	{
		//$this->template->header->this_page = 'offerreports';
        
		// Load Akismet API Key (Spam Blocker)
		$api_akismet = Kohana::config('settings.api_akismet');
		
		// Sanitize the listing's incident id before proceeding
		$incident_id = intval($incident_id);
        
        $customer = Auth::instance()->get_user();
        $customer_id = 0;
        if ($customer)
            $customer_id = $customer->id;
		
		if ($incident_id > 0 AND Offerincident_Model::is_valid_offerincident($incident_id,FALSE))
		{
			$content = View::factory('offer_details');
			$incident = ORM::factory('offerincident', $incident_id);
			
            $order = Database::instance()->query("SELECT customer_token FROM orders WHERE offerincident_id = $incident_id AND customer_id = $customer_id");
            if (count($order))
                Session::instance()->set('offer_' . $incident_id . '_token', $order[0]->customer_token);
            //$db = Database::instance();
            //$result = $db->query("SELECT * FROM offerincident WHERE offerincident.id = $incident_id");
            
			//if (count($result) == 0)
			if (!$incident->loaded || ($incident->loaded && $incident->offerincident_active == 0))
				url::redirect(url::site());
			//$incident = $result[0];
			// Setup and initialize comment form field names
			$form = array(
					'comment_author' => '',
					'comment_description' => '',
					'comment_email' => '',
					'comment_ip' => '',
					'captcha' => '',
					'comment_rating' => 0,
					'comment_rating_scale' => array('No star','1 star','2 stars','3 stars','4 stars' ,'5 stars')
				);
	
			$captcha = Captcha::factory();
			$errors = $form;
			$form_error = FALSE;
			
			// Check, has the form been submitted, if so, setup validation

			if ($_POST AND Kohana::config('settings.allow_comments') )
			{
				// Instantiate Validation, use $post, so we don't overwrite $_POST fields with our own things

				$post = Validation::factory($_POST);

				// Add some filters

				$post->pre_filter('trim', TRUE);

				// Add some rules, the input field, followed by a list of checks, carried out in order

				if (!$this->user)
				{
					$post->add_rules('comment_author', 'required', 'length[3,100]');
					$post->add_rules('comment_email', 'required','email', 'length[4,100]');
				}
				$post->add_rules('comment_description', 'required');
				$post->add_rules('captcha', 'required', 'Captcha::valid');

				// Test to see if things passed the rule checks

				if ($post->validate())
				{
					// Yes! everything is valid

					if ($api_akismet != "")
					{
						// Run Akismet Spam Checker
						$akismet = new Akismet();

						// Comment data
						$comment = array(
							'website' => "",
							'body' => $post->comment_description,
							'user_ip' => $_SERVER['REMOTE_ADDR']
						);

						if ($this->user)
						{
							$comment['author'] = $this->user->name;
							$comment['email'] = $this->user->email;
						}
						else
						{
							$comment['author'] = $post->comment_author;
							$comment['email'] = $post->comment_email;
						}

						$config = array(
							'blog_url' => url::site(),
							'api_key' => $api_akismet,
							'comment' => $comment
						);

						$akismet->init($config);

						if ($akismet->errors_exist())
						{
							if ($akismet->is_error('AKISMET_INVALID_KEY'))
							{
								// throw new Kohana_Exception('akismet.api_key');
							}
							elseif ($akismet->is_error('AKISMET_RESPONSE_FAILED'))
							{
								// throw new Kohana_Exception('akismet.server_failed');
							}
							elseif ($akismet->is_error('AKISMET_SERVER_NOT_FOUND'))
							{
								// throw new Kohana_Exception('akismet.server_not_found');
							}

							// If the server is down, we have to post
							// the comment :(
							// $this->_post_comment($comment);

							$comment_spam = 0;
						}
						else
						{
							$comment_spam = ($akismet->is_spam()) ? 1 : 0;
						}
					}
					else
					{
						// No API Key!!
						$comment_spam = 0;
					}

					$comment = new Comment_Model();
					$comment->offerincident_id = $incident_id;
					$rating = new Rating_Model();
					$rating->offerincident_id = $incident_id;
					
					if ($this->user)
					{
						$comment->user_id = $this->user->id;
						$comment->comment_author = $this->user->name;
						$comment->comment_email = $this->user->email;
						$rating->user_id = $this->user->id;
					}
					else
					{
						$comment->comment_author = strip_tags($post->comment_author);
						$comment->comment_email = strip_tags($post->comment_email);
					}
					
					$comment->comment_description = strip_tags($post->comment_description);
					$comment->comment_ip = $_SERVER['REMOTE_ADDR'];
					$comment->comment_date = date("Y-m-d H:i:s",time());
					$rating->rating_date = $comment->comment_date;
					$rating->rating = strip_tags($post->comment_rating);
					$rating->rating_ip = $_SERVER['REMOTE_ADDR'];

					// Activate comment for now
					if ($comment_spam == 1)
					{
						$comment->comment_spam = 1;
						$comment->comment_active = 0;
					}
					else
					{
						$comment->comment_spam = 0;
						$comment->comment_active = (Kohana::config('settings.allow_comments') == 1)? 1 : 0;
					}
					$comment->save();
					$rating->comment_id = $comment->id;
					$rating->save();

					// Event::comment_add - Added a New Comment
					Event::run('ushahidi_action.comment_add', $comment);

					// Notify Admin Of New Comment
					try{
					   $send = notifications::notify_admins(
    						"[".Kohana::config('settings.site_name')."] ".
    							Kohana::lang('notifications.admin_new_comment.subject'),
    							Kohana::lang('notifications.admin_new_comment.message')
    							."\n\n'".strtoupper($incident->offerincident_title)."'"
    							."\n".url::base().'offer/details/'.$incident_id
    						);
                    }catch (Exception $e){
                        // Email sending error
                    }
					// Redirect
					url::redirect('offer/details/'.$incident_id);

				}
				else
				{
					// No! We have validation errors, we need to show the form again, with the errors
					// Repopulate the form fields
					$form = arr::overwrite($form, $post->as_array());

					// Populate the error fields, if any
					$errors = arr::overwrite($errors, $post->errors('comments'));
					$form_error = TRUE;
				}
			}
			
			$content->incident = $incident;
            //$content->url = 'offer': '';
			$content->incident_user = User_Model::get_user_by_id($incident->user_id);
            $content->user_subscribed = Offersubscription_Model::is_retailer_subscribed($customer_id, $incident->user_id);
            $content->tab = (isset($_GET['t']) && $_GET['t'] != '') ? $_GET['t'] : 'rr';
            $content->tab_count = array('rr' => 0,  'so' => 0, 'no' => 0);
            
            $content->coupon = $incident->offer_coupon;
            if ($incident->scope == 1)
			     $content->token = $this->_generate_customer_token();
			$this->template->header->page_title .= $incident->offerincident_title . ' - Offer ' . Kohana::config('settings.title_delimiter');
			$this->template->header->header_block = $this->themes->header_block();
            
            if (Auth::instance()->logged_in()){
                $this->template->header->new_offers = Offerincident_Model::get_new_offers_count();
            }

			$this->template->content = $content;
			$this->template->footer->footer_block = $this->themes->footer_block();
			
			// Get average offer rating
			$db = Database::instance();
			$average_rating = $db->query("SELECT AVG(rating) 'ave' FROM rating WHERE offerincident_id = $incident_id AND comment_id IS NOT NULL");
			$this->template->content->incident_rating = round($average_rating[0]->ave);
            
            // Get average brand rating
            $user_id = $incident->user_id;
            $average_rating = $db->query("SELECT AVG(rating) 'ave' FROM rating WHERE incident_id IN (SELECT id FROM incident WHERE user_id = $user_id) AND comment_id IS NOT NULL");
            $this->template->content->brand_rating = round($average_rating[0]->ave);
            
            // Add offers with similar tags
            $db = Database::instance();
            $similar = $db->query("SELECT offerincident.id, offerincident_end_date end_date, offerincident_title title, AVG(rating) rating, offerincident_description description, offerincident_offercategory.offercategory_id category FROM offerincident_offercategory INNER JOIN offerincident ON offerincident.id = offerincident_offercategory.offerincident_id LEFT JOIN rating ON rating.offerincident_id = offerincident.id WHERE offerincident.id != $incident_id AND offerincident_start_date < NOW() AND offerincident_end_date > NOW() AND offerincident_active = 1 AND offercategory_id IN (SELECT offerincident_offercategory.offercategory_id FROM offerincident_offercategory WHERE offerincident_offercategory.offerincident_id = $incident_id) GROUP BY offerincident.id LIMIT 0, 10");
            $this->template->content->similar_incidents = $similar;
            $this->template->content->tab_count['so'] = count($similar);
            
            // Get the 10 nearest offers within a radius of 20km
            $max_radius = 20;
            $lat = Session::instance()->get('map_center_lat');
            $lng = Session::instance()->get('map_center_lng');
            $neighbours = $db->query("SELECT offerincident.id, offerincident_end_date end_date, offerincident_title as title,  offerincident_description as description, AVG(rating) rating, location_name as location, ( 6371 * acos( cos( radians( $lat ) ) * cos( radians( latitude ) ) * cos( radians( longitude ) - radians( $lng ) ) + sin( radians( $lat ) ) * sin( radians( latitude ) ) ) ) AS distance FROM offerincident  
INNER JOIN offer_incidents oi ON oi.offerincident_id = offerincident.id
INNER JOIN incident ON incident.id = oi.incident_id 
INNER JOIN location ON incident.location_id = location.id LEFT JOIN rating ON rating.offerincident_id = offerincident.id WHERE offerincident.id != $incident_id AND offerincident_start_date < NOW() AND offerincident_end_date > NOW() AND offerincident_active = 1 AND longitude BETWEEN ( $lng -20 / abs( cos( radians( $lat ) ) *69 ) ) AND ( $lng +20 / abs( cos( radians( $lat ) ) *69 ) ) AND latitude BETWEEN ( $lat - ( $max_radius / 69 ) ) AND ( $lat + ( $max_radius / 69 ) ) GROUP BY offerincident.id HAVING distance < $max_radius ORDER BY distance ASC LIMIT 0, 10");
            $this->template->content->nearby_offers = $neighbours;
            //print_r($neighbours);
            $this->template->content->tab_count['no'] = count($neighbours);
			
			// TODO: Retrieve Media
            /** $incident_photo = array();

            foreach($incident->media as $media)
            {
                if ($media->media_type == 1)
                {
                    $incident_photo[] = array(
                                            'large' => url::convert_uploaded_to_abs($media->media_link),
                                            'thumb' => url::convert_uploaded_to_abs($media->media_thumb)
                                            );
                }
            }*/
            
			// Retrieve Comments
			$this->template->content->comments = "";
			if (Kohana::config('settings.allow_comments'))
			{
				$this->template->content->comments = new View('listing_comments');
				$incident_comments = array();
				if ($incident_id)
				{
					$incident_comments = Offerincident_Model::get_comments($incident_id);
				}
				$this->template->content->comments->incident_comments = $incident_comments;
                $this->template->content->tab_count['rr'] = count($incident_comments);
			}
		
		}
		else
		{
			url::redirect(url::site());
		}
		
        // Get marker
        $icon = (Kohana::config('settings.default_marker_all')) ? Kohana::config('settings.default_marker_all') : 'default_marker.png';
        $icon_path = Kohana::config('upload.directory') . '/' . $icon;
        $icon_size = getimagesize($icon_path);
        $icon_width = $icon_size[0];
        $icon_height = $icon_size[1];
        
		// Javascript Header
        $this->themes->map_enabled = TRUE;
        $this->themes->js = new View('listing_view_js');
        $this->themes->js->controller_prefix = 'offer';
        $this->themes->js->main_icon = $icon;
        $this->themes->js->main_icon_width = $icon_width;
        $this->themes->js->main_icon_height = $icon_height;
        //$this->themes->js->other_icons = 'offer_featured.png';
        $this->themes->js->incident_id = $incident->id;
        $this->themes->js->default_map = Kohana::config('settings.default_map');
        $this->themes->js->default_zoom = Kohana::config('settings.default_zoom');// TODO: Get incident zoom field set by retailer
        $this->themes->js->latitude = Kohana::config('settings.default_lat');//$incident->location->latitude;
        $this->themes->js->longitude = Kohana::config('settings.default_lon');//$incident->location->longitude;
        $this->themes->js->incident_zoom = $incident->offerincident_zoom;
		
		// Are we allowed to submit comments?
		$this->template->content->comments_form = "";
		if (Kohana::config('settings.allow_comments'))
		{
			$this->template->content->comments_form = new View('listing_comments_form');
			$this->template->content->comments_form->user = $this->user;
			$this->template->content->comments_form->form = $form;
			$this->template->content->comments_form->captcha = $captcha;
			$this->template->content->comments_form->errors = $errors;
			$this->template->content->comments_form->form_error = $form_error;
		}
        
        // Rebuild Header Block
        $this->template->header->header_block = $this->themes->header_block();
        $this->template->footer->footer_block = $this->themes->footer_block();
	}
}