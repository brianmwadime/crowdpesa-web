<?php defined('SYSPATH') or die('No direct script access.');
/**
 * OFFERJ2ME Controller
 * Handles the tasks the J2ME app can't handle by itself or
 * through the API like GeoCoding
 *
 * PHP version 5
 * LICENSE: This source file is subject to LGPL license 
 * that is available through the world-wide-web at the following URI:
 * http://www.gnu.org/copyleft/lesser.html
 * @author     BeeBuy Team <info@beebuy.com> 
 * @package    CrowdPesa - http://crowdpesa.com
 * @subpackage Controllers
 * @copyright  CrowdPesa - http://crowdpesa.com
 * @license    http://www.gnu.org/copyleft/lesser.html GNU Lesser General Public License (LGPL) 
*/

class Offerj2me_Controller extends Controller
{
	public function __construct()
    {
        parent::__construct();
		//$profile = new Profiler;
	}
	
	public function index()
	{
		$offerincident_title = "";
		$offerincident_description = "";
		$offerincident_date = "";
		$offerincident_hour = "";
		$offerincident_minute = "";
		$offerincident_ampm = "";
		$offerincident_offercategory = 0;
		$latitude = "";
		$longitude = "";
		$location_name = "";
		$person_first = "";
		$person_last = "";
		$offerincident_photo = "";

		if (isset($_POST['offerincident_title']))
			$offerincident_title = $_POST['offerincident_title'];
		if (isset($_POST['offerincident_description']))
			$offerincident_description = $_POST['offerincident_description'];
		if (isset($_POST['offerincident_date']))
			$offerincident_date = $_POST['offerincident_date'];
		if (isset($_POST['offerincident_hour']))
			$offerincident_hour = $_POST['offerincident_hour'];
		if (isset($_POST['offerincident_minute']))
			$offerincident_minute = $_POST['offerincident_minute'];
		if (isset($_POST['offerincident_ampm']))
			$offerincident_ampm = $_POST['offerincident_ampm'];
		if (isset($_POST['offerincident_offercategory']))
			$offerincident_offercategory = $_POST['offerincident_offercategory'];
		if (isset($_POST['latitude']))
			$latitude = $_POST['latitude'];
		if (isset($_POST['longitude']))
			$longitude = $_POST['longitude'];
		if (isset($_POST['location_name']))
			$location_name = $_POST['location_name'];
		if (isset($_POST['person_first']))
			$person_first = $_POST['person_first'];
		if (isset($_POST['person_last']))
			$person_last = $_POST['person_last'];
		
		// Upload Temporary Image If Any
		if(isset($_FILES['image_name'])
			AND isset($_FILES['image_name']['error'])
			AND isset($_FILES['image_name']['name'])
			AND isset($_FILES['image_name']['type'])
			AND isset($_FILES['image_name']['tmp_name'])
			AND isset($_FILES['image_name']['size'])
			AND is_uploaded_file($_FILES['image_name']['tmp_name'])
			AND (int) $_FILES['image_name']['error'] === UPLOAD_ERR_OK
			)
		{
			$offerincident_photo = upload::save('image_name');
			$offerincident_photo = "@".$offerincident_photo;
		}
		

		// On the fly GeoCoding with Google Maps
		$key = Kohana::config('settings.api_google');
		
		$url = "https://maps.google.com/maps/geo?q=".urlencode($location_name)."&output=csv&key=".$key;

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_HEADER,0);
		curl_setopt($ch, CURLOPT_USERAGENT, $_SERVER["HTTP_USER_AGENT"]);
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

		$data = curl_exec($ch);
		curl_close($ch);

		// Check our Response code to ensure success
		if (substr($data,0,3) == "200")
		{
			$data = explode(",",$data);

			$accuracy = $data[1]; // [0-9], 9=Most Detail
			$latitude = $data[2];
			$longitude = $data[3];
			
			$offerincident_offercategory = serialize( array(intval($offerincident_offercategory)) );

			$mobile_post = array(
			  'task' =>'offerreport',
			  'offerincident_title' => $offerincident_title,
			  'offerincident_description' => $offerincident_description, 
			  'offerincident_date' => date("m/d/Y"), 
			  'offerincident_hour' => $offerincident_hour, 
			  'offerincident_minute' => $offerincident_minute,
			  'offerincident_ampm' => $offerincident_ampm,
			  'offerincident_offercategory' => $offerincident_offercategory,
			  'latitude' => $latitude,
			  'longitude' => $longitude, 
			  'location_name' => $location_name
			);
			if ($offerincident_photo != "") 
			{
				$mobile_post['offerincident_photo[]'] = $offerincident_photo;
			}
			
			//print_r($mobile_post);

			$ch = curl_init();
			curl_setopt($ch, CURLOPT_HEADER,1);
	   		curl_setopt($ch, CURLOPT_URL, url::site().'api' );
	   		curl_setopt($ch, CURLOPT_POST, 1 );
			curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1 );
	   		curl_setopt($ch, CURLOPT_POSTFIELDS, $mobile_post);
	   		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	   		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
	   		$postResult = curl_exec($ch);

	   		if (curl_errno($ch))
			{
	       		print curl_error($ch);
	       		print Kohana::lang('ui_admin.error_post_offerincident');
	   		}
			else
			{
				echo $postResult;
			}
	   		curl_close($ch);
		}
		else
		{
			echo Kohana::lang('ui_admin.error_geocoding').' '.substr($data,0,3);
		}
	}

}
