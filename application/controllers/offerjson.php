<?php defined('SYSPATH') or die('No direct script access.');
/**
 * Offerjson Controller
 * Generates Map GeoJSON File
 *
 * PHP version 5
 * LICENSE: This source file is subject to LGPL license
 * that is available through the world-wide-web at the following URI:
 * http://www.gnu.org/copyleft/lesser.html
 * @author	   BeeBuy Team <info@beebuy.com>
 * @package	   CrowdPesa - http://crowdpesa.com
 * @subpackage Controllers
 * @copyright  CrowdPesa - http://crowdpesa.com
 * @license	   http://www.gnu.org/copyleft/lesser.html GNU Lesser General Public License (LGPL)
 */

class Offerjson_Controller extends Template_Controller
{
	/**
	 * Automatically render the views
	 * @var bool
	 */
	public $auto_render = TRUE;

	/**
	 * Name of the view template for this controller
	 * @var string
	 */
	public $template = 'offerjson';

	/**
	 * Database table prefix
	 * @var string
	 */
	protected $table_prefix;

	// Geometry data
	private static $geometry_data = array();

	public function __construct()
	{
		parent::__construct();

		// Set Table Prefix
		$this->table_prefix = Kohana::config('database.default.table_prefix');

		// Cacheable JSON Controller
		$this->is_cachable = TRUE;
	}


	/**
	 * Generate JSON in NON-CLUSTER mode
	 */
	public function index()
	{
		$offerjson = "";
		$offerjson_item = "";
		$offerjson_array = array();
		$color = Kohana::config('settings.default_map_all');
		$icon = "";

		$media_type = (isset($_GET['m']) AND intval($_GET['m']) > 0)? intval($_GET['m']) : 0;
		
		// Get the offer and offer-category id
		$offercategory_id = (isset($_GET['c']) AND intval($_GET['c']) > 0)? intval($_GET['c']) : 0;
		$offerincident_id = (isset($_GET['i']) AND intval($_GET['i']) > 0)? intval($_GET['i']) : 0;
		
		// Get the offercategory colour
		if (Offercategory_Model::is_valid_offercategory($offercategory_id))
		{
			$color = ORM::factory('offercategory', $offercategory_id)->offercategory_color;
		}
		
		// Fetch the offers
		$markers = (isset($_GET['page']) AND intval($_GET['page']) > 0)? offerreports::fetch_offerincidents(TRUE) : offerreports::fetch_offerincidents();
		
		// Variable to store individual item for offer detail page
		$offerjson_item_first = "";	
		foreach ($markers as $marker)
		{
			$thumb = "";
			if ($media_type == 1)
			{
				$media = ORM::factory('offerincident', $marker->offerincident_id)->media;
				if ($media->count())
				{
					foreach ($media as $photo)
					{
						if ($photo->media_thumb)
						{ 
							// Get the first thumb
							$prefix = url::base().Kohana::config('upload.relative_directory');
							$thumb = $prefix."/".$photo->media_thumb;
							break;
						}
					}
				}
			}
			
			$offerjson_item = "{";
			$offerjson_item .= "\"type\":\"Feature\",";
			$offerjson_item .= "\"properties\": {";
			$offerjson_item .= "\"id\": \"".$marker->offerincident_id."\", \n";

			$encoded_title = utf8tohtml::convert($marker->offerincident_title, TRUE);
			$encoded_title = str_ireplace('"','&#34;',$encoded_title);
			$encoded_title = json_encode($encoded_title);
			$encoded_title = str_ireplace('"', '', $encoded_title);

			$offerjson_item .= "\"name\":\"" . str_replace(chr(10), ' ', str_replace(chr(13), ' ', "<a "
					. "href='".url::base()."offer/details/".$marker->offerincident_id."'>".$encoded_title)."</a>") . "\","
					. "\"link\": \"".url::base()."offer/details/".$marker->offerincident_id."\", ";

			$offerjson_item .= (isset($offercategory))
				? "\"offercategory\":[" . $offercategory_id . "], "
				: "\"offercategory\":[0], ";

			$offerjson_item .= "\"color\": \"".$color."\", \n";
			$offerjson_item .= "\"icon\": \"".$icon."\", \n";
			$offerjson_item .= "\"thumb\": \"".$thumb."\", \n";
			$offerjson_item .= "\"timestamp\": \"" . strtotime($marker->offerincident_date) . "\"";
			$offerjson_item .= "},";
			$offerjson_item .= "\"geometry\": {";
			$offerjson_item .= "\"type\":\"Point\", ";
			$offerjson_item .= "\"coordinates\":[" . $marker->longitude . ", " . $marker->latitude . "]";
			$offerjson_item .= "}";
			$offerjson_item .= "}";

			if ($marker->offerincident_id == $offerincident_id)
			{
				$offerjson_item_first = $offerjson_item;
			}
			else
			{
				array_push($offerjson_array, $offerjson_item);
			}
			
			// Get Offer Geometries
			$geometry = $this->_get_geometry($marker->offerincident_id, $marker->offerincident_title, $marker->offerincident_date);
			if (count($geometry))
			{
				$offerjson_item = implode(",", $geometry);
				array_push($offerjson_array, $offerjson_item);
			}
		}
		
		if ($offerjson_item_first)
		{
			// Push individual marker in last so that it is layered on top when pulled into map
			array_push($offerjson_array, $offerjson_item_first);
		}
		
		$offerjson = implode(",", $offerjson_array);

		header('Content-type: application/offerjson; charset=utf-8');
		$this->template->offerjson = $offerjson;
	}


	/**
	 * Generate JSON in CLUSTER mode
	 */
	public function cluster()
	{
		// Database
		$db = new Database();

		$offerjson = "";
		$offerjson_item = "";
		$offerjson_array = array();
		$geometry_array = array();

		$color = Kohana::config('settings.default_map_all');
		$icon = "";

		// Get Zoom Level
		$zoomLevel = (isset($_GET['z']) AND !empty($_GET['z'])) ?
			(int) $_GET['z'] : 8;

		//$distance = 60;
		$distance = (10000000 >> $zoomLevel) / 100000;
		
		// Fetch the offers using the specified parameters
		$offerincidents = offerreports::fetch_offerincidents();
		
		// Offercategory ID
		$offercategory_id = (isset($_GET['c']) AND intval($_GET['c']) > 0) ? intval($_GET['c']) : 0;
		
		// Start date
		$start_date = (isset($_GET['s']) AND intval($_GET['s']) > 0) ? intval($_GET['s']) : NULL;
		
		// End date
		$end_date = (isset($_GET['e']) AND intval($_GET['e']) > 0) ? intval($_GET['e']) : NULL;
		
		if (Offercategory_Model::is_valid_offercategory($offercategory_id))
		{
			// Get the color
			$color = ORM::factory('offercategory', $offercategory_id)->offercategory_color;
		}

		// Create markers by marrying the locations and offerincidents
		$markers = array();
		foreach ($offerincidents as $offerincident)
		{
			$markers[] = array(
				'id' => $offerincident->offerincident_id,
				'offerincident_title' => $offerincident->offerincident_title,
				'latitude' => $offerincident->latitude,
				'longitude' => $offerincident->longitude,
				'thumb' => ''
				);
		}

		$clusters = array();	// Clustered
		$singles = array();		// Non Clustered

		// Loop until all markers have been compared
		while (count($markers))
		{
			$marker	 = array_pop($markers);
			$cluster = array();

			// Compare marker against all remaining markers.
			foreach ($markers as $key => $target)
			{
				// This function returns the distance between two markers, at a defined zoom level.
				// $pixels = $this->_pixelDistance($marker['latitude'], $marker['longitude'],
				// $target['latitude'], $target['longitude'], $zoomLevel);

				$pixels = abs($marker['longitude']-$target['longitude']) +
					abs($marker['latitude']-$target['latitude']);
					
				// If two markers are closer than defined distance, remove compareMarker from array and add to cluster.
				if ($pixels < $distance)
				{
					unset($markers[$key]);
					$target['distance'] = $pixels;
					$cluster[] = $target;
				}
			}

			// If a marker was added to cluster, also add the marker we were comparing to.
			if (count($cluster) > 0)
			{
				$cluster[] = $marker;
				$clusters[] = $cluster;
			}
			else
			{
				$singles[] = $marker;
			}
		}

		// Create Offerjson
		foreach ($clusters as $cluster)
		{
			// Calculate cluster center
			$bounds = $this->_calculateCenter($cluster);
			$cluster_center = $bounds['center'];
			$southwest = $bounds['sw'];
			$northeast = $bounds['ne'];

			// Number of Items in Cluster
			$cluster_count = count($cluster);
			
			// Get the time filter
			$time_filter = ( ! empty($start_date) AND ! empty($end_date))
				? "&s=".$start_date."&e=".$end_date
				: "";
			
			// Build out the JSON string
			$offerjson_item = "{";
			$offerjson_item .= "\"type\":\"Feature\",";
			$offerjson_item .= "\"properties\": {";
			$offerjson_item .= "\"name\":\"" . str_replace(chr(10), ' ', str_replace(chr(13), ' ', "<a href=" . url::base()
				 . "offerreports/index/?c=".$offercategory_id."&sw=".$southwest."&ne=".$northeast.$time_filter.">" . $cluster_count . " Offerreports</a>")) . "\",";
			$offerjson_item .= "\"link\": \"".url::base()."offerreports/index/?c=".$offercategory_id."&sw=".$southwest."&ne=".$northeast.$time_filter."\", ";
			$offerjson_item .= "\"offercategory\":[0], ";
			$offerjson_item .= "\"color\": \"".$color."\", ";
			$offerjson_item .= "\"icon\": \"".$icon."\", ";
			$offerjson_item .= "\"thumb\": \"\", ";
			$offerjson_item .= "\"timestamp\": \"0\", ";
			$offerjson_item .= "\"count\": \"" . $cluster_count . "\"";
			$offerjson_item .= "},";
			$offerjson_item .= "\"geometry\": {";
			$offerjson_item .= "\"type\":\"Point\", ";
			$offerjson_item .= "\"coordinates\":[" . $cluster_center . "]";
			$offerjson_item .= "}";
			$offerjson_item .= "}";

			array_push($offerjson_array, $offerjson_item);
		}

		foreach ($singles as $single)
		{
			$offerjson_item = "{";
			$offerjson_item .= "\"type\":\"Feature\",";
			$offerjson_item .= "\"properties\": {";
			$offerjson_item .= "\"name\":\"" . str_replace(chr(10), ' ', str_replace(chr(13), ' ', "<a href=" . url::base()
					. "offer/details/" . $single['id'] . "/>".str_replace('"','\"',$single['offerincident_title'])."</a>")) . "\",";
			$offerjson_item .= "\"link\": \"".url::base()."offer/details/".$single['id']."\", ";
			$offerjson_item .= "\"offercategory\":[0], ";
			$offerjson_item .= "\"color\": \"".$color."\", ";
			$offerjson_item .= "\"icon\": \"".$icon."\", ";
			// $offerjson_item .= "\"thumb\": \"".$single['thumb']."\", ";
			$offerjson_item .= "\"timestamp\": \"0\", ";
			$offerjson_item .= "\"count\": \"" . 1 . "\"";
			$offerjson_item .= "},";
			$offerjson_item .= "\"geometry\": {";
			$offerjson_item .= "\"type\":\"Point\", ";
			$offerjson_item .= "\"coordinates\":[" . $single['longitude'] . ", " . $single['latitude'] . "]";
			$offerjson_item .= "}";
			$offerjson_item .= "}";

			array_push($offerjson_array, $offerjson_item);
		}

		$offerjson = implode(",", $offerjson_array);
		
		// 
		// E.Kala July 27, 2011
		// @todo Parking this geometry business for review
		// 
		
		// if (count($geometry_array))
		// {
		// 	$offerjson = implode(",", $geometry_array).",".$offerjson;
		// }
		
		header('Content-type: application/offerjson; charset=utf-8');
		$this->template->offerjson = $offerjson;
	}
    
    /**
     * Function to fetch markers of all places where a given offer is running
     */
    public function places($offerincident_id = 0)
    {
        $offerjson = "";
        $offerjson_item = "";
        $offerjson_array = array();
        
        $places = offerreports::fetch_offer_locations($offerincident_id);
        foreach ($places as $row)
        {
            /*$offerjson_item = "{";
            $offerjson_item .= "\"type\":\"Feature\",";
            $offerjson_item .= "\"properties\": {";
            $offerjson_item .= "\"id\": \"".$row->id."\", ";
            $offerjson_item .= "\"type\": \"place\", ";
            $encoded_title = utf8tohtml::convert($row->incident_title,TRUE);
            $encoded_title = str_ireplace('"','&#34;',$encoded_title);
            $encoded_title = json_encode($encoded_title);
            $encoded_title = str_ireplace('"','',$encoded_title);

            $offerjson_item .= "\"name\":\"" . str_replace(chr(10), ' ', str_replace(chr(13), ' ', "<a href='" . url::base()
                    . "listing/details/" . $row->id . "'>".$encoded_title."</a>")) . "\",";
            $offerjson_item .= "\"link\": \"".url::base()."listing/details/".$row->id."\", ";
            $offerjson_item .= "\"offercategory\":[0], ";
            $offerjson_item .= "\"timestamp\": \"" . strtotime($row->incident_date) . "\"";
            $offerjson_item .= "},";
            $offerjson_item .= "\"geometry\": {";
            $offerjson_item .= "\"type\":\"Point\", ";
            $offerjson_item .= "\"coordinates\":[" . $row->longitude . ", " . $row->latitude . "]";
            $offerjson_item .= "}";
            $offerjson_item .= "}";*/
            
            $offerjson_item = array();
            $offerjson_item['type'] = 'Feature';
            $offerjson_item['properties'] = array();
            $offerjson_item['properties']['id'] = $row->id;
            $offerjson_item['properties']['type'] = 'place';
            $offerjson_item['properties']['name'] = $row->incident_title;
            $offerjson_item['properties']['link'] = url::base()."listing/details/".$row->id;
            $offerjson_item['properties']['offercategory'] = array(0);
            $offerjson_item['properties']['timestamp'] = strtotime($row->incident_date);
            
            $offerjson_item['geometry'] = array();
            $offerjson_item['geometry']['type'] = 'Point';
            $offerjson_item['geometry']['coordinates'] = array($row->longitude, $row->latitude);

            array_push($offerjson_array, $offerjson_item);
        }
        
        //$offerjson = implode(",", $offerjson_array);
        $json = json_encode(array(
            "type" => "FeatureCollection",
            "features" => $offerjson_array
        ));
        header('Content-type: application/offerjson; charset=utf-8');
        $this->template->offerjson = $json;//$offerjson;
    }

	/**
	 * Retrieve Single Marker
	 */
	public function single($offerincident_id = 0)
	{
		$offerjson = "";
		$offerjson_item = "";
		$offerjson_array = array();

		// Get the neigbouring offers
		$neighbours = Offerincident_Model::get_neighbouring_offerincidents($offerincident_id, FALSE, 20, 100);

		if ($neighbours)
		{
			// Load the offer
			// @todo Get this fixed
			$marker = ORM::factory('offerincident', $offerincident_id);
			
			// Get the offer date
			$offerincident_date = date('Y-m', strtotime($marker->offerincident_date));

			foreach ($neighbours as $row)
			{
				$offerjson_item = "{";
				$offerjson_item .= "\"type\":\"Feature\",";
				$offerjson_item .= "\"properties\": {";
				$offerjson_item .= "\"id\": \"".$row->id."\", ";
                $offerjson_item .= "\"type\": \"offer\", ";
				$encoded_title = utf8tohtml::convert($row->offerincident_title,TRUE);
				$encoded_title = str_ireplace('"','&#34;',$encoded_title);
				$encoded_title = json_encode($encoded_title);
				$encoded_title = str_ireplace('"','',$encoded_title);

				$offerjson_item .= "\"name\":\"" . str_replace(chr(10), ' ', str_replace(chr(13), ' ', "<a href='" . url::base()
						. "offer/details/" . $row->id . "'>".$encoded_title."</a>")) . "\",";
				$offerjson_item .= "\"link\": \"".url::base()."offer/details/".$row->id."\", ";
				$offerjson_item .= "\"offercategory\":[0], ";
				$offerjson_item .= "\"timestamp\": \"" . strtotime($row->offerincident_date) . "\"";
				$offerjson_item .= "},";
				$offerjson_item .= "\"geometry\": {";
				$offerjson_item .= "\"type\":\"Point\", ";
				$offerjson_item .= "\"coordinates\":[" . $row->longitude . ", " . $row->latitude . "]";
				$offerjson_item .= "}";
				$offerjson_item .= "}";

				array_push($offerjson_array, $offerjson_item);
			}
			
			// Single Main Offer
			$offerjson_single = "{";
			$offerjson_single .= "\"type\":\"Feature\",";
			$offerjson_single .= "\"properties\": {";
			$offerjson_single .= "\"id\": \"".$marker->id."\", ";
            $offerjson_single .= "\"type\": \"offer\", ";

			$encoded_title = utf8tohtml::convert($marker->offerincident_title,TRUE);

			$offerjson_single .= "\"name\":\"" . str_replace(chr(10), ' ', str_replace(chr(13), ' ', "<a href='" . url::base()
					. "offer/details/" . $marker->id . "'>".$encoded_title."</a>")) . "\",";
			$offerjson_single .= "\"link\": \"".url::base()."offer/details/".$marker->id."\", ";
			$offerjson_single .= "\"offercategory\":[0], ";
			$offerjson_single .= "\"timestamp\": \"" . strtotime($marker->offerincident_date) . "\"";
			
			// Get Offer Geometries
			$geometry = $this->_get_geometry($marker->id, $marker->offerincident_title, $marker->offerincident_date);
			
			// If there are no geometries, use Single Offer Marker
			if ( ! count($geometry))
			{
				$offerjson_item = "{";
				$offerjson_item .= "\"type\":\"Feature\",";
				$offerjson_item .= "\"properties\": {";
				$offerjson_item .= "\"id\": \"".$marker->id."\", ";
                $offerjson_item .= "\"type\": \"offer\", ";
                
				$encoded_title = utf8tohtml::convert($marker->offerincident_title,TRUE);

				$offerjson_item .= "\"name\":\"" . str_replace(chr(10), ' ', str_replace(chr(13), ' ', "<a href='" . url::base()
					. "offer/details/" . $marker->id . "'>".$encoded_title."</a>")) . "\",";
				$offerjson_item .= "\"link\": \"".url::base()."offer/details/".$marker->id."\", ";
				$offerjson_item .= "\"offercategory\":[0], ";
				$offerjson_item .= "\"timestamp\": \"" . strtotime($marker->offerincident_date) . "\"";
				$offerjson_item .= "},\"geometry\":";
				$offerjson_item .= "{\"type\":\"Point\", ";
				$offerjson_item .= "\"coordinates\":[" . $marker->location->longitude . ", " . $marker->location->latitude . "]";
				$offerjson_item .= "}";
				$offerjson_item .= "}";
			}
			else
			{
				$offerjson_item = implode(",", $geometry);
			}

			array_push($offerjson_array, $offerjson_item);
		}


		$offerjson = implode(",", $offerjson_array);
		
		header('Content-type: application/offerjson; charset=utf-8');
		$this->template->offerjson = $offerjson;
	}

	/**
	 * Retrieve timeline JSON
	 */
	public function timeline( $offercategory_id = 0 )
	{
		$offercategory_id = (int) $offercategory_id;

		$this->auto_render = FALSE;
		$db = new Database();

		$interval = (isset($_GET["i"]) AND !empty($_GET["i"])) ?
			$_GET["i"] : "month";

		// Get Offer-category Info
		if ($offercategory_id > 0)
		{
			$offercategory = ORM::factory("offercategory", $offercategory_id);
			if ($offercategory->loaded)
			{
				$offercategory_title = $offercategory->offercategory_title;
				$offercategory_color = "#".$offercategory->offercategory_color;
			}
			else
			{
				break;
			}
		}
		else
		{
			$offercategory_title = "All Offercategories";
			$offercategory_color = "#990000";
		}

		// Get the Counts
		$select_date_text = "DATE_FORMAT(offerincident_date, '%Y-%m-01')";
		$groupby_date_text = "DATE_FORMAT(offerincident_date, '%Y%m')";
		if ($interval == 'day')
		{
			$select_date_text = "DATE_FORMAT(offerincident_date, '%Y-%m-%d')";
			$groupby_date_text = "DATE_FORMAT(offerincident_date, '%Y%m%d')";
		}
		elseif ($interval == 'hour')
		{
			$select_date_text = "DATE_FORMAT(offerincident_date, '%Y-%m-%d %H:%M')";
			$groupby_date_text = "DATE_FORMAT(offerincident_date, '%Y%m%d%H')";
		}
		elseif ($interval == 'week')
		{
			$select_date_text = "STR_TO_DATE(CONCAT(CAST(YEARWEEK(offerincident_date) AS CHAR), ' Sunday'), '%X%V %W')";
			$groupby_date_text = "YEARWEEK(offerincident_date)";
		}

		$graph_data = array();
		$graph_data[0] = array();
		$graph_data[0]['label'] = $offercategory_title;
		$graph_data[0]['color'] = $offercategory_color;
		$graph_data[0]['data'] = array();

		// Gather allowed ids if we are looking at a specific offer-category

		$allowed_ids = array();
		if($offercategory_id != 0)
		{
			$query = 'SELECT ic.offerincident_id AS offerincident_id FROM '.$this->table_prefix.'offerincident_offercategory AS ic INNER JOIN '.$this->table_prefix.'offercategory AS c ON (ic.offercategory_id = c.id)  WHERE c.id='.$offercategory_id.' OR c.parent_id='.$offercategory_id.';';
			$query = $db->query($query);

			foreach ( $query as $items )
			{
				$allowed_ids[] = $items->offerincident_id;
			}

		}

		// Add aditional filter here to only allow for offers that are in the requested offer-category
		$offerincident_id_in = '';
		if(count($allowed_ids) AND $offercategory_id != 0)
		{
			$offerincident_id_in = ' AND id IN ('.implode(',',$allowed_ids).')';
		}
		elseif(count($allowed_ids) == 0 AND $offercategory_id != 0)
		{
			$offerincident_id_in = ' AND 3 = 4';
		}

		$query = 'SELECT UNIX_TIMESTAMP('.$select_date_text.') AS time, COUNT(id) AS number FROM '.$this->table_prefix.'offerincident WHERE offerincident_active = 1 '.$offerincident_id_in.' GROUP BY '.$groupby_date_text;
		$query = $db->query($query);

		foreach ( $query as $items )
		{
			array_push($graph_data[0]['data'],
				array($items->time * 1000, $items->number));
		}

		echo json_encode($graph_data);
	}
	

	/**
	 * Read in new layer KML via file_get_contents
	 * @param int $layer_id - ID of the new KML Layer
	 */
	public function layer($layer_id = 0)
	{
		$this->template = "";
		$this->auto_render = FALSE;

		$layer = ORM::factory('layer')
			->where('layer_visible', 1)
			->find($layer_id);

		if ($layer->loaded)
		{
			$layer_url = $layer->layer_url;
			$layer_file = $layer->layer_file;

			if ($layer_url != '')
			{
				// Pull from a URL
				$layer_link = $layer_url;
			}else{
				// Pull from an uploaded file
				$layer_link = Kohana::config('upload.directory').'/'.$layer_file;
			}

			$content = file_get_contents($layer_link);

			if ($content !== false)
			{
				echo $content;
			}
			else
			{
				echo "";
			}
		}
		else
		{
			echo "";
		}
	}


	/**
	 * Read in new layer JSON from shared connection
	 * @param int $sharing_id - ID of the new Share Layer
	 */
	public function share( $sharing_id = false )
	{	
		$offerjson = "";
		$offerjson_item = "";
		$offerjson_array = array();
		$sharing_data = "";
		$clustering = Kohana::config('settings.allow_clustering');
		
		if ($sharing_id)
		{
			// Get This Sharing ID Color
			$sharing = ORM::factory('sharing')
				->find($sharing_id);
			
			if( ! $sharing->loaded )
				return;
			
			$sharing_url = $sharing->sharing_url;
			$sharing_color = $sharing->sharing_color;
			
			if ($clustering)
			{
				// Database
				$db = new Database();
				
				// Start Date
				$start_date = (isset($_GET['s']) && !empty($_GET['s'])) ?
					(int) $_GET['s'] : "0";

				// End Date
				$end_date = (isset($_GET['e']) && !empty($_GET['e'])) ?
					(int) $_GET['e'] : "0";

				// SouthWest Bound
				$southwest = (isset($_GET['sw']) && !empty($_GET['sw'])) ?
					$_GET['sw'] : "0";

				$northeast = (isset($_GET['ne']) && !empty($_GET['ne'])) ?
					$_GET['ne'] : "0";
				
				// Get Zoom Level
				$zoomLevel = (isset($_GET['z']) && !empty($_GET['z'])) ?
					(int) $_GET['z'] : 8;

				//$distance = 60;
				$distance = (10000000 >> $zoomLevel) / 100000;
				
				$filter = "";
				$filter .= ($start_date) ? 
					" AND offerincident_date >= '" . date("Y-m-d H:i:s", $start_date) . "'" : "";
				$filter .= ($end_date) ? 
					" AND offerincident_date <= '" . date("Y-m-d H:i:s", $end_date) . "'" : "";

				if ($southwest && $northeast)
				{
					list($latitude_min, $longitude_min) = explode(',', $southwest);
					list($latitude_max, $longitude_max) = explode(',', $northeast);

					$filter .= " AND latitude >=".(float) $latitude_min.
						" AND latitude <=".(float) $latitude_max;
					$filter .= " AND longitude >=".(float) $longitude_min.
						" AND longitude <=".(float) $longitude_max;
				}
				
				$filter .= " AND sharing_id = ".(int)$sharing_id;

				$query = $db->query("SELECT * FROM `".$this->table_prefix."sharing_offerincident` WHERE 1=1 $filter ORDER BY offerincident_id ASC "); 

				$markers = $query->result_array(FALSE);

				$clusters = array();	// Clustered
				$singles = array();		// Non Clustered

				// Loop until all markers have been compared
				while (count($markers))
				{
					$marker	 = array_pop($markers);
					$cluster = array();

					// Compare marker against all remaining markers.
					foreach ($markers as $key => $target)
					{
						// This function returns the distance between two markers, at a defined zoom level.
						// $pixels = $this->_pixelDistance($marker['latitude'], $marker['longitude'], 
						// $target['latitude'], $target['longitude'], $zoomLevel);

						$pixels = abs($marker['longitude']-$target['longitude']) + 
							abs($marker['latitude']-$target['latitude']);
						// echo $pixels."<BR>";
						// If two markers are closer than defined distance, remove compareMarker from array and add to cluster.
						if ($pixels < $distance)
						{
							unset($markers[$key]);
							$target['distance'] = $pixels;
							$cluster[] = $target;
						}
					}

					// If a marker was added to cluster, also add the marker we were comparing to.
					if (count($cluster) > 0)
					{
						$cluster[] = $marker;
						$clusters[] = $cluster;
					}
					else
					{
						$singles[] = $marker;
					}
				}

				// Create Offerjson
				foreach ($clusters as $cluster)
				{
					// Calculate cluster center
					$bounds = $this->_calculateCenter($cluster);
					$cluster_center = $bounds['center'];
					$southwest = $bounds['sw'];
					$northeast = $bounds['ne'];

					// Number of Items in Cluster
					$cluster_count = count($cluster);

					$offerjson_item = "{";
					$offerjson_item .= "\"type\":\"Feature\",";
					$offerjson_item .= "\"properties\": {";
					$offerjson_item .= "\"name\":\"" . str_replace(chr(10), ' ', str_replace(chr(13), ' ', "<a href='http://" . $sharing_url . "/offerreports/index/?c=0&sw=".$southwest."&ne=".$northeast."'>" . $cluster_count . " Offerreports</a>")) . "\",";
					$offerjson_item .= "\"link\": \"http://".$sharing_url."offerreports/index/?c=0&sw=".$southwest."&ne=".$northeast."\", ";		  
					$offerjson_item .= "\"offercategory\":[0], ";
					$offerjson_item .= "\"icon\": \"\", ";
					$offerjson_item .= "\"color\": \"".$sharing_color."\", ";
					$offerjson_item .= "\"timestamp\": \"0\", ";
					$offerjson_item .= "\"count\": \"" . $cluster_count . "\"";
					$offerjson_item .= "},";
					$offerjson_item .= "\"geometry\": {";
					$offerjson_item .= "\"type\":\"Point\", ";
					$offerjson_item .= "\"coordinates\":[" . $cluster_center . "]";
					$offerjson_item .= "}";
					$offerjson_item .= "}";

					array_push($offerjson_array, $offerjson_item);
				}

				foreach ($singles as $single)
				{
					$offerjson_item = "{";
					$offerjson_item .= "\"type\":\"Feature\",";
					$offerjson_item .= "\"properties\": {";
					$offerjson_item .= "\"name\":\"" . str_replace(chr(10), ' ', str_replace(chr(13), ' ', "<a href='http://" . $sharing_url . "/offer/details/" . $single['id'] . "'>".$single['offerincident_title']."</a>")) . "\",";
					$offerjson_item .= "\"link\": \"http://".$sharing_url."offer/details/".$single['id']."\", ";
					$offerjson_item .= "\"offercategory\":[0], ";
					$offerjson_item .= "\"icon\": \"\", ";
					$offerjson_item .= "\"color\": \"".$sharing_color."\", ";
					$offerjson_item .= "\"timestamp\": \"0\", ";
					$offerjson_item .= "\"count\": \"" . 1 . "\"";
					$offerjson_item .= "},";
					$offerjson_item .= "\"geometry\": {";
					$offerjson_item .= "\"type\":\"Point\", ";
					$offerjson_item .= "\"coordinates\":[" . $single['longitude'] . ", " . $single['latitude'] . "]";
					$offerjson_item .= "}";
					$offerjson_item .= "}";

					array_push($offerjson_array, $offerjson_item);
				}

				$offerjson = implode(",", $offerjson_array);
				
			}
			else
			{
				// Retrieve all markers
				$markers = ORM::factory('sharing_offerincident')
										->where('sharing_id', $sharing_id)
										->find_all();

				foreach ($markers as $marker)
				{	
					$offerjson_item = "{";
					$offerjson_item .= "\"type\":\"Feature\",";
					$offerjson_item .= "\"properties\": {";
					$offerjson_item .= "\"id\": \"".$marker->offerincident_id."\", \n";

					$encoded_title = utf8tohtml::convert($marker->offerincident_title,TRUE);

					$offerjson_item .= "\"name\":\"" . str_replace(chr(10), ' ', str_replace(chr(13), ' ', "<a href='http://" . $sharing_url . "/offer/details/" . $marker->offerincident_id . "'>".$encoded_title."</a>")) . "\",";
					$offerjson_item .= "\"link\": \"http://".$sharing_url."offer/details/".$marker->offerincident_id."\", ";
					$offerjson_item .= "\"icon\": \"\", ";
					$offerjson_item .= "\"color\": \"".$sharing_color ."\", \n";
					$offerjson_item .= "\"timestamp\": \"" . strtotime($marker->offerincident_date) . "\"";
					$offerjson_item .= "},";
					$offerjson_item .= "\"geometry\": {";
					$offerjson_item .= "\"type\":\"Point\", ";
					$offerjson_item .= "\"coordinates\":[" . $marker->longitude . ", " . $marker->latitude . "]";
					$offerjson_item .= "}";
					$offerjson_item .= "}";

					array_push($offerjson_array, $offerjson_item);
				}

				$offerjson = implode(",", $offerjson_array);
			}
		}
		
		 header('Content-type: application/offerjson; charset=utf-8');
		$this->template->offerjson = $offerjson;
	}


	/**
	 * Get Geometry JSON
	 * @param int $offerincident_id
	 * @param string $offerincident_title
	 * @param int $offerincident_date
	 * @return array $geometry
	 */
	private function _get_geometry($offerincident_id, $offerincident_title, $offerincident_date)
	{
		$geometry = array();
		if ($offerincident_id)
		{
			$geom_data = $this->_get_geometry_data_for_offerincident($offerincident_id);
			$wkt = new Wkt();

			foreach ( $geom_data as $item )
			{
				$geom = $wkt->read($item->geometry);
				$geom_array = $geom->getGeoInterface();

				$offerjson_item = "{";
				$offerjson_item .= "\"type\":\"Feature\",";
				$offerjson_item .= "\"properties\": {";
				$offerjson_item .= "\"id\": \"".$offerincident_id."\", ";
				$offerjson_item .= "\"feature_id\": \"".$item->id."\", ";

				$title = ($item->geometry_label) ? 
					utf8tohtml::convert($item->geometry_label,TRUE) : 
					utf8tohtml::convert($offerincident_title,TRUE);
					
				$fillcolor = ($item->geometry_color) ? 
					utf8tohtml::convert($item->geometry_color,TRUE) : "ffcc66";
					
				$strokecolor = ($item->geometry_color) ? 
					utf8tohtml::convert($item->geometry_color,TRUE) : "CC0000";
					
				$strokewidth = ($item->geometry_strokewidth) ? $item->geometry_strokewidth : "3";

				$offerjson_item .= "\"name\":\"" . str_replace(chr(10), ' ', str_replace(chr(13), ' ', "<a href='" . url::base() . "offer/details/" . $offerincident_id . "'>".$title."</a>")) . "\",";

				$offerjson_item .= "\"description\": \"" . utf8tohtml::convert($item->geometry_comment,TRUE) . "\", ";
				$offerjson_item .= "\"color\": \"" . $fillcolor . "\", ";
				$offerjson_item .= "\"strokecolor\": \"" . $strokecolor . "\", ";
				$offerjson_item .= "\"strokewidth\": \"" . $strokewidth . "\", ";
				$offerjson_item .= "\"link\": \"".url::base()."offer/details/".$offerincident_id."\", ";
				$offerjson_item .= "\"offercategory\":[0], ";
				$offerjson_item .= "\"timestamp\": \"" . strtotime($offerincident_date) . "\"";
				$offerjson_item .= "},\"geometry\":".json_encode($geom_array)."}";
				$geometry[] = $offerjson_item;
			}
		}
		
		return $geometry;
	}


	/**
	 * Get geometry records from the database and cache 'em.
	 *
	 * They're heavily read from, no point going back to the db constantly to
	 * get them.
	 * @param int $offerincident_id - Offerincident to get geometry for
	 * @return array
	 */
	public function _get_geometry_data_for_offerincident($offerincident_id) {
		if (self::$geometry_data) {
			return isset(self::$geometry_data[$offerincident_id]) ? self::$geometry_data[$offerincident_id] : array();
		}

		$db = new Database();
		// Get Offerincident Geometries via SQL query as ORM can't handle Spatial Data
		$sql = "SELECT id, offerincident_id, AsText(geometry) as geometry, geometry_label, 
			geometry_comment, geometry_color, geometry_strokewidth FROM ".$this->table_prefix."geometry";
		$query = $db->query($sql);

		foreach ( $query as $item )
		{
			self::$geometry_data[$item->offerincident_id][] = $item;
		}

		return isset(self::$geometry_data[$offerincident_id]) ? self::$geometry_data[$offerincident_id] : array();
	}


	/**
	 * Convert Longitude to Cartesian (Pixels) value
	 * @param double $lon - Longitude
	 * @return int
	 */
	private function _lonToX($lon)
	{
		return round(OFFSET + RADIUS * $lon * pi() / 180);
	}

	/**
	 * Convert Latitude to Cartesian (Pixels) value
	 * @param double $lat - Latitude
	 * @return int
	 */
	private function _latToY($lat)
	{
		return round(OFFSET - RADIUS *
					log((1 + sin($lat * pi() / 180)) /
					(1 - sin($lat * pi() / 180))) / 2);
	}

	/**
	 * Calculate distance using Cartesian (pixel) coordinates
	 * @param int $lat1 - Latitude for point 1
	 * @param int $lon1 - Longitude for point 1
	 * @param int $lon2 - Latitude for point 2
	 * @param int $lon2 - Longitude for point 2
	 * @return int
	 */
	private function _pixelDistance($lat1, $lon1, $lat2, $lon2, $zoom)
	{
		$x1 = $this->_lonToX($lon1);
		$y1 = $this->_latToY($lat1);

		$x2 = $this->_lonToX($lon2);
		$y2 = $this->_latToY($lat2);

		return sqrt(pow(($x1-$x2),2) + pow(($y1-$y2),2)) >> (21 - $zoom);
	}

	/**
	 * Calculate the center of a cluster of markers
	 * @param array $cluster
	 * @return array - (center, southwest bound, northeast bound)
	 */
	private function _calculateCenter($cluster)
	{
		// Calculate average lat and lon of clustered items
		$south = 0;
		$west = 0;
		$north = 0;
		$east = 0;

		$lat_sum = $lon_sum = 0;
		foreach ($cluster as $marker)
		{
			if (!$south)
			{
				$south = $marker['latitude'];
			}
			elseif ($marker['latitude'] < $south)
			{
				$south = $marker['latitude'];
			}

			if (!$west)
			{
				$west = $marker['longitude'];
			}
			elseif ($marker['longitude'] < $west)
			{
				$west = $marker['longitude'];
			}

			if (!$north)
			{
				$north = $marker['latitude'];
			}
			elseif ($marker['latitude'] > $north)
			{
				$north = $marker['latitude'];
			}

			if (!$east)
			{
				$east = $marker['longitude'];
			}
			elseif ($marker['longitude'] > $east)
			{
				$east = $marker['longitude'];
			}

			$lat_sum += $marker['latitude'];
			$lon_sum += $marker['longitude'];
		}
		$lat_avg = $lat_sum / count($cluster);
		$lon_avg = $lon_sum / count($cluster);

		$center = $lon_avg.",".$lat_avg;
		$sw = $west.",".$south;
		$ne = $east.",".$north;

		return array(
			"center"=>$center,
			"sw"=>$sw,
			"ne"=>$ne
		);
	}
}
