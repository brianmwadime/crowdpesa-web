<?php defined('SYSPATH') or die('No direct script access.');

/**
 * This controller is used to list/ view and edit offers
 *
 * PHP version 5
 * LICENSE: This source file is subject to LGPL license
 * that is available through the world-wide-web at the following URI:
 * http://www.gnu.org/copyleft/lesser.html
 * @author	   BeeBuy Team <info@beebuy.com>
 * @package	   CrowdPesa - http://crowdpesa.com
 * @copyright  CrowdPesa - http://crowdpesa.com
 * @license	   http://www.gnu.org/copyleft/lesser.html GNU Lesser General Public License (LGPL)
 */

class Offerreports_Controller extends Main_Controller {
	/**
	 * Whether an admin console user is logged in
	 * @var bool
	 */
	var $logged_in;

	public function __construct()
	{
		parent::__construct();

		$this->themes->validator_enabled = TRUE;

		// Is the Admin Logged In?
		$this->logged_in = Auth::instance()->logged_in();

	}

	/**
	 * Displays all offers.
	 */
	public function index()
	{
		// Cacheable Controller
		$this->is_cachable = TRUE;

		$this->template->header->this_page = 'offerreports';
		$this->template->content = new View('offerreports');
		$this->themes->js = new View('offerreports_js');

		// Store any exisitng URL parameters
		$this->themes->js->url_params = json_encode($_GET);

		// Enable the map
		$this->themes->map_enabled = TRUE;

		// Set the latitude and longitude
		$this->themes->js->latitude = Kohana::config('settings.default_lat');
		$this->themes->js->longitude = Kohana::config('settings.default_lon');
		$this->themes->js->default_map = Kohana::config('settings.default_map');
		$this->themes->js->default_zoom = Kohana::config('settings.default_zoom');

		// Load the alert radius view
		$alert_radius_view = new View('alert_radius_view');
		$alert_radius_view->show_usage_info = FALSE;
		$alert_radius_view->enable_find_location = FALSE;
		$alert_radius_view->css_class = "rb_location-radius";

		$this->template->content->alert_radius_view = $alert_radius_view;

		// Get locale
		$l = Kohana::config('locale.language.0');

		// Get the offerreport listing view
		$offerreport_listing_view = $this->_get_offerreport_listing_view($l);

		// Set the view
		$this->template->content->offerreport_listing_view = $offerreport_listing_view;

		// Load the offercategory
		$offercategory_id = (isset($_GET['c']) AND intval($_GET['c']) > 0)? intval($_GET['c']) : 0;
		$offercategory = ORM::factory('offercategory', $offercategory_id);

		if ($offercategory->loaded)
		{
			// Set the offercategory title
			$this->template->content->offercategory_title = Offercategory_Lang_Model::offercategory_title($offercategory_id,$l);
		}
		else
		{
			$this->template->content->offercategory_title = "";
		}

		// Collect offerreport stats
		$this->template->content->offerreport_stats = new View('offerreports_stats');
		// Total Offerreports

		$total_offerreports = Offerincident_Model::get_total_offerreports(TRUE);

		// Get the date of the oldest offerreport
		if (isset($_GET['s']) AND !empty($_GET['s']) AND intval($_GET['s']) > 0)
		{
			$oldest_timestamp =  intval($_GET['s']);
		}
		else
		{
			$oldest_timestamp = Offerincident_Model::get_oldest_offerreport_timestamp();
		}

		//Get the date of the latest offerreport
		if (isset($_GET['e']) AND !empty($_GET['e']) AND intval($_GET['e']) > 0)
		{
			$latest_timestamp = intval($_GET['e']);
		}
		else
		{
			$latest_timestamp = Offerincident_Model::get_latest_offerreport_timestamp();
		}


		// Round the number of days up to the nearest full day
		$days_since = ceil((time() - $oldest_timestamp) / 86400);
		$avg_offerreports_per_day = ($days_since < 1)? $total_offerreports : round(($total_offerreports / $days_since),2);

		// Percent Verified
		$total_verified = Offerincident_Model::get_total_offerreports_by_verified(TRUE);
		$percent_verified = ($total_offerreports == 0) ? '-' : round((($total_verified / $total_offerreports) * 100),2).'%';

		// Offercategory tree view
		$this->template->content->offercategory_tree_view = offercategory::get_offercategory_tree_view();

		// Additional view content
		$this->template->content->custom_forms_filter = new View('offerreports_submit_custom_forms');
		$disp_custom_fields = customforms::get_custom_form_fields();
		$this->template->content->custom_forms_filter->disp_custom_fields = $disp_custom_fields;
		$this->template->content->oldest_timestamp = $oldest_timestamp;
		$this->template->content->latest_timestamp = $latest_timestamp;
		$this->template->content->offerreport_stats->total_offerreports = $total_offerreports;
		$this->template->content->offerreport_stats->avg_offerreports_per_day = $avg_offerreports_per_day;
		$this->template->content->offerreport_stats->percent_verified = $percent_verified;
		$this->template->content->services = Service_Model::get_array();

		$this->template->header->header_block = $this->themes->header_block();
		$this->template->footer->footer_block = $this->themes->footer_block();
	}
	
	public function subscribe_user($id = 0){
		if(empty($id)) url::redirect('offerreports');
		
		if(!$this->auth->logged_in()) url::redirect('login');
		$user = $this->auth->get_user();
		$user_id = $user->id;
		
		$sub = new Offersubscription_Model();
		$sub->user_id = $user_id;
		$sub->offercategory_id = $id;
		$sub->save();
		
		url::redirect('offerreports');
	}
	
	public function unsubscribe_user($id = 0){
		if(empty($id)) url::redirect('offerreports');
		if(!$this->auth->logged_in()) url::redirect('login');
		$user = $this->auth->get_user();
		$user_id = $user->id;
		
		ORM::factory('offersubscription')->where('user_id='.$user_id)->where('offercategory_id='.$id)->delete_all();
		
		url::redirect('offerreports');
	}

	/**
	 * Helper method to load the offerreport listing view
	 */
	private function _get_offerreport_listing_view($locale = '')
	{
		// Check if the local is empty
		if (empty($locale))
		{
			$locale = Kohana::config('locale.language.0');
		}

		// Load the offerreport listing view
		$offerreport_listing = new View('offerreports_listing');

		// Fetch all offerincidents
		$all_offerincidents = offerreports::fetch_offerincidents();

		// Pagination
		$pagination = new Pagination(array(
				'style' => 'front-end-reports',
				'query_string' => 'page',
				'items_per_page' => (int) Kohana::config('settings.items_per_page'),
				'total_items' => $all_offerincidents->count()
				));

		// Offerreports
		$offerincidents = Offerincident_Model::get_offerincidents(offerreports::$params, $pagination);

		// Swap out offercategory titles with their proper localizations using an array (cleaner way to do this?)
		$localized_offercategories = array();
		foreach ($offerincidents as $offerincident)
		{
			$offerincident = ORM::factory('offerincident', $offerincident->offerincident_id);
			foreach ($offerincident->offercategory AS $offercategory)
			{
				$ct = (string)$offercategory->offercategory_title;
				if ( ! isset($localized_offercategories[$ct]))
				{
					$localized_offercategories[$ct] = Offercategory_Lang_Model::offercategory_title($offercategory->id, $locale);
				}
			}
		}
		// Set the view content
		$offerreport_listing->offerincidents = $offerincidents;
		$offerreport_listing->localized_offercategories = $localized_offercategories;

		//Set default as not showing pagination. Will change below if necessary.
		$offerreport_listing->pagination = "";

		// Pagination and Total Num of Offerreport Stats
		$plural = ($pagination->total_items == 1)? "" : "s";

		// Set the next and previous page numbers
		$offerreport_listing->next_page = $pagination->next_page;
		$offerreport_listing->previous_page = $pagination->previous_page;

		if ($pagination->total_items > 0)
		{
			$current_page = ($pagination->sql_offset/ $pagination->items_per_page) + 1;
			$total_pages = ceil($pagination->total_items/ $pagination->items_per_page);

			if ($total_pages >= 1)
			{
				$offerreport_listing->pagination = $pagination;

				// Show the total of offerreport
				// @todo This is only specific to the frontend offerreports theme
				$offerreport_listing->stats_breadcrumb = $pagination->current_first_item.'-'
											. $pagination->current_last_item.' of '.$pagination->total_items.' '
											. Kohana::lang('ui_main.offerreports');
			}
			else
			{ // If we don't want to show pagination
				$offerreport_listing->stats_breadcrumb = $pagination->total_items.' '.Kohana::lang('ui_admin.offerreports');
			}
		}
		else
		{
			$offerreport_listing->stats_breadcrumb = '('.$pagination->total_items.' offerreport'.$plural.')';
		}

		// Return
		return $offerreport_listing;
	}

	public function fetch_offerreports()
	{
		$this->template = "";
		$this->auto_render = FALSE;

		if ($_GET)
		{
			$offerreport_listing_view = $this->_get_offerreport_listing_view();
			print $offerreport_listing_view;
		}
		else
		{
			print "";
		}
	}

	/**
	 * Submits a new offerreport.
	 */
	public function submit($id = FALSE, $saved = FALSE)
	{
		$db = new Database();

		// First, are we allowed to submit new offerreports?
		if ( ! Kohana::config('settings.allow_offerreports'))
		{
			url::redirect(url::site().'main');
		}

		$this->template->header->this_page = 'offerreports_submit';
		$this->template->content = new View('offerreports_submit');

		//Retrieve API URL
		$this->template->api_url = Kohana::config('settings.api_url');

		// Setup and initialize form field names
		$form = array(
			'offerincident_title' => '',
			'offerincident_description' => '',
			'offerincident_date' => '',
			'offerincident_hour' => '',
			'offerincident_minute' => '',
			'offerincident_ampm' => '',
			'latitude' => '',
			'longitude' => '',
			'geometry' => array(),
			'location_name' => '',
			'country_id' => '',
			'country_name'=>'',
			'offerincident_offercategory' => array(),
			'offerincident_news' => array(),
			'offerincident_video' => array(),
			'offerincident_photo' => array(),
			'offerincident_zoom' => '',
			'person_first' => '',
			'person_last' => '',
			'person_email' => '',
			'form_id'	  => '',
			'custom_field' => array()
		);

		// Copy the form as errors, so the errors will be stored with keys corresponding to the form field names
		$errors = $form;
		$form_error = FALSE;

		$form_saved = ($saved == 'saved');

		// Initialize Default Values
		$form['offerincident_date'] = date("m/d/Y",time());
		$form['offerincident_hour'] = date('g');
		$form['offerincident_minute'] = date('i');
		$form['offerincident_ampm'] = date('a');
		$form['country_id'] = Kohana::config('settings.default_country');

		// Initialize Default Value for Hidden Field Country Name, just incase Reverse Geo coding yields no result
		$country_name = ORM::factory('country',$form['country_id']);
		$form['country_name'] = $country_name->country;

		// Initialize custom field array
		$form['custom_field'] = customforms::get_custom_form_fields($id,'',true);

		//GET custom forms
		$forms = array();
		foreach (customforms::get_custom_forms() as $custom_forms)
		{
			$forms[$custom_forms->id] = $custom_forms->form_title;
		}

		$this->template->content->forms = $forms;


		// Check, has the form been submitted, if so, setup validation
		if ($_POST)
		{
			// Instantiate Validation, use $post, so we don't overwrite $_POST fields with our own things
			$post = array_merge($_POST, $_FILES);

			// Test to see if things passed the rule checks
			if (offerreports::validate($post))
			{

				// STEP 1: SAVE LOCATION
				$location = new Location_Model();
				offerreports::save_location($post, $location);

				// STEP 2: SAVE INCIDENT
				$offerincident = new Offerincident_Model();
				offerreports::save_offerreport($post, $offerincident, $location->id);

				// STEP 2b: SAVE INCIDENT GEOMETRIES
				offerreports::save_offerreport_geometry($post, $offerincident);

				// STEP 3: SAVE CATEGORIES
				offerreports::save_offercategory($post, $offerincident);

				// STEP 4: SAVE MEDIA
				offerreports::save_media($post, $offerincident);

				// STEP 5: SAVE CUSTOM FORM FIELDS
				offerreports::save_custom_fields($post, $offerincident);

				// STEP 6: SAVE PERSONAL INFORMATION
				offerreports::save_personal_info($post, $offerincident);

				// Action::offerreport_add/offerreport_submit - Added a New Offerreport
				//++ Do we need two events for this? Or will one suffice?
				//ETHERTON: Yes. Those of us who often write plugins for
				//Ushahidi would like to have access to the $post arrays
				//and the offerreport object. Back in the day we even had access
				//to the $post object, so if our plugins didn't get the
				//appropriate input we could raise an error, but alas,
				//those days are gone. Now I suppose you could do something
				//like Event::run('ushahidi_action.offerreport_add', array($post, $offerincident));
				//but for the sake of backward's compatibility, please don't
				//Thanks.
				Event::run('ushahidi_action.offerreport_submit', $post);
				Event::run('ushahidi_action.offerreport_add', $offerincident);


				url::redirect('offerreports/thanks');
			}

			// No! We have validation errors, we need to show the form again, with the errors
			else
			{
				// Repopulate the form fields
				$form = arr::overwrite($form, $post->as_array());

				// Populate the error fields, if any
				$errors = arr::overwrite($errors, $post->errors('offerreport'));
				$form_error = TRUE;
			}
		}

		// Retrieve Country Cities
		$default_country = Kohana::config('settings.default_country');
		$this->template->content->cities = $this->_get_cities($default_country);
		$this->template->content->multi_country = Kohana::config('settings.multi_country');

		$this->template->content->id = $id;
		$this->template->content->form = $form;
		$this->template->content->errors = $errors;
		$this->template->content->form_error = $form_error;

		$offercategories = $this->get_offercategories($form['offerincident_offercategory']);
		$this->template->content->offercategories = $offercategories;

		// Pass timezone
		$this->template->content->site_timezone = Kohana::config('settings.site_timezone');

		// Pass the submit offerreport message
		$this->template->content->site_submit_offerreport_message = Kohana::config('settings.site_submit_offerreport_message');

		// Retrieve Custom Form Fields Structure
		$this->template->content->custom_forms = new View('offerreports_submit_custom_forms');
		$disp_custom_fields = customforms::get_custom_form_fields($id,$form['form_id'], FALSE);
		$this->template->content->disp_custom_fields = $disp_custom_fields;
		$this->template->content->stroke_width_array = $this->_stroke_width_array();
		$this->template->content->custom_forms->disp_custom_fields = $disp_custom_fields;
		$this->template->content->custom_forms->form = $form;

		// Javascript Header
		$this->themes->map_enabled = TRUE;
		$this->themes->datepicker_enabled = TRUE;
		$this->themes->treeview_enabled = TRUE;
		$this->themes->colorpicker_enabled = TRUE;

		$this->themes->js = new View('offerreports_submit_edit_js');
		$this->themes->js->edit_mode = FALSE;
		$this->themes->js->offerincident_zoom = FALSE;
		$this->themes->js->default_map = Kohana::config('settings.default_map');
		$this->themes->js->default_zoom = Kohana::config('settings.default_zoom');
		if (!$form['latitude'] OR !$form['latitude'])
		{
			$this->themes->js->latitude = Kohana::config('settings.default_lat');
			$this->themes->js->longitude = Kohana::config('settings.default_lon');
		}
		else
		{
			$this->themes->js->latitude = $form['latitude'];
			$this->themes->js->longitude = $form['longitude'];
		}
		$this->themes->js->geometries = $form['geometry'];


		// Rebuild Header Block
		$this->template->header->header_block = $this->themes->header_block();
		$this->template->footer->footer_block = $this->themes->footer_block();
	}

	 /**
	 * Displays a offerreport.
	 * @param boolean $id If id is supplied, a offerreport with that id will be
	 * retrieved.
	 */
	public function view($id = FALSE)
	{
		$this->template->header->this_page = 'offerreports';
		$this->template->content = new View('offerreports_view');

		// Load Akismet API Key (Spam Blocker)
		$api_akismet = Kohana::config('settings.api_akismet');

		// Sanitize the offerreport id before proceeding
		$id = intval($id);

		if ($id > 0 AND Offerincident_Model::is_valid_offerincident($id,FALSE))
		{
			$offerincident = ORM::factory('offerincident')
				->where('id',$id)
				->where('offerincident_active',1)
				->find();

			if ( ! $offerincident->loaded)	// Not Found
			{
				url::redirect('offerreports/view/');
			}

			// Comment Post?
			// Setup and initialize form field names

			$form = array(
				'comment_author' => '',
				'comment_description' => '',
				'comment_email' => '',
				'comment_ip' => '',
				'captcha' => '',
				'comment_rating' => 0,
				'comment_rating_scale' => array('No star','1 star','2 stars','3 stars','4 stars' ,'5 stars')
			);

			$captcha = Captcha::factory();
			$errors = $form;
			$form_error = FALSE;

			// Check, has the form been submitted, if so, setup validation

			if ($_POST AND Kohana::config('settings.allow_comments') )
			{
				// Instantiate Validation, use $post, so we don't overwrite $_POST fields with our own things

				$post = Validation::factory($_POST);

				// Add some filters

				$post->pre_filter('trim', TRUE);

				// Add some rules, the input field, followed by a list of checks, carried out in order

				if ( ! $this->user)
				{
					$post->add_rules('comment_author', 'required', 'length[3,100]');
					$post->add_rules('comment_email', 'required','email', 'length[4,100]');
				}
				$post->add_rules('comment_description', 'required');
				$post->add_rules('captcha', 'required', 'Captcha::valid');

				// Test to see if things passed the rule checks

				if ($post->validate())
				{
					// Yes! everything is valid

					if ($api_akismet != "")
					{
						// Run Akismet Spam Checker
						$akismet = new Akismet();

						// Comment data
						$comment = array(
							'website' => "",
							'body' => $post->comment_description,
							'user_ip' => $_SERVER['REMOTE_ADDR']
						);

						if ($this->user)
						{
							$comment['author'] = $this->user->name;
							$comment['email'] = $this->user->email;
						}
						else
						{
							$comment['author'] = $post->comment_author;
							$comment['email'] = $post->comment_email;
						}

						$config = array(
							'blog_url' => url::site(),
							'api_key' => $api_akismet,
							'comment' => $comment
						);

						$akismet->init($config);

						if ($akismet->errors_exist())
						{
							if ($akismet->is_error('AKISMET_INVALID_KEY'))
							{
								// throw new Kohana_Exception('akismet.api_key');
							}
							elseif ($akismet->is_error('AKISMET_RESPONSE_FAILED'))
							{
								// throw new Kohana_Exception('akismet.server_failed');
							}
							elseif ($akismet->is_error('AKISMET_SERVER_NOT_FOUND'))
							{
								// throw new Kohana_Exception('akismet.server_not_found');
							}

							// If the server is down, we have to post
							// the comment :(
							// $this->_post_comment($comment);

							$comment_spam = 0;
						}
						else
						{
							$comment_spam = ($akismet->is_spam()) ? 1 : 0;
						}
					}
					else
					{
						// No API Key!!
						$comment_spam = 0;
					}

					$comment = new Comment_Model();
					$comment->offerincident_id = $id;
					$rating = new Rating_Model();
					$rating->offerincident_id = $id;
					
					if ($this->user)
					{
						$comment->user_id = $this->user->id;
						$comment->comment_author = $this->user->name;
						$comment->comment_email = $this->user->email;
						$rating->user_id = $this->user->id;
					}
					else
					{
						$comment->comment_author = strip_tags($post->comment_author);
						$comment->comment_email = strip_tags($post->comment_email);
					}
					
					$comment->comment_description = strip_tags($post->comment_description);
					$comment->comment_ip = $_SERVER['REMOTE_ADDR'];
					$comment->comment_date = date("Y-m-d H:i:s",time());
					$rating->rating_date = $comment->comment_date;
					$rating->rating = strip_tags($post->comment_rating);
					$rating->rating_ip = $_SERVER['REMOTE_ADDR'];

					// Activate comment for now
					if ($comment_spam == 1)
					{
						$comment->comment_spam = 1;
						$comment->comment_active = 0;
					}
					else
					{
						$comment->comment_spam = 0;
						$comment->comment_active = (Kohana::config('settings.allow_comments') == 1)? 1 : 0;
					}
					$comment->save();
					$rating->comment_id = $comment->id;
					$rating->save();

					// Event::comment_add - Added a New Comment
					Event::run('ushahidi_action.comment_add', $comment);

					// Notify Admin Of New Comment
					$send = notifications::notify_admins(
						"[".Kohana::config('settings.site_name')."] ".
							Kohana::lang('notifications.admin_new_comment.subject'),
							Kohana::lang('notifications.admin_new_comment.message')
							."\n\n'".strtoupper($offerincident->offerincident_title)."'"
							."\n".url::base().'offerreports/view/'.$id
						);

					// Redirect
					url::redirect('offerreports/view/'.$id);

				}
				else
				{
					// No! We have validation errors, we need to show the form again, with the errors
					// Repopulate the form fields
					$form = arr::overwrite($form, $post->as_array());

					// Populate the error fields, if any
					$errors = arr::overwrite($errors, $post->errors('comments'));
					$form_error = TRUE;
				}
			}

			// Filters
			$offerincident_title = $offerincident->offerincident_title;
			$offerincident_description = nl2br($offerincident->offerincident_description);
			Event::run('ushahidi_filter.offerreport_title', $offerincident_title);
			Event::run('ushahidi_filter.offerreport_description', $offerincident_description);

			// Add Features
			$this->template->content->features_count = $offerincident->geometry->count();
			$this->template->content->features = $offerincident->geometry;
			$this->template->content->offerincident_id = $offerincident->id;
			$this->template->content->offerincident_title = $offerincident_title;
			$this->template->content->offerincident_description = $offerincident_description;
			$this->template->content->offerincident_location = $offerincident->location->location_name;
			$this->template->content->offerincident_latitude = $offerincident->location->latitude;
			$this->template->content->offerincident_longitude = $offerincident->location->longitude;
			$this->template->content->offerincident_date = date('M j Y', strtotime($offerincident->offerincident_date));
			$this->template->content->offerincident_time = date('H:i', strtotime($offerincident->offerincident_date));
			$this->template->content->offerincident_offercategory = $offerincident->offerincident_offercategory;

			// Offerincident rating
			$db = Database::instance();
			$average_rating = $db->query("SELECT AVG(rating) 'ave' FROM rating WHERE offerincident_id = $id AND comment_id IS NOT NULL");
			//$this->template->content->offerincident_rating = ($offerincident->offerincident_rating == '')
			//	? 0
			//	: $offerincident->offerincident_rating;
			$this->template->content->offerincident_rating = round($average_rating[0]->ave);

			// Retrieve Media
			$offerincident_news = array();
			$offerincident_video = array();
			$offerincident_photo = array();

			foreach($offerincident->media as $media)
			{
				if ($media->media_type == 4)
				{
					$offerincident_news[] = $media->media_link;
				}
				elseif ($media->media_type == 2)
				{
					$offerincident_video[] = $media->media_link;
				}
				elseif ($media->media_type == 1)
				{
					$offerincident_photo[] = array(
											'large' => url::convert_uploaded_to_abs($media->media_link),
											'thumb' => url::convert_uploaded_to_abs($media->media_thumb)
											);
				}
			}

			$this->template->content->offerincident_verified = $offerincident->offerincident_verified;

			// Retrieve Comments (Additional Information)
			$this->template->content->comments = "";
			if (Kohana::config('settings.allow_comments'))
			{
				$this->template->content->comments = new View('offerreports_comments');
				$offerincident_comments = array();
				if ($id)
				{
					$offerincident_comments = Offerincident_Model::get_comments($id);
				}
				$this->template->content->comments->offerincident_comments = $offerincident_comments;
			}
		}
		else
		{
			url::redirect('main');
		}

		// Add Neighbors
		$this->template->content->offerincident_neighbors = Offerincident_Model::get_neighbouring_offerincidents($id, TRUE, 0, 5);

		// News Source links
		$this->template->content->offerincident_news = $offerincident_news;


		// Video links
		$this->template->content->offerincident_videos = $offerincident_video;

		// Images
		$this->template->content->offerincident_photos = $offerincident_photo;

		// Create object of the video embed class
		$video_embed = new VideoEmbed();
		$this->template->content->videos_embed = $video_embed;

		// Javascript Header
		$this->themes->map_enabled = TRUE;
		$this->themes->photoslider_enabled = TRUE;
		$this->themes->videoslider_enabled = TRUE;
		$this->themes->js = new View('offerreports_view_js');
		$this->themes->js->offerincident_id = $offerincident->id;
		$this->themes->js->default_map = Kohana::config('settings.default_map');
		$this->themes->js->default_zoom = Kohana::config('settings.default_zoom');
		$this->themes->js->latitude = $offerincident->location->latitude;
		$this->themes->js->longitude = $offerincident->location->longitude;
		$this->themes->js->offerincident_zoom = $offerincident->offerincident_zoom;
		$this->themes->js->offerincident_photos = $offerincident_photo;

		// Initialize custom field array
		$this->template->content->custom_forms = new View('offerreports_view_custom_forms');
		$form_field_names = customforms::get_custom_form_fields($id, $offerincident->form_id, FALSE, "view");
		$this->template->content->custom_forms->form_field_names = $form_field_names;

		// Are we allowed to submit comments?
		$this->template->content->comments_form = "";
		if (Kohana::config('settings.allow_comments'))
		{
			$this->template->content->comments_form = new View('offerreports_comments_form');
			$this->template->content->comments_form->user = $this->user;
			$this->template->content->comments_form->form = $form;
			$this->template->content->comments_form->form_field_names = $form_field_names;
			$this->template->content->comments_form->captcha = $captcha;
			$this->template->content->comments_form->errors = $errors;
			$this->template->content->comments_form->form_error = $form_error;
		}

		// If the Admin is Logged in - Allow for an edit link
		$this->template->content->logged_in = $this->logged_in;

		// Rebuild Header Block
		$this->template->header->header_block = $this->themes->header_block();
		$this->template->footer->footer_block = $this->themes->footer_block();
	}

	/**
	 * Offerreport Thanks Page
	 */
	public function thanks()
	{
		$this->template->header->this_page = 'offerreports_submit';
		$this->template->content = new View('offerreports_submit_thanks');

		// Rebuild Header Block
		$this->template->header->header_block = $this->themes->header_block();
		$this->template->footer->footer_block = $this->themes->footer_block();
	}

	/**
	 * Offerreport Rating.
	 * @param boolean $id If id is supplied, a rating will be applied to selected offerreport
	 */
	public function rating($id = false)
	{
		$this->template = "";
		$this->auto_render = FALSE;

		if (!$id)
		{
			echo json_encode(array("status"=>"error", "message"=>"ERROR!"));
		}
		else
		{
			if (!empty($_POST['action']) AND !empty($_POST['type']))
			{
				$action = $_POST['action'];
				$type = $_POST['type'];

				// Is this an ADD(+1) or SUBTRACT(-1)?
				if ($action == 'add')
				{
					$action = 1;
				}
				elseif ($action == 'subtract')
				{
					$action = -1;
				}
				else
				{
					$action = 0;
				}

				if (!empty($action) AND ($type == 'original' OR $type == 'comment'))
				{
					// Has this User or IP Address rated this post before?
					if ($this->user)
					{
						$filter = "user_id = ".$this->user->id;
					}
					else
					{
						$filter = "rating_ip = '".$_SERVER['REMOTE_ADDR']."' ";
					}

					if ($type == 'original')
					{
						$previous = ORM::factory('rating')
							->where('offerincident_id',$id)
							->where($filter)
							->find();
					}
					elseif ($type == 'comment')
					{
						$previous = ORM::factory('rating')
							->where('comment_id',$id)
							->where($filter)
							->find();
					}

					// If previous exits... update previous vote
					$rating = new Rating_Model($previous->id);

					// Are we rating the original post or the comments?
					if ($type == 'original')
					{
						$rating->offerincident_id = $id;
					}
					elseif ($type == 'comment')
					{
						$rating->comment_id = $id;
					}

					// Is there a user?
					if ($this->user)
					{
						$rating->user_id = $this->user->id;

						// User can't rate their own stuff
						if ($type == 'original')
						{
							if ($rating->offerincident->user_id == $this->user->id)
							{
								echo json_encode(array("status"=>"error", "message"=>"Can't rate your own Offerreports!"));
								exit;
							}
						}
						elseif ($type == 'comment')
						{
							if ($rating->comment->user_id == $this->user->id)
							{
								echo json_encode(array("status"=>"error", "message"=>"Can't rate your own Comments!"));
								exit;
							}
						}
					}

					$rating->rating = $action;
					$rating->rating_ip = $_SERVER['REMOTE_ADDR'];
					$rating->rating_date = date("Y-m-d H:i:s",time());
					$rating->save();

					// Get total rating and send back to offerjson
					$total_rating = $this->_get_rating($id, $type);

					echo json_encode(array("status"=>"saved", "message"=>"SAVED!", "rating"=>$total_rating));
				}
				else
				{
					echo json_encode(array("status"=>"error", "message"=>"Nothing To Do!"));
				}
			}
			else
			{
				echo json_encode(array("status"=>"error", "message"=>"Nothing To Do!"));
			}
		}
	}

	public function geocode()
	{
		$this->template = "";
		$this->auto_render = FALSE;

		if (isset($_POST['address']) AND ! empty($_POST['address']))
		{
			$geocode = map::geocode($_POST['address']);
			if ($geocode)
			{
				echo json_encode(array("status"=>"success", "message"=>array($geocode['lat'], $geocode['lon'])));
			}
			else
			{
				echo json_encode(array("status"=>"error", "message"=>"ERROR!"));
			}
		}
		else
		{
			echo json_encode(array("status"=>"error", "message"=>"ERROR!"));
		}
	}

	/**
	 * Retrieves Cities
	 */
	private function _get_cities()
	{
		$cities = ORM::factory('city')->orderby('city', 'asc')->find_all();
		$city_select = array('' => Kohana::lang('ui_main.offerreports_select_city'));

		foreach ($cities as $city)
		{
			$city_select[$city->city_lon.",".$city->city_lat] = $city->city;
		}

		return $city_select;
	}

	/**
	 * Retrieves Total Rating For Specific Post
	 * Also Updates The Offerincident & Comment Tables (Ratings Column)
	 */
	private function _get_rating($id = FALSE, $type = NULL)
	{
		if (!empty($id) AND ($type == 'original' OR $type == 'comment'))
		{
			if ($type == 'original')
			{
				$which_count = 'offerincident_id';
			}
			elseif ($type == 'comment')
			{
				$which_count = 'comment_id';
			}
			else
			{
				return 0;
			}

			$total_rating = 0;

			// Get All Ratings and Sum them up
			foreach (ORM::factory('rating')
							->where($which_count,$id)
							->find_all() as $rating)
			{
				$total_rating += $rating->rating;
			}

			// Update Counts
			if ($type == 'original')
			{
				$offerincident = ORM::factory('offerincident', $id);
				if ($offerincident->loaded == TRUE)
				{
					$offerincident->offerincident_rating = $total_rating;
					$offerincident->save();
				}
			}
			elseif ($type == 'comment')
			{
				$comment = ORM::factory('comment', $id);
				if ($comment->loaded == TRUE)
				{
					$comment->comment_rating = $total_rating;
					$comment->save();
				}
			}

			return $total_rating;
		}
		else
		{
			return 0;
		}
	}

	/**
	 * Validates a numeric array. All items contained in the array must be numbers or numeric strings
	 *
	 * @param array $nuemric_array Array to be verified
	 */
	private function _is_numeric_array($numeric_array=array())
	{
		if (count($numeric_array) == 0)
			return FALSE;
		else
		{
			foreach ($numeric_array as $item)
			{
				if (! is_numeric($item))
					return FALSE;
			}

			return TRUE;
		}
	}

	/**
	 * Array with Geometry Stroke Widths
    */
	private function _stroke_width_array()
	{
		for ($i = 0.5; $i <= 8 ; $i += 0.5)
		{
			$stroke_width_array["$i"] = $i;
		}

		return $stroke_width_array;
	}

	/**
	 * Ajax call to update Offerincident Offerreporting Form
    */
    public function switch_form()
    {
        $this->template = "";
        $this->auto_render = FALSE;
        isset($_POST['form_id']) ? $form_id = $_POST['form_id'] : $form_id = "1";
        isset($_POST['offerincident_id']) ? $offerincident_id = $_POST['offerincident_id'] : $offerincident_id = "";

		$form_fields = customforms::switcheroo($offerincident_id,$form_id);
        echo json_encode(array("status"=>"success", "response"=>$form_fields));
    }
	/**
	 * Helper method to load the featured offers
	 */
	private function get_featured_offers(){
	}

}
