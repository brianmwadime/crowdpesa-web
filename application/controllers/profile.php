<?php defined('SYSPATH') or die('No direct script access.');

/**
 * This controller is used to view user profiles
 *
 * PHP version 5
 * LICENSE: This source file is subject to LGPL license
 * that is available through the world-wide-web at the following URI:
 * http://www.gnu.org/copyleft/lesser.html
 * @author	   Ushahidi Team <team@ushahidi.com>
 * @package	   Ushahidi - http://source.ushahididev.com
 * @copyright  Ushahidi - http://www.ushahidi.com
 * @license	   http://www.gnu.org/copyleft/lesser.html GNU Lesser General Public License (LGPL)
 */

class Profile_Controller extends Main_Controller {

	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Displays the default "profile" page
	 */
	public function index()
	{
		
		// Cacheable Controller
		$this->is_cachable = TRUE;

		$this->template->header->this_page = 'profile';
		$this->template->content = new View('profile_browse');
		

		$this->template->content->users = User_Model::get_public_users();
		$this->template->header->page_title .= Kohana::lang('ui_main.browse_profiles').Kohana::config('settings.title_delimiter');
		$this->template->header->header_block = $this->themes->header_block();
		$this->template->footer->footer_block = $this->themes->footer_block();
		
	}

	/**
	 * Displays a profile page for a user
	 */
	public function user()
	{
		// Cacheable Controller
		$this->is_cachable = TRUE;

		$this->template->header->this_page = 'profile';
		$status = '';
			if (!empty($_GET['status']))
		{
			$status = $_GET['status'];

		}else{
			$status = 'p';
		}

		// Check if we are looking for a user. Argument must be set to continue.
		if( ! isset(Router::$arguments[0]))
		{
			url::redirect('profile');
		}

		$username = Router::$arguments[0];

		// We won't allow profiles to be public if the username is an email address
		if (valid::email($username))
		{
			url::redirect('profile');
		}
		$db= Database::instance();
		$user = User_Model::get_user_by_username($username);
		$retailer_info = new Retailer_Info_Model($user->id);

		// We only want to show public profiles here
		if($user->public_profile == 1)
		{
								
			$db = Database::instance();
											
			$this->template->content = new View('retailers_profile');

			$this->template->content->user = $user;
			$this->template->content->retailer_info = $retailer_info;

			// User Reputation Score
			$this->template->content->reputation = reputation::calculate($user->id);

			// All retailer profile info
			$db= Database::instance();
			$incidents_pagination = self::get_places_pagination($user->id);
			$offers_pagination = self::get_offers_pagination($user->id);
			$this->template->content->incidents = self::get_retailer_places($user->id,$incidents_pagination);
			$this->template->content->offers = self::get_retailer_offers($user->id,$offers_pagination);
			$this->template->content->incidents_pagination = $incidents_pagination;
			$this->template->content->offers_pagination = $offers_pagination;
			
			// Get Badges
			$this->template->content->badges = Badge_Model::users_badges($user->id);
			
			// Logged in user id (false if not logged in)
			$logged_in_id = FALSE;
			if(isset(Auth::instance()->get_user()->id))
			{
				$logged_in_id = Auth::instance()->get_user()->id;
			}
			$this->template->content->logged_in_id = $logged_in_id;

			// Is this the logged in user?
			$logged_in_user = FALSE;
			if($logged_in_id == $user->id){
				$logged_in_user = TRUE;
			}
			$this->template->content->logged_in_user = $logged_in_user;
		}else{
			// this is a private profile so get out of here
			url::redirect('profile');
		}
		$this->template->content->status = $status;
		$this->template->header->page_title .= $user->name.Kohana::config('settings.title_delimiter');

		$this->template->header->header_block = $this->themes->header_block();
		$this->template->footer->footer_block = $this->themes->footer_block();
	}
	
	public function get_retailer_places($retailer_id, $pagination){
		$db = Database::instance();
		$places = $db->query("SELECT incident.*,media.media_thumb FROM incident LEFT JOIN
			media ON media.incident_id = incident.id WHERE incident_active = 1 AND incident.user_id = $retailer_id GROUP BY incident.id ORDER BY incident_title ASC LIMIT $pagination->sql_offset,".(int) Kohana::config('settings.items_per_page_admin').'');
		return $places;
	}
	public function get_retailer_offers($retailer_id, $pagination){
		$db = Database::instance();
		$offers = $db->query("SELECT offerincident.*, media.media_thumb FROM offerincident LEFT JOIN 
			media ON media.offerincident_id = offerincident.id WHERE offerincident_active = 1 AND offerincident.user_id = $retailer_id GROUP BY offerincident.id ORDER BY offerincident_dateadd DESC LIMIT $pagination->sql_offset,".(int) Kohana::config('settings.items_per_page_admin').'');
		return $offers;
	}
	public function get_offers_pagination($retailer_id){
		$filter = 'offerincident_end_date > NOW()';
		$pagination = new Pagination(array(
			'query_string'	 => 'page',
			'items_per_page' => (int) Kohana::config('settings.items_per_page_admin'),
			'total_items'	 => ORM::factory('offerincident')
					->where('user_id', $retailer_id)
					->where('offerincident_verified','1')
				->where('offerincident_active','1')
				->where($filter)
				->count_all()
			));
		return $pagination;
	}
	public function get_places_pagination($retailer_id){
		$pagination = new Pagination(array(
			'query_string'	 => 'page',
			'items_per_page' => (int) Kohana::config('settings.items_per_page_admin'),
			'total_items'	 => ORM::factory('incident')
					->where('user_id', $retailer_id)
					->where('incident_verified','1')
					->where('incident_active','1')
					->count_all()
			));
		return $pagination;
	}

} // End Profile
