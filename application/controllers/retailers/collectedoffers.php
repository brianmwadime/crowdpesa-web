<?php defined('SYSPATH') or die('No direct script access.');
/**
 * Offerreports Controller.
 * This controller will take care of adding and editing offerreports in the Member section.
 *
 * PHP version 5
 * LICENSE: This source file is subject to LGPL license
 * that is available through the world-wide-web at the following URI:
 * http://www.gnu.org/copyleft/lesser.html
 * @author	   BeeBuy Team <info@beebuy.com>
 * @package	   CrowdPesa - http://crowdpesa.com
 * @subpackage Members
 * @copyright  CrowdPesa - http://crowdpesa.com
 * @license	   http://www.gnu.org/copyleft/lesser.html GNU Lesser General Public License (LGPL)
 */

class Collectedoffers_Controller extends Retailers_Controller {
	
	function __construct()
	{
		parent::__construct();

		$this->template->this_page = 'collectedoffers';
	}


	/**
	* Lists the offerreports.
	* @param int $page
	*/
	function index($page = 1)
	{
		$this->template->content = new View('retailers/collected_offers');
		$this->template->content->title = Kohana::lang('ui_admin.collected_offers');


		if (!empty($_GET['status']))
		{
			$status = $_GET['status'];

			if (strtolower($status) == 'p')
			{
				$filter = 'picked = 1';
			}
			elseif (strtolower($status) == 'np')
			{
				$filter = 'picked = 0';
			}
			else
			{
				$status = "0";
				$filter = '1=1';
			}
		}
		else
		{
			$status = "0";
			$filter = "1=1";
		}

		// Get Search Keywords (If Any)
		if (isset($_GET['k']))
		{
			//	Brute force input sanitization
			
			// Phase 1 - Strip the search string of all non-word characters 
			$keyword_raw = preg_replace('/[^\w+]\w*/', '', $_GET['k']);
			
			// Strip any HTML tags that may have been missed in Phase 1
			$keyword_raw = strip_tags($keyword_raw);
			
			// Phase 3 - Invoke Kohana's XSS cleaning mechanism just incase an outlier wasn't caught
			// in the first 2 steps
			$keyword_raw = $this->input->xss_clean($keyword_raw);
			
			$filter .= " AND (".$this->_get_searchstring($keyword_raw).")";
		}
		else
		{
			$keyword_raw = "";
		}

		// check, has the form been submitted?
		$form_error = FALSE;
		$form_saved = FALSE;
		$form_action = "";
		
		

		// Pagination
		$pagination = new Pagination(array(
			'query_string'	 => 'page',
			'items_per_page' => (int) Kohana::config('settings.items_per_page_admin'),
			'total_items'	 => ORM::factory('order')
				->join('offerincident','offerincident.id','orders.offerincident_id','INNER')
				->where('offerincident.user_id', $this->user->id)
				->count_all()
			));

		$collected_offers = ORM::factory('order')
			->select('orders.*','offerincident.offer_coupon','users.name','offerincident.offerincident_title','offerincident.offerincident_description')
			->join('users', 'orders.customer_id', 'users.id','INNER')
			->join('offerincident','offerincident.id','orders.offerincident_id','INNER')
			->where($filter)
			->where('offerincident.user_id', $this->user->id)
			->orderby('orders.date', 'desc')
			->find_all((int) Kohana::config('settings.items_per_page_admin'), $pagination->sql_offset);
		

		$this->template->content->collected_offers = $collected_offers;		
		$this->template->content->pagination = $pagination;
		$this->template->content->form_error = $form_error;
		$this->template->content->form_saved = $form_saved;
		$this->template->content->form_action = $form_action;

		// Total Offerreports
		$this->template->content->total_items = $pagination->total_items;

		// Status Tab
		$this->template->content->status = $status;

		// Javascript Header
		$this->template->js = new View('admin/offerreports_js');
	}


	/**
	* Edit a offerreport
	* @param bool|int $id The id no. of the offerreport
	* @param bool|string $saved
	*/
	public function edit($id = FALSE, $saved = FALSE)
	{
		if($id){
			$order = ORM::factory('order')
				->select('orders.*','users.name','offerincident.offerincident_title','offerincident.offerincident_description')
				->join('users', 'orders.customer_id', 'users.id','INNER')
				->join('offerincident','offerincident.id','orders.offerincident_id','INNER')
				->find($id);
		$this->template->content = new View('retailers/collected_offer');
		$this->template->content->order = $order;
		// setup and initialize form field names
		$form = array('customer_token' => '');

		//	Copy the form as errors, so the errors will be stored with keys
		//	corresponding to the form field names
		$errors = $form;
		$form_error = FALSE;
		$form_saved = FALSE;

		// check, has the form been submitted, if so, setup validation
		if ($_POST)
		{	
			$post = Validation::factory($_POST);
			
				if($order->customer_token == $post->customer_token)
				{
					$order->picked = '1';
					$order->save();
					$form_saved = TRUE;
				}else{
					$form_error = 'The Tokens did not match';
				}
			
		}
	

		$this->template->content->form = $form;
		$this->template->content->errors = $errors;
		$this->template->content->form_error = $form_error;
		$this->template->content->form_saved = $form_saved;
		
	
		}
	
}
/**
	 * Creates a SQL string from search keywords
	 */
	private function _get_searchstring($keyword_raw)
	{
		$or = '';
		$where_string = '';


		// Stop words that we won't search for
		// Add words as needed!!
		$stop_words = array('the', 'and', 'a', 'to', 'of', 'in', 'i', 'is', 'that', 'it',
		'on', 'you', 'this', 'for', 'but', 'with', 'are', 'have', 'be',
		'at', 'or', 'as', 'was', 'so', 'if', 'out', 'not');

		$keywords = explode(' ', $keyword_raw);
		
		if (is_array($keywords) && !empty($keywords))
		{
			array_change_key_case($keywords, CASE_LOWER);
			$i = 0;
			
			foreach($keywords as $value)
			{
				if (!in_array($value,$stop_words) && !empty($value))
				{
					$chunk = mysql_real_escape_string($value);
					if ($i > 0) {
						$or = ' OR ';
					}
					$where_string = $where_string.$or."name LIKE '%$chunk%' OR offerincident_title LIKE '%$chunk%'";
					$i++;
				}
			}
		}

		if ($where_string)
		{
			return $where_string;
		}
		else
		{
			return "1=1";
		}
	}

}


