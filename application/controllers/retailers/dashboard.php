<?php defined('SYSPATH') or die('No direct script access.');
/**
 * This controller is used for the main Admin panel
 *
 * PHP version 5
 * LICENSE: This source file is subject to LGPL license
 * that is available through the world-wide-web at the following URI:
 * http://www.gnu.org/copyleft/lesser.html
 * @author	   Ushahidi Team <team@ushahidi.com>
 * @package	   Ushahidi - http://source.ushahididev.com
 * @subpackage Members
 * @copyright  Ushahidi - http://www.ushahidi.com
 * @license	   http://www.gnu.org/copyleft/lesser.html GNU Lesser General Public License (LGPL)
 */

class Dashboard_Controller extends Retailers_Controller {

	function __construct()
	{
		parent::__construct();
	}

	function index()
	{
		$this->template->content = new View('retailers/dashboard');
		$this->template->content->title = Kohana::lang('ui_admin.dashboard');
		$this->template->this_page = 'dashboard';

		// User
		$this->template->content->user = $this->user;

		// User Reputation Score
		$this->template->content->reputation = reputation::calculate($this->user->id);

		// Get Badges
		$this->template->content->badges = Badge_Model::users_badges($this->user->id);

		// Retrieve Dashboard Counts...
		// Total Reports
		$this->template->content->reports_total = ORM::factory('incident')
			->where("user_id", $this->user->id)
			->count_all();

		// Total Unapproved Reports
		$this->template->content->reports_unapproved = ORM::factory('incident')
			->where('incident_active', '0')
			->where("user_id", $this->user->id)
			->count_all();
			
		//**************************************************************
		// Total Reports
		$this->template->content->offerreports_total = ORM::factory('offerincident')
			->where("user_id", $this->user->id)
			->count_all();

		// Total Unapproved Reports
		$this->template->content->offerreports_unapproved = ORM::factory('offerincident')
			->where('offerincident_active', '0')
			->where("user_id", $this->user->id)
			->count_all();
		//**************************************************************

		// Total Checkins
		$this->template->content->checkins = ORM::factory('checkin')
			->where("user_id", $this->user->id)
			->count_all();

		// Total Alerts
		$this->template->content->alerts = ORM::factory('alert')
			->where("user_id", $this->user->id)
			->count_all();

		// Total Votes
		$this->template->content->votes = ORM::factory('rating')
			->where("user_id", $this->user->id)
			->count_all();

		// Total Votes Positive
		$this->template->content->votes_up = ORM::factory('rating')
			->where("user_id", $this->user->id)
			->where("rating", "1")
			->count_all();

		// Total Votes Negative
		$this->template->content->votes_down = ORM::factory('rating')
			->where("user_id", $this->user->id)
			->where("rating", "-1")
			->count_all();
		//Total Collected Offers
		$this->template->content->total_collected_offers = ORM::factory('order')
			->join('offerincident','orders.offerincident_id','offerincident.id')
			->where('offerincident.user_id',$this->user->id)
			->count_all();
		//Total Un Collected Offers
		$this->template->content->total_uncollected_offers = ORM::factory('order')
			->join('offerincident','orders.offerincident_id','offerincident.id')
			->where('offerincident.user_id',$this->user->id)
			->where('orders.picked','0')
			->count_all();
		//Total Un Collected Offers
		$this->template->content->total_subscriptions = ORM::factory('offersubscription')->get_subscription_count($this->user->id);
		//Total checkins by this retailer
		$this->template->content->total_checkins = Checkin_Model::get_retailer_checkin_count($this->user->id);
		//Get latest reviews for displat
		$this->template->content->timelines = $this->get_timeline($this->user->id);
		// Get reports for display
		$this->template->content->incidents = ORM::factory('incident')
				->where("user_id", $this->user->id)
				->limit(5)
				->orderby('incident_dateadd', 'desc')
				->find_all();
				
		//**************************************************************
		// Get offer-reports for display
		$this->template->content->offerincidents = ORM::factory('offerincident')
				->where("user_id", $this->user->id)
				->limit(5)
				->orderby('offerincident_dateadd', 'desc')
				->find_all();
		//**************************************************************

		// To support the "welcome" or "not enough info on user" form
		if($this->user->public_profile == 1)
		{
			$this->template->content->profile_public = TRUE;
			$this->template->content->profile_private = FALSE;
		}else{
			$this->template->content->profile_public = FALSE;
			$this->template->content->profile_private = TRUE;
		}

		$this->template->content->hidden_welcome_fields = array
		(
			'email' => $this->user->email,
			'notify' => $this->user->notify,
			'color' => $this->user->color,
			'password' => '', // Don't set a new password from here
			'needinfo' => 0 // After we save this form once, we don't need to show it again
		);

		/*
		// Javascript Header
		$this->template->flot_enabled = TRUE;
		$this->template->js = new View('admin/dashboard_js');
		// Graph
		$this->template->js->all_graphs = Incident_Model::get_incidents_by_interval('ALL',NULL,NULL,'all');
		$this->template->js->current_date = date('Y') . '/' . date('m') . '/01';
		*/

		// Javascript Header
		$this->template->protochart_enabled = TRUE;
		$this->template->js = new View('admin/stats_js');

		$this->template->content->failure = '';

		// Build dashboard chart

		// Set the date range (how many days in the past from today?)
		// Default to one year if invalid or not set
		$range = (isset($_GET['range']) AND preg_match('/^\d+$/', $_GET['range']) > 0)
			? (int) $_GET['range']
			: 365;

		// Phase 3 - Invoke Kohana's XSS cleaning mechanism just incase an outlier wasn't caught
		$range = $this->input->xss_clean($range);
		$incident_data = Incident_Model::get_number_reports_by_date($range, $this->user->id);
		$data = array('Reports'=>$incident_data);
		$options = array('xaxis'=>array('mode'=>'"time"'));
		$this->template->content->report_chart = protochart::chart('report_chart',$data,$options,array('Reports'=>'CC0000'),410,310);
		
		//**************************************************************
		// Build offer-dashboard chart

		// Set the date range (how many days in the past from today?)
		// Default to one year if invalid or not set
		$range = 0;
		if (isset($_GET['range']))
		{
			// Sanitize the range parameter
			$range = $this->input->xss_clean($_GET['range']);
			$range = (intval($range) > 0)? intval($range) : 0;
		}
		
		$offerincident_data = Offerincident_Model::get_number_offerreports_by_date($range, $this->user->id);
		$data = array('Offerreports'=>$offerincident_data);
		$options = array('xaxis'=>array('mode'=>'"time"'));
		
		$this->template->content->offerreport_chart = protochart::chart('offerreport_chart',$data,$options,array('Offerreports'=>'00CC00'),410,310);
		//**************************************************************
		
	}

    //timeline
    public function get_timeline($retailer_id){
        $db = Database::instance();
        $result = array();
        $reviews = $db->query(" SELECT comment_author as author, comment_description as description,offerincident_id,incident_id,comment_date as date  FROM `comment` LEFT JOIN incident ON incident.id = comment.incident_id LEFT JOIN offerincident ON offerincident.id = offerincident_id  WHERE article_id IS NULL AND (incident.user_id = $retailer_id OR  offerincident.user_id = $retailer_id) ORDER BY comment_date DESC LIMIT 0,5");
        foreach ($reviews as $review){
            $i = strtotime($review->date);
            $result[$i] = array(
                //"id" => $review->id,
                "type" => 'r',
                "user" => $review->author,
                "msg" => $review->description,
                "date" => $review->date,
            );
        }
        $checkins = $db->query("SELECT users.name as author, checkin.id, checkin_description as description, checkin_date as date FROM `checkin`
                    INNER JOIN users ON checkin.user_id = users.id
                    INNER JOIN location ON checkin.location_id = location.id
                    LEFT JOIN incident ON location.id = incident.location_id
                    WHERE incident.user_id = $retailer_id
                    ORDER BY date DESC LIMIT 0,5");
        foreach ($checkins as $checkin){
            $i = strtotime($checkin->date);
            $result[$i] = array(
                //"id" => $checkin->id,
                "type" => 'c',
                "user" => $checkin->author,
                "msg" => $checkin->description,
                "date" => $checkin->date,
            );
        }
        ksort($result);
        return $result;
                
    }
   
}
?>
