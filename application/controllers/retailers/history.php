<?php defined('SYSPATH') or die('No direct script access.');
/**
 * Reports Controller.
 * This controller will take care of adding and editing reports in the Member section.
 *
 * PHP version 5
 * LICENSE: This source file is subject to LGPL license
 * that is available through the world-wide-web at the following URI:
 * http://www.gnu.org/copyleft/lesser.html
 * @author	   Ushahidi Team <team@ushahidi.com>
 * @package	   Ushahidi - http://source.ushahididev.com
 * @subpackage Members
 * @copyright  Ushahidi - http://www.ushahidi.com
 * @license	   http://www.gnu.org/copyleft/lesser.html GNU Lesser General Public License (LGPL)
 */

class History_Controller extends Retailers_Controller {
	

	function __construct()
	{
		parent::__construct();

		$this->template->this_page = 'history';
	}


	/**
	* Lists the reports.
	* @param int $page
	*/
	function index($page = 1)
	{
		$this->template->content = new View('retailers/transaction_history');
		$this->template->content->title = Kohana::lang('ui_admin.orders');
		
		if (!empty($_GET['status']))
		{
			$status = $_GET['status'];

			if (strtolower($status) == 'c')
			{
				$filter = 'collected = 1';
			}
			elseif (strtolower($status) == 'p')
			{
				$filter = 'collected = 0';
			}
			else
			{
				$status = "0";
				$filter = '1=1';
			}
		}
		else
		{
			$status = "0";
			$filter = "1=1";
		}

		// Get Search Keywords (If Any)
		if (isset($_GET['k']))
		{
			//	Brute force input sanitization
			
			// Phase 1 - Strip the search string of all non-word characters 
			$keyword_raw = preg_replace('/[^\w+]\w*/', '', $_GET['k']);
			
			// Strip any HTML tags that may have been missed in Phase 1
			$keyword_raw = strip_tags($keyword_raw);
			
			// Phase 3 - Invoke Kohana's XSS cleaning mechanism just incase an outlier wasn't caught
			// in the first 2 steps
			$keyword_raw = $this->input->xss_clean($keyword_raw);
			
			$filter .= " AND (".$this->_get_searchstring($keyword_raw).")";
		}
		else
		{
			$keyword_raw = "";
		}

		$db = Database::instance();
		// Pagination
		$pagination = new Pagination(array(
			'query_string'	 => 'page',
			'items_per_page' => (int) Kohana::config('settings.items_per_page_admin'),
			'total_items'	 => ORM::factory('transaction')->where('retailer_id',$this->user->id)->where($filter)->count_all()
			));

		

		$transactions = ORM::factory('transaction')
			->select('transaction_history.*','o.collected','o.retailer_coupon','o.customer_coupon',
			'c.name AS cust_name','i.offerincident_title AS title','i.offerincident_description AS description')
			->join('orders AS o','transaction_history.id','o.trans_id','INNER')
			->join('users AS c','c.id','transaction_history.customer_id','INNER')
			->join('offerincident AS i','i.id','transaction_history.offerincident_id','INNER')
			->where($filter)
			->where('retailer_id', $this->user->id)			
			->find_all((int) Kohana::config('settings.items_per_page_admin'), $pagination->sql_offset);

			//$orders = $db->query('SELECT * FROM orders INNER JOIN transaction_history ON (transaction_history.id = orders.trans_id)');
			//var_dump($orders);
			//var_dump($pagination);
		
			//exit;
		// Status Tab
		
		$this->template->content->status = $status;
		$this->template->content->transactions = $transactions;
		$this->template->content->pagination = $pagination;
		$this->template->content->total_items = $pagination->total_items;
		
		$this->template->content->title = Kohana::lang('ui_admin.payments');
	}
	public function view($id = FALSE, $saved = FALSE)
	{
		$this->template->content = new View('retailers/transaction_view');

		// setup and initialize form field names
		

		//	Copy the form as errors, so the errors will be stored with keys
		//	corresponding to the form field names
		//$errors = $form;
		$form_error = FALSE;
		$form_saved = FALSE;

		
		$transaction = ORM::factory('transaction')
			->select('transaction_history.*','o.collected','o.retailer_coupon','o.customer_coupon',
			'c.name AS customer_name','c.email','i.offerincident_title AS title','i.offerincident_description AS description')
			->join('orders AS o','transaction_history.id','o.trans_id','INNER')
			->join('users AS c','c.id','transaction_history.customer_id','INNER')
			->join('offerincident AS i','i.id','transaction_history.offerincident_id','INNER')
			->where('transaction_history.id', $id)			
			->find($id);
		$this->template->content->transaction = $transaction;
		//$this->template->content->form = $form;
		//$this->template->content->errors = $errors;
		$this->template->content->form_error = $form_error;
		$this->template->content->form_saved = $form_saved;

	}
}
