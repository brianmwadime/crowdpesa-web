<?php defined('SYSPATH') or die('No direct script access.');
/**
 * Reports Controller.
 * This controller will take care of adding and editing reports in the Member section.
 *
 * PHP version 5
 * LICENSE: This source file is subject to LGPL license
 * that is available through the world-wide-web at the following URI:
 * http://www.gnu.org/copyleft/lesser.html
 * @author	   Ushahidi Team <team@ushahidi.com>
 * @package	   Ushahidi - http://source.ushahididev.com
 * @subpackage Members
 * @copyright  Ushahidi - http://www.ushahidi.com
 * @license	   http://www.gnu.org/copyleft/lesser.html GNU Lesser General Public License (LGPL)
 */

class Payments_Controller extends Retailers_Controller {
	

	function __construct()
	{
		parent::__construct();

		$this->template->this_page = 'payments';
	}


	/**
	* Lists the reports.
	* @param int $page
	*/
	function index($page = 1)
	{
		$this->template->content = new View('retailers/payments');
		$this->template->content->title = Kohana::lang('ui_admin.orders');
	
		if (!empty($_GET['status']))
		{
			$status = $_GET['status'];

			if (strtolower($status) == 'f')
			{
				$filter = 'collected = 1';
			}
			elseif (strtolower($status) == 'p')
			{
				$filter = 'collected = 0';
			}
			else
			{
				$status = "0";
				$filter = '1=1';
			}
		}
		else
		{
			$status = "0";
			$filter = "1=1";
		}

		// Get Search Keywords (If Any)
		if (isset($_GET['k']))
		{
			//	Brute force input sanitization
			
			// Phase 1 - Strip the search string of all non-word characters 
			$keyword_raw = preg_replace('/[^\w+]\w*/', '', $_GET['k']);
			
			// Strip any HTML tags that may have been missed in Phase 1
			$keyword_raw = strip_tags($keyword_raw);
			
			// Phase 3 - Invoke Kohana's XSS cleaning mechanism just incase an outlier wasn't caught
			// in the first 2 steps
			$keyword_raw = $this->input->xss_clean($keyword_raw);
			
			$filter .= " AND (".$this->_get_searchstring($keyword_raw).")";
		}
		else
		{
			$keyword_raw = "";
		}

		$db = Database::instance();
		// Pagination
		$pagination = new Pagination(array(
			'query_string'	 => 'page',
			'items_per_page' => (int) Kohana::config('settings.items_per_page_admin'),
			'total_items'	 => ORM::factory('order')
			->join('transaction_history', 'transaction_history.id', 'orders.trans_id','INNER')
			->where($filter)
			->where('retailer_id', $this->user->id)
			->count_all()
			));

		

		$orders = ORM::factory('order')
			->select('orders.*','transaction_history.amount','transaction_history.quantity','users.name','offerincident_title','offerincident_description')
			->join('transaction_history', 'orders.trans_id','transaction_history.id','INNER')
			->join('users', 'users.id','transaction_history.customer_id','INNER')
			->join('offerincident', 'offerincident.id','transaction_history.offerincident_id','INNER')
			->where($filter)
			->where('retailer_id', $this->user->id)			
			->find_all((int) Kohana::config('settings.items_per_page_admin'), $pagination->sql_offset);

			//$orders = $db->query('SELECT * FROM orders INNER JOIN transaction_history ON (transaction_history.id = orders.trans_id)');
			//var_dump($orders);
			//var_dump($pagination);
		
			//exit;
		// Status Tab
		
		$this->template->content->status = $status;
		$this->template->content->orders = $orders;
		$this->template->content->pagination = $pagination;
		$this->template->content->total_items = $pagination->total_items;
		
		$this->template->content->title = Kohana::lang('ui_admin.payments');
	}

	public function edit($id = FALSE, $saved = FALSE)
	{
		$this->template->content = new View('retailers/payments_edit');

		// setup and initialize form field names
		$form = array
		(
			'id' => '',
			'trans_id' => '',
			'date' => '',
			'collected' => '',
			'customer_coupon' => '',
			'retailer_coupon' => ''
			
		);

		//	Copy the form as errors, so the errors will be stored with keys
		//	corresponding to the form field names
		$errors = $form;
		$form_error = FALSE;
		$form_saved = FALSE;

		// check, has the form been submitted, if so, setup validation
		if ($_POST)
		{
			$post = Validation::factory($_POST);

			 //	 Add some filters
			$post->pre_filter('trim', TRUE);
			$post->add_rules('customer_coupon','required','alpha_numeric');
			
	
		
		}
		else
		{
			$order = ORM::factory('order',$id);
			$form['id'] = $order->id;
			$form['trans_id'] = $order->trans_id;
			$form['date'] = $order->date;
			$form['collected'] = $order->collected;
			$form['customer_coupon'] = $order->customer_coupon;
			$form['retailer_coupon'] = $order->retailer_coupon;
		}
		$order_details = ORM::factory('order')
		->select('orders.*','transaction_history.*','users.*','offerincident.*')
		->join('transaction_history','transaction_history.id','orders.trans_id','INNER')
		->join('users', 'users.id','transaction_history.customer_id','INNER')
		->join('offerincident', 'offerincident.id','transaction_history.offerincident_id','INNER')
		->where('orders.id',$id)
		->find($id);
		$this->template->content->order_details = $order_details;
		$this->template->content->form = $form;
		$this->template->content->errors = $errors;
		$this->template->content->form_error = $form_error;
		$this->template->content->form_saved = $form_saved;

	}
}
