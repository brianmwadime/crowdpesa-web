<?php defined('SYSPATH') or die('No direct script access.');
/**
 * Search controller
 *
 * PHP version 5
 * LICENSE: This source file is subject to LGPL license
 * that is available through the world-wide-web at the following URI:
 * http://www.gnu.org/copyleft/lesser.html
 * @author     Ushahidi Team <team@ushahidi.com>
 * @package    Ushahidi - http://source.ushahididev.com
 * @copyright  Ushahidi - http://www.ushahidi.com
 * @license    http://www.gnu.org/copyleft/lesser.html GNU Lesser General Public License (LGPL)
 */

class Search_Controller extends Main_Controller {

	public function __construct()
	{
		parent::__construct();
	}
	
	/**
	 * Build a search query with relevancy
	 * Stop word control included
	 */
	public function index($page = 1)
	{
		$this->template->content = new View('search');
        $this->template->content->this_page = 'search';
		$smenu="";
		$search_query = "";
		$keyword_string = "";
		$where_string = "";
		$where_string2 = "";
		$plus = "";
		$or = "";
		$search_info = "";
		$html = "";
		$pagination = "";

		// Stop words that we won't search for
		// Add words as needed!!
		$stop_words = array('the', 'and', 'a', 'to', 'of', 'in', 'i', 'is', 'that', 'it',
			'on', 'you', 'this', 'for', 'but', 'with', 'are', 'have', 'be',
			'at', 'or', 'as', 'was', 'so', 'if', 'out', 'not'
		);

		if ($_GET)
		{
			/**
			 * NOTES: 15/10/2010 - Emmanuel Kala <emmanuel@ushahidi.com>
			 *
			 * The search string undergoes a 3-phase sanitization process. This is not optimal
			 * but it works for now. The Kohana provided XSS cleaning mechanism does not expel
			 * content contained in between HTML tags this the "bruteforce" input sanitization.
			 *
			 * However, XSS is attempted using Javascript tags, Kohana's routing mechanism strips
			 * the "<script>" tags from the URL variables and passes inline text as part of the URL
			 * variable - This has to be fixed
			 */

			// Phase 1 - Fetch the search string and perform initial sanitization
			$keyword_raw = (isset($_GET['k']))? preg_replace('#/\w+/#', '', $_GET['k']) : "";

			// Phase 2 - Strip the search string of any HTML and PHP tags that may be present for additional safety
			$keyword_raw = strip_tags($keyword_raw);

			// Phase 3 - Apply Kohana's XSS cleaning mechanism
			$keyword_raw = $this->input->xss_clean($keyword_raw);
		}
		else
		{
			$keyword_raw = "";
		}

		// Database instance
		$db = new Database();

		$keywords = explode(' ', $keyword_raw);
		if (is_array($keywords) AND ! empty($keywords)) 
		{
			array_change_key_case($keywords, CASE_LOWER);
			$i = 0;

			foreach($keywords as $value)
			{
				if ( ! in_array($value,$stop_words) AND ! empty($value))
				{
					// Escape the string for query safety
					$chunk = $db->escape_str($value);

					if ($i > 0)
					{
						$plus = ' + ';
						$or = ' OR ';
					}

					$where_string = $where_string.$or."(incident_title LIKE '%$chunk%' OR incident_description LIKE '%$chunk%')";
					$i++;
					$where_string2 = $where_string2.$or."(offerincident_title LIKE '%$chunk%' OR offerincident_description LIKE '%$chunk%')";
					$i++;
				}
			}
			
			if ( (!empty($where_string)) OR (!empty($where_string2)))
			{
				// Limit the result set to only those reports that have been approved
				$where_string = '(' . $where_string . ') AND i.incident_active = 1 ';
				$incidents_query = "SELECT i.id, i.incident_title AS title, m.media_thumb, i.incident_description AS description, i.incident_datemodify AS item_date, (CASE WHEN i.incident_active = 1 THEN 'listing/details' ELSE '' END) AS page, (CASE WHEN i.incident_active = 1 THEN '' ELSE '' END) AS featured, u.username AS username, u.public_profile FROM "
								. $this->table_prefix."incident AS i JOIN "
								.$this->table_prefix."users AS u ON (u.id = i.user_id ) LEFT JOIN "
								.$this->table_prefix."media AS m ON (m.incident_id = i.id ) "
								. "WHERE ".$where_string.""
								. "ORDER BY i.incident_date DESC ";
				
				// Limit the result set to only those offers that are active
				$where_string2 = '(' . $where_string2 . ') AND offerincident_start_date < NOW() AND offerincident_end_date > NOW() AND o.offerincident_active = 1 ';
				$offers_query = "SELECT o.id, o.offerincident_title AS title, m.media_thumb, o.offerincident_description AS description, o.offerincident_start_date AS item_date, (CASE WHEN o.offerincident_active = 1 THEN 'offer/details' ELSE '' END) AS page, (CASE WHEN o.offerincident_featured = 1 THEN 'featured' ELSE '' END) AS featured, u.username AS username, u.public_profile FROM "
								 . $this->table_prefix."offerincident AS o JOIN "
								 .$this->table_prefix."users AS u ON (u.id = o.user_id ) LEFT JOIN "
								 .$this->table_prefix."media AS m ON (m.offerincident_id = o.id ) "
								 . "WHERE ".$where_string2.""
								 . "GROUP BY o.id ORDER BY o.offerincident_date DESC ";
			}
		}

			if ( ! empty($incidents_query) OR ! empty($offers_query) )
		{
			
			$querys[] = $db->query($incidents_query);
				
			$querys[] = $db->query($offers_query);
			
			//merge the two queries
			//merge the two queries
			$results = array();
			if (isset($_GET["action"]) AND $_GET['action'] == "all"){
			foreach ($querys as $query) {
				$results = arr::merge($results,$query);
			}
			}elseif (isset($_GET["action"])AND $_GET["action"] == "places"){
				$results = arr::merge($querys[0]);
			}elseif (isset($_GET["action"])AND $_GET["action"] == "offers"){
				$results = arr::merge($querys[1]);
			}
			// Pagination
			$pagination = new Pagination(array(
									'query_string' => 'page',
									'items_per_page' => (int) Kohana::config('settings.items_per_page'),
									'total_items' => count($results)
			));
				
			if ((int) Kohana::config('settings.items_per_page') > 0) {
				$result_items = array_splice($results, $pagination->sql_offset, (int) Kohana::config('settings.items_per_page'));
			} else {
				$result_items = $results;
			}
			// Results Bar
			if ($pagination->total_items != 0)
			{
				$search_info .= "<div class=\"search_info\">"
							. Kohana::lang('ui_admin.showing_results')
							. ' '. ( $pagination->sql_offset + 1 )
							. ' '.Kohana::lang('ui_admin.to')
							//Set to show the exact number of items remaining in the pagination queue
							. ' '.( ( $pagination->sql_offset) + count($result_items) )
							. ' '.Kohana::lang('ui_admin.of').' '. $pagination->total_items
							. ' '.Kohana::lang('ui_admin.searching_for').' <strong>'. $keyword_raw . "</strong>"
							. "</div>";
			}else{
				$search_info .= "<div class=\"search_info\">0 ".Kohana::lang('ui_admin.results')."</div>";

				$html .= "<div class=\"search_result\">";
				$html .= "<h3>".Kohana::lang('ui_admin.your_search_for')."<strong> ".$keyword_raw."</strong> ".Kohana::lang('ui_admin.match_no_documents')."</h3>";
				$html .= "</div>";

				$pagination = "";
			}
		
			
			foreach ($result_items as $result)
			{
				$result_title = $result->title;
				$highlight_title = "";
				$result_title_arr = explode(' ', $result_title);
				/***----------Highlight-----------------***/
				$highlight_title = "";
				$result_title_arr = explode(' ', $result_title);

				foreach($result_title_arr as $value)
				{
					if (in_array(strtolower($value),$keywords) AND !in_array(strtolower($value),$stop_words))
					{
						$highlight_title .= "<span class=\"search_highlight\">" . $value . "</span> ";
					}
					else
					{
						$highlight_title .= $value . " ";
					}
				}

				$result_description = $result->description;

				// Remove any markup, otherwise trimming below will mess things up
				$result_description = strip_tags($result_description);

				// Trim to 180 characters without cutting words
				if ((strlen($result_description) > 180) AND (strlen($result_description) > 1))
				{
					$whitespaceposition = strpos($result_description," ",175)-1;
					$result_description = substr($result_description, 0, $whitespaceposition);
				}

				$highlight_description = "";
				$result_description_arr = explode(' ', $result_description);

				foreach($result_description_arr as $value)
				{
					if (in_array(strtolower($value),$keywords) && !in_array(strtolower($value),$stop_words))
					{
						$highlight_description .= "<span class=\"search_highlight\">" . $value . "</span> ";
					}
					else
					{
						$highlight_description .= $value . " ";
					}
				}
				/*--------------------------------------**/
				if($result->featured=="featured"){
					$html .="<div id=\"post_".$result->id."\" class=\"post-".$result->id." event type-event status-publish hentry post featured_post\">";
					$html .="<div class=\"post-content\">";
					$html .="<span class=\"featured_img\">featured</span>";
				}else{
					$html .="<div id=\"post_".$result->id."\" class=\"post-".$result->id." event type-event status-publish hentry post\">";
					$html .="<div class=\"post-content\">";
				}
				/*if($page =="reports"){
					$post_image = $this->_get_post_media($result->id,"incident_id");
				}else{
					$post_image = $this->_get_post_media($result->id,"offerincident_id");
				}*/
						
				if($result->media_thumb && url::exists(url::site() . Kohana::config('upload.relative_directory') . '/' . $result->media_thumb)){
					$html .="<a class=\"post_img\" href=\"".url::base().$result->page."/".$result->id."\"> <img src=\"".url::site() . Kohana::config('upload.relative_directory') . '/'.$result->media_thumb."\" alt=\"".$result->title."\" title=\"".$result->title."\"></a>";
			    }else{
					$html .="<a class=\"post_img\" href=\"".url::base().$result->page."/".$result->id."\"> <img src=\"".url::site() . Kohana::config('upload.relative_directory') . "/no_image_t.png\" alt=\"".$result->title."\" title=\"".$result->title."\"></a>";	
				}
				
				$html .="<h2><a href=\"".url::base().$result->page."/".$result->id."\">".$highlight_title."</a></h2>";
				
				if($result->public_profile == 1){
					$html .="<div class=\"post-meta listing_meta\"> By <span class=\"post-author\"> <a href=\"".url::base()."profile/user/".$result->username."\" title=\"Posts by ".$result->username."\">";
				}else{
					$html .="<div class=\"post-meta listing_meta\"> By <span class=\"post-author\"> <a href=\"#\" title=\"Posts by ".$result->username."\">";	
				}
				
				$item_date = date("F d Y",strtotime($result->item_date));
				
				$html .="".$result->username."</a> </span> on <span class=\"post-date\">".$item_date."</span>";
				
				if($result->page =="reports"){
					$total_reviews = ($this->_get_reviews($result->id,"incident_id")) ? $this->_get_reviews($result->id,"incident_id") : 0 ;
				}else{
					$total_reviews = ($this->_get_reviews($result->id,"offerincident_id")) ? $this->_get_reviews($result->id,"offerincident_id") : 0 ;
				}
				
				$html .="</div><div class=\"post_right\"><a href=\"".url::base().$result->page."/view/".$result->id."\" class=\"pcomments\">".$total_reviews."&nbsp;&nbsp;review </a> ";
				/**Rating**/
				
				if($result->page =="reports"){
					$rating = $this->_get_ratings($result->id,"incident_id");
				}else{
					$rating = $this->_get_ratings($result->id,"offerincident_id");
				}
				
				$html .="<span class=\"rating\">";
				 for ($i = 1; $i <= 5; $i++)
				 {
					 if ($rating >= $i){
						
						$html .="<img alt=\"* \" src=\"".url::site()."themes/pesatheme/images/dummy_pix/rating_on.png\" />";
					}else{
						$html .="<img alt=\"* \" src=\"".url::site()."themes/pesatheme/images/dummy_pix/rating_off.png\" />";
					}
				 }
				 $html .="</span>";
				/*---Add to favourite-----*/
				if ($page == 'offerreports'){
				    $html .="<span id=\"favorite_property_".$result->id."\" class=\"fav\"><a href=\"javascript:void(0);\" class=\"addtofav\" onclick=\"javascript:addToFavourite($result->id,'add');\">Collect Offer</a></span>";
                }
				$html .="</div>".$highlight_description . " ...<a href=\"".url::base().$result->page."/view/".$result->id."\" class=\"read_more\">Read more</a>";
				$html .="<div class=\"post_bottom list_bottom\"><span class=\"post-category\"></span></div></div></div>";
                
			}
			
		}else{
			// Results Bar
			$search_info .= "<div class=\"search_info\">0 ".Kohana::lang('ui_admin.results')."</div>";

			$html .= "<div class=\"search_result\">";
			$html .= "<h3>".Kohana::lang('ui_admin.your_search_for')."<strong>".$keyword_raw."</strong> ".Kohana::lang('ui_admin.match_no_documents')."</h3>";
			$html .= "</div>";
		}
		

		$html .= $pagination;
		
		$smenu .="<ul class=\"sort_by\">";
		$smenu .= "<li class=\"title\"> Sort by : </li>";
		$search_term = $_GET['k'];
			if($_GET['action'] =="all"){
				$smenu .= "<li class=\"current\"> <a href=\"".url::site()."search/?k=".$search_term."&action=all\">  All </a></li>";
				$smenu .="<li class=\"\"> <a href=\"".url::site()."search/?k=".$search_term."&action=places\">  Places </a></li>";
				$smenu .="<li class=\"\"> <a href=\"".url::site()."search/?k=".$search_term."&action=offers\">  Offers  </a></li>";
				
			}elseif($_GET['action'] =="offers"){
				$smenu .= "<li class=\"\"> <a href=\"".url::site()."search/?k=".$search_term."&action=all\">  All </a></li>";
				$smenu .="<li class=\"\"> <a href=\"".url::site()."search/?k=".$search_term."&action=places\">  Places </a></li>";
				$smenu .="<li class=\"current\"> <a href=\"".url::site()."search/?k=".$search_term."&action=offers\">  Offers  </a></li>";
				
			}elseif($_GET['action'] =="places"){
				$smenu .= "<li class=\"\"> <a href=\"".url::site()."search/?k=".$search_term."&action=all\">  All </a></li>";
				$smenu .="<li class=\"current\"> <a href=\"".url::site()."search/?k=".$search_term."&action=places\">  Places </a></li>";
				$smenu .="<li class=\"\"> <a href=\"".url::site()."search/?k=".$search_term."&action=offers\">  Offers  </a></li>";
				
			}
			$smenu .="</ul>";	
		$this->template->content->search_menu = $smenu;
		$this->template->content->search_info = $search_info;
		$this->template->content->search_keyword = $keyword_raw;
		$this->template->content->search_results = $html;

		// Rebuild Header Block
		$this->template->header->header_block = $this->themes->header_block();
		$this->template->footer->footer_block = $this->themes->footer_block();
	}
    
    
	private function _get_reviews($id,$type)
    	{
		$total_comments = ORM::factory('comment')->where($type, $id)->count_all();
    	   	
		return $total_comments;
    	}
	
	private function _get_post_media($id,$type)
    	{
		$image = ORM::factory('media')->where($type, $id);
    	   	return $image;
    	}
	
	private function _get_ratings($id,$type)
    	{
		$db = Database::instance();
		$average_rating = $db->query("SELECT AVG(rating) AS ave FROM rating WHERE $type = $id AND comment_id IS NOT NULL");
		$rated = round($average_rating[0]->ave);	
    	   	
		return $rated;
    	}
    
    
}
