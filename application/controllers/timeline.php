<?php defined('SYSPATH') or die('No direct script access.');
/**
 * This controller handles external requests for site widgets.
 *
 * @version 01 - Hezron Obuchele 2012-04-25
 *
 * PHP version 5
 * LICENSE: Not for reuse or modification without the express 
 * written authorization from BeeBuy Investments Ltd.
 *
 * Widget_Controller
 * @author     Hezron Obuchele
 * @package    Ushahidi - http://source.ushahididev.com
 * @subpackage Controllers
 * @copyright  BeeBuy Investments Ltd. - http://www.beebuy.biz
 */

class Timeline_Controller extends Controller {
	
	public function __construct()
	{
		
		// Call parent constructor
		parent::__construct();
	}
	
	/**
	 * Get latest 10 timeline entries
	 */
	public function index($start_after_time = null) {
	    if ($start_after_time)
            $start_after_time = date('Y-m-d h:i:s', $start_after_time);
		header("Content-type: application/json");
        $db = Database::instance();
        $response = array();
        // pull last 10 checkins in db
        $query = "SELECT checkin.id, name, checkin_date date, checkin_description description, longitude, latitude FROM checkin INNER JOIN location ON location.id = checkin.location_id INNER JOIN users ON users.id = checkin.user_id";
        // determine if updating or loading for the first time
        if(is_numeric($start_after_time)) {
            // pull subsequent 10 checkins
            $query .= " WHERE checkin.checkin_date > '$start_after_time'";
        }
        
        $query .= " ORDER BY checkin.checkin_date DESC LIMIT 10";
        $result = $db->query($query);
        
        foreach($result as $r) {
            $response[strtotime($r->date)] = array(
                            'id' => $r->id,
                            'name' => $r->name,
                            'desc' => $r->description,
                            'posted' => $r->date,
                            'lon' => $r->longitude,
                            'lat' => $r->latitude,
                            );
        }
        
        echo json_encode($response);
        exit;
        // pull last 10 offers in db
        $query = "SELECT name, checkin_date date, checkin_description description, CONCAT(longitude, ',', latitude) AS point FROM checkin INNER JOIN location ON location.id = checkin.location_id INNER JOIN users ON users.id = checkin.user_id";
        // if user has logged in pull offers based on their subscriptions
        if (Auth::instance()->logged_in()) {
            $query = "SELECT name, checkin_date date, checkin_description description, CONCAT(longitude, ',', latitude) AS point FROM checkin INNER JOIN location ON location.id = checkin.location_id INNER JOIN users ON users.id = checkin.user_id";
        }
        
        // determine if updating or loading for the first time
        if(($start_after_time)) {
            // pull subsequent 10 checkins
            $query .= " WHERE offerincident.offerincident_date > '$start_after_time'";
        }
        
        $query .= " ORDER BY offerincident.offerincident_date DESC LIMIT 10";
        $result = $db->query($query);
        
        foreach($result as $r) {
            $response[strtotime($r->date)] = array(
                            'id' => $r->id,
                            'name' => $r->name,
                            'desc' => $r->description,
                            'point' => $r->point
                            );
        }
        
        // sort array by date
        ksort($response);
        
        // return data to script
		echo json_encode($response);
	}
	
}
