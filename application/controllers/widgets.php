<?php defined('SYSPATH') or die('No direct script access.');
/**
 * This controller handles external requests for site widgets.
 *
 * @version 01 - Hezron Obuchele 2012-04-25
 *
 * PHP version 5
 * LICENSE: Not for reuse or modification without the express 
 * written authorization from BeeBuy Investments Ltd.
 *
 * Widget_Controller
 * @author     Hezron Obuchele
 * @package    Ushahidi - http://source.ushahididev.com
 * @subpackage Controllers
 * @copyright  BeeBuy Investments Ltd. - http://www.beebuy.biz
 */

class Widgets_Controller extends Template_Controller {
	
	/**
	 * Automatically render the views
	 * @var bool
	 */
	public $auto_render = TRUE;
	
	public function __construct()
	{
		// Switch template based on action
		$this->template = 'widgets/map_dynamic';
		
		//$this->template .=  () ;
		// Call parent constructor
		parent::__construct();

		// Cacheable controller
		$this->is_cachable = FALSE;
	}
	
	/**
	 * Starting point
	 */
	public function index()
	{
		echo '';
	}

    /**
     * Dynamic map widget
     */
    public function map($retailer_id = 0, $size = null)
    {
        if (intval($retailer_id) > 0){
            $size = explode('x', $size);
            $this->template->size = (count($size) == 2 && is_int($size[0]) && is_int($size[1])) ? $size : array(400,220);
            $this->template->center = array(Kohana::config('settings.default_lat'),Kohana::config('settings.default_lon'));
            $this->template->zoom = Kohana::config('settings.default_zoom');
            // Get the user's listings
            $db = Database::instance();
            $places = $db->query("SELECT incident_title, latitude, longitude, location_name FROM incident INNER JOIN
                location ON location.id = incident.location_id WHERE incident_active = 1 AND incident.user_id = {$retailer_id} GROUP BY incident.id ORDER BY incident_title ASC");
                
            $this->template->markers = array();
            foreach ($places as $place) {
                $title = $place->incident_title;
                $coords = $place->latitude . ',' . $place->longitude;
                $this->template->markers[$title] = array($coords, $place->location_name);
            }
        }
    }
	
	/**
	 * Starting point
	 */
	public function staticmap($location_id, $size = null)
	{
		header("Content-type: image/png");
		$this->template->size = ($size) ? $size : '500x300';
		$this->template->zoom = 10;
		$this->template->markers = array(array('type' => 'offer', 'coords' => '-1.28,36.9'),array('type' => 'offer', 'coords' => '-1.41,36.9'),array('type' => 'offer', 'coords' => '-1.35,36.7'));
		$this->template->coords = array('dest' => '-1.27,36.68', 'origin' => '-1.42,36.952');
	}
}
