<?php defined('SYSPATH') OR die('No direct access allowed.');
/**
 * Blocks helper class.
 *
 * @package    Admin
 * @author     Ushahidi Team
 * @copyright  (c) 2011 Ushahidi Team
 * @license    http://www.ushahidi.com/license.html
 */
class blocks_Core {
	
    private static $_blocks = null;
	/**
	 * Open A Block
	 *
	 * @return string
	 */
	public static function open($id = NULL)
	{
		if ($id)
		{
			echo "<li id=\"block-".$id."\"><div>";
		}
		else
		{
		  echo "<li><div>";
		}
		
	}
	
	/**
	 * Close A Block
	 *
	 * @return string
	 */
	public static function close()
	{
		echo "</div></li>";
	}
	
	/**
	 * Block Title
	 *
	 * @return string
	 */
	public static function title($title = NULL)
	{
		if ($title)
		{
			echo "<h5>$title</h5>";
		}
	}
	
	/**
	 * Register A Block
	 *
	 * @param array $block an array with classname, name and description
	 */
	public static function register($block = array())
	{
		// global variable that contains all the blocks
		$blocks = Kohana::config("settings.blocks");
		if ( ! is_array($blocks) )
		{
			$blocks = array();
		}
		
		if ( is_array($block) AND 
			array_key_exists("classname", $block) AND 
			array_key_exists("name", $block) AND 
			array_key_exists("description", $block) )
		{
			if ( ! array_key_exists($block["classname"], $blocks))
			{
				$blocks[$block["classname"]] = array(
					"name" => $block["name"],
					"description" => $block["description"]
				);
			}
		}
		asort($blocks);
		Kohana::config_set("settings.blocks", $blocks);
	}
	
	/**
	 * Render all the active blocks
	 *
	 * @return string block html
	 */	
	public static function render()
	{
		// Get Active Blocks
		$settings = ORM::factory('settings', 1);
		$active_blocks = $settings->blocks;
		$active_blocks = array_filter(explode("|", $active_blocks));
        // Capture rendered block conent
        ob_start();
		foreach ($active_blocks as $block)
		{
			$block = new $block();
			
			$block->block();
		}
        $all_blocks = ob_get_contents();
        ob_end_clean();
        
        $blocks = Database::instance()->select('name, ordering, position, pages')->from('content_blocks')->in('name', $active_blocks)->get();
        $blocks_db = array();
        foreach ($blocks as $block){
            $blocks_db[$block->name] = array('ordering' => $block->ordering, 'position' => $block->position, 'pages' => explode('|', $block->pages));
        }
        $blocks = array(); // associative array containing block names as keys and their data
        $block_indices = array();// array to track indices of the blocks as the blocks array is associative
        // Generate $blocks array based on active blocks & track each block item's index
        foreach($active_blocks as $block){
            $item = str_ireplace('_block', '', $block);
            $blocks[$item] = array('pattern' => '#\<li id="' . 'block-' . $item . '"\>(.*)\<\/li\>#s', 'content' => '', 'position' => $blocks_db[$block]['position'], 'pages' => $blocks_db[$block]['pages'], 'ordering' => $blocks_db[$block]['ordering']);
            $block_indices[] = $item;
        }
        
        // extract in reverse order as first pattern matches everything to the last block
        $blocks = array_reverse($blocks);
        $block_indices = array_reverse($block_indices, true); //true ensures the keys->value associations are reserved on reverse
        $current_block = count($blocks)-1;
        foreach($blocks as $name => &$data){
            if ($current_block == 0){
                $data['content'] = preg_replace($blocks[$block_indices[$current_block+1]]['pattern'], '', $all_blocks);
            }else{
                preg_match_all($data['pattern'], $all_blocks, $matches);
                $data['content'] = $matches[0][0];
                //if not last block replace previous block content in all_blocks with an empty string to the end
                if ($current_block != count($blocks)-1)
                    $data['content'] = preg_replace($blocks[$block_indices[$current_block+1]]['pattern'], '', $data['content']);
            }
            $data['content'] = str_ireplace('<li id="block-' . $name . '">', '', $data['content']);
            $data['content'] = str_ireplace('</li>', '', $data['content']);
            $current_block--;
        }
        self::$_blocks = $blocks;
	}
    
    /**
     * Render all content blocks in the given position for the current page
     * @param $position content area on the template to output the blocks in e.g. right
     * @param $current_page current page being displayed
     *
     * @return void (echoes content)
     */
    public static function render_position($position, $current_page) {
        if (!self::$_blocks)
            self::render();
        $blocks_to_render = array();
        foreach (self::$_blocks as $name => $data){
            if ((array_search($current_page, $data['pages']) !== false || array_search('all', $data['pages']) !== false) && $data['position'] == strtolower($position))
                $blocks_to_render[$data['ordering']] = $data['content'];
        }
        ksort($blocks_to_render);
        foreach ($blocks_to_render as $block)
            echo $block;
    }
	
	/**
	 * Sort Active and Non-Active Blocks
	 * 
	 * @param array $active array of active blocks
	 * @param array $registered array of all blocks
	 * @return array merged and sorted array of active and inactive blocks
	 */
	public static function sort($active = array(), $registered = array())
	{
		// Remove Empty Keys
		$active = array_filter($active);
		$registered = array_filter($registered);
		
		$sorted_array = array();
		$sorted_array = array_intersect($active, $registered);
		return array_merge($sorted_array, array_diff($registered, $sorted_array));
	}
}