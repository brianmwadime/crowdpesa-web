<?php 
/**
 * Offerreports Helper class.
 *
 * This class holds functions used for new offerreport submission from both the backend and frontend.
 *
 * PHP version 5
 * LICENSE: This source file is subject to LGPL license
 * that is available through the world-wide-web at the following URI:
 * http://www.gnu.org/copyleft/lesser.html
 * @author     BeeBuy Team <info@beebuy.com>
 * @package    CrowdPesa - http://crowdpesa.com
 * @offercategory   Helpers
 * @copyright  CrowdPesa - http://crowdpesa.com
 * @license    http://www.gnu.org/copyleft/lesser.html GNU Lesser General Public License (LGPL)
 */
class blog_Core {
	
	/**
	 * Maintains the list of parameters used for fetching offerincidents
	 * in the fetch_offerincidents method
	 * @var array
	 */
	public static $params = array();
	
	/**
	 * Function to save news, photos and videos
	 *
	 * @param mixed $location_model
	 * @param mixed $post
	 *
	 */
	public static function save_media($post, $article)
	{
		// Delete Previous Entries
		ORM::factory('media')->where('article_id',$article->id)->where('media_type <> 1')->delete_all();
		
		
		//print_r($post); exit;
		// a. Photos
		$filenames = upload::save('article_photo');
		$i = 1;
		foreach ($filenames as $filename)
		{
			$new_filename = $article->id.'_'.$i.'_'.time();

			$file_type = strrev(substr(strrev($filename),0,4));
					
			// IMAGE SIZES: 800X600, 400X300, 89X59
					
			// Large size
			Image::factory($filename)->resize(800,600,Image::AUTO)
				->save(Kohana::config('upload.directory', TRUE).$new_filename.$file_type);

			// Medium size
			Image::factory($filename)->resize(290,240,Image::NONE)
				->save(Kohana::config('upload.directory', TRUE).$new_filename.'_m'.$file_type);
					
			// Thumbnail
			Image::factory($filename)->resize(158,110,Image::NONE)
				->save(Kohana::config('upload.directory', TRUE).$new_filename.'_t'.$file_type);
				
			// Name the files for the DB
			$media_link = $new_filename.$file_type;
			$media_medium = $new_filename.'_m'.$file_type;
			$media_thumb = $new_filename.'_t'.$file_type;
				
			// Okay, now we have these three different files on the server, now check to see
			//   if we should be dropping them on the CDN
			
			if (Kohana::config("cdn.cdn_store_dynamic_content"))
			{
				$media_link = cdn::upload($media_link);
				$media_medium = cdn::upload($media_medium);
				$media_thumb = cdn::upload($media_thumb);
				
				// We no longer need the files we created on the server. Remove them.
				$local_directory = rtrim(Kohana::config('upload.directory', TRUE), '/').'/';
				unlink($local_directory.$new_filename.$file_type);
				unlink($local_directory.$new_filename.'_m'.$file_type);
				unlink($local_directory.$new_filename.'_t'.$file_type);
			}

			// Remove the temporary file
			unlink($filename);

			// Save to DB
			$photo = new Media_Model();
			$photo->article_id = $article->id;
			$photo->media_type = 1; // Images
			$photo->media_link = $media_link;
			$photo->media_medium = $media_medium;
			$photo->media_thumb = $media_thumb;
			$photo->media_date = date("Y-m-d H:i:s",time());
			$photo->save();
			$i++;
		}
	}
	public static function save_tags($posted_tags, $article)
	{
		// Delete Previous Entries
		ORM::factory('article_tags')->where('article_id',$article->id)->delete_all();
		foreach($posted_tags as &$item){
			$tag = new Article_Tags_Model();
			$tag->article_id = $article->id;
			$tag->tag = trim($item);
			$tag->save();
			
		}
		
		
	}
	
	
}
?>
