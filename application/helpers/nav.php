<?php defined('SYSPATH') OR die('No direct access allowed.');
/**
 * Front-End Nav helper class.
 *
 * @package    Nav
 * @author     Ushahidi Team
 * @copyright  (c) 2008 Ushahidi Team
 * @license    http://www.ushahidi.com/license.html
 */
class nav_Core {
	
	
    private static function _place_categories(){
        // Get all active top level categories
        $parent_categories = array();
        // Get locale
        $l = Kohana::config('locale.language.0');
        foreach (ORM::factory('category')
                ->where('category_visible', '1')
                ->where('id != 5')
                ->where('parent_id', '0')
                ->find_all() as $category)
        {
            // Get The Children
            $children = array();
            foreach ($category->children as $child)
            {
                $child_visible = $child->category_visible;
                if ($child_visible)
                {
                    // Check for localization of child category
                    $display_title = Category_Lang_Model::category_title($child->id,$l);
                    $ca_img = ($child->category_image != NULL) ? url::convert_uploaded_to_abs($child->category_image) : NULL;
                    $children[$child->id] = array(
                        $display_title,
                        $child->category_color,
                        $ca_img
                    );
                }
            }

            // Check for localization of parent category
            $display_title = Category_Lang_Model::category_title($category->id,$l);

            // Put it all together
            $ca_img = ($category->category_image != NULL) ? url::convert_uploaded_to_abs($category->category_image) : NULL;
            $parent_categories[$category->id] = array(
                $display_title,
                $category->category_color,
                $ca_img,
                $children
            );
        }
        return $parent_categories;
    }

    private static function _offer_categories(){
        // Get all active top level categories
        $parent_categories = array();
        // Get locale
        $l = Kohana::config('locale.language.0');
        foreach (ORM::factory('offercategory')
                ->where('offercategory_visible', '1')
                ->where('id != 5')
                ->where('parent_id', '0')
                ->find_all() as $category)
        {
            // Get The Children
            $children = array();
            foreach ($category->children as $child)
            {
                $child_visible = $child->offercategory_visible;
                if ($child_visible)
                {
                    // Check for localization of child category
                    $display_title = Offercategory_Lang_Model::offercategory_title($child->id,$l);
                    $ca_img = ($child->offercategory_image != NULL) ? url::convert_uploaded_to_abs($child->offercategory_image) : NULL;
                    $children[$child->id] = array(
                        $display_title,
                        $child->offercategory_color,
                        $ca_img
                    );
                }
            }

            // Check for localization of parent category
            $display_title = Offercategory_Lang_Model::offercategory_title($category->id,$l);

            // Put it all together
            $ca_img = ($category->offercategory_image != NULL) ? url::convert_uploaded_to_abs($category->offercategory_image) : NULL;
            $parent_categories[$category->id] = array(
                $display_title,
                $category->offercategory_color,
                $ca_img,
                $children
            );
        }
        return $parent_categories;
    }
    
    /**
     * Render a submenu in megamenu form with all categories not having children in one block
     * If all categories have no children, it returns the submenu in simple list form  
     * @param array $categries items to display on submenu
     * @param string $context site context to put on urls
     * @return string $sub_menu html for the submenu 
     */
     
    private static function _render_sub_menu_categories($categories, $context)
    {
        if (!is_array($categories) || count($categories) == 0)
            return '';
        $is_mega_menu = false; // Determines the menu type to be returned based on child content
        $category_title_other = "Others";
        $other_category = array();// Holds categories without children
        // Open mega menu row
        $sub_menu = '<div class="sub-container mega" style="top: 40px;"><ul class="sub-menu sub">';
        $sub_menu .= '<div class="row">';
        foreach ($categories as $category => $category_info) {
            $category_title = $category_info[0];
            // Render children if they exist
            if( sizeof($category_info[3]) != 0)
            {
                $is_mega_menu = true;
                $sub_menu .= '<li class="menu-item mega-hdr"><a class="mega-hdr-a" href="#">' . $category_title .'</a><ul class="sub-menu">';
                foreach ($category_info[3] as $child => $child_info)
                {
                        $child_title = $child_info[0];
                        $sub_menu .= '<li class="menu-item"><a href="'. url::site() .  '?context=' . $context . '&category=' . $child . '">' . $child_title .'</a></li>';
                }
                $sub_menu .= '</ul></li>';
            }else{
                $other_category[$category] = $category_title;
            }
        }
        // Render categories without chldren
        $sub_menu .= '<li class="menu-item mega-hdr"><a class="mega-hdr-a" href="#">' . $category_title_other .'</a><ul class="sub-menu">';
        foreach ($other_category as $cat_id => $cat_title)
        {
                $sub_menu .= '<li class="menu-item"><a href="'. url::site() .  '?context=' . $context . '&category=' . $cat_id . '">' . $cat_title .'</a></li>';
        }
        $sub_menu .= '</ul></li>';
        // Close megamenu row
        $sub_menu .= '</div></ul></div>';
        // Return menu in simple list form if no category contains any children 
        if(!$is_mega_menu)
            $sub_menu = self::_render_sub_menu_categories_non_mega($categories, $context);
        return $sub_menu;
    }
    
    /**
     * Render a submenu in megamenu form with all categories having their own block  
     * @param array $categries items to display on submenu
     * @param string $context site context to put on urls
     * @return string $sub_menu html for the submenu 
     */
    
    private static function _render_sub_menu_categories_alternative($categories, $context)
    {
        if (!is_array($categories) || count($categories) == 0)
            return '';
        $sub_menu = '<div class="sub-container mega" style="top: 40px;"><ul class="sub-menu sub">';
        $sub_menu .= '<div class="row">';
        foreach ($categories as $category => $category_info) {
            $category_title = $category_info[0];
            $sub_menu .= '<li class="menu-item mega-hdr"><a class="mega-hdr-a" href="#">' . $category_title .'</a>';
            // Get Children
            if( sizeof($category_info[3]) != 0)
            {
                $sub_menu .= '<ul class="sub-menu">';
                foreach ($category_info[3] as $child => $child_info)
                {
                        $child_title = $child_info[0];
                        $sub_menu .= '<li class="menu-item"><a href="'. url::site() .  '?context=' . $context . '&category=' . $child . '">' . $child_title .'</a></li>';
                }
                $sub_menu .= '</ul>';
            }else{
                $sub_menu .= '<ul class="sub-menu">';
                $sub_menu .= '<li class="menu-item"><a href="'. url::site() .  '?context=' . $context . '&category=' . $category . '">' . $category_title .'</a></li>';
                $sub_menu .= '</ul>';
            }
            $sub_menu .= '</li>';
        }
        
        $sub_menu .= '</div></ul></div>';
        return $sub_menu;
    }

    /**
     * Render a submenu in simple list form 
     * @param array $categries items to display on submenu
     * @param string $context site context to put on urls
     * @return string $sub_menu html for the submenu 
     */
     
    private static function _render_sub_menu_categories_non_mega($categories, $context)
    {
        if (!is_array($categories) || count($categories) == 0)
            return '';
        $sub_menu = '<div class="sub-container non-mega" style="top: 40px;"><ul class="sub-menu sub">';
        foreach ($categories as $category => $category_info) {
            $category_title = $category_info[0];
            $sub_menu .= '<li class="menu-item"><a href="'. url::site() .  '?context=' . $context . '&category=' . $category . '">' . $category_title .'</a></li>';
        }
        $sub_menu .= '</ul></div>';
        return $sub_menu;
    }
    
    /**
     * Generate Main Tabs
     * @param string $this_page
     * @param array $dontshow
     * @return string $menu
     */
	public static function main_tabs($this_page = FALSE, $dontshow = FALSE)
	{
		$menu = "";
		
		if( ! is_array($dontshow))
		{
			// Set $dontshow as an array to prevent errors
			$dontshow = array();
		}
		
		// Home
		if( ! in_array('home',$dontshow))
		{
			$menu .= "<li class=\"menu-item\"><a href=\"".url::site()."main\" ";
			$menu .= ($this_page == 'home') ? " class=\"active\"" : "";
		 	$menu .= ">".Kohana::lang('ui_main.home')."</a></li>";
		 }

		// Reports List
		/*if( ! in_array('reports',$dontshow))
		{
			$menu .= "<li class=\"menu-item\"><a class=\"mega\" href=\"".url::site()."?context=places\" ";//$menu .= "<li><a href=\"".url::site()."reports\" ";
			$menu .= ($this_page == 'reports') ? " class=\"active mega-menu-link\"" : " class=\"mega-menu-link\"";
		 	$menu .= ">".Kohana::lang('ui_main.reports')."</a>";
            $menu .= self::_render_sub_menu_categories(self::_place_categories(), 'places');
            $menu .= "</li>";
		 }*/
        
        // Places
        if( ! in_array('places',$dontshow))
        {
            $menu .= "<li class=\"menu-item\"><a class=\"dc-mega\" href=\"#\"";//.url::site()."?context=places\" ";
            //$menu .= ($this_page == 'reports') ? " class=\"active\"" : "";
            $menu .= ">".Kohana::lang('ui_main.reports')."<span class=\"dc-mega-icon\"></span></a>";
            $menu .= self::_render_sub_menu_categories(self::_place_categories(), 'places');
            $menu .= "</li>";
         }
        
        // Offers
        if( ! in_array('offers',$dontshow))
        {
            $menu .= "<li class=\"menu-item\"><a class=\"dc-mega\" href=\"#\"";//.url::site()."?context=offers\" ";
            //$menu .= ($this_page == 'offers') ? " class=\"active\"":"";
            $menu .= ">".Kohana::lang('ui_main.offers')."<span class=\"dc-mega-icon\"></span></a>";
            $menu .= self::_render_sub_menu_categories(self::_offer_categories(), 'offers');
            $menu .= "</li>";
        }
        
        // User menus
       // Account		
        if( ! in_array('account',$dontshow) && (Auth::instance()->logged_in('member') || Auth::instance()->logged_in('customer') || Auth::instance()->logged_in('retailer')))
        {
            $menu .= "<li class=\"menu-item\"><a class=\"dc-mega\" href=\"".''."\" ";
            //$menu .= ($this_page == 'reports') ? " class=\"active\"" : "";
            $menu .= ">".Kohana::lang('ui_main.account')."<span class=\"dc-mega-icon\"></span></a>";
			$menu .='<div class="sub-container non-mega" style="top: 40px;"><ul class="sub-menu sub">';
			if(Auth::instance()->logged_in('member') || Auth::instance()->logged_in('customer')){
    			$menu .= '<li class="menu-item"><a href="'.url::site().'customers/profile'. '">' . 'Edit Profile' .'</a></li>';
                $menu .= '<li class="menu-item"><a href="'.url::site().'customers/invite'. '">' . 'Invite your friends' .'</a></li>';
                $menu .= '<li class="menu-item"><a href="'.url::site().'customers/offerreports/myoffers/?status=a'. '">' . 'Collected Offers' .'</a></li>';
    			$menu .= '<li class="menu-item"><a href="'.url::site().'customers/checkins'. '">' . 'My Checkins' .'</a></li>';	
    			$menu .= '<li class="menu-item"><a href="'.url::site().'customers/subscriptions'. '">' . 'Subscriptions' .'</a></li>';	
			}
			else if(Auth::instance()->logged_in('retailer')){
				$menu .= '<li class="menu-item"><a href="'.url::site().'retailers/dashboard'. '">' . 'Dashboard' .'</a></li>';
				$menu .= '<li class="menu-item"><a href="'.url::site().'retailers/offerreports'. '">' . 'My Offers' .'</a></li>';
    			$menu .= '<li class="menu-item"><a href="'.url::site().'retailers/reports'. '">' . 'My Places' .'</a></li>';	
    			$menu .= '<li class="menu-item"><a href="'.url::site().'retailers/collectedoffers'. '">' . 'Orders' .'</a></li>';
			}
        	$menu .= '</ul></div>';
            
            $menu .= "</li>";
			
			
		 
         }
		
		// Reports Submit
		if( ! in_array('reports_submit',$dontshow))
		{
			if (Kohana::config('settings.allow_reports'))
			{
				$menu .= "<li class=\"menu-item\"><a href=\"".url::site()."reports/submit\" ";
				$menu .= ($this_page == 'reports_submit') ? " class=\"active\"":"";
			 	$menu .= ">".Kohana::lang('ui_main.submit')."</a></li>";
			}
		}
		
		// Alerts
		if(! in_array('alerts',$dontshow))
		{
			if(Kohana::config('settings.allow_alerts'))
			{
				$menu .= "<li class=\"menu-item\"><a href=\"".url::site()."alerts\" ";
				$menu .= ($this_page == 'alerts') ? " class=\"active\"" : "";
				$menu .= ">".Kohana::lang('ui_main.alerts')."</a></li>";
			}
		}
		
		// Custom Pages
		$pages = ORM::factory('page')->where('page_active', '1')->find_all();
		foreach ($pages as $page)
		{
			$menu .= "<li class=\"menu-item\"><a href=\"".url::site()."page/index/".$page->id."\" ";
			$menu .= ($this_page == 'page_'.$page->id) ? " class=\"active\"" : "";
		 	$menu .= ">".$page->page_tab."</a></li>";
		}
        
        // Contacts
        if( ! in_array('contact',$dontshow))
        {
            if (Kohana::config('settings.site_contact_page'))
            {
                $menu .= "<li class=\"menu-item\"><a href=\"".url::site()."contact\" ";
                $menu .= ($this_page == 'contact') ? " class=\"active\"" : "";
                $menu .= ">".Kohana::lang('ui_main.contact')."</a></li>";   
            }
        }

		echo $menu;
		
		// Action::nav_admin_reports - Add items to the admin reports navigation tabs
		Event::run('ushahidi_action.nav_main_top', $this_page);
	}
	
	
}

