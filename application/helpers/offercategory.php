<?php
/**
 * Offercategory helper. Displays offercategories on the front-end.
 *
 * @package    Offercategory
 * @author     Ushahidi Team
 * @copyright  (c) 2008 Ushahidi Team
 * @license    http://www.ushahidi.com/license.html
 */
class offercategory_Core {

	/**
	 * Displays a single offercategory checkbox.
	 */
	public static function display_offercategory_checkbox($offercategory, $selected_offercategories, $form_field, $enable_parents = FALSE)
	{
		$html = '';

		$cid = $offercategory->id;

		// Get locale
		$l = Kohana::config('locale.language.0');
		
		$offercategory_title = Offercategory_Lang_Model::offercategory_title($cid, $l);

		//$offercategory_title = $offercategory->offercategory_title;
		$offercategory_color = $offercategory->offercategory_color;

		// Offercategory is selected.
		$offercategory_checked = in_array($cid, $selected_offercategories);
		
		// Visible Child Count
		$vis_child_count = 0;
		foreach ($offercategory->children as $child)
		{
			$child_visible = $child->offercategory_visible;
			if ($child_visible)
			{
				// Increment Visible Child count
				++$vis_child_count;
			}
		}

		$disabled = "";
		if (!$enable_parents AND $offercategory->children->count() > 0 AND $vis_child_count >0)
		{
			$disabled = " disabled=\"disabled\"";	
		}

		$html .= form::checkbox($form_field.'[]', $cid, $offercategory_checked, ' class="check-box"'.$disabled);
		$html .= $offercategory_title;

		return $html;
	}


	/**
	 * Display offercategory tree with input checkboxes.
	 */
	public static function tree($offercategories, array $selected_offercategories, $form_field, $columns = 1, $enable_parents = FALSE)
	{
		$html = '';

		// Validate columns
		$columns = (int) $columns;
		if ($columns == 0)
		{
			$columns = 1;
		}

		$offercategories_total = $offercategories->count();

		// Format offercategories for column display.
		$this_col = 1; // column number
		$maxper_col = round($offercategories_total/$columns); // Maximum number of elements per column
		$i = 1;  // Element Count
		foreach ($offercategories as $offercategory)
		{

			// If this is the first element of a column, start a new UL
			if ($i == 1)
			{
				$html .= '<ul id="offercategory-column-'.$this_col.'">';
			}

			// Display parent offercategory.
			$html .= '<li>';
			$html .= offercategory::display_offercategory_checkbox($offercategory, $selected_offercategories, $form_field, $enable_parents);
			
			// Visible Child Count
			$vis_child_count = 0;
			foreach ($offercategory->children as $child)
			{
				$child_visible = $child->offercategory_visible;
				if ($child_visible)
				{
					// Increment Visible Child count
					++$vis_child_count;
				}
			}
			// Display child offercategories.
			if ($offercategory->children->count() > 0 AND $vis_child_count > 0)
			{
				$html .= '<ul>';
				foreach ($offercategory->children as $child)
				{
					$child_visible = $child->offercategory_visible;
					if ($child_visible)
					{
						$html .= '<li>';
						$html .= offercategory::display_offercategory_checkbox($child, $selected_offercategories, $form_field, $enable_parents);
					}
				}
				$html .= '</ul>';
			}
			$i++;

			// If this is the last element of a column, close the UL
			if ($i > $maxper_col || $i == $offercategories_total)
			{
				$html .= '</ul>';
				$i = 1;
				$this_col++;
			}
		}

		return $html;
	}
	
	/**
	 * Generates a offercategory tree view - recursively iterates
	 *
	 * @return string
	 */
	public static function get_offercategory_tree_view()
	{
		// To hold the offercategory data
		$offercategory_data = array();
		
		// Database table prefix
		$table_prefix = Kohana::config('database.default.table_prefix');
		
		// Database instance
		$db = new Database();
		
		// Fetch all the top level parent offercategories
		foreach (Offercategory_Model::get_offercategories(0,FALSE,TRUE) as $offercategory)
		{
			self::_extend_offercategory_data($offercategory_data, $offercategory);
		}
		
		// NOTES: Emmanuel Kala - Aug 5, 2011
		// Initialize the offerreport totals for the parent offercategories just in case
		// the deployment does not have sub-offercategories in which case the query
		// below won't return a result
		self::_init_parent_offercategory_offerreport_totals($offercategory_data, $table_prefix);
		
		// Query to fetch the offerreport totals for the parent offercategories
		$sql = "SELECT c2.id,  COUNT(DISTINCT ic.offerincident_id)  AS offerreport_count "
			. "FROM ".$table_prefix."offercategory c, ".$table_prefix."offercategory c2, ".$table_prefix."offerincident_offercategory ic "
			. "INNER JOIN ".$table_prefix."offerincident i ON (ic.offerincident_id = i.id) "
			. "WHERE (ic.offercategory_id = c.id OR ic.offercategory_id = c2.id) "
			. "AND c.parent_id = c2.id "
			. "AND i.offerincident_active = 1 "
			. "AND c2.offercategory_visible = 1 "
			. "AND c.offercategory_visible = 1 "
			. "AND c2.parent_id = 0 "
			. "AND c2.offercategory_title != \"NONE\""
			. "GROUP BY c2.id "
			. "ORDER BY c2.id ASC";
		
		// Update the offerreport_count field of each top-level offercategory
		foreach ($db->query($sql) as $offercategory_total)
		{
			// Check if the offercategory exists
			if (array_key_exists($offercategory_total->id, $offercategory_data))
			{
				// Update
				$offercategory_data[$offercategory_total->id]['offerreport_count'] = $offercategory_total->offerreport_count;
			}
		}
		
		// Fetch the other offercategories
		$sql = "SELECT c.id, c.parent_id, c.offercategory_title, c.offercategory_color, COUNT(c.id) offerreport_count "
			. "FROM ".$table_prefix."offercategory c "
			. "INNER JOIN ".$table_prefix."offerincident_offercategory ic ON (ic.offercategory_id = c.id) "
			. "INNER JOIN ".$table_prefix."offerincident i ON (ic.offerincident_id = i.id) "
			. "WHERE c.offercategory_visible = 1 "
			. "AND c.offercategory_title != \"NONE\" "
			. "AND i.offerincident_active = 1 "
			. "GROUP BY c.offercategory_title "
			. "ORDER BY c.offercategory_title ASC";
		
		// Add child offercategories
		foreach ($db->query($sql) as $offercategory)
		{
			// Extend the offercategory data array
			self::_extend_offercategory_data($offercategory_data, $offercategory);
			
			if (array_key_exists($offercategory->parent_id, $offercategory_data))
			{
				// Add children
				$offercategory_data[$offercategory->parent_id]['children'][$offercategory->id] = array(
					'offercategory_title' => Offercategory_Lang_Model::offercategory_title($offercategory->id,Kohana::config('locale.language.0')),
					'parent_id' => $offercategory->parent_id,
					'offercategory_color' => $offercategory->offercategory_color,
					'offerreport_count' => $offercategory->offerreport_count,
					'children' => array()
				);
			}
		}
		
		// Generate and return the HTML
		return self::_generate_treeview_html($offercategory_data);
	}
	
	/**
	 * Helper method for adding parent offercategories to the offercategory data
	 *
	 * @param array $array Pointer to the array containing the offercategory data
	 * @param mixed $offercategory Object Offercategory object to be added tot he array
	 */
	private static function _extend_offercategory_data(array & $array, $offercategory)
	{
		// Check if the offercategory is a top-level parent offercategory
		$temp_offercategory = ($offercategory->parent_id == 0) ? $offercategory : ORM::factory('offercategory', $offercategory->parent_id);

		if ($temp_offercategory instanceof Offercategory_Model AND ! $temp_offercategory->loaded)
			return FALSE;

		// Extend the array
		if ( ! array_key_exists($temp_offercategory->id, $array))
		{
			// Get the offerreport count
			$offerreport_count = property_exists($temp_offercategory, 'offerreport_count')? $temp_offercategory->offerreport_count : 0;
			
			$array[$temp_offercategory->id] = array(
				'offercategory_title' => Offercategory_Lang_Model::offercategory_title($temp_offercategory->id,Kohana::config('locale.language.0')),
				'parent_id' => $temp_offercategory->parent_id,
				'offercategory_color' => $temp_offercategory->offercategory_color,
				'offerreport_count' => $offerreport_count,
				'children' => array()
			);

			return TRUE;
		}

		return FALSE;
	}
	
	/**
	 * Traverses an array containing offercategory data and returns a tree view
	 *
	 * @param array $offercategory_data
	 * @return string
	 */
	private static function _generate_treeview_html($offercategory_data)
	{
		// To hold the treeview HTMl
		$tree_html = "";
		//**************************************************************
		$user_id = 0;
		$auth = new Auth();
		$logged_in_status = $auth->logged_in();
		if($logged_in_status){
			$user = $auth->get_user();
			$user_id = $user->id;
		}
		//**************************************************************
		foreach ($offercategory_data as $id => $offercategory)
		{
			// Determine the offercategory class
			$offercategory_class = ($offercategory['parent_id'] > 0)? " class=\"offerreport-listing-offercategory-child\"" : "";
			//**************************************************************
			$subscribe_link = '<span class="item-subscribe" onClick="Sub('.$id.')">[Subscribe]</span>';
			if(Offersubscription_Model::is_subscribed($user_id, $id) && $logged_in_status){
				$subscribe_link = '<span class="item-subscribe" onClick="Unsub('.$id.')">[Unsubscribe]</span>';
			}else if($logged_in_status){
				$subscribe_link = '<span class="item-subscribe" onClick="Sub('.$id.')">[Subscribe]</span>';
			}
			//**************************************************************
			$tree_html .= "<li".$offercategory_class.">"
							. "<a href=\"#\" class=\"cat_selected\" id=\"filter_link_cat_".$id."\">"
							. "<span class=\"item-swatch\" style=\"background-color: #".$offercategory['offercategory_color']."\">&nbsp;</span>"
							. "<span class=\"item-title\">".strip_tags($offercategory['offercategory_title'])."</span>"
							. "<span class=\"item-count\">".$offercategory['offerreport_count']."</span>"
							. $subscribe_link
							. "</a></li>";//********************************
							
			$tree_html .= self::_generate_treeview_html($offercategory['children']);
		}
		
		$subscribe_js = '<script type="text/javascript">
			function Sub(id){
				var url = "'.url::base().'" + "offerreports/subscribe_user/" + id;
				window.location = url;
			}

			function Unsub(id){
				var url = "'.url::base().'" + "offerreports/unsubscribe_user/" + id;
				window.location = url;
			}
			</script>';
		
		// Return
		return $subscribe_js.$tree_html;
	}
	
	/**
	 * Initializes the offerreport totals for the parent offercategories
	 *
	 * @param array $offercategory_data Array of the parent offercategories
	 * @param string $table_prefix Database table prefix
	 */
	private static function _init_parent_offercategory_offerreport_totals(array & $offercategory_data, $table_prefix)
	{
		// Query to fetch the offerreport totals for the parent offercategories
		$sql = "SELECT c.id, COUNT(DISTINCT ic.offerincident_id) AS offerreport_count "
			. "FROM ".$table_prefix."offercategory c "
			. "INNER JOIN ".$table_prefix."offerincident_offercategory ic ON (ic.offercategory_id = c.id) "
			. "INNER JOIN ".$table_prefix."offerincident i ON (ic.offerincident_id = i.id) "
			. "WHERE c.offercategory_visible = 1 "
			. "AND i.offerincident_active = 1 "
			. "AND c.parent_id = 0 "
			. "GROUP BY c.id";
		
		// Fetch the records
		$result = Database::instance()->query($sql);
		
		// Set the offerreport totals for each of the parent categorie
		foreach ($result as $offercategory)
		{
			if (array_key_exists($offercategory->id, $offercategory_data))
			{
				$offercategory_data[$offercategory->id]['offerreport_count'] = $offercategory->offerreport_count;
			}
		}
		
		// Garbage collection
		unset ($sql, $result);
	}
}
