<?php 
/**
 * Offerreports Helper class.
 *
 * This class holds functions used for new offerreport submission from both the backend and frontend.
 *
 * PHP version 5
 * LICENSE: This source file is subject to LGPL license
 * that is available through the world-wide-web at the following URI:
 * http://www.gnu.org/copyleft/lesser.html
 * @author     BeeBuy Team <info@beebuy.com>
 * @package    CrowdPesa - http://crowdpesa.com
 * @offercategory   Helpers
 * @copyright  CrowdPesa - http://crowdpesa.com
 * @license    http://www.gnu.org/copyleft/lesser.html GNU Lesser General Public License (LGPL)
 */
class offerreports_Core {
	
	/**
	 * Maintains the list of parameters used for fetching offerincidents
	 * in the fetch_offerincidents method
	 * @var array
	 */
	public static $params = array();
	
			
	/**
	 * 
	 * @param array $post Values to be validated
	 * @param bool $admin_section Whether the validation is for the admin section
	 */
	public static function validate(array & $post, $admin_section = FALSE)
	{
		// Exception handling
		if ( ! isset($post) OR ! is_array($post))
			return FALSE;
		
		// Create validation object
		$post = Validation::factory($post)
				->pre_filter('trim', TRUE);
		
		$post->add_rules('offerincident_title','required', 'length[3,200]');
		$post->add_rules('offerincident_description','required');
		//$post->add_rules('places','required');
		
		
			
		if (($post->offerincident_start_ampm != "am" AND $post->offerincident_start_ampm != "pm") )
		{
			$post->add_error('offerincident_start_ampm','values');
		}
		if (($post->offerincident_end_ampm != "am" AND $post->offerincident_end_ampm != "pm")){
			$post->add_error('offerincident_end_ampm','values');
		}
			
		

		//XXX: Hack to validate for no checkboxes checked
		if ( ! isset($post->places)|| empty($post->places)||$post->places==null)
		{
			$post->places = array();
			$post->add_error('places','required');
		}
		else
		{
			$post->add_rules('places.*','required','numeric');
		}

		
		

		// Validate only the fields that are filled in
		if ( ! empty($post->offerincident_news))
		{
			foreach ($post->offerincident_news as $key => $url) 
			{
				if ( ! empty($url) AND !(bool) filter_var($url, FILTER_VALIDATE_URL, FILTER_FLAG_HOST_REQUIRED))
				{
					$post->add_error('offerincident_news','url');
				}
			}
		}

		// Validate only the fields that are filled in
		if ( ! empty($post->offerincident_video))
		{
			foreach ($post->offerincident_video as $key => $url) 
			{
				if (!empty($url) AND !(bool) filter_var($url, FILTER_VALIDATE_URL, FILTER_FLAG_HOST_REQUIRED))
				{
					$post->add_error('offerincident_video','url');
				}
			}
		}
		
		// If deployment is a single country deployment, check that the location mapped is in the default country
		/*if ( ! Kohana::config('settings.multi_country'))
		{
			$country = Country_Model::get_country_by_name($post->country_name);
			if ($country AND $country->id != Kohana::config('settings.default_country'))
			{
				$post->add_error('country_name','single_country');
			}
		}*/
		
		// Validate photo uploads
		$post->add_rules('offerincident_photo', 'upload::valid', 'upload::type[gif,jpg,png]', 'upload::size[2M]');


		// Validate Personal Information
		if ( ! empty($post->person_first))
		{
			$post->add_rules('person_first', 'length[3,100]');
		}

		if ( ! empty($post->person_last))
		{
			$post->add_rules('person_last', 'length[2,100]');
		}

		if ( ! empty($post->person_email))
		{
			$post->add_rules('person_email', 'email', 'length[3,100]');
		}
		
		// Extra validation rules for the admin section
		if ($admin_section)
		{
			//$post->add_rules('location_id','numeric');
			$post->add_rules('message_id','numeric');
			$post->add_rules('offerincident_active','required', 'between[0,1]');
			//$post->add_rules('offerincident_verified','required', 'between[0,1]');
			//$post->add_rules('offerincident_zoom', 'numeric');
		}
		
		// Custom form fields validation
		$errors = customforms::validate_custom_form_fields($post);

		// Check if any errors have been returned
		if (count($errors) > 0)
		{
			foreach ($errors as $field_name => $error)
			{
				$post->add_error($field_name, $error);
			}
		}

		//> END custom form fields validation
		
		// Return
		return $post->validate();
	}
	
	/**
	 * Function to save offerreport location
	 * 
	 * @param Validation $post
	 * @param Location_Model $location Instance of the location model
	 */
	public static function save_location($post, $location)
	{
		// Load the country
		$country = isset($post->country_name)
			? Country_Model::get_country_by_name($post->country_name)
			: new Country_Model(Kohana::config('settings.default_country'));
			
		// Fetch the country id
		$country_id = ( ! empty($country) AND $country->loaded)? $country->id : 0;
		
		// Assign country_id retrieved
		$post->country_id = $country_id;
		$location->location_name = $post->location_name;
		$location->latitude = $post->latitude;
		$location->longitude = $post->longitude;
		$location->country_id = $country_id;
		$location->location_date = date("Y-m-d H:i:s",time());
		$location->save();
		
		// Garbage collection
		unset ($country, $country_id);
	}
    
	private function _generate_retailer_token () {
        $chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";  
        $length = 5;
        $str = substr(str_shuffle($chars), 0, $length);
        return $str;
    }
    
    private function _generate_offer_coupon ($prefix = '') {
        $chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
        if ($prefix != '')
            $chars = "0123456789";
        $length = 10 - strlen($prefix);
        $str = substr(str_shuffle($chars), 0, $length);
        return $prefix . $str;
    }
	
	/**
	 * Saves an offerincident
	 *
	 * @param Validation $post Validation object with the data to be saved
	 * @param Offerincident_Model $offerincident Offerincident_Model instance to be modified
	 * @param Location_Model $location_model Location to be attached to the offerincident
	 * @param int $id ID no. of the offerreport
	 *
	 */
	public static function save_offerreport($post, $offerincident)
	{
		// Exception handling
		if ( ! $post instanceof Validation_Core AND  ! $offerincident instanceof Offerincident_Model)
		{
			// Throw exception
			throw new Kohana_Exception('Invalid parameter types');
		}
		
		// Verify that the location id exists
		//if ( ! Location_Model::is_valid_location($location_id))
		//{
		//	throw new Kohana_Exception(sprintf('Invalid location id specified: ', $location_id));
		//}
		
		// Is this new or edit?
		if ($offerincident->loaded)	
		{
			// Edit
			$offerincident->offerincident_datemodify = date("Y-m-d H:i:s",time());
		}
		else		
		{
			// New
			$offerincident->offerincident_dateadd = date("Y-m-d H:i:s",time());
			$offerincident->offer_coupon = self::_generate_offer_coupon($post->coupon_prefix);
		}
		
		//$offerincident->location_id = $location_id;
		//$offerincident->locale = $post->locale;
		if (isset($post->form_id))
		{
			$offerincident->form_id = $post->form_id;
		}
		
		//$offerincident->locale = $post->locale;
		if (isset($post->offerincidenttype_id))
		{
			$offerincident->offerincidenttype_id = $post->offerincidenttype_id;
		}
		
		// Check if the user id has been specified
		if (isset($_SESSION['auth_user']))
		{
			$offerincident->user_id = $_SESSION['auth_user']->id;
		}
		
		$offerincident->offerincident_title = $post->offerincident_title;
		$offerincident->offerincident_description = $post->offerincident_description;
		$offerincident->offerincident_price = $post->offerincident_price;
		$offerincident->offerincident_active = '1';
		$offerincident->published = $post->published;
		$offerincident->offer_type_id = $post->offer_type_id;
		$offerincident->offerincident_verified = '1';
		//$offerincident->offer_coupon = $post->offer_coupon;
		//$offerincident->coupon_prefix = $post->coupon_prefix;
		$offerincident->max_coupons = $post->max_coupons;

		
		//Add offer start time
		$offerincident_start_date=explode("/",$post->offerincident_start_date);
		// Where the $_POST['date'] is a value posted by form in mm/dd/yyyy format
		$offerincident_start_date = $offerincident_start_date[2]."-".$offerincident_start_date[0]."-".$offerincident_start_date[1];

		$offerincident_start_time = $post->offerincident_start_hour . ":" . $post->offerincident_start_minute . ":00 " . $post->offerincident_start_ampm;
		$offerincident->offerincident_start_date = date( "Y-m-d H:i:s", strtotime($offerincident_start_date . " " . $offerincident_start_time) );
		//Add offer end time	
		$offerincident_end_date=explode("/",$post->offerincident_end_date);
		// Where the $_POST['date'] is a value posted by form in mm/dd/yyyy format
		$offerincident_end_date=$offerincident_end_date[2]."-".$offerincident_end_date[0]."-".$offerincident_end_date[1];

		$offerincident_end_time = $post->offerincident_end_hour . ":" . $post->offerincident_end_minute . ":00 " . $post->offerincident_end_ampm;
		$offerincident->offerincident_end_date = date( "Y-m-d H:i:s", strtotime($offerincident_end_date . " " . $offerincident_end_time) );
		// Is this an Email, SMS, Twitter submitted offerreport?
		if ( ! empty($post->service_id))
		{
			// SMS
			if ($post->service_id == 1)
			{
				$offerincident->offerincident_mode = 2;
			}
			// Email
			elseif($post->service_id == 2)
			{
				$offerincident->offerincident_mode = 3;
			}
			// Twitter
			elseif ($post->service_id == 3)
			{
				$offerincident->offerincident_mode = 4;
			}
			else
			{
				// Default to Web Form
				$offerincident->offerincident_mode = 1;
			}
		}
		
		// Approval Status
		if (isset($post->offerincident_active))
		{
			$offerincident->offerincident_active = $post->offerincident_active;
		}
		// Verification status
		if (isset($post->offerincident_verified))
		{
			$offerincident->offerincident_verified = $post->offerincident_verified;
		}
		// Featured status
		if (isset($post->offerincident_featured))
		{
			$offerincident->offerincident_featured = $post->offerincident_featured;
		}
		
		// Offerincident zoom
		if ( ! empty($post->offerincident_zoom))
		{
			$offerincident->offerincident_zoom = intval($post->offerincident_zoom);
		}
		// Tag this as a offerreport that needs to be sent out as an alert
		if ($offerincident->offerincident_active == 1 AND $offerincident->offerincident_alert_status != 2)
		{ 
			// 2 = offerreport that has had an alert sent
			$offerincident->offerincident_alert_status = '1';
		}
		
		// Remove alert if offerreport is unactivated and alert hasn't yet been sent
		if ($offerincident->offerincident_active == 0 AND $offerincident->offerincident_alert_status == 1)
		{
			$offerincident->offerincident_alert_status = '0';
		}
		
		// Save the offerincident
		$offerincident->save();
	}
	
	/**
	 * Function to record the verification/approval actions
	 *
	 * @param mixed $post
	 * @param mixed $verify Instance of the verify model
	 * @param mixed $offerincident
	 */
	public static function verify_approve($post, $verify, $offerincident)
	{
		// @todo Exception handling
		
		$verify->offerincident_id = $offerincident->id;
		
		// Record 'Verified By' Action
		$verify->user_id = $_SESSION['auth_user']->id;			
		$verify->verified_date = date("Y-m-d H:i:s",time());
				
		if ($post->offerincident_active == 1)
		{
			$verify->verified_status = '1';
		}
		elseif ($post->offerincident_verified == 1)
		{
			$verify->verified_status = '2';
		}
		elseif ($post->offerincident_active == 1 AND $post->offerincident_verified == 1)
		{
			$verify->verified_status = '3';
		}
		else
		{
			$verify->verified_status = '0';
		}
		
		// Save
		$verify->save();		
	} 
	
	/**
	 * Function that saves offerincident geometries
	 *
	 * @param Offerincident_Model $offerincident
	 * @param mixed $offerincident
	 *
	 */
	public static function save_offerreport_geometry($post, $offerincident)
	{
		// Delete all current geometry
		ORM::factory('geometry')->where('offerincident_id',$offerincident->id)->delete_all();
		
		if (isset($post->geometry)) 
		{
			// Database object
			$db = new Database();
			
			// SQL for creating the offerincident geometry
			$sql = "INSERT INTO ".Kohana::config('database.default.table_prefix')."geometry "
				. "(offerincident_id, geometry, geometry_label, geometry_comment, geometry_color, geometry_strokewidth) "
				. "VALUES(%d, GeomFromText('%s'), '%s', '%s', '%s', %s)";
				
			foreach($post->geometry as $item)
			{
				if ( ! empty($item))
				{
					//Decode JSON
					$item = json_decode($item);
					//++ TODO - validate geometry
					$geometry = (isset($item->geometry)) ? $db->escape_str($item->geometry) : "";
					$label = (isset($item->label)) ? $db->escape_str(substr($item->label, 0, 150)) : "";
					$comment = (isset($item->comment)) ? $db->escape_str(substr($item->comment, 0, 255)) : "";
					$color = (isset($item->color)) ? $db->escape_str(substr($item->color, 0, 6)) : "";
					$strokewidth = (isset($item->strokewidth) AND (float) $item->strokewidth) ? (float) $item->strokewidth : "2.5";
					if ($geometry)
					{
						// 	Format the SQL string
						$sql = sprintf($sql, $offerincident->id, $geometry, $label, $comment, $color, $strokewidth);
						
						// Execute the query
						$db->query($sql);
					}
				}
			}
		}
	}
	
	/**
	 * Function to save offerincident offercategories
	 *
	 * @param mixed $post
	 * @param mixed $offerincident_model
	 */
	public static function save_offercategory($post, $offerincident)
	{
		
				// Delete Previous Entries
				ORM::factory('offerincident_offercategory')->where('offerincident_id', $offerincident->id)->delete_all();
				
				foreach ($post->offerincident_offercategory as $item)
				{
					$offerincident_offercategory = new Offerincident_Offercategory_Model();
					$offerincident_offercategory->offerincident_id = $offerincident->id;
					$offerincident_offercategory->offercategory_id = $item;
					$offerincident_offercategory->save();
				}
			
	}
	
	/**
	 * Function to save news, photos and videos
	 *
	 * @param mixed $location_model
	 * @param mixed $post
	 *
	 */
	public static function save_media($post, $offerincident)
	{
		// Delete Previous Entries
		ORM::factory('media')->where('offerincident_id',$offerincident->id)->where('media_type <> 1')->delete_all();
		
		// a. News
		foreach ($post->offerincident_news as $item)
		{
			if ( ! empty($item))
			{
				$news = new Media_Model();
				$news->location_id = $offerincident->location_id;
				$news->offerincident_id = $offerincident->id;
				$news->media_type = 4;		// News
				$news->media_link = $item;
				$news->media_date = date("Y-m-d H:i:s",time());
				$news->save();
			}
		}

		// b. Video
		foreach ($post->offerincident_video as $item)
		{
			if ( ! empty($item))
			{
				$video = new Media_Model();
				$video->location_id = $offerincident->location_id;
				$video->offerincident_id = $offerincident->id;
				$video->media_type = 2;		// Video
				$video->media_link = $item;
				$video->media_date = date("Y-m-d H:i:s",time());
				$video->save();
			}
		}

		// c. Photos
		$filenames = upload::save('offerincident_photo');
		$i = 1;
		foreach ($filenames as $filename)
		{
			$new_filename = $offerincident->id.'_'.$i.'_'.time();

			$file_type = strrev(substr(strrev($filename),0,4));
					
			// IMAGE SIZES: 800X600, 400X300, 89X59
					
			// Large size
			Image::factory($filename)->resize(800,600,Image::AUTO)
				->save(Kohana::config('upload.directory', TRUE).$new_filename.$file_type);

			// Medium size
			Image::factory($filename)->resize(290,240,Image::NONE)
				->save(Kohana::config('upload.directory', TRUE).$new_filename.'_m'.$file_type);
					
			// Thumbnail
			Image::factory($filename)->resize(158,110,Image::NONE)
				->save(Kohana::config('upload.directory', TRUE).$new_filename.'_t'.$file_type);
				
			// Name the files for the DB
			$media_link = $new_filename.$file_type;
			$media_medium = $new_filename.'_m'.$file_type;
			$media_thumb = $new_filename.'_t'.$file_type;
				
			// Okay, now we have these three different files on the server, now check to see
			//   if we should be dropping them on the CDN
			
			if (Kohana::config("cdn.cdn_store_dynamic_content"))
			{
				$media_link = cdn::upload($media_link);
				$media_medium = cdn::upload($media_medium);
				$media_thumb = cdn::upload($media_thumb);
				
				// We no longer need the files we created on the server. Remove them.
				$local_directory = rtrim(Kohana::config('upload.directory', TRUE), '/').'/';
				unlink($local_directory.$new_filename.$file_type);
				unlink($local_directory.$new_filename.'_m'.$file_type);
				unlink($local_directory.$new_filename.'_t'.$file_type);
			}

			// Remove the temporary file
			unlink($filename);

			// Save to DB
			$photo = new Media_Model();
			$photo->location_id = $offerincident->location_id;
			$photo->offerincident_id = $offerincident->id;
			$photo->media_type = 1; // Images
			$photo->media_link = $media_link;
			$photo->media_medium = $media_medium;
			$photo->media_thumb = $media_thumb;
			$photo->media_date = date("Y-m-d H:i:s",time());
			$photo->save();
			$i++;
		}
	}
	
	/**
	 * Function to save custom field values
	 *
	 * @param mixed $offerincident_model
	 * @param mixed $post
	 *
	 */
	public static function save_custom_fields($post, $offerincident)
	{
		if (isset($post->custom_field))
		{
			foreach($post->custom_field as $key => $value)
			{
				$form_response = ORM::factory('form_response')
								->where('form_field_id', $key)
								->where('offerincident_id', $offerincident->id)
								->find();
													 
				if ($form_response->loaded == true)
				{
					$form_response->form_field_id = $key;
					$form_response->form_response = $value;
					$form_response->save();
				}
				else
				{
					$form_response = new Form_Response_Model();
					$form_response->form_field_id = $key;
					$form_response->offerincident_id = $offerincident->id;
					$form_response->form_response = $value;
					$form_response->save();
				}
			}
		}	
	}
	
	/**
	 * Function to save personal information
	 *
	 * @param mixed $offerincident_model
	 * @param mixed $post
	 *
	 */
	public static function save_personal_info($post, $offerincident)
	{
		// Delete Previous Entries
		ORM::factory('offerincident_person')->where('offerincident_id',$offerincident->id)->delete_all();
		
		$person = new Offerincident_Person_Model();
		$person->location_id = $offerincident->location_id;
		$person->offerincident_id = $offerincident->id;
		$person->person_first = $post->person_first;
		$person->person_last = $post->person_last;
		$person->person_email = $post->person_email;
		$person->person_date = date("Y-m-d H:i:s",time());
		$person->save();		
	}
	
    public static function fetch_offer_locations($offer_id)
    {
         $query = "SELECT incident.id, incident_title, incident_date, latitude, longitude
                    FROM `offerincident`
                    INNER JOIN offer_incidents ON offer_incidents.offerincident_id = offerincident.id
                    INNER JOIN incident ON incident.id = offer_incidents.incident_id
                    INNER JOIN location ON incident.location_id = location.id
                    WHERE offerincident_id = $offer_id
                    ORDER BY offerincident_id
                    LIMIT 0 , 40";
         return Database::instance()->query($query);
                    
    }
	/**
	 * Helper function to fetch and optionally paginate the list of 
	 * offerincidents/offerreports via the Offerincident Model using one or all of the 
	 * following URL parameters
	 *	- offercategory
	 *	- location bounds
	 *	- offerincident mode
	 *	- media
	 *	- location radius
	 *
	 * @param $paginate Optionally paginate the offerincidents - Default is FALSE
	 * @return Database_Result
	 */
	public static function fetch_offerincidents($paginate = FALSE, $items_per_page = 0)
	{
		// Reset the paramters
		self::$params = array();
		
		// Initialize the offercategory id
		$offercategory_id = 0;
		
		$table_prefix = Kohana::config('database.default.table_prefix');
		
		// Fetch the URL data into a local variable
		$url_data = array_merge($_GET);
		
		// Check if some parameter values are separated by "," except the location bounds
		$exclude_params = array('c' => '', 'v' => '', 'm' => '', 'mode' => '', 'sw'=> '', 'ne'=> '');
		foreach ($url_data as $key => $value)
		{
			if (array_key_exists($key, $exclude_params) AND !is_array($value))
			{
				if (is_array(explode(",", $value)))
				{
					$url_data[$key] = explode(",", $value);
				}
			}
		}
		
		//> BEGIN PARAMETER FETCH
		
		// 
		// Check for the offercategory parameter
		// 
		if ( isset($url_data['c']) AND !is_array($url_data['c']) AND is_numeric($url_data['c']) AND intval($url_data['c']) > 0)
		{
			// Get the offercategory ID
			$offercategory_id = intval($_GET['c']);
			
			// Add offercategory parameter to the parameter list
			array_push(self::$params,
				'(c.id = '.$offercategory_id.' OR c.parent_id = '.$offercategory_id.')',
				'c.offercategory_visible = 1'
			);
		}
		elseif (isset($url_data['c']) AND is_array($url_data['c']))
		{
			// Sanitize each of the offercategory ids
			$offercategory_ids = array();
			foreach ($url_data['c'] as $c_id)
			{
				if (intval($c_id) > 0)
				{
					$offercategory_ids[] = intval($c_id);
				}
			}
			
			// Check if there are any offercategory ids
			if (count($offercategory_ids) > 0)
			{
				$offercategory_ids = implode(",", $offercategory_ids);
			
				array_push(self::$params,
					'(c.id IN ('.$offercategory_ids.') OR c.parent_id IN ('.$offercategory_ids.'))',
					'c.offercategory_visible = 1'
				);
			}
		}else{
			array_push(self::$params,
				'c.offercategory_visible = 1'
			);
		}
		
		// 
		// Offerincident modes
		// 
		if (isset($url_data['mode']) AND is_array($url_data['mode']))
		{
			$offerincident_modes = array();
			
			// Sanitize the modes
			foreach ($url_data['mode'] as $mode)
			{
				if (intval($mode) > 0)
				{
					$offerincident_modes[] = intval($mode);
				}
			}
			
			// Check if any modes exist and add them to the parameter list
			if (count($offerincident_modes) > 0)
			{
				array_push(self::$params, 
					'i.offerincident_mode IN ('.implode(",", $offerincident_modes).')'
				);
			}
		}
		
		// 
		// Location bounds parameters
		// 
		if (isset($url_data['sw']) AND isset($url_data['ne']))
		{
			$southwest = $url_data['sw'];
			$northeast = $url_data['ne'];
			
			if ( count($southwest) == 2 AND count($northeast) == 2 )
			{
				$lon_min = (float) $southwest[0];
				$lon_max = (float) $northeast[0];
				$lat_min = (float) $southwest[1];
				$lat_max = (float) $northeast[1];
			
				// Add the location conditions to the parameter list
				array_push(self::$params, 
					'l.latitude >= '.$lat_min,
					'l.latitude <= '.$lat_max,
					'l.longitude >= '.$lon_min,
					'l.longitude <= '.$lon_max
				);
			}
		}
		
		// 
		// Location bounds - based on start location and radius
		// 
		if (isset($url_data['radius']) AND isset($url_data['start_loc']))
		{
			//if $url_data['start_loc'] is just comma delimited strings, then make it into an array
			if(!is_array($url_data['start_loc']))
			{
				$url_data['start_loc'] = explode(",", $url_data['start_loc']);
			}
			if (intval($url_data['radius']) > 0 AND is_array($url_data['start_loc']))
			{
				$bounds = $url_data['start_loc'];			
				if (count($bounds) == 2 AND is_numeric($bounds[0]) AND is_numeric($bounds[1]))
				{
					self::$params['radius'] = array(
						'distance' => intval($url_data['radius']),
						'latitude' => $bounds[0],
						'longitude' => $bounds[1]
					);
				}
			}
		}
		
		// 
		// Check for offerincident date range parameters
		// 
		if (isset($url_data['from']) AND isset($url_data['to']))
		{
			$date_from = date('Y-m-d', strtotime($url_data['from']));
			$date_to = date('Y-m-d', strtotime($url_data['to']));
			
			array_push(self::$params, 
				'i.offerincident_date >= "'.$date_from.'"',
				'i.offerincident_date <= "'.$date_to.'"'
			);
		}
		
		/**
		 * ---------------------------
		 * NOTES: E.Kala July 13, 2011
		 * ---------------------------
		 * Additional checks for date parameters specified in timestamp format
		 * This only affects those submitted from the main page
		 */
		
		// Start Date
		if (isset($_GET['s']) AND intval($_GET['s']) > 0)
		{
			$start_date = intval($_GET['s']);
			array_push(self::$params, 
				'i.offerincident_date >= "'.date("Y-m-d H:i:s", $start_date).'"'
			);
		}

		// End Date
		if (isset($_GET['e']) AND intval($_GET['e']))
		{
			$end_date = intval($_GET['e']);
			array_push(self::$params, 
				'i.offerincident_date <= "'.date("Y-m-d H:i:s", $end_date).'"'
			);
		}
		
		// 
		// Check for media type parameter
		// 
		if (isset($url_data['m']) AND is_array($url_data['m']))
		{
			// An array of media filters has been specified
			// Validate the media types
			$media_types = array();
			foreach ($url_data['m'] as $media_type)
			{
				if (intval($media_type) > 0)
				{
					$media_types[] = intval($media_type);
				}
			}
			if (count($media_types) > 0)
			{
				array_push(self::$params, 
					'i.id IN (SELECT DISTINCT offerincident_id FROM '.$table_prefix.'media WHERE media_type IN ('.implode(",", $media_types).'))'
				);
			}
			
		}
		elseif (isset($url_data['m']) AND !is_array($url_data['m']))
		{
			// A single media filter has been specified
			$media_type = $url_data['m'];
			
			// Sanitization
			if (intval($media_type) > 0)
			{
				array_push(self::$params, 
					'i.id IN (SELECT DISTINCT offerincident_id FROM '.$table_prefix.'media WHERE media_type = '.$media_type.')'
				);
			}
		}
		
		// 
		// Check if the verification status has been specified
		// 
		if (isset($url_data['v']) AND is_array($url_data['v']))
		{
			$verified_status = array();
			foreach ($url_data['v'] as $verified)
			{
				if (intval($verified) >= 0)
				{
					$verified_status[] = intval($verified);
				}
			}
			
			if (count($verified_status) > 0)
			{
				array_push(self::$params, 
					'i.offerincident_verified IN ('.implode(",", $verified_status).')'
				);
			}
		}
		elseif (isset($url_data['v']) AND !is_array($url_data['v']) AND intval($url_data) >= 0)
		{
			array_push(self::$param, 
				'i.offerincident_verified = '.intval($url_data['v'])
			);
		}
		
		//
		// Check if they're filtering over custom form fields
		//
		if (isset($url_data['cff']) AND is_array($url_data['cff']))
		{
			$where_text = "";
			$i = 0;
			foreach ($url_data['cff'] as $field)
			{			
				$field_id = $field[0];
				if (intval($field_id) < 1)
					continue;

				$field_value = $field[1];
				if (is_array($field_value))
				{
					$field_value = implode(",", $field_value);
				}
								
				$i++;
				if ($i > 1)
				{
					$where_text .= " OR ";
				}
				
				$where_text .= "(form_field_id = ".intval($field_id)
					. " AND form_response = '".Database::instance()->escape_str(trim($field_value))."')";
			}
			
			// Make sure there was some valid input in there
			if ($i > 0)
			{
				// I run a database query here because it's way faster to get the valid IDs in a seperate database query than it is
				//to run this query nested in the main query. 
				$db = new Database();
				$rows = $db->query('SELECT DISTINCT offerincident_id FROM '.$table_prefix.'form_response WHERE '.$where_text);
				$offerincident_ids = '';
				foreach($rows as $row)
				{
					if($offerincident_ids != ''){$offerincident_ids .= ',';}
					$offerincident_ids .= $row->offerincident_id;
				}
				//make sure there are IDs found
				if($offerincident_ids != '')
				{
					array_push(self::$params, 'i.id IN ('.$offerincident_ids.')');
				}
				else
				{
					array_push(self::$params, 'i.id IN (0)');
				}
			}
			
		} // End of handling cff
		
		// In case a plugin or something wants to get in on the parameter fetching fun
		Event::run('ushahidi_filter.fetch_offerincidents_set_params', self::$params);
		
		//> END PARAMETER FETCH

		
		// Fetch all the offerincidents
		$all_offerincidents = Offerincident_Model::get_offerincidents(self::$params);
		
		if ($paginate)
		{
			// Set up pagination
			$page_limit = (intval($items_per_page) > 0)? $items_per_page : intval (Kohana::config('settings.items_per_page'));
			
			$pagination = new Pagination(array(
					'style' => 'front-end-offerreports',
					'query_string' => 'page',
					'items_per_page' => $page_limit,
					'total_items' => $all_offerincidents->count()
					));
			
			// Return paginated results
			return Offerincident_Model::get_offerincidents(self::$params, $pagination);
		}
		else
		{
			// Return
			return $all_offerincidents;
		}
	}

    /**
     * Helper function to fetch and optionally paginate the list of 
     * places with offers via the Offerincident Model using one or all of the 
     * following URL parameters
     *  - offercategory
     *  - location bounds
     *  - offerincident mode
     *  - media
     *  - location radius
     *
     * @param $paginate Optionally paginate the offerincidents - Default is FALSE
     * @return Database_Result
     */
    public static function fetch_places_with_offers($paginate = FALSE, $items_per_page = 0)
    {
        // Reset the paramters
        self::$params = array();
        
        // Initialize the offercategory id
        $offercategory_id = 0;
        
        $table_prefix = Kohana::config('database.default.table_prefix');
        
        // Fetch the URL data into a local variable
        $url_data = array_merge($_GET);
        
        // Check if some parameter values are separated by "," except the location bounds
        $exclude_params = array('c' => '', 'v' => '', 'm' => '', 'mode' => '', 'sw'=> '', 'ne'=> '');
        foreach ($url_data as $key => $value)
        {
            if (array_key_exists($key, $exclude_params) AND !is_array($value))
            {
                if (is_array(explode(",", $value)))
                {
                    $url_data[$key] = explode(",", $value);
                }
            }
        }
        
        //> BEGIN PARAMETER FETCH
        
        // 
        // Check for the offercategory parameter
        // 
        if ( isset($url_data['c']) AND !is_array($url_data['c']) AND intval($url_data['c']) > 0)
        {
            // Get the offercategory ID
            $offercategory_id = intval($_GET['c']);
            
            // Add offercategory parameter to the parameter list
            array_push(self::$params,
                '(c.id = '.$offercategory_id.' OR c.parent_id = '.$offercategory_id.')',
                'c.offercategory_visible = 1'
            );
        }
        elseif (isset($url_data['c']) AND is_array($url_data['c']))
        {
            // Sanitize each of the offercategory ids
            $offercategory_ids = array();
            foreach ($url_data['c'] as $c_id)
            {
                if (intval($c_id) > 0)
                {
                    $offercategory_ids[] = intval($c_id);
                }
            }
            
            // Check if there are any offercategory ids
            if (count($offercategory_ids) > 0)
            {
                $offercategory_ids = implode(",", $offercategory_ids);
            
                array_push(self::$params,
                    '(c.id IN ('.$offercategory_ids.') OR c.parent_id IN ('.$offercategory_ids.'))',
                    'c.offercategory_visible = 1'
                );
            }
        }/*else{
            array_push(self::$params,
                'c.offercategory_visible = 1'
            );
        }*/
        
        // 
        // Offerincident modes
        // 
        if (isset($url_data['mode']) AND is_array($url_data['mode']))
        {
            $offerincident_modes = array();
            
            // Sanitize the modes
            foreach ($url_data['mode'] as $mode)
            {
                if (intval($mode) > 0)
                {
                    $offerincident_modes[] = intval($mode);
                }
            }
            
            // Check if any modes exist and add them to the parameter list
            if (count($offerincident_modes) > 0)
            {
                array_push(self::$params, 
                    'i.incident_mode IN ('.implode(",", $offerincident_modes).')'
                );
            }
        }
        
        // 
        // Location bounds parameters
        // 
        if (isset($url_data['sw']) AND isset($url_data['ne']))
        {
            $southwest = $url_data['sw'];
            $northeast = $url_data['ne'];
            
            if ( count($southwest) == 2 AND count($northeast) == 2 )
            {
                $lon_min = (float) $southwest[0];
                $lon_max = (float) $northeast[0];
                $lat_min = (float) $southwest[1];
                $lat_max = (float) $northeast[1];
            
                // Add the location conditions to the parameter list
                array_push(self::$params, 
                    'l.latitude >= '.$lat_min,
                    'l.latitude <= '.$lat_max,
                    'l.longitude >= '.$lon_min,
                    'l.longitude <= '.$lon_max
                );
            }
        }
        
        $session = Session::instance();
        self::$params['radius'] = array(
                        'distance' => $session->get('map_radius'),
                        'latitude' => $session->get('map_center_lat'),
                        'longitude' => $session->get('map_center_lng')
                        );
        // 
        // Location bounds - based on start location and radius
        // 
        if (isset($url_data['radius']) AND isset($url_data['start_loc']))
        {
            //if $url_data['start_loc'] is just comma delimited strings, then make it into an array
            if(!is_array($url_data['start_loc']))
            {
                $url_data['start_loc'] = explode(",", $url_data['start_loc']);
            }
            if (intval($url_data['radius']) > 0 AND is_array($url_data['start_loc']))
            {
                $bounds = $url_data['start_loc'];           
                if (count($bounds) == 2 AND is_numeric($bounds[0]) AND is_numeric($bounds[1]))
                {
                    self::$params['radius'] = array(
                        'distance' => intval($url_data['radius']),
                        'latitude' => $bounds[0],
                        'longitude' => $bounds[1]
                    );
                }
            }
        }
        
        // 
        // Check for offerincident date range parameters
        // 
        if (isset($url_data['from']) AND isset($url_data['to']))
        {
            $date_from = date('Y-m-d', strtotime($url_data['from']));
            $date_to = date('Y-m-d', strtotime($url_data['to']));
            
            array_push(self::$params, 
                'o.offerincident_start_date >= "'.$date_from.'"',
                'o.offerincident_end_date <= "'.$date_to.'"'
            );
        }
        
        /**
         * ---------------------------
         * NOTES: E.Kala July 13, 2011
         * ---------------------------
         * Additional checks for date parameters specified in timestamp format
         * This only affects those submitted from the main page
         */
        
        // Start Date
        // Start Date
        /*if (isset($_GET['s']) AND intval($_GET['s']) > 0)
        {
            $start_date = intval($_GET['s']);
            array_push(self::$params, 
                'o.offerincident_start_date >= "'.date("Y-m-d H:i:s", $start_date).'"'
            );
        }

        // End Date
        if (isset($_GET['e']) AND intval($_GET['e']))
        {
            $end_date = intval($_GET['e']);
            array_push(self::$params, 
                'o.offerincident_end_date <= "'.date("Y-m-d H:i:s", $end_date).'"'
            );
        }*/
        
        array_push(self::$params, 'o.offerincident_start_date <= "'.date("Y-m-d H:i:s").'"');
        array_push(self::$params, 'o.offerincident_end_date >= "'.date("Y-m-d H:i:s").'"');
        
        // 
        // Check for media type parameter
        // 
        if (isset($url_data['m']) AND is_array($url_data['m']))
        {
            // An array of media filters has been specified
            // Validate the media types
            $media_types = array();
            foreach ($url_data['m'] as $media_type)
            {
                if (intval($media_type) > 0)
                {
                    $media_types[] = intval($media_type);
                }
            }
            if (count($media_types) > 0)
            {
                array_push(self::$params, 
                    'i.id IN (SELECT DISTINCT incident_id FROM '.$table_prefix.'media WHERE media_type IN ('.implode(",", $media_types).'))'
                );
            }
            
        }
        elseif (isset($url_data['m']) AND !is_array($url_data['m']))
        {
            // A single media filter has been specified
            $media_type = $url_data['m'];
            
            // Sanitization
            if (intval($media_type) > 0)
            {
                array_push(self::$params, 
                    'i.id IN (SELECT DISTINCT incident_id FROM '.$table_prefix.'media WHERE media_type = '.$media_type.')'
                );
            }
        }
        
        // 
        // Check if the verification status has been specified
        // 
        if (isset($url_data['v']) AND is_array($url_data['v']))
        {
            $verified_status = array();
            foreach ($url_data['v'] as $verified)
            {
                if (intval($verified) >= 0)
                {
                    $verified_status[] = intval($verified);
                }
            }
            
            if (count($verified_status) > 0)
            {
                array_push(self::$params, 
                    'i.incident_verified IN ('.implode(",", $verified_status).')'
                );
            }
        }
        elseif (isset($url_data['v']) AND !is_array($url_data['v']) AND intval($url_data) >= 0)
        {
            array_push(self::$param, 
                'i.incident_verified = '.intval($url_data['v'])
            );
        }
        
        //
        // Check if they're filtering over custom form fields
        //
        if (isset($url_data['cff']) AND is_array($url_data['cff']))
        {
            $where_text = "";
            $i = 0;
            foreach ($url_data['cff'] as $field)
            {           
                $field_id = $field[0];
                if (intval($field_id) < 1)
                    continue;

                $field_value = $field[1];
                if (is_array($field_value))
                {
                    $field_value = implode(",", $field_value);
                }
                                
                $i++;
                if ($i > 1)
                {
                    $where_text .= " OR ";
                }
                
                $where_text .= "(form_field_id = ".intval($field_id)
                    . " AND form_response = '".Database::instance()->escape_str(trim($field_value))."')";
            }
            
            // Make sure there was some valid input in there
            if ($i > 0)
            {
                // I run a database query here because it's way faster to get the valid IDs in a seperate database query than it is
                //to run this query nested in the main query. 
                $db = new Database();
                $rows = $db->query('SELECT DISTINCT incident_id FROM '.$table_prefix.'form_response WHERE '.$where_text);
                $offerincident_ids = '';
                foreach($rows as $row)
                {
                    if($offerincident_ids != ''){$offerincident_ids .= ',';}
                    $offerincident_ids .= $row->offerincident_id;
                }
                //make sure there are IDs found
                if($offerincident_ids != '')
                {
                    array_push(self::$params, 'i.id IN ('.$offerincident_ids.')');
                }
                else
                {
                    array_push(self::$params, 'i.id IN (0)');
                }
            }
            
        } // End of handling cff
        
        // In case a plugin or something wants to get in on the parameter fetching fun
        Event::run('ushahidi_filter.fetch_offerincidents_set_params', self::$params);
        
        //> END PARAMETER FETCH

        
        // Fetch all the offerincidents
        $all_offerincidents = Offerincident_Model::fetch_offerincidents(self::$params);
        //print_r(self::$params);
        
        if ($paginate)
        {
            // Set up pagination
            $page_limit = (intval($items_per_page) > 0)? $items_per_page : intval (Kohana::config('settings.items_per_page'));
            
            $pagination = new Pagination(array(
                    'style' => 'front-end-offerreports',
                    'query_string' => 'page',
                    'items_per_page' => $page_limit,
                    'total_items' => $all_offerincidents->count()
                    ));
            
            // Return paginated results
            return Offerincident_Model::fetch_offerincidents(self::$params, $pagination);
        }
        else
        {
            // Return
            return $all_offerincidents;
        }
    }

	/**
	 * Function to save offerincident place
	 *
	 * @param mixed $post
	 * @param mixed $offerincident_model
	 */
	public static function save_offerplace($post, $offerincident)
	{
		
		
		if(!empty($post->places)&&$post->places!=null){
			
		// Delete Previous Entries
			ORM::factory('offer_incident')->where('offerincident_id', $offerincident->id)->delete_all();
			
			foreach ($post->places as $item)
			{
				
				$offer_incidents = new Offer_Incident_Model();
				$offer_incidents->incident_id =$item;
				$offer_incidents->offerincident_id = $offerincident->id;
				$offer_incidents->save();
			}
				
		}else{
			ORM::factory('offer_incident')->where('offerincident_id', $offerincident->id)->delete_all();
		}
	}	
}
?>
