<?php defined('SYSPATH') OR die('No direct access allowed.');
/**
 * Reputation Score helper class
 * 
 *
 * @package	   Reputation
 * @author	   Ushahidi Team
 * @copyright  (c) 2008 Ushahidi Team
 * @license	   http://www.ushahidi.com/license.html
 */

class offerreputation_Core {
	
	/**
	 * Calculate Total Reputation Score for User
	 * @param int User ID
	 * @return int reputation score
	 */
	public static function calculate($user_id)	
	{
		$subdomain = '';
		if (substr_count($_SERVER["HTTP_HOST"],'.') > 1 AND Kohana::config('config.enable_mhi') == TRUE)
		{
			$subdomain = substr($_SERVER["HTTP_HOST"],0,strpos($_SERVER["HTTP_HOST"],'.'));
		}
		
		$cache = Cache::instance();
		// This is kind of a heavy query, so we'll use a 10 minute cache for $totals
		$total = $cache->get($subdomain.'_reputation');

		if ($total == NULL)
		{ // Cache is Empty so Re-Cache
			$total = 0;
			$upvoted_offerreports = 10;
			$approved_offerreports = 15;
			$verified_offerreports = 20;
			$upvoted_comments = 5;
			$downvoted_offerreports = 2;
			$downvoted_comments = 1;

			// Get Offerreports Approved Verified
			$offerreports = ORM::factory("offerincident")
						->where("user_id", $user_id)
						->find_all();
						
			foreach ($offerreports as $offerreport)
			{
				if ($offerreport->offerincident_active)
				{
					$total += $approved_offerreports;
				}

				if ($offerreport->offerincident_verified)
				{
					$total += $verified_offerreports;
				}
			}

			// Get Totals on [My] Offerreports that have been voted on
			$ratings = ORM::factory("rating")
						->join("offerincident", "offerincident.id", "rating.offerincident_id")
						->join("users", "users.id", "offerincident.user_id")
						->where("users.id", $user_id)
						->find_all();
			foreach ($ratings as $rating)
			{
				if ($rating->rating > 0)
				{ // Upvote
					$total += ( $rating->rating * $upvoted_offerreports );
				}
				elseif ($rating->rating < 0)
				{ // Downvote
					$total += ( $rating->rating * $downvoted_offerreports );
				}
			}

			// Get Totals on [My] Comments that have been voted on
			$ratings = ORM::factory("rating")
				->join("comment", "comment.id", "rating.comment_id")
				->join("users", "users.id", "comment.user_id")
				->where("users.id", $user_id)
				->find_all();
				
			foreach ($ratings as $rating)
			{
				if ($rating->rating > 0)
				{ // Upvote
					$total += ( $rating->rating * $upvoted_comments );
				}
				elseif ($rating->rating < 0)
				{ // Downvote
					$total += ( $rating->rating * $downvoted_comments );
				}
			}

			$cache->set($subdomain.'_reputation', $total, array('reputation'), 600); // 10 Minutes
		}

		return $total;
	}
}