<?php defined('SYSPATH') or die('No direct script access.');

class reports_block {

	public function __construct()
	{
		$block = array(
			"classname" => "reports_block",
			"name" => "Reports",
			"description" => "List the 10 latest reports in the system"
		);

		blocks::register($block);
	}

	public function block()
	{
		$content = new View('blocks/main_reports');

		// Get Reports
        // XXX: Might need to replace magic no. 8 with a constant
		$content->total_items = ORM::factory('incident')
			->where('incident_active', '1')
			->limit('8')->count_all();
		$content->incidents = ORM::factory('incident')
			->where('incident_active', '1')
			->limit('10')
			->orderby('incident_date', 'desc')
			->find_all();

		echo $content;
	}
}

new reports_block;

// get latest articles from blog
class news_block {

	public function __construct()
	{
		$block = array(
			"classname" => "news_block",
			"name" => "Site News",
			"description" => "List the 3 latest blog articles"
		);

		blocks::register($block);
	}

	public function block()
	{
		$content = new View('blocks/main_news');
		// Get the articles
		$content->articles = ORM::factory('articles')
			->where('published', '1')
			->limit('3')
			->orderby('published_date', 'desc')
			->find_all();
		
		$content->comment_count = array();
		$db = Database::instance();
		foreach ($content->articles as $a) {
			$comment_count = $db->query("SELECT COUNT(*) 'total' FROM comment WHERE article_id = $a->id");
			$content->comment_count[$a->id] = $comment_count[0]->total;
		}

		echo $content;
	}
}

new news_block;

/*Featured Offers Block*/
class featured_offers_block {

	public function __construct()
	{
		$block = array(
			"classname" => "featured_offers_block",
			"name" => "Featured Offers",
			"description" => "List the 3 latest featured offers"
		);

		blocks::register($block);
	}

	public function block()
	{
		$content = new View('blocks/main_featured_offers');
		// Get latest featured offers
		$filter = 'offerincident_start_date < NOW() AND offerincident_end_date > NOW() AND offerincident_active = 1';
		$content->offers = ORM::factory('offerincident')
			->where('offerincident_featured', '1')
            ->where($filter)
			->limit('18')
			->orderby('offerincident_dateadd', 'desc')
			->find_all();
		// get comment count and average rating for each offer
		$content->average_rating = array();
		$content->comment_count = array();
		$db = Database::instance();
		foreach ($content->offers as $o) {
			$average_rating = $db->query("SELECT AVG(rating) 'ave' FROM rating WHERE offerincident_id = $o->id AND comment_id IS NOT NULL");
			$content->average_rating[$o->id] = round($average_rating[0]->ave);
			$comment_count = $db->query("SELECT COUNT(*) 'total' FROM comment WHERE offerincident_id = $o->id");
			$content->comment_count[$o->id] = $comment_count[0]->total;
		}
		echo $content;
	}
}

new featured_offers_block;

/*Featured Offers Block*/
class featured_offers_single_block {

    public function __construct()
    {
        $block = array(
            "classname" => "featured_offers_single_block",
            "name" => "Random Featured Offer",
            "description" => "List a random featured offer"
        );

        blocks::register($block);
    }

    public function block()
    {
        $content = new View('blocks/main_featured_offers_single');
        // Get latest featured offers
        $filter = 'offerincident_start_date < NOW() AND offerincident_end_date > NOW() AND offerincident_active = 1';
        $offers = ORM::factory('offerincident')
            ->where('offerincident_featured', '1')
            ->where($filter)
            ->limit('20')
            ->orderby('offerincident_dateadd', 'desc')
            ->find_all();
        // get a random one
        $offer = $offers[rand(0, count($offers) - 1)];
        $content->offer = $offer; 
        //var_dump($content->offer);exit;
        // get comment count and average rating for each offer
        $db = Database::instance();
        $average_rating = $db->query("SELECT AVG(rating) 'ave' FROM rating WHERE offerincident_id = $offer->id AND comment_id IS NOT NULL");
        $content->average_rating = round($average_rating[0]->ave);
        $comment_count = $db->query("SELECT COUNT(*) 'total' FROM comment WHERE offerincident_id = $offer->id");
        $content->comment_count = $comment_count[0]->total;
        echo $content;
    }
}

new featured_offers_single_block;

/*Latest Offers Block*/
class latest_offers_block {

	public function __construct()
	{
		$block = array(
			"classname" => "latest_offers_block",
			"name" => "Latest Offers",
			"description" => "List the 5 latest offers"
		);

		blocks::register($block);
	}

	public function block()
	{
		$content = new View('blocks/main_latest_offers');
		// Get RSS News Feeds
		$db = Database::instance();
		// TODO: Replace NOW() with user time
		$content->offers = $db->query('SELECT * FROM offerincident WHERE offerincident_start_date < NOW() AND offerincident_end_date > NOW() AND offerincident_active = 1 ORDER BY offerincident_start_date DESC LIMIT 0, 5');
		echo $content;
	}
}

new latest_offers_block;

/*Popular Offers Block*/
class popular_offers_block {

    public function __construct()
    {
        $block = array(
            "classname" => "popular_offers_block",
            "name" => "Popular Offers",
            "description" => "List the 5 most popular offers"
        );

        blocks::register($block);
    }

    public function block()
    {
        $content = new View('blocks/main_popular_offers');
        // Get RSS News Feeds
        $db = Database::instance();
        // TODO: Replace NOW() with user time
        $content->offers = $db->query('SELECT offerincident.*, (COUNT(orders.date)/(DATEDIFF(MAX(orders.date), MIN(orders.date))+1)) as ratio FROM offerincident INNER JOIN orders ON offerincident.id = orders.offerincident_id WHERE offerincident_start_date < NOW() AND offerincident_end_date > NOW() AND offerincident_active = 1 ORDER BY ratio DESC LIMIT 0, 5');
        echo $content;
    }
}

new popular_offers_block;

/*Latest Reviews Block*/
class latest_reviews_block {

	public function __construct()
	{
		$block = array(
			"classname" => "latest_reviews_block",
			"name" => "Latest Reviews",
			"description" => "List the most recent user comments on offers"
		);

		blocks::register($block);
	}

	public function block()
	{
		$content = new View('blocks/main_latest_reviews');
		// Get RSS News Feeds
		$db = Database::instance();
		// TODO: Replace NOW() with user time
		$content->reviews = $db->query("SELECT comment.id 'comment_id', comment.offerincident_id 'offer_id', comment.comment_author, comment.comment_description, comment.comment_date, comment.user_id, rating.rating 'comment_rating', offerincident.offerincident_title 'comment_title' FROM comment INNER JOIN rating ON comment.id = rating.comment_id INNER JOIN offerincident ON offerincident.id = comment.offerincident_id WHERE comment.offerincident_id IS NOT NULL AND comment.comment_active = 1 AND comment.comment_spam = 0 ORDER BY comment.comment_date DESC LIMIT 0, 3");
		echo $content;
	}
}

new latest_reviews_block;