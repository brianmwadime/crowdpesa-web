<?php
	$lang = array(
	'comments_form_error' => 'error!',
	'places' => array(
		'required' => 'You must select atleast one place'
	),
	'offerincident_title' => array(
		'required' => 'You assign this offer a title',
		'length'=>'The "Offer Title" field must be at least 3 and no more 200 characters long.'
	),
	'offerincident_description' => array(
		'required' => 'The "Description" field is required.'
	),
	'offerincident_start_ampm' => array(
		'values' => 'The am/pm field does not appear to contain a valid value?'
	),
	'offerincident_end_ampm' => array(
		'values' => 'The am/pm field does not appear to contain a valid value?'
	),
	'person_email' => array(
		'email' => 'The email field does not appear to contain a valid email address?',
		'length' => 'The email field must be at least 4 and no more 64 characters long.',
	),
	'person_first' => array(
		'length' => 'The first name field must be at least 3 and no more 100 characters long.',
	),
	'person_last' => array(
		'length' => 'The last name field must be at least 2 and no more 100 characters long.',
	),
	'to_date' => array(
		'date_mmddyyyy' => 'The TO date field does not appear to contain a valid date?',
		'range' => 'Please enter a valid TO date. It cannot be greater than today.',
		'range_greater' => 'Your FROM date cannot be greater than your TO date.',
	));
?>
