<?php
/**
 * Report Importer Library
 *
 * Imports reports within CSV file referenced by filehandle.
 * 
 * PHP version 5
 * LICENSE: This source file is subject to LGPL license
 * that is available through the world-wide-web at the following URI:
 * http://www.gnu.org/copyleft/lesser.html
 * @author     Ushahidi Team <team@ushahidi.com>
 * @package    Ushahidi - http://source.ushahididev.com
 * @copyright  Ushahidi - http://www.ushahidi.com
 * @license    http://www.gnu.org/copyleft/lesser.html GNU Lesser General Public License (LGPL)
 *
 */
class OfferReportsImporter {
	
	function __construct() 
	{
		$this->notices = array();
		$this->errors = array();		
		$this->totalrows = 0;
		$this->importedrows = 0;
		$this->offers_added = array();
		//$this->categories_added = array();
		//$this->locations_added = array();
		$this->offer_places_added = array();
		$this->offer_categories_added = array();
	}
	
	/**
	 * Function to import CSV file referenced by the file handle
	 * @param string $filehandle
	 * @return bool 
	 */
	function import($filehandle,$user_id = null) 
	{
		$csvtable = new Csvtable($filehandle);
		// Set the required columns of the CSV file
		$requiredcolumns = array('OFFER TITLE','OFFER VALUE','DESCRIPTION');
		foreach ($requiredcolumns as $requiredcolumn)
		{
			// If the CSV file is missing any required column, return an error
			if (!$csvtable->hasColumn($requiredcolumn))
			{
				$this->errors[] = 'CSV file is missing required column "'.$requiredcolumn.'"';
			}
		}
		
		if (count($this->errors))
		{
			return false;
		}
		
		// So we can assign category id to incidents, based on category title
		$this->category_ids = ORM::factory('offercategory')->select_list('offercategory_title','id'); 
		//Since we capitalize the category names from the CSV file, we should also capitlize the 
		//category titles here so we get case insensative behavior. For some reason users don't
		//always captilize the cateogry names as they enter them in
		$temp_cat = array();
		foreach($this->category_ids as $key=>$value)
		{
			$temp_cat[strtoupper($key)] = $value;
		}
		$this->category_ids = $temp_cat;
		
		// So we can check if incident already exists in database
		//$this->offer_ids = ORM::factory('offerincident')->select_list('id','id'); 
		$this->time = date("Y-m-d H:i:s",time());
		$rows = $csvtable->getRows();
		$this->totalrows = count($rows);
		$this->rownumber = 0;
	 	
		// Loop through CSV rows
	 	foreach($rows as $row)
	 	{
			$this->rownumber++;
			
			if ($this->importreport($row,$user_id))
				{
					$this->importedrows++;
				}
				else
				{
					$this->rollback();
					return false;
				}
			
		} 
		return true;
	}
	
	/**
	 * Function to undo import of reports
	 */
	function rollback()
	{
		if (count($this->offers_added)) ORM::factory('offerincident')->delete_all($this->offers_added);
		//if (count($this->categories_added)) ORM::factory('category')->delete_all($this->categories_added);
		//if (count($this->places_added)) ORM::factory('location')->delete_all($this->locations_added);
		//if (count($this->offer_categories_added)) ORM::factory('location')->delete_all($this->offer_categories_added);
	}
	
	/**
	 * Function to import a report form a row in the CSV file
	 * @param array $row
	 * @return bool
	 */
	function importreport($row,$user)
	{
		// If the date is not in proper date format


		if (!strtotime($row['START DATE']))
		{
			$this->errors[] = 'Could not parse start date "'.htmlspecialchars($row['START DATE']).'" on line '
			.($this->rownumber+1);
		}
		if (!strtotime($row['END DATE']))
		{
			$this->errors[] = 'Could not parse end date "'.htmlspecialchars($row['END DATE']).'" on line '
			.($this->rownumber+1);
		}
		
		if (count($this->errors)) 
		{
			return false;
		}
		
		// STEP 1:SAVE INCIDENT
		$offer = new Offerincident_Model();
		$offer->user_id = ($user==null)?$row['RETAILER ID']:$user;
		$offer->offerincident_title = $row['OFFER TITLE'];
		$offer->max_coupons = $row['MAX COUPONS'];
		$offer->offerincident_price = $row['OFFER VALUE'];
		$offer->offerincident_description = isset($row['DESCRIPTION']) ? $row['DESCRIPTION'] : '';
		$offer->offerincident_start_date = date("d-m-Y H:i:s",strtotime($row['START DATE']));
		$offer->offerincident_end_date = date("d-m-Y H:i:s",strtotime($row['END DATE']));
		//$offer->incident_active = (isset($row['APPROVED']) AND $row['APPROVED'] == 'YES') ? 1 : 0;
		//$offer->incident_verified = (isset($row['VERIFIED']) AND $row['VERIFIED'] == 'YES') ? 1 :0;
		$offer->save();
		$this->offers_added[] = $offer->id;
		
		// STEP 3: SAVE CATEGORIES
		// If CATEGORY column exists
		if (isset($row['CATEGORY']))
		{
			$categorynames = explode(',',trim($row['CATEGORY']));
			// Add categories to incident
			foreach ($categorynames as $categoryname)
			{
				// There seems to be an uppercase convention for categories... Don't know why
				$categoryname = strtoupper(trim($categoryname));
				
				// For purposes of adding an entry into the incident_category table
				$offer_category = new Offerincident_Offercategory_Model();
				$offer_category->offerincident_id = $offer->id; 
				
				// If category name exists, add entry in incident_category table
				if ($row['CATEGORY'] != '')
				{
					if (!isset($this->category_ids[$categoryname]))
					{
						$this->notices[] = 'There exists no such category "'.htmlspecialchars($categoryname).'" in database yet.'
						.' Added to database.';
						
					}else{
						$offer_category->offercategory_id = $this->category_ids[$categoryname];
					$offer_category->save();
					$this->offer_categories_added[] = $offer_category->id;
					}
					
				}
				
				else
				{
					// Unapprove the report
					$offer_update = ORM::factory('offerincident',$offer->id);
					$offer_update->offerincident_active = 0;
					$offer_update->save();

					// Assign reports to special category for orphaned reports: NONE
					$offer_category->offercategory_id = '5';
					$offer_category->save();
				}	
			} 
		}
		
		// If CATEGORY column doesn't exist, 
		else
		{
			// Unapprove the report
			$offer_update = ORM::factory('offerincident',$offer->id);
			$offer_update->offerincident_active = 0;
			$offer_update->save();
			
			// Assign reports to special category for orphaned reports: NONE
			$offer_category = new Offerincident_Offercategory_Model();
			$offer_category->offerincident_id = $offer->id;
			$offer_category->offercategory_id = '5';
			$offer_category->save();
		} 

		//check whether offer aready exists
		$places =ORM::factory('incident')->select('id')->where('user_id',$user)->as_array();;
		foreach($places as $place){
			$offer_place = new Offer_Incident_Model();
			$offer_place->offerincident_id = $offer->id;
			$offer_place->incident_id = $place;
			$this->offer_places_added[] = $place;
		}
		echo $this->offer_places_added;

	return true;
	}

}

?>
