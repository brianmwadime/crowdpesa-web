<?php defined('SYSPATH') or die('No direct script access.');
/**
 * Timeline_Api_Object
 *
 * This class handles timeline activities via the API.
 *
 * @version 1 - Brian Herbert (not sure what this version is for, though)
 *
 * PHP version 5
 * LICENSE: This source file is subject to LGPL license
 * that is available through the world-wide-web at the following URI:
 * http://www.gnu.org/copyleft/lesser.html
 * @author     Ushahidi Team <team@ushahidi.com>
 * @package    Ushahidi - http://source.ushahididev.com
 * @module     API Controller
 * @copyright  Ushahidi - http://www.ushahidi.com
 * @license    http://www.gnu.org/copyleft/lesser.html GNU Lesser General Public License (LGPL)
 */

class Timeline_Api_Object extends Api_Object_Core {

    private $sort; // Sort descriptor ASC or DESC
    private $order_field; // Column name by which to order the records
    private $checkin_photo = FALSE;
    private $abs_upload_url = FALSE;
    
    public function __construct($api_service)
    {
        parent::__construct($api_service);
        $this->abs_upload_url = url::site().Kohana::config('upload.relative_directory', TRUE);
        
        // If Checkins aren't enabled, we want to essentially shut off this API library
        
        if(Kohana::config('settings.checkins') != 1)
        {
        	// Say what is going on
        	$this->set_ci_error_message(array(
                "error" => $this->api_service->get_error_msg(010)
            ));
			$this->show_response();
        	
        	// TODO: What would be a more appropriate way of doing this?
        	
        	die();
        }
    }
    
    /**
     * Implementation of abstract method in parent
     *
     * Handles the API task parameters
     */
    public function perform_task()
    {	
		// Check if the 'context' parameter has been specified
        if ( ! $this->api_service->verify_array_index($this->request, 'context'))
        {
            // Set "all" as the default method for fetching feeds
            $this->context = 'all';
        }
        else
        {
            $this->context = ($this->request['context'] != '')? strtolower($this->request['context']) : 'all';
        }
        
        $this->_do_get_tl($this->context);	
        $this->show_response();
	}
	
	public function _do_get_tl($context)
	{
		$data = $this->gather_feeds(
		            $context,
					@$this->request['time'],
					@$this->request['userid'],
					@$this->request['mobileid'],
					@$this->request['mapdata']
				);
					
					
		if(count($data) > 0 && isset($data['feeds']))
		{
			// Data!
            ksort($data['feeds']);
			$this->response = array(
					"payload" => array(
						"feeds" => array_reverse($data['feeds'], true),
						//"closestfeed" => $data['closestfeed'],
						"users" => $data["users"],
						"domain" => $this->domain,
						"success" => "true"
					),
					"error" => $this->api_service->get_error_msg(0)
					);
		}
		else
		{
			// No data
			$this->response = array(
					"payload" => array(
						"domain" => $this->domain,
						"success" => "false"
					),
					"error" => $this->api_service->get_error_msg(007)
					);
		}		
		
	}
	
	public function gather_feeds($context,$time,$user_id,$mobileid,$mapdata)
	{
		$data = array();
		
		if($mobileid != '')
		{
			$find_user = ORM::factory('user_devices')->find($mobileid);
			$user_id = $find_user->user_id;
		}
		
		if($user_id == '')
		{
			$where_checkin_user_id = array('checkin.user_id !=' => '-1');
            $where_offer_user_id = array('offerincident.user_id !=' => '-1');
		}else{
			$where_checkin_user_id = array('checkin.user_id' => $user_id);
            $where_offer_user_id = array('offerincident.user_id' => $user_id);
		}
		
		if($time == '')
		{
			$where_checkin_id = array('checkin.id !=' => '-1');
            $where_offer_id = array('offerincident.id !=' => '-1');
		}else{
			$where_checkin_id = array('checkin.id' => $id);
            $where_offer_id = array('offerincident.id' => $id);
		}
		
		$order_checkins_by = 'checkin.id';
		$order_offers_by = 'offerincident.id';
		$sort = 'DESC';
		
		$start_after_time = date('Y-m-d H:i:s', 0);
        if (isset($this->request['sincetime']) && is_numeric($this->request['sincetime']))
            $start_after_time = date('Y-m-d H:i:s', $this->request['sincetime']);
		
		$limit = 20;
		$offset = 0;
		
        $session = Session::instance();
        $max_radius = $session->get('map_radius'); // in km
        $lat = $session->get('map_center_lat');
        $lng = $session->get('map_center_lng');
        
        //$data["closestfeed"] = array('distance' => $max_radius);
        $seen_latest_ci = array();
        $users_names = array();
        $i = 0;
        if ($context == 'all' || $context == 'places')
        {
            // pull nearest locations within max_radius ordered by distance
            $query = "SELECT users.name as user_name, users.username as user_username, users.color as user_color, users.public_profile, checkin.id, checkin.user_id, checkin_description, checkin.location_id, checkin_date, checkin_type, location_name, latitude, longitude, checkin.incident_id,
                    ( 6371 * acos( cos( radians( $lat ) ) * cos( radians( latitude ) ) * cos( radians( longitude ) - radians( $lng ) ) + sin( radians( $lat ) ) * sin( radians( latitude ) ) ) ) AS distance
                    FROM `checkin`
                    INNER JOIN location ON checkin.location_id = location.id
                    INNER JOIN users ON checkin.user_id = users.id
                    LEFT JOIN incident ON location.id = incident.location_id
                    WHERE checkin.checkin_date > '$start_after_time'
                    AND longitude BETWEEN ( $lng -20 / abs( cos( radians( $lat ) ) *69 ) ) AND ( $lng +20 / abs( cos( radians( $lat ) ) *69 ) )
                    AND latitude BETWEEN ( $lat - ( $max_radius / 69 ) ) AND ( $lat + ( $max_radius / 69 ) )
                    HAVING distance < $max_radius
                    ORDER BY distance
                    LIMIT $offset , $limit";
            $checkins = Database::instance()->query($query);
    		foreach($checkins as $checkin)
    		{
    			//if (count($data))// && $checkins->count() > 1)
                $i = strtotime($checkin->checkin_date);
    			$data["feeds"][$i] = array(
    				"id" => ($checkin->incident_id) ? $checkin->incident_id : $checkin->id,
    				"type" => 'ci',
    				"draw_marker" => ($checkin->incident_id) ? 0 : 1,
    				"incident_ids" => array(($checkin->incident_id) ? $checkin->incident_id : 0),
    				"user" => $checkin->user_id,
    				"loc" => $checkin->location_id,
    				"title" => $checkin->location_name,
    				"msg" => $checkin->checkin_description,//text::limit_chars($checkin->checkin_description, 115, ' ...', true),
    				"date" => $checkin->checkin_date,
    				"distance" => $checkin->distance,
    				"lat" => $checkin->latitude,
    				"lon" => $checkin->longitude
    			);
    			
                /*if ($data["closestfeed"]["distance"] > $checkin->distance){
                    $data["closestfeed"]["distance"] = $checkin->distance;
                    $data["closestfeed"]["lon"] = $checkin->longitude;
                    $data["closestfeed"]["lat"] = $checkin->latitude;
                }*/
    			
    			$users_names[(int)$checkin->user_id] = array("id"=>$checkin->user_id,"name"=>$checkin->user_name,"color"=>$checkin->user_color,"username"=>$checkin->user_username,"profile"=>$checkin->public_profile);
    			
    			$j = 0;
                
                $query = "SELECT * FROM `media` WHERE media.checkin_id = $checkin->id";
                
    			foreach (Database::instance()->query($query) as $media)
    			{
    			    $data["feeds"][$i]['media'][(int)$j] = array(
    			    	"id" => $media->id,
    			    	"type" => $media->media_type,
    			    	"link" => url::convert_uploaded_to_abs($media->media_link),
    			    	"medium" => url::convert_uploaded_to_abs($media->media_medium),
    			    	"thumb" => url::convert_uploaded_to_abs($media->media_thumb)
    			    );
    			    $j++;
    			}
    			
    			$j = 0;
    			/**
    			// If we are displaying some extra map data...
    			
    			if($mapdata != '')
    			{
    				
    				if( ! isset($seen_latest_ci[$checkin->user_id]))
    				{
    					$opacity = 1;
    				}else{
    					$opacity = .5;
    				}
    				
    				$seen_latest_ci[$checkin->user_id] = $checkin->user_id;
    				$data["feeds"][$i]['opacity'] = $opacity;
    				
    			}*/
    			
    			$i++;
    		}
        }
        
        if ($context == 'all' || $context == 'offers')
        {
            /*$query = "SELECT users.name as user_name, users.username as user_username, users.color as user_color, offerincident.id, offerincident.user_id, offerincident_start_date, offerincident_description, offerincident_title, offerincident.location_id, offerincident_date, offerincident_dateadd, latitude, longitude, incident.id AS incident_id,
                    ( 6371 * acos( cos( radians( $lat ) ) * cos( radians( latitude ) ) * cos( radians( longitude ) - radians( $lng ) ) + sin( radians( $lat ) ) * sin( radians( latitude ) ) ) ) AS distance
                    FROM `offerincident`
                    INNER JOIN location ON offerincident.location_id = location.id
                    INNER JOIN users ON offerincident.user_id = users.id
                    WHERE offerincident.offerincident_active = 1
                    AND (offerincident_date > '$start_after_time' OR offerincident_dateadd > '$start_after_time')
                    AND offerincident_start_date < NOW()
                    AND offerincident_end_date > NOW()
                    AND longitude BETWEEN ( $lng -20 / abs( cos( radians( $lat ) ) *69 ) ) AND ( $lng +20 / abs( cos( radians( $lat ) ) *69 ) )
                    AND latitude BETWEEN ( $lat - ( $max_radius / 69 ) ) AND ( $lat + ( $max_radius / 69 ) )
                    HAVING distance < $max_radius
                    ORDER BY distance
                    LIMIT $offset , $limit";
             */
            $query = "SELECT users.name as user_name, users.username as user_username, users.color as user_color, users.public_profile, offerincident.id, offerincident.user_id, offerincident_start_date, offerincident_description, offerincident_title, offerincident.location_id, offerincident_date, offerincident_dateadd, latitude, longitude, GROUP_CONCAT(incident.id SEPARATOR ',')
AS place_ids,
                    ( 6371 * acos( cos( radians( $lat ) ) * cos( radians( latitude ) ) * cos( radians( longitude ) - radians( $lng ) ) + sin( radians( $lat ) ) * sin( radians( latitude ) ) ) ) AS distance
                    FROM `offerincident`
                    INNER JOIN offer_incidents ON offer_incidents.offerincident_id = offerincident.id
                    INNER JOIN incident ON incident.id = offer_incidents.incident_id
                    INNER JOIN location ON incident.location_id = location.id
                    INNER JOIN users ON offerincident.user_id = users.id
                    WHERE offerincident.offerincident_active = 1
                    AND (offerincident_date > '$start_after_time' OR offerincident_dateadd > '$start_after_time')
                    AND offerincident_start_date < NOW()
                    AND offerincident_end_date > NOW()
                    AND longitude BETWEEN ( $lng -20 / abs( cos( radians( $lat ) ) *69 ) ) AND ( $lng +20 / abs( cos( radians( $lat ) ) *69 ) )
                    AND latitude BETWEEN ( $lat - ( $max_radius / 69 ) ) AND ( $lat + ( $max_radius / 69 ) )
                    GROUP BY offerincident_start_date
                    HAVING distance < $max_radius
                    ORDER BY distance
                    LIMIT $offset , $limit";
            //die($query);        
            if (Auth::instance()->logged_in()){
                // get user subscriptions and from the table use retailer_id to get new offer updates
                $user_id = Auth_Core::instance()->get_user()->id;
                $query = "SELECT users.name as user_name, users.username as user_username, users.color as user_color, users.public_profile, offerincident.id, offerincident.user_id, offerincident_start_date, offerincident_description, offerincident_title, offerincident.location_id, offerincident_date, offerincident_dateadd, latitude, longitude, GROUP_CONCAT(incident.id SEPARATOR ',')
AS place_ids, (0) as distance
                    FROM `offerincident`
                    INNER JOIN offer_incidents ON offer_incidents.offerincident_id = offerincident.id
                    INNER JOIN incident ON incident.id = offer_incidents.incident_id
                    INNER JOIN location ON incident.location_id = location.id
                    INNER JOIN users ON offerincident.user_id = users.id
                    WHERE offerincident.offerincident_active = 1
                    AND offerincident.user_id IN (SELECT retailer_id FROM offersubscription WHERE user_id = $user_id)
                    AND (offerincident_date > '$start_after_time' OR offerincident_dateadd > '$start_after_time')
                    AND offerincident_start_date < NOW()
                    AND offerincident_end_date > NOW()
                    GROUP BY offerincident_start_date
                    LIMIT $offset , $limit";
            }
            //die($query);
            $offers = Database::instance()->query($query);
            if (count($offers) && $offers[0]->id)
            {
                foreach($offers as $offer)
                {
                    //$offer_date = $offer->offerincident_date ? $offer->offerincident_date : $offer->offerincident_dateadd;
                    $i = strtotime($offer->offerincident_dateadd); //TODO: Look for way to sort feed by start time instead of date_add as the user views offers based on time of publication not addition
                    $data["feeds"][$i] = array(
                        "id" => $offer->id,
                        "type" => 'ofr',
                        "draw_marker" => 0,
                        "incident_ids" => explode(",", $offer->place_ids),
                        "user" => $offer->user_id,
                        "loc" => $offer->location_id,
                        "title" => $offer->offerincident_title,
                        "msg" => text::limit_chars($offer->offerincident_description, 115, ' ...', true),
                        "date" => $offer->offerincident_start_date,
                        "distance" => $offer->distance,
                        "lat" => $offer->latitude,
                        "lon" => $offer->longitude
                    );
                    
                    /*if ($data["closestfeed"]["distance"] > $offer->distance){
                        $data["closestfeed"]["distance"] = $offer->distance;
                        $data["closestfeed"]["lon"] = $offer->longitude;
                        $data["closestfeed"]["lat"] = $offer->latitude;
                    }*/
                    
                    $users_names[(int)$offer->user_id] = array("id"=>$offer->user_id,"name"=>$offer->user_name,"color"=>$offer->user_color,"username"=>$offer->user_username,"profile"=>$offer->public_profile);
                    
                    $j = 0;
                    $query = "SELECT * FROM `media` WHERE media.offerincident_id = $offer->id";
                    
                    /**foreach (Database::instance()->query($query) as $media)
                    {
                        $data["feeds"][$i]['media'][(int)$j] = array(
                            "id" => $media->id,
                            "type" => $media->media_type,
                            "link" => url::convert_uploaded_to_abs($media->media_link),
                            "medium" => url::convert_uploaded_to_abs($media->media_medium),
                            "thumb" => url::convert_uploaded_to_abs($media->media_thumb)
                        );
                        $j++;
                    }
                    
                    $j = 0;
                    
                    // If we are displaying some extra map data...
                    
                    if($mapdata != '')
                    {
                        
                        if( ! isset($seen_latest_ci[$checkin->user_id]))
                        {
                            $opacity = 1;
                        }else{
                            $opacity = .5;
                        }
                        
                        $seen_latest_ci[$checkin->user_id] = $checkin->user_id;
                        $data["feeds"][$i]['opacity'] = $opacity;
                        
                    }
                    */
                    $i++;
                }
            }
        }

		foreach($users_names as $user_data)
		{
			$data["users"][] = $user_data;
		}
		
		return $data;
	}
	
	// This function helps support some random string action for user accounts and filenames
	//   This supports strings up to 32 characters in length due to the md5 hash
	private function getRandomString($length = 31)
	{
		return substr(md5(uniqid(rand(), true)), 0, $length);
	}
	
	public function show_response()
	{
		if ($this->response_type == 'json')
        {
            echo json_encode($this->response);
        } 
        else 
        {
            echo $this->array_as_xml($this->response, array());
        }
	}
	
	public function random_color()
	{
		$hex = array("00","33","66","99","CC","FF");
		$r1 = array_rand($hex);
		$r2 = array_rand($hex);
		$r3 = array_rand($hex);
		return $hex[$r1].$hex[$r2].$hex[$r3];
	}
	
	public function set_ci_error_message($resp)
	{
		$this->response = $resp;
	}
}
