<?php defined('SYSPATH') or die('No direct script access.');

/**
 * Model for Blog articles
 *
 */

class Article_Tags_Model extends ORM
{
	/**
	 * Database table name
	 * @var string
	 */
	protected $table_name = 'article_tags';
	
	/**
	 * Validates and optionally saves a new page record from an array
	 *
	 * @param array $array
	 * @param $save bool
	 * @return bool
	 */
	public function validate(array & $array, $save = FALSE)
	{
		// Setup validation
		$array = Validation::factory($array)
					->pre_filter('trim', TRUE)
					->add_rules('post_title','required', 'length[3,150]')
					->add_rules('post_author', 'required')
					->add_rules('post_content','required')
					->add_rules('post_image','required');
						
		// Pass validation to parent and return
		return parent::validate($array, $save);
	}
	
	/**
	 * Checks if a tag is non-numeric and exists in the database
	 *
	 * @param int $tag_id
	 * @return bool
	 */
	public static function is_valid_tag($tag)
	{
		return (preg_match('/^[a-zA-z]([a-zA-Z0-9])$/', $tag) > 0)
			? self::factory('article_tags')->where('tag', $tag)->loaded
			: FALSE;
	}
	
	/**
	 * Gets the tags for an article
	 * @param int $article_id Database ID of the tag
	 * @return array array of tags
	 */
	public static function get_article_tags($article_id)
	{
		$tags = array();
		if (Articles_Model::is_valid_article($article_id))
		{
			$db = Database::instance();
			$result = $db->query("SELECT tag FROM article_tags WHERE article_id = $article_id");
			foreach ($result as $row)
				$tags[] = $row->tag;
		}
		return $tags;
	}
	
	/**
	 * Gets the all tags for an article
	 * @return array array of tags
	 */
	public static function get_all_tags()
	{
		$tags = array();
		$db = Database::instance();
		$result = $db->query("SELECT tag, COUNT(tag) 'count' FROM article_tags GROUP BY tag");
		foreach ($result as $row)
			$tags[$row->tag] = $row->count;
		return $tags;
	}
}
