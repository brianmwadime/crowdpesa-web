<?php defined('SYSPATH') or die('No direct script access.');

/**
 * Model for Blog articles
 *
 */

class Articles_Model extends ORM
{
	/**
	 * Database table name
	 * @var string
	 */
	protected $table_name = 'articles';
	protected $has_many = array('media');
	
	/**
	 * Validates and optionally saves a new page record from an array
	 *
	 * @param array $array
	 * @param $save bool
	 * @return bool
	 */
	public function validate(array & $array, $save = FALSE)
	{
		// Setup validation
		$array = Validation::factory($array)
					->pre_filter('trim', TRUE)
					->add_rules('post_title','required', 'length[3,150]')
					->add_rules('post_author', 'required')
					->add_rules('post_content','required')
					->add_rules('post_image','required');
						
		// Pass validation to parent and return
		return parent::validate($array, $save);
	}
	
	/**
	 * Checks if a article_id is non-zero and exists in the database
	 *
	 * @param int $article_id
	 * @return bool
	 */
	public static function is_valid_article($article_id)
	{
		return (preg_match('/^[1-9](\d*)$/', $article_id) > 0)
			? self::factory('articles', $article_id)->loaded
			: FALSE;
	}
	
	/**
	 * Gets articles having the given tags
	 * @param array $tags tags being matched
	 * @return array array of articles
	 */
	public static function get_articles_with_tags($tags, $offset = 0, $records = 5){
		if (empty($tags))
			return false;
		// get all ids of articles having the given tag
		$tags = "('" . implode("','", $tags) . "')";
		//$tags = str_ireplace("''", '', $tags);
		$db = Database::instance();
		$result = $db->query("SELECT article_tags.article_id, articles.title FROM article_tags INNER JOIN articles ON articles.id = article_tags.article_id WHERE tag IN $tags LIMIT $offset, $records");
		return $result;
	}
	
	public static function get_similar_articles_with_tags($article_id, $tags, $offset = 0, $records = 5){
		if (empty($tags))
			return false;
		// get all ids of articles having the given tag
		$tags = "('" . implode("','", $tags) . "')";
		//$tags = str_ireplace("''", '', $tags);
		$db = Database::instance();
		$result = $db->query("SELECT article_tags.article_id, articles.title FROM article_tags INNER JOIN articles ON articles.id = article_tags.article_id WHERE tag IN $tags AND article_tags.article_id != $article_id GROUP BY articles.title LIMIT $offset, $records");
		return $result;
	}
	
	/**
	 * Gets the comments for an article
	 * @param int $article_id Database ID of the article
	 * @return mixed FALSE if the article_id is non-existent, ORM_Iterator if it exists
	 */
	public static function get_comments($article_id)
	{
		if (self::is_valid_article($article_id))
		{
			$db = Database::instance();
			$comments = $db->query("SELECT comment.id, comment.article_id, comment.comment_author, comment.comment_description, comment.comment_date, rating.rating 'comment_rating' FROM comment INNER JOIN rating ON comment.id = rating.comment_id WHERE comment.article_id = $article_id AND comment.comment_active = 1 AND comment.comment_spam = 0");
			return $comments;
		}
		else
		{
			return FALSE;
		}
	}
}
