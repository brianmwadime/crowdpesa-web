<?php defined('SYSPATH') or die('No direct script access.');

/**
 * Model for Checkins
 *
 *
 * PHP version 5
 * LICENSE: This source file is subject to LGPL license
 * that is available through the world-wide-web at the following URI:
 * http://www.gnu.org/copyleft/lesser.html
 * @author     Ushahidi Team <team@ushahidi.com>
 * @package    Ushahidi - http://source.ushahididev.com
 * @subpackage Models
 * @copyright  Ushahidi - http://www.ushahidi.com
 * @license    http://www.gnu.org/copyleft/lesser.html GNU Lesser General Public License (LGPL)
 */

class Checkin_Model extends ORM
{	
	protected $belongs_to = array('user');
	protected $has_many = array('media','comment');
	protected $has_one = array('location','user');

	// Database table name
	protected $table_name = 'checkin';

	//Get retailer Checkin Count
	public static function get_retailer_checkin_count($retailer_id){
		$checkin_count = ORM::factory('checkin')
						->join('location','location.id','location_id','INNER')
						->join('incident','incident.location_id','location.id','LEFT')
						->where('incident.user_id',$retailer_id)
						->count_all();
		return $checkin_count;
	//TODO: add function to get customer checkin count 
	}
}
