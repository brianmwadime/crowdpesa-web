<?php
/**
 * Model for retailer info
 *
 *
 * PHP version 5
 * LICENSE: This source file is subject to LGPL license
 * that is available through the world-wide-web at the following URI:
 * http://www.gnu.org/copyleft/lesser.html
 * @author     Ushahidi Team <team@ushahidi.com>
 * @package    Ushahidi - http://source.ushahididev.com
 * @subpackage Models
 * @copyright  Ushahidi - http://www.ushahidi.com
 * @license    http://www.gnu.org/copyleft/lesser.html GNU Lesser General Public License (LGPL)
 */

class Content_Block_Model extends ORM {

	//protected $belongs_to = array('user');
    
    // Database table name
    protected $table_name = 'content_blocks';
    protected $primary_key = 'name';

    /**
	 * Checks if a article_id is non-zero and exists in the database
	 *
	 * @param int $article_id
	 * @return bool
	 */
	public static function is_valid_content_block($block_name)
	{
		return (!empty($block_name))
			? self::factory('content_block', $block_name)->loaded
			: FALSE;
	}

} // End User_Model
