<?php defined('SYSPATH') or die('No direct script access.');

/**
 * Model for Offer-categories for the Offers
 *
 * PHP version 5
 * LICENSE: This source file is subject to LGPL license 
 * that is available through the world-wide-web at the following URI:
 * http://www.gnu.org/copyleft/lesser.html
 * @author     BeeBuy Team <info@beebuy.com> 
 * @package    CrowdPesa - http://crowdpesa.com
 * @copyright  CrowdPesa - http://crowdpesa.com
 * @license    http://www.gnu.org/copyleft/lesser.html GNU Lesser General Public License (LGPL) 
 */

class Offer_Incident_Model extends ORM {	

	
	/**
	 * Database table name
	 * @var string
	 */
	protected $table_name = 'offer_incidents';
	
	
	

}
