<?php defined('SYSPATH') or die('No direct script access.');

/**
 * Model for Offer-categories for the Offers
 *
 * PHP version 5
 * LICENSE: This source file is subject to LGPL license 
 * that is available through the world-wide-web at the following URI:
 * http://www.gnu.org/copyleft/lesser.html
 * @author     BeeBuy Team <info@beebuy.com> 
 * @package    CrowdPesa - http://crowdpesa.com
 * @copyright  CrowdPesa - http://crowdpesa.com
 * @license    http://www.gnu.org/copyleft/lesser.html GNU Lesser General Public License (LGPL) 
 */

class Offercategory_Model extends ORM_Tree {	
	/**
	 * One-to-many relationship definition
	 * @var array
	 */
	protected $has_many = array('offerincident' => 'offerincident_offercategory', 'offercategory_lang');
	
	/**
	 * Database table name
	 * @var string
	 */
	protected $table_name = 'offercategory';
	
	/**
	 * Name of the child table for this model - recursive
	 * @var string
	 */ 
	protected $children = "offercategory";
	
	/**
	 * Default sort order
	 * @var array
	 */
	protected $sorting = array("offercategory_position" => "asc");
	
	/**
	 * Validates and optionally saves a offer-category record from an array
	 *
	 * @param array $array Values to check
	 * @param bool $save Saves the record when validation succeeds
	 * @return bool
	 */
	public function validate(array & $array, $save = FALSE)
	{
		// Set up validation
		$array = Validation::factory($array)
					->pre_filter('trim', TRUE)
					->add_rules('parent_id','required', 'numeric')
					->add_rules('offercategory_title','required', 'length[3,80]')
					->add_rules('offercategory_description','required')
					->add_rules('offercategory_color','required', 'length[6,6]');
		
		
		// Validation checks where parent_id > 0
		if ($array->parent_id > 0)
		{
			if ( ! empty($this->id) AND ($this->id == $array->parent_id))
			{
				// Error
				Kohana::log('error', 'The parent id and offercategory id are the same!');
				$array->add_error('parent_id', 'same');
			}
			else
			{
				// Ensure parent_id exists in the DB
				$array->add_callbacks('parent_id', 'Offercategory_Model::is_valid_offercategory');
			}
		}
		
		// Pass on validation to parent and return
		return parent::validate($array, $save);
	}
	
	/**
	 * Gets the list of offer-categories from the database as an array
	 *
	 * @param int $offercategory_id Database id of the offercategory
	 * @param string $local Localization to use
	 * @return array
	 */
	public static function offercategories($offercategory_id = NULL, $locale='en_US')
	{
		$offercategories = (empty($offercategory_id) OR ! self::is_valid_offercategory($offercategory_id))
			? ORM::factory('offercategory')->where('locale', $locale)->find_all()
			: ORM::factory('offercategory')->where('id', $offercategory_id)->find_all();
		
		// To hold the return values
		$cats = array();
		
		foreach($offercategories as $offercategory)
		{
			$cats[$offercategory->id]['offercategory_id'] = $offercategory->id;
			$cats[$offercategory->id]['offercategory_title'] = $offercategory->offercategory_title;
			$cats[$offercategory->id]['offercategory_color'] = $offercategory->offercategory_color;
			$cats[$offercategory->id]['offercategory_image'] = $offercategory->offercategory_image;
			$cats[$offercategory->id]['offercategory_image_thumb'] = $offercategory->offercategory_image_thumb;
		}
		
		return $cats;
	}

	/**
	 * Checks if the specified offer-category ID is of type INT and exists in the database
	 *
	 * @param	int	$offercategory_id Database id of the offercategory to be looked up
	 * @return	bool
	 */
	public static function is_valid_offercategory($offercategory_id)
	{
		// Hiding errors/warnings here because child offercategories are seeing offercategory_id as an obj and this fails poorly
		return ( ! is_object($offercategory_id) AND intval($offercategory_id) > 0)
				? self::factory('offercategory', intval($offercategory_id))->loaded
				: FALSE;
	}
	
	/**
	 * Given a parent id, gets the immediate children for the offer-category, else gets the list
	 * of all offer-categories with parent id 0
	 *
	 * @param int $parent_id
	 * @return ORM_Iterator
	 */
	public static function get_offercategories($parent_id = 0, $exclude_trusted = TRUE, $exclude_hidden = TRUE)
	{
		// Check if the specified parent is valid
		$where = (intval($parent_id) > 0 AND self::is_valid_offercategory($parent_id))
			? array('parent_id' => $parent_id)
			: array('parent_id' => 0);
			
		// Make sure the offercategory is visible
		if ($exclude_hidden)
		{
			$where = array_merge($where, array('offercategory_visible' =>'1'));
		}
		
		// Exclude trusted offerreports
		if ($exclude_trusted)
		{
			$where = array_merge($where, array('offercategory_title !=' => 'Trusted Offerreports'));
		}
		
		// Return
		return self::factory('offercategory')
			->where($where)
			->where('offercategory_title != "NONE"')
			->orderby('offercategory_position', 'ASC')
			->orderby('offercategory_title', 'ASC')
			->find_all();
	}
}
