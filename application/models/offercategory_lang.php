<?php defined('SYSPATH') or die('No direct script access.');

/**
 * Model for Localization of Categories
 *
 * PHP version 5
 * LICENSE: This source file is subject to LGPL license
 * that is available through the world-wide-web at the following URI:
 * http://www.gnu.org/copyleft/lesser.html
 * @author     Ushahidi Team <team@ushahidi.com>
 * @package    Ushahidi - http://source.ushahididev.com
 * @subpackage Models
 * @copyright  Ushahidi - http://www.ushahidi.com
 * @license    http://www.gnu.org/copyleft/lesser.html GNU Lesser General Public License (LGPL)
 */

class Offercategory_Lang_Model extends ORM
{
	protected $belongs_to = array('offercategory');

	protected $primary_key = 'id';

	// Database table name
	protected $table_name = 'offercategory_lang';

	static function offercategory_langs($offercategory_id=FALSE)
	{
		if($offercategory_id != FALSE)
		{
			$offercategory_langs = ORM::factory('offercategory_lang')->where(array('offercategory_id'=>$offercategory_id))->find_all();
		}else{
			$offercategory_langs = ORM::factory('offercategory_lang')->find_all();
		}

		$cat_langs = array();
		foreach($offercategory_langs as $offercategory_lang) {
			$cat_langs[$offercategory_lang->offercategory_id][$offercategory_lang->locale]['id'] = $offercategory_lang->id;
			$cat_langs[$offercategory_lang->offercategory_id][$offercategory_lang->locale]['offercategory_title'] = $offercategory_lang->offercategory_title;
		}

		return $cat_langs;
	}

	static function offercategory_title($offercategory_id,$locale)
	{
		$offercategory_lang = ORM::factory('offercategory_lang')
							->where(array('offercategory_id'=>$offercategory_id,'locale'=>$locale))
							->find_all();

		foreach($offercategory_lang as $cat){
			if(isset($cat->offercategory_title) AND $cat->offercategory_title != '')
			{
				return $cat->offercategory_title;
			}
		}
		
		// If we didn't find one, grab the default title
		$offercategory_title = ORM::factory('offercategory')
							->where(array('id'=>$offercategory_id))
							->find_all();
		foreach($offercategory_title as $cat){
			if(isset($cat->offercategory_title) AND $cat->offercategory_title != '')
			{
				return $cat->offercategory_title;
			}
		}
		
		return FALSE;
	}

}
