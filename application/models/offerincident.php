<?php defined('SYSPATH') or die('No direct script access.');

/**
 * Model for reported offers
 *
 *
 * PHP version 5
 * LICENSE: This source file is subject to LGPL license
 * that is available through the world-wide-web at the following URI:
 * http://www.gnu.org/copyleft/lesser.html
 * @author     BeeBuy Team <info@beebuy.com>
 * @package    CrowdPesa - http://crowdpesa.com
 * @copyright  CrowdPesa - http://crowdpesa.com
 * @license    http://www.gnu.org/copyleft/lesser.html GNU Lesser General Public License (LGPL)
 */

class Offerincident_Model extends ORM {
	/**
	 * One-to-may relationship definition
	 * @var array
	 */
	protected $has_many = array('offercategory' => 'offerincident_offercategory', 'media', 'verify', 'comment',
		'rating', 'alert' => 'alert_sent', 'offerincident_lang', 'form_response','cluster' => 'cluster_offerincident',
		'geometry');

	/**
	 * One-to-one relationship definition
	 * @var array
	 */
	protected $has_one = array('location','offerincident_person','user','message','twitter','form','offer_type');

	/**
	 * Many-to-one relationship definition
	 * @var array
	 */
	protected $belongs_to = array('sharing','users');

	/**
	 * Database table name
	 * @var string
	 */
	protected $table_name = 'offerincident';

	/**
	 * Prevents cached items from being reloaded
	 * @var bool
	 */
	protected $reload_on_wakeup   = FALSE;

	/**
	 * Gets a list of all visible offer-categories
	 * @todo Move this to the offercategory model
	 * @return array
	 */
	public static function get_active_offercategories()
	{
		// Get all active offer-categories
		$offercategories = array();
		foreach (ORM::factory('offercategory')
			->where('offercategory_visible', '1')
			->find_all() as $offercategory)
		{
			// Create a list of all offer-categories
			$offercategories[$offercategory->id] = array($offercategory->offercategory_title, $offercategory->offercategory_color);
		}
		return $offercategories;
	}

	/**
	 * Get the total number of offers
	 *
	 * @param boolean $approved - Only count approved offers if true
	 * @return int
	 */
	public static function get_total_offerreports($approved = FALSE)
	{
		return ($approved)
			? ORM::factory('offerincident')->where('offerincident_active', '1')->count_all()
			: ORM::factory('offerincident')->count_all();
	}

	/**
	 * Get the total number of verified or unverified offers
	 *
	 * @param boolean $verified - Only count verified offers if true, unverified if false
	 * @return int
	 */
	public static function get_total_offerreports_by_verified($verified = FALSE)
	{
		return ($verified)
			? ORM::factory('offerincident')->where('offerincident_verified', '1')->where('offerincident_active', '1')->count_all()
			: ORM::factory('offerincident')->where('offerincident_verified', '0')->where('offerincident_active', '1')->count_all();
	}

	/**
	 * Get the earliest offer date
	 *
	 * @param boolean $approved - Oldest approved offer timestamp if true (oldest overall if false)
	 * @return string
	 */
	public static function get_oldest_offerreport_timestamp($approved = TRUE)
	{
		$result = ($approved)
			? ORM::factory('offerincident')->where('offerincident_active', '1')->orderby(array('offerincident_date'=>'ASC'))->find_all(1,0)
			: ORM::factory('offerincident')->where('offerincident_active', '0')->orderby(array('offerincident_date'=>'ASC'))->find_all(1,0);

		foreach($result as $offerreport)
		{
			return strtotime($offerreport->offerincident_date);
		}
	}

	/**
	 * Get the latest offer date
	 * @return string
	 */
	public static function get_latest_offerreport_timestamp($approved = TRUE)
	{
		$result = ($approved)
			? ORM::factory('offerincident')->where('offerincident_active', '1')->orderby(array('offerincident_date'=>'DESC'))->find_all(1,0)
			: ORM::factory('offerincident')->where('offerincident_active', '0')->orderby(array('offerincident_date'=>'DESC'))->find_all(1,0);

		foreach($result as $offerreport)
		{
			return strtotime($offerreport->offerincident_date);
		}
	}

	private static function offercategory_graph_text($sql, $offercategory)
	{
		$db = new Database();
		$query = $db->query($sql);
		$graph_data = array();
		$graph = ", \"".  $offercategory[0] ."\": { label: '". str_replace("'","",$offercategory[0]) ."', ";
		foreach ( $query as $month_count )
		{
			array_push($graph_data, "[" . $month_count->time * 1000 . ", " . $month_count->number . "]");
		}
		$graph .= "data: [". join($graph_data, ",") . "], ";
		$graph .= "color: '#". $offercategory[1] ."' ";
		$graph .= " } ";
		return $graph;
	}

	public static function get_offerincidents_by_interval($interval='month',$start_date=NULL,$end_date=NULL,$active='true',$media_type=NULL)
	{
		// Table Prefix
		$table_prefix = Kohana::config('database.default.table_prefix');

		// get graph data
		// could not use DB query builder. It does not support parentheses yet
		$db = new Database();

		$select_date_text = "DATE_FORMAT(offerincident_date, '%Y-%m-01')";
		$groupby_date_text = "DATE_FORMAT(offerincident_date, '%Y%m')";
		if ($interval == 'day')
		{
			$select_date_text = "DATE_FORMAT(offerincident_date, '%Y-%m-%d')";
			$groupby_date_text = "DATE_FORMAT(offerincident_date, '%Y%m%d')";
		}
		elseif ($interval == 'hour')
		{
			$select_date_text = "DATE_FORMAT(offerincident_date, '%Y-%m-%d %H:%M')";
			$groupby_date_text = "DATE_FORMAT(offerincident_date, '%Y%m%d%H')";
		}
		elseif ($interval == 'week')
		{
			$select_date_text = "STR_TO_DATE(CONCAT(CAST(YEARWEEK(offerincident_date) AS CHAR), ' Sunday'), '%X%V %W')";
			$groupby_date_text = "YEARWEEK(offerincident_date)";
		}

		$date_filter = ($start_date) ? ' AND offerincident_date >= "' . $start_date . '"' : "";

		if ($end_date)
		{
			$date_filter .= ' AND offerincident_date <= "' . $end_date . '"';
		}

		$active_filter = ($active == 'all' || $active == 'false')? $active_filter = '0,1' : '1';

		$joins = '';
		$general_filter = '';
		if (isset($media_type) AND is_numeric($media_type))
		{
			$joins = 'INNER JOIN '.$table_prefix.'media AS m ON m.offerincident_id = i.id';
			$general_filter = ' AND m.media_type IN ('. $media_type  .')';
		}

		$graph_data = array();
		$all_graphs = array();

		$all_graphs['0'] = array();
		$all_graphs['0']['label'] = 'All Offercategories';
		$query_text = 'SELECT UNIX_TIMESTAMP(' . $select_date_text . ') AS time,
					   COUNT(*) AS number
					   FROM '.$table_prefix.'offerincident AS i ' . $joins . '
					   WHERE offerincident_active IN (' . $active_filter .')' .
		$general_filter .'
					   GROUP BY ' . $groupby_date_text;
		$query = $db->query($query_text);
		$all_graphs['0']['data'] = array();
		foreach ( $query as $month_count )
		{
			array_push($all_graphs['0']['data'],
				array($month_count->time * 1000, $month_count->number));
		}
		$all_graphs['0']['color'] = '#990000';

		$query_text = 'SELECT offercategory_id, offercategory_title, offercategory_color, UNIX_TIMESTAMP(' . $select_date_text . ')
							AS time, COUNT(*) AS number
								FROM '.$table_prefix.'offerincident AS i
							INNER JOIN '.$table_prefix.'offerincident_offercategory AS ic ON ic.offerincident_id = i.id
							INNER JOIN '.$table_prefix.'offercategory AS c ON ic.offercategory_id = c.id
							' . $joins . '
							WHERE offerincident_active IN (' . $active_filter . ')
								  ' . $general_filter . '
							GROUP BY ' . $groupby_date_text . ', offercategory_id ';
		$query = $db->query($query_text);
		foreach ($query as $month_count)
		{
			$offercategory_id = $month_count->offercategory_id;
			if (!isset($all_graphs[$offercategory_id]))
			{
				$all_graphs[$offercategory_id] = array();
				$all_graphs[$offercategory_id]['label'] = $month_count->offercategory_title;
				$all_graphs[$offercategory_id]['color'] = '#'. $month_count->offercategory_color;
				$all_graphs[$offercategory_id]['data'] = array();
			}
			array_push($all_graphs[$offercategory_id]['data'],
				array($month_count->time * 1000, $month_count->number));
		}
		$graphs = json_encode($all_graphs);
		return $graphs;
	}

	/**
	 * Get the number of offers by date for dashboard chart
	 *
	 * @param int $range No. of days in the past
	 * @param int $user_id
	 * @return array
	 */
	public static function get_number_offerreports_by_date($range = NULL, $user_id = NULL)
	{
		// Table Prefix
		$table_prefix = Kohana::config('database.default.table_prefix');

		// Database instance
		$db = new Database();

		// Filter by User
		$user_id = (int) $user_id;
		$u_sql = ($user_id)? " AND user_id = ".$user_id." " : "";

		// Query to generate the offerreport count
		$sql = 'SELECT COUNT(id) as count, DATE(offerincident_date) as date, MONTH(offerincident_date) as month, DAY(offerincident_date) as day, '
			. 'YEAR(offerincident_date) as year '
			. 'FROM '.$table_prefix.'offerincident ';

		// Check if the range has been specified and is non-zero then add predicates to the query
		if ($range != NULL AND intval($range) > 0)
		{
			$sql .= 'WHERE offerincident_date >= DATE_SUB(CURDATE(), INTERVAL '.$db->escape_str($range).' DAY) ';
		}
		else
		{
			$sql .= 'WHERE 1=1 ';
		}

		// Group and order the records
		$sql .= $u_sql.'GROUP BY date ORDER BY offerincident_date ASC';

		$query = $db->query($sql);
		$result = $query->result_array(FALSE);

		$array = array();
		foreach ($result AS $row)
		{
			$timestamp = mktime(0, 0, 0, $row['month'], $row['day'], $row['year']) * 1000;
			$array["$timestamp"] = $row['count'];
		}

		return $array;
	}

	/**
	 * Gets a list of dates of all approved offers
	 *
	 * @return array
	 */
	public static function get_offerincident_dates()
	{
		//$offerincidents = ORM::factory('offerincident')->where('offerincident_active',1)->offerincident_date->find_all();
		$offerincidents = ORM::factory('offerincident')->where('offerincident_active',1)->select_list('id', 'offerincident_date');
		$array = array();
		foreach ($offerincidents as $id => $offerincident_date)
		{
			$array[] = $offerincident_date;
		}
		return $array;
	}

	/**
	 * Checks if a specified offer-incident id is numeric and exists in the database
	 *
	 * @param int $offerincident_id ID of the offerincident to be looked up
	 * @param bool $approved Whether the offerincident has been approved
	 * @return bool
	 */
	public static function is_valid_offerincident($offerincident_id, $approved = FALSE)
	{
		$where = ($approved == TRUE)? array("offerincident_active" => "1") : array("id >" => 0);
		return (intval($offerincident_id) > 0)
			? ORM::factory('offerincident')->where($where)->find(intval($offerincident_id))->loaded
			: FALSE;
	}

	/**
	 * Gets the offers that match the conditions specified in the $where parameter
	 * The conditions must relate to columns in the offerincident, location, offerincident_offercategory
	 * offercategory and media tables
	 *
	 * @param array $where List of conditions to apply to the query
	 * @param mixed $limit No. of records to fetch or an instance of Pagination
	 * @param string $order_field Column by which to order the records
	 * @param string $sort How to order the records - only ASC or DESC are allowed
	 * @return Database_Result
	 */
	public static function get_offerincidents($where = array(), $limit = NULL, $order_field = NULL, $sort = NULL)
	{
		// Get the table prefix
		$table_prefix = Kohana::config('database.default.table_prefix');

		// To store radius parameters
		$radius = array();
		$having_clause = "";
		if (array_key_exists('radius', $where))
		{
			// Grab the radius parameter
			$radius = $where['radius'];

			// Delete radius parameter from the list of predicates
			unset ($where['radius']);
		}

		// Query
		$sql = 'SELECT DISTINCT i.id offerincident_id, i.offerincident_title, i.offerincident_description, i.offerincident_date, i.offerincident_mode, i.offerincident_active,i.offerincident_featured,i.offerincident_dateadd, '
			. 'i.offerincident_verified ';

		// Check if all the parameters exist
		// if (count($radius) > 0 AND array_key_exists('latitude', $radius) AND array_key_exists('longitude', $radius)
		// 	AND array_key_exists('distance', $radius))
		// {
		// 	// Calculate the distance of each point from the starting point
		// 	$sql .= ", ((ACOS(SIN(%s * PI() / 180) * SIN(l.`latitude` * PI() / 180) + COS(%s * PI() / 180) * "
		// 		. "	COS(l.`latitude` * PI() / 180) * COS((%s - l.`longitude`) * PI() / 180)) * 180 / PI()) * 60 * 1.1515) AS distance ";

		// 	$sql = sprintf($sql, $radius['latitude'], $radius['latitude'], $radius['longitude']);

		// 	// Set the "HAVING" clause
		// 	$having_clause = "HAVING distance <= ".intval($radius['distance'])." ";
		// }

		$sql .=  'FROM '.$table_prefix.'offerincident i '
			//. 'INNER JOIN '.$table_prefix.'location l ON (i.location_id = l.id) '
			. 'INNER JOIN '.$table_prefix.'offerincident_offercategory ic ON (ic.offerincident_id = i.id) '
			. 'INNER JOIN '.$table_prefix.'offercategory c ON (ic.offercategory_id = c.id) ';
		
		// Check if the all offerreports flag has been specified
		if (array_key_exists('all_offerreports', $where) AND $where['all_offerreports'] == TRUE)
		{
			unset ($where['all_offerreports']);
			$sql .= 'WHERE 1=1 ';
		}
		else
		{
			$sql .= 'WHERE i.offerincident_active = 1 ';
		}

		// Check for the additional conditions for the query
		if ( ! empty($where) AND count($where) > 0)
		{
			foreach ($where as $predicate)
			{
				$sql .= 'AND '.$predicate.' ';
			}
		}

		// Add the having clause
		$sql .= $having_clause;

		// Check for the order field and sort parameters
		if ( ! empty($order_field) AND ! empty($sort) AND (strtoupper($sort) == 'ASC' OR strtoupper($sort) == 'DESC'))
		{
			$sql .= 'ORDER BY '.$order_field.' '.$sort.' ';
		}
		else
		{
			$sql .= 'ORDER BY i.offerincident_date DESC ';
		}

		// Check if the record limit has been specified
		if ( ! empty($limit) AND is_int($limit) AND intval($limit) > 0)
		{
			$sql .= 'LIMIT 0, '.$limit;
		}
		elseif ( ! empty($limit) AND $limit instanceof Pagination_Core)
		{
			$sql .= 'LIMIT '.$limit->sql_offset.', '.$limit->items_per_page;
		}

		// Kohana::log('debug', $sql);
		// Database instance for the query
		$db = new Database();

		// Return
		return $db->query($sql);
	}
public static function fetch_offerincidents($where = array(), $limit = NULL, $order_field = NULL, $sort = NULL)
	{
		// Get the table prefix
		$table_prefix = Kohana::config('database.default.table_prefix');

		// To store radius parameters
		$radius = array();
		$having_clause = "";
		if (array_key_exists('radius', $where))
		{
			// Grab the radius parameter
			$radius = $where['radius'];

			// Delete radius parameter from the list of predicates
			unset ($where['radius']);
		}

		// Query
		$sql = 'SELECT i.incident_id, o.id offerincident_id, o.offerincident_title incident_title, o.offerincident_description incident_description, ic.offercategory_id category_id, o.offerincident_date incident_date, o.offerincident_mode incident_mode, o.offerincident_active incident_active, o.offerincident_featured incident_featured, o.offerincident_dateadd incident_dateadd, o.offerincident_verified incident_verified FROM `offerincident` o  INNER JOIN offerincident_offercategory ic ON ic.offerincident_id = o.id INNER JOIN incident i ON i.id = oi.incident_id GROUP BY oi.incident_id';
        
		$sql = 'SELECT DISTINCT i.id incident_id, i.incident_title, i.incident_description, ic.offercategory_id category_id, i.incident_date, i.incident_mode, i.incident_active, '
            . 'i.incident_verified, i.location_id, l.country_id, l.location_name, l.latitude, l.longitude ';

        // Check if all the parameters exist
        if (count($radius) > 0 AND array_key_exists('latitude', $radius) AND array_key_exists('longitude', $radius)
            AND array_key_exists('distance', $radius))
        {
            // Calculate the distance of each point from the starting point
            $sql .= ", ((ACOS(SIN(%s * PI() / 180) * SIN(l.`latitude` * PI() / 180) + COS(%s * PI() / 180) * "
                . " COS(l.`latitude` * PI() / 180) * COS((%s - l.`longitude`) * PI() / 180)) * 180 / PI()) * 60 * 1.1515) AS distance ";

            $sql = sprintf($sql, $radius['latitude'], $radius['latitude'], $radius['longitude']);

            // Set the "HAVING" clause
            $having_clause = "HAVING distance <= ".intval($radius['distance'])." ";
        }

        $sql .=  'FROM '.$table_prefix.'offerincident o '
            . 'INNER JOIN '.$table_prefix.'offer_incidents oi ON o.id = oi.offerincident_id '
            . 'INNER JOIN '.$table_prefix.'incident i ON i.id = oi.incident_id '
            . 'INNER JOIN '.$table_prefix.'location l ON (i.location_id = l.id) '
            . 'INNER JOIN '.$table_prefix.'offerincident_offercategory ic ON ic.offerincident_id = o.id '
            . 'INNER JOIN '.$table_prefix.'offercategory c ON (ic.offercategory_id = c.id) ';
        
        // Check if the all reports flag has been specified
        if (array_key_exists('all_reports', $where) AND $where['all_reports'] == TRUE)
        {
            unset ($where['all_reports']);
            $sql .= 'WHERE 1=1 ';
        }
        else
        {
            $sql .= 'WHERE i.incident_active = 1 ';
        }

        // Check for the additional conditions for the query
        if ( ! empty($where) AND count($where) > 0)
        {
            foreach ($where as $predicate)
            {
                $sql .= 'AND '.$predicate.' ';
            }
        }
        
        // Group by incident_id to avoid duplicate data
        $sql .= 'GROUP BY oi.incident_id ';
        // Add the having clause
        $sql .= $having_clause;

        // Check for the order field and sort parameters
        if ( ! empty($order_field) AND ! empty($sort) AND (strtoupper($sort) == 'ASC' OR strtoupper($sort) == 'DESC'))
        {
            $sql .= 'ORDER BY '.$order_field.' '.$sort.' ';
        }
        else
        {
            $sql .= 'ORDER BY i.incident_date DESC ';
        }

        // Check if the record limit has been specified
        if ( ! empty($limit) AND is_int($limit) AND intval($limit) > 0)
        {
            $sql .= 'LIMIT 0, '.$limit;
        }
        elseif ( ! empty($limit) AND $limit instanceof Pagination_Core)
        {
            $sql .= 'LIMIT '.$limit->sql_offset.', '.$limit->items_per_page;
        }

        // Kohana::log('debug', $sql);
        // Database instance for the query
        $db = new Database();
//echo $sql;exit;
        // Return
        return $db->query($sql);
	}

	/**xxxxx
	 * Gets the comments for an offer
	 * @param int $offerincident_id Database ID of the offerincident
	 * @return mixed FALSE if the offerincident id is non-existent, ORM_Iterator if it exists
	 */
	public static function get_comments($offerincident_id)
	{
		if (self::is_valid_offerincident($offerincident_id))
		{
			$db = Database::instance();
			$comments = $db->query("SELECT comment.id, comment.offerincident_id, comment.comment_author, comment.comment_description, comment.comment_date, rating.rating 'comment_rating' FROM comment INNER JOIN rating ON comment.id = rating.comment_id WHERE comment.offerincident_id = $offerincident_id AND comment.comment_active = 1 AND comment.comment_spam = 0");
			
			/*$where = array(
				'offerincident_id' => $offerincident_id,
				'comment_active' => '1',
				'comment_spam' => '0'
			);

			// Fetch the comments
			return ORM::factory('comment')
					->where($where)
					->orderby('comment_date', 'asc')
					->find_all();*/
			return $comments;
		}
		else
		{
			return FALSE;
		}
	}

	/**
	 * Given an offer, gets the list of offers within a specified radius
	 *
	 * @param int $offerincident_id Database ID of the offerincident to be used to fetch the neighbours
	 * @param int $distance Radius within which to fetch the neighbouring offerincidents
	 * @param int $num_neigbours Number of neigbouring offerincidents to fetch
	 * @return mixed FALSE is the parameters are invalid, Result otherwise
	 */
	public static function get_neighbouring_offerincidents($offerincident_id, $order_by_distance = FALSE, $distance = 0, $num_neighbours)
	{
		if (self::is_valid_offerincident($offerincident_id))
		{
			// Get the table prefix
			$table_prefix = Kohana::config('database.default.table_prefix');

			// Get the location object and extract the latitude and longitude
			$location = self::factory('offerincident', $offerincident_id)->location;
			$latitude = $location->latitude;
			$longitude = $location->longitude;

			// Garbage collection
			unset ($location);

			// Query to fetch the neighbour
			$sql = "SELECT DISTINCT i.*, l.`latitude`, l.`longitude`, l.location_name, "
				. "((ACOS(SIN($latitude * PI() / 180) * SIN(l.`latitude` * PI() / 180) + COS($latitude * PI() / 180) * "
				. "	COS(l.`latitude` * PI() / 180) * COS(($longitude - l.`longitude`) * PI() / 180)) * 180 / PI()) * 60 * 1.1515) AS distance "
				. "FROM `".$table_prefix."offerincident` AS i "
				. "INNER JOIN `".$table_prefix."location` AS l ON (l.`id` = i.`location_id`) "
				. "INNER JOIN `".$table_prefix."offerincident_offercategory` AS ic ON (i.`id` = ic.`offerincident_id`) "
				. "INNER JOIN `".$table_prefix."offercategory` AS c ON (ic.`offercategory_id` = c.`id`) "
				. "WHERE i.offerincident_active = 1 "
				. "AND i.id <> ".$offerincident_id." ";

			// Check if the distance has been specified
			if (intval($distance) > 0)
			{
				$sql .= "HAVING distance <= ".intval($distance)." ";
			}

			// If the order by distance parameter is TRUE
			if ($order_by_distance)
			{
				$sql .= "ORDER BY distance ASC ";
			}
			else
			{
				$sql .= "ORDER BY i.`offerincident_date` DESC ";
			}

			// Has the no. of neigbours been specified
			if (intval($num_neighbours) > 0)
			{
				$sql .= "LIMIT ".intval($num_neighbours);
			}

			// Fetch records and return
			return Database::instance()->query($sql);
		}
		else
		{
			return FALSE;
		}
	}

	/**
	 * Sets approval of an offer
	 * @param int $offerincident_id
	 * @param int $val Set to 1 or 0 for approved or not approved
	 * @return bool
	 */
	public static function set_approve($offerincident_id,$val)
	{
		$offerincident = ORM::factory('offerincident',$offerincident_id);
		$offerincident->offerincident_active = $val;
		return $offerincident->save();
	}

	/**
	 * Sets offer as verified or not
	 * @param int $offerincident_id
	 * @param int $val Set to 1 or 0 for verified or not verified
	 * @return bool
	 */
	public static function set_verification($offerincident_id,$val)
	{
		$offerincident = ORM::factory('offerincident',$offerincident_id);
		$offerincident->offerincident_verified = $val;
		return $offerincident->save();
	}
	/**
	 * Sets  offer as featured
	 * @param int $offerincident_id
	 * @param int $val Set to 1 or 0 for approved or not approved
	 * @return bool
	 */
	public static function set_featured($offerincident_id,$val)
	{
		$offerincident = ORM::factory('offerincident',$offerincident_id);
		$offerincident->offerincident_featured = $val;
		return $offerincident->save();
	}
	/**
	 *Returns offers collected by user
	 *@param int $user_id
	 *@return Database result 
	 */
	public static function get_collected_offers($user_id,$offset,$items_per_page,$filter){
			$db = Database::instance();
			$collected_offers = $db->query("SELECT orders .* , offerincident_title,offerincident.offerincident_start_date AS offerincident_date, offerincident_description,offer_coupon, media_thumb
FROM orders INNER JOIN offerincident ON offerincident.id = orders.offerincident_id 
LEFT JOIN media ON media.offerincident_id = orders.offerincident_id
WHERE orders.customer_id =$user_id AND $filter GROUP BY orders.offerincident_id LIMIT $offset , $items_per_page");
		return $collected_offers;
	}
    
    /**
     *Returns number of new offers since last time user logged in
     *@return int Number of offers 
     */
    public static function get_new_offers_count(){
            $db = Database::instance();
            $last_login = date("Y-m-d H:i:s", Auth::instance()->get_user()->last_login);// TODO: Kohana writes to this field on login so capture that value before it's overwritten and store in session or in this field
            $new_offers = $db->query("SELECT * FROM offerincident WHERE offerincident_start_date BETWEEN '{$last_login}' AND NOW() AND offerincident_end_date > NOW() AND offerincident_active = 1 ");
        return count($new_offers);
    }
}