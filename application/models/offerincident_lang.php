<?php defined('SYSPATH') or die('No direct script access.');

/**
* Model for Localizations for each Offer
 *
 * PHP version 5
 * LICENSE: This source file is subject to LGPL license 
 * that is available through the world-wide-web at the following URI:
 * http://www.gnu.org/copyleft/lesser.html
 * @author     BeeBuy Team <info@beebuy.com> 
 * @package    CrowdPesa - http://crowdpesa.com
 * @subpackage Models
 * @copyright  CrowdPesa - http://crowdpesa.com
 * @license    http://www.gnu.org/copyleft/lesser.html GNU Lesser General Public License (LGPL) 
 */

class Offerincident_Lang_Model extends ORM
{
	protected $belongs_to = array('offerincident');
	
	// Database table name
	protected $table_name = 'offerincident_lang';

}
