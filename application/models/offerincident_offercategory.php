<?php defined('SYSPATH') or die('No direct script access.');

/**
* Model for Categories for each Offer
 *
 * PHP version 5
 * LICENSE: This source file is subject to LGPL license 
 * that is available through the world-wide-web at the following URI:
 * http://www.gnu.org/copyleft/lesser.html
 * @author     BeeBuy Team <info@beebuy.com> 
 * @package    CrowdPesa - http://crowdpesa.com
 * @subpackage Models
 * @copyright  CrowdPesa - http://crowdpesa.com
 * @license    http://www.gnu.org/copyleft/lesser.html GNU Lesser General Public License (LGPL) 
 */

class Offerincident_Offercategory_Model extends ORM
{
	protected $belongs_to = array('offerincident', 'offercategory');
	
	// Database table name
	protected $table_name = 'offerincident_offercategory';
	
	/**
	 * Assigns a offercategory id to an offerincident if it hasn't already been assigned
	 * @param int $offerincident_id offerincident to assign the offercategory to
	 * @param int $offercategory_id offercategory id of the offercategory you want to assign to the offerincident
	 * @return array
	 */
	public static function assign_offercategory_to_offerincident($offerincident_id,$offercategory_id)
	{	
		
		// Check to see if it is already added to that offercategory
		//    If it's not, add it.
		
		$offerincident_offercategory = ORM::factory('offerincident_offercategory')->where(array('offerincident_id'=>$offerincident_id,'offercategory_id'=>$offercategory_id))->find_all();
		
		if( ! $offerincident_offercategory->count() )
		{
			$new_offerincident_offercategory = ORM::factory('offerincident_offercategory');
			$new_offerincident_offercategory->offercategory_id = $offercategory_id;
			$new_offerincident_offercategory->offerincident_id = $offerincident_id;
			$new_offerincident_offercategory->save();
		}
		
		return true;
	}
}
