<?php defined('SYSPATH') or die('No direct script access.');

/**
 * Model for Countries where incidents occured
 *
 * PHP version 5
 * LICENSE: This source file is subject to LGPL license 
 * that is available through the world-wide-web at the following URI:
 * http://www.gnu.org/copyleft/lesser.html
 * @author     Ushahidi Team <team@ushahidi.com> 
 * @package    Ushahidi - http://source.ushahididev.com
 * @subpackage Models
 * @copyright  Ushahidi - http://www.ushahidi.com
 * @license    http://www.gnu.org/copyleft/lesser.html GNU Lesser General Public License (LGPL) 
 */

class Offersubscription_Model extends ORM
{
	/**
	 * Many-to-one relationship definition
	 *
	 * @var array
	 */
	protected $belongs_to = array('offerincident');
	protected $sorting = false;
	/**
	 * Database table name
	 *
	 * @var string
	 */
	protected $table_name = 'offersubscription';
	
	public static function is_subscribed($user_id, $offercategory_id){
		$dt = ORM::factory('offersubscription')->where(array('user_id'=>$user_id, 'offercategory_id' => $offercategory_id))->find_all();
		if(count($dt) > 0) return true;
		return false;
	}
    
    public static function is_retailer_subscribed($user_id, $retailer_id){
        $dt = ORM::factory('offersubscription')->where(array('user_id' => $user_id, 'retailer_id' => $retailer_id))->find_all();
        if(count($dt) > 0) return true;
        return false;
    }
     //Get Subscription count
    public function get_subscription_count($retailer_id){
    	
    	$subscription_count =  ORM::factory('offersubscription')->where('retailer_id',$retailer_id)->count_all();
    	return $subscription_count;;
    }
}
