<?php
/**
 * Model for Offer type
 *
 *
 * PHP version 5
 * LICENSE: This source file is subject to LGPL license
 * that is available through the world-wide-web at the following URI:
 * http://www.gnu.org/copyleft/lesser.html
 * @author     Ushahidi Team <team@ushahidi.com>
 * @package    Ushahidi - http://source.ushahididev.com
 * @subpackage Models
 * @copyright  Ushahidi - http://www.ushahidi.com
 * @license    http://www.gnu.org/copyleft/lesser.html GNU Lesser General Public License (LGPL)
 */

class Package_Model extends ORM {

	protected $belongs_to = array('users');
    
    // Database table name
    protected $table_name = 'packages';
    protected $primary_key = 'id';
    /**
	 * Validates and optionally saves a category record from an array
	 *
	 * @param array $array Values to check
	 * @param bool $save Saves the record when validation succeeds
	 * @return bool
	 */
	public function validate(array & $array, $save = FALSE)
	{
		$passed_array = $array;
		// Set up validation
		$array = Validation::factory($array)
					->pre_filter('trim', TRUE)
					->add_rules('package_name','required', 'length[3,80]')
					->add_rules('description','required');
		//print_r($passed_array);exit;
		if (parent::validate($array, $save)){
			foreach ($passed_array as $field => $value){
				if (isset($this->$field))
					$this->$field = $value;
				
			}
			//exit;
			return true;
		}
		if ($save)
			$this->save();
		
		// Pass on validation to parent and return
		return false;//parent::validate($array, $save);
	}
	
	/**
	 * Gets the list of categories from the database as an array
	 *
	 * @param int $category_id Database id of the category
	 * @param string $local Localization to use
	 * @return array
	 */
	public static function packages($id = NULL)
	{
		$categories = (empty($id) OR ! self::is_valid_category($id))
			? ORM::factory('category')->where('locale', $locale)->find_all()
			: ORM::factory('category')->where('id', $category_id)->find_all();
		
		// To hold the return values
		$cats = array();
		
		foreach($categories as $category)
		{
			$cats[$category->id]['category_id'] = $category->id;
			$cats[$category->id]['category_title'] = $category->category_title;
			$cats[$category->id]['category_color'] = $category->category_color;
			$cats[$category->id]['category_image'] = $category->category_image;
			$cats[$category->id]['category_image_thumb'] = $category->category_image_thumb;
		}
		
		return $cats;
	}

	/**
	 * Checks if the specified category ID is of type INT and exists in the database
	 *
	 * @param	int	$category_id Database id of the category to be looked up
	 * @return	bool
	 */
	public static function is_valid_package($id)
	{
		// Hiding errors/warnings here because child categories are seeing category_id as an obj and this fails poorly
		return ( ! is_object($id) AND intval($id) > 0)
				? self::factory('package', intval($id))->loaded
				: FALSE;
	}
	
	/**
	 * Given a parent id, gets the immediate children for the category, else gets the list
	 * of all categories with parent id 0
	 *
	 * @param int $parent_id
	 * @return ORM_Iterator
	 */
	public static function get_packages($id = 0)
	{
		
		
		// Return
		return self::factory('package')
			
			->orderby('package_name', 'ASC')
			->find_all();
	}

} // End User_Model
