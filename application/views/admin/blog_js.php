/**
 * Main Blog js file.
 * 
 * Handles javascript stuff related to blog function.
 *
 * PHP version 5
 */

<?php require SYSPATH.'../application/views/admin/form_utils_js.php' ?>

		// Ajax Submission
		function blogAction ( action, confirmAction, article_id )
		{
			var statusMessage;
			if( !isChecked( "article" ) && article_id=='' )
			{ 
				alert('Please select at least one article.');
			} else {
				var answer = confirm('<?php echo Kohana::lang('ui_admin.are_you_sure_you_want_to'); ?> ' + confirmAction + '?')
				if (answer){
					
					// Set Submit Type
					$("#action").attr("value", action);
					
					if (article_id != '') 
					{
						// Submit Form For Single Item
						$("#article_single").attr("value", article_id);
						$("#blogMain").submit();
					}
					else
					{
						// Set Hidden form item to 000 so that it doesn't return server side error for blank value
						$("#article_single").attr("value", "000");
						
						// Submit Form For Multiple Items
						$("#blogMain").submit();
					}
				
				} else {
					return false;
				}
			}
		}
		
		function showLog(id)
		{
			$('#' + id).toggle(400);
		}
		

