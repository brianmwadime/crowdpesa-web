<?php 
/**
 * invites view page.
 *
 * PHP version 5
 * LICENSE: This source file is subject to LGPL license 
 * that is available through the world-wide-web at the following URI:
 * http://www.gnu.org/copyleft/lesser.html
 * @author     Ushahidi Team <team@ushahidi.com> 
 * @package    Ushahidi - http://source.ushahididev.com
 * @module     API Controller
 * @copyright  Ushahidi - http://www.ushahidi.com
 * @license    http://www.gnu.org/copyleft/lesser.html GNU Lesser General Public License (LGPL) 
 */
?>
			<div class="bg">
				<h2>
					<?php echo Kohana::lang('ui_admin.invites'); ?>
				</h2>

<?php
	Event::run('ushahidi_action.admin_invites_custom_layout');
	// Kill the rest of the page if this event has been utilized by a plugin
	if( ! Event::has_run('ushahidi_action.admin_invites_custom_layout')){
?>

				<!-- tabs -->
				<div class="tabs">
					<!-- tab -->
					<div class="tab">
						<ul>
							<li><a href="#" onClick="invitesAction('d', 'DELETE', '')"><?php echo strtoupper(Kohana::lang('ui_main.delete'));?></a></li>
							<li><a href="#" onClick="invitesAction('s', 'ACCEPT', '')"><?php echo strtoupper(Kohana::lang('ui_main.accept'));?></a></li>
						</ul>
					</div>
				</div>
				<?php
				if ($form_error) {
				?>
					<!-- red-box -->
					<div class="red-box">
						<h3><?php echo Kohana::lang('ui_main.error');?></h3>
						<ul><?php echo Kohana::lang('ui_main.select_one');?></ul>
					</div>
				<?php
				}

				if ($form_saved) {
				?>
					<!-- green-box -->
					<div class="green-box" id="submitStatus">
						<h3><?php echo Kohana::lang('ui_admin.invites_success_action');?> <?php echo $form_action; ?> <a href="#" id="hideMessage" class="hide">hide this message</a></h3>
					</div>
				<?php
				}
				?>
				<!-- report-table -->
				<?php print form::open(NULL, array('id' => 'inviteMain', 'name' => 'inviteMain')); ?>
					<input type="hidden" name="action" id="action" value="">
					<input type="hidden" name="level"  id="level"  value="">
					<input type="hidden" name="invite_id[]" id="invite_single" value="">
					<div class="table-holder">
						<table class="table">
							<thead>
								<tr>
									<th class="col-1"><input id="checkall" type="checkbox" class="check-box" onclick="CheckAll( this.id, 'invite_id[]' )" /></th>
									<th class="col-2"><?php echo Kohana::lang('ui_main.email');?></th>
									
									<th class="col-4"><?php echo Kohana::lang('ui_main.actions');?></th>
								</tr>
							</thead>
							<tfoot>
								<tr class="foot">
									<td colspan="4">
										<?php echo $pagination; ?>
									</td>
								</tr>
							</tfoot>
							<tbody>
								<?php
								if ($total_items == 0)
								{
								?>
									<tr>
										<td colspan="4" class="col">
											<h3><?php echo Kohana::lang('ui_main.no_results');?></h3>
										</td>
									</tr>
								<?php	
								}
								foreach ($invites as $invite)
								{
									$invite_id = $invite->id;
									$invite_from = $invite->email;
									
									?>
									<tr>
										<td class="col-1"><input name="invite_id[]" id="invite" value="<?php echo $invite_id; ?>" type="checkbox" class="check-box"/></td>
										<td class="col-2">
											<div class="post">
												
												<?php
												// Action::invite_extra_admin  - invite Additional/Extra Stuff
												Event::run('ushahidi_action.invite_extra_admin', $invite_id);
												?>
											</div>
											<ul class="info">
												<?php
													if(strstr($invite_from,'@')) {
														echo '<a href="mailto:'.$invite_from.'">'.$invite_from.'</a>';
													}else{
														echo '<small><em>Unknown Email Address</em> '.$invite_from.'</small>';
													}
												?>
											</ul>
										</td>
										
										<td class="col-4">
											<ul>
												
												<li><a href="javascript:invitesAction('s','ACCEPT','<?php echo(rawurlencode($invite_id)); ?>')" class=""><?php echo Kohana::lang('ui_main.accept');?></a></li>
												<li><a href="javascript:invitesAction('d','DELETE','<?php echo(rawurlencode($invite_id)); ?>')" class="del"><?php echo Kohana::lang('ui_main.delete');?></a></li>
											</ul>
										</td>
									</tr>
									<?php
								}
								?>
							</tbody>
						</table>
					</div>
				<?php print form::close(); ?>
			</div>

<?php
	}
?>
