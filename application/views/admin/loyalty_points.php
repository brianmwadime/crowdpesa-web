<?php 
/**
 * Sms view page.
 *
 * PHP version 5
 * LICENSE: This source file is subject to LGPL license 
 * that is available through the world-wide-web at the following URI:
 * http://www.gnu.org/copyleft/lesser.html
 * @author     Ushahidi Team <team@ushahidi.com> 
 * @package    Ushahidi - http://source.ushahididev.com
 * @module     API Controller
 * @copyright  Ushahidi - http://www.ushahidi.com
 * @license    http://www.gnu.org/copyleft/lesser.html GNU Lesser General Public License (LGPL) 
 */
?>
			<div class="bg">

				<h2>
					<?php admin::settings_subtabs("loyaltypoints"); ?>
				</h2>
				<?php print form::open(); ?>
				<div class="report-form">
					<?php
					if ($form_error) {
					?>
						<!-- red-box -->
						<div class="red-box">
							<h3><?php echo Kohana::lang('ui_main.error');?></h3>
							<ul>
							<?php
							foreach ($errors as $error_item => $error_description)
							{
								// print "<li>" . $error_description . "</li>";
								print (!$error_description) ? '' : "<li>" . $error_description . "</li>";
							}
							?>
							</ul>
						</div>
					<?php
					}

					if ($form_saved) {
					?>
						<!-- green-box -->
						<div class="green-box">
							<h3><?php echo Kohana::lang('ui_main.configuration_saved');?></h3>
						</div>
					<?php
					}
					?>				
					<div class="head">
						<h3><?php echo Kohana::lang('settings.loyaltypoints.title');?></h3>
						<input type="image" src="<?php echo url::file_loc('img'); ?>media/img/admin/btn-cancel.gif" class="cancel-btn" />
						<input type="image" src="<?php echo url::file_loc('img'); ?>media/img/admin/btn-save-settings.gif" class="save-rep-btn" />
					</div>
					<!-- column -->
		
					<div class="sms_holder">
                    <div class="row" style="margin-top:20px;">
							<h4>Configure the Loyalty Points Award System</h4>
						</div>
						<div class="row">
							<h4><?php echo Kohana::lang('ui_main.points_per_checkin');?>: <span><?php echo Kohana::lang('settings.loyaltypoints.checkinpoints');?></span></h4>
							<?php print form::input('points_per_checkin', $form['points_per_checkin'], ' class="text title_2"'); ?>
						</div>

						
						<div class="row">
							<h4><?php echo Kohana::lang('ui_main.points_per_service');?>: <span><?php echo Kohana::lang('settings.loyaltypoints.servicepoints');?></span></h4>
							<?php print form::input('points_per_service', $form['points_per_service'], ' class="text title_2"'); ?>
						</div>
						<div class="row">
							<h4><?php echo Kohana::lang('ui_main.offer_value');?>: <span><?php echo Kohana::lang('settings.loyaltypoints.value');?></span></h4>
							<?php print form::input('offer_value', $form['offer_value'], ' class="text title_2"'); ?>
						</div>
						<div class="row">
							<h4><?php echo Kohana::lang('ui_main.offer_point_value');?>: <span><?php echo Kohana::lang('settings.loyaltypoints.valuepoints');?></span></h4>
							<?php print form::input('offer_point_value', $form['offer_point_value'], ' class="text title_2"'); ?>
						</div>
					</div>
		
					<div class="simple_border"></div>
		
					<input type="image" src="<?php echo url::file_loc('img'); ?>media/img/admin/btn-save-settings.gif" class="save-rep-btn" />
					<input type="image" src="<?php echo url::file_loc('img'); ?>media/img/admin/btn-cancel.gif" class="cancel-btn" />
				</div>
				<?php print form::close(); ?>
			</div>
