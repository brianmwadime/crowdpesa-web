<?php 
/**
 * Categories view page.
 *
 * PHP version 5
 * LICENSE: This source file is subject to LGPL license 
 * that is available through the world-wide-web at the following URI:
 * http://www.gnu.org/copyleft/lesser.html
 * @author     Ushahidi Team <team@ushahidi.com> 
 * @package    Ushahidi - http://source.ushahididev.com
 * @module     API Controller
 * @copyright  Ushahidi - http://www.ushahidi.com
 * @license    http://www.gnu.org/copyleft/lesser.html GNU Lesser General Public License (LGPL) 
 */
?>
			<div class="bg">
				<h2>
					<?php admin::offerreports_subtabs("offer_types"); ?>
				</h2>
				<?php
				if ($form_error) {
				?>
					<!-- red-box -->
					<div class="red-box">
						<h3><?php echo Kohana::lang('ui_main.error');?></h3>
						<ul>
						<?php
						foreach ($errors as $error_item => $error_description)
						{
							// print "<li>" . $error_description . "</li>";
							print (!$error_description) ? '' : "<li>" . $error_description . "</li>";
						}
						?>
						</ul>
					</div>
				<?php
				}

				if ($form_saved) {
				?>
					<!-- green-box -->
					<div class="green-box">
						<h3><?php echo Kohana::lang('ui_main.category_has_been');?> <?php echo $form_action; ?>!</h3>
					</div>
				<?php
				}
				?>
				
				<!-- tabs -->
				<div class="tabs">
					<!-- tabset -->
					<a name="add"></a>
					<ul class="tabset">
						<li><a href="#" class="active" onclick="show_addedit(true)"><?php echo Kohana::lang('ui_main.add_edit');?></a></li>
					</ul>
					<!-- tab -->
					<div class="tab" id="addedit" style="display:none">
						<?php print form::open(NULL,array('enctype' => 'multipart/form-data', 
							'id' => 'catMain', 'name' => 'catMain')); ?>
						<input type="hidden" id="offer_type_id" name="offer_type_id" value="<?php echo $form['offer_type_id']; ?>" />
						<input type="hidden" name="action" id="action" value="a"/>
						<div class="tab_form_item">
							<strong><?php echo Kohana::lang('ui_main.title');?>:</strong><br />
							<?php print form::input('title', $form['title'], ' class="text"'); ?><br/>
							
							
						</div>

						

						<div class="tab_form_item">
							<strong><?php echo Kohana::lang('ui_main.description');?>:</strong><br />
							<?php print form::input('description', $form['description'], ' class="long"'); ?>
						</div>
						
					
						<div style="clear:both"></div>
						<div class="tab_form_item">
							<strong><?php echo Kohana::lang('ui_main.image_icon');?>:</strong><br />
							<?php

								// I removed $category_image from the second parameter to fix bug #161
								print form::upload('icon', '', '');
							?>
						</div>
						<div style="clear:both"></div>
						<div class="tab_form_item">
							&nbsp;<br />
							<input type="image" src="<?php echo url::file_loc('img'); ?>media/img/admin/btn-save.gif" class="save-rep-btn" />
						</div>
						<?php print form::close(); ?>			
					</div>
				</div>
				
				<!-- report-table -->
				<div class="report-form">
					<?php print form::open(NULL,array('id' => 'catListing',
					 	'name' => 'catListing')); ?>
						<input type="hidden" name="action" id="category_action" value="">
						<input type="hidden" name="offer_type_id" id="category_id_action" value="">
						<div class="table-holder">
							<table class="table" id="categorySort">
								<thead>
									<tr class="nodrag">
										<th class="col-1">&nbsp;</th>
										<th class="col-2"><?php echo Kohana::lang('ui_main.offer_types');?></th>
										<th class="col-3"><?php echo Kohana::lang('ui_main.icon');?></th>
										<th class="col-4"><?php echo Kohana::lang('ui_main.actions');?></th>
									</tr>
								</thead>
								<tfoot>
									<tr class="foot nodrag">
										<td colspan="4">
											<?php echo $pagination; ?>
										</td>
									</tr>
								</tfoot>
								<tbody>
									<?php
									if ($total_items == 0)
									{
									?>
										<tr class="nodrag">
											<td colspan="4" class="col" id="row1">
												<h3><?php echo Kohana::lang('ui_main.no_results');?></h3>
											</td>
										</tr>
									<?php	
									}
									foreach ($offertypes as $offertype)
									{
										$offer_type_id = $offertype->id;
										$title = $offertype->title;
										$description = substr($offertype->description, 0, 150);
										$offertype_image = ($offertype->icon != NULL) ? url::convert_uploaded_to_abs($offertype->icon) : NULL;
										
										?>
										<tr id="<?php echo $offer_type_id; ?>">
											<td class="col-1 col-drag-handle">&nbsp;</td>
											<td class="col-2">
												<div class="post">
													<h4><?php echo $title; ?></h4>
													<p><?php echo $description; ?></p>
												</div>
											</td>
											<td class="col-3">
											<?php 
												echo "<img src=\"".$offertype_image."\">";
												echo "&nbsp;[<a href=\"javascript:catAction('i','DELETE ICON','".rawurlencode($offer_type_id)."')\">".Kohana::lang('ui_main.delete')."</a>]";
											
											?>
											</td>
											<td class="col-4">
												<ul>
													<li class="none-separator"><a href="#add" onClick="fillFields('<?php echo(rawurlencode($offer_type_id)); ?>','<?php echo(rawurlencode($title)); ?>','<?php echo(rawurlencode($description)); ?>')"><?php echo Kohana::lang('ui_main.edit');?></a></li>
													<li><a href="javascript:catAction('d','DELETE','<?php echo(rawurlencode($offer_type_id)); ?>')" class="del"><?php echo Kohana::lang('ui_main.delete');?></a></li>
												</ul>
												
												
												
											</td>
										</tr>
										<?php
										
										// Get All Category Children
										}?>
								</tbody>
							</table>
						</div>
					<?php print form::close(); ?>
				</div>

			</div>
