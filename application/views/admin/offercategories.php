<?php 
/**
 * Offercategories view page.
 *
 * PHP version 5
 * LICENSE: This source file is subject to LGPL license 
 * that is available through the world-wide-web at the following URI:
 * http://www.gnu.org/copyleft/lesser.html
 * @author     BeeBuy Team <info@beebuy.com> 
 * @package    CrowdPesa - http://crowdpesa.com
 * @module     API Controller
 * @copyright  CrowdPesa - http://crowdpesa.com
 * @license    http://www.gnu.org/copyleft/lesser.html GNU Lesser General Public License (LGPL) 
 */
?>
			<div class="bg">
				<h2>
					<?php admin::manage_subtabs("offercategories"); ?>
				</h2>
				<?php
				if ($form_error) {
				?>
					<!-- red-box -->
					<div class="red-box">
						<h3><?php echo Kohana::lang('ui_main.error');?></h3>
						<ul>
						<?php
						foreach ($errors as $error_item => $error_description)
						{
							// print "<li>" . $error_description . "</li>";
							print (!$error_description) ? '' : "<li>" . $error_description . "</li>";
						}
						?>
						</ul>
					</div>
				<?php
				}

				if ($form_saved) {
				?>
					<!-- green-box -->
					<div class="green-box">
						<h3><?php echo Kohana::lang('ui_main.offercategory_has_been');?> <?php echo $form_action; ?>!</h3>
					</div>
				<?php
				}
				?>
				
				<!-- tabs -->
				<div class="tabs">
					<!-- tabset -->
					<a name="add"></a>
					<ul class="tabset">
						<li><a href="#" class="active" onclick="show_addedit(true)"><?php echo Kohana::lang('ui_main.add_edit');?></a></li>
					</ul>
					<!-- tab -->
					<div class="tab" id="addedit" style="display:none">
						<?php print form::open(NULL,array('enctype' => 'multipart/form-data', 
							'id' => 'catMain', 'name' => 'catMain')); ?>
						<input type="hidden" id="offercategory_id" name="offercategory_id" value="<?php echo $form['offercategory_id']; ?>" />
						<input type="hidden" name="action" id="action" value="a"/>
						<div class="tab_form_item">
							<strong><?php echo Kohana::lang('ui_main.offercategory_name');?>:</strong><br />
							<?php print form::input('offercategory_title', $form['offercategory_title'], ' class="text"'); ?><br/>
							<a href="#" id="offercategory_translations" class="new-cat" style="clear:both;">Offercategory Translations</a>
							<div id="offercategory_translations_form_fields" style="display:none;">
								<div style="clear:both;"></div>
								<?php
									foreach($locale_array as $lang_key => $lang_name){
										echo '<div style="margin-top:10px;"><strong>'.$lang_name.':</strong></div>';
										print form::input('offercategory_title_lang['.$lang_key.']', $form['offercategory_title_'.$lang_key], ' class="text" id="offercategory_title_'.$lang_key.'"');
										echo '<br />';
									}
								?>

							</div>
						</div>

						<script type="text/javascript">
						    $(document).ready(function() {

						    $('a#offercategory_translations').click(function() {
							    $('#offercategory_translations_form_fields').toggle(400);
							    $('#offercategory_translations').toggle(0);
							    return false;
							});

							});
						</script>

						<div class="tab_form_item">
							<strong><?php echo Kohana::lang('ui_main.description');?>:</strong><br />
							<?php print form::input('offercategory_description', $form['offercategory_description'], ' class="text"'); ?>
						</div>
						<div class="tab_form_item">
							<strong><?php echo Kohana::lang('ui_admin.color');?>:</strong><br />
							<?php print form::input('offercategory_color', $form['offercategory_color'], ' class="text"'); ?>
							<script type="text/javascript" charset="utf-8">
								$(document).ready(function() {
									$('#offercategory_color').ColorPicker({
										onSubmit: function(hsb, hex, rgb) {
											$('#offercategory_color').val(hex);
										},
										onChange: function(hsb, hex, rgb) {
											$('#offercategory_color').val(hex);
										},
										onBeforeShow: function () {
											$(this).ColorPickerSetColor(this.value);
										}
									})
									.bind('keyup', function(){
										$(this).ColorPickerSetColor(this.value);
									});
								});
							</script>
						</div>
						<div class="tab_form_item">
							<strong><?php echo Kohana::lang('ui_main.parent_offercategory');?>:</strong><br />
							<?php print form::dropdown('parent_id', $parents_array, '0'); ?>
						</div>
						<div style="clear:both"></div>
						<div class="tab_form_item">
							<strong><?php echo Kohana::lang('ui_main.image_icon');?>:</strong><br />
							<?php

								// I removed $offercategory_image from the second parameter to fix bug #161
								print form::upload('offercategory_image', '', '');
							?>
						</div>
						<div style="clear:both"></div>
						<div class="tab_form_item">
							&nbsp;<br />
							<input type="image" src="<?php echo url::file_loc('img'); ?>media/img/admin/btn-save.gif" class="save-rep-btn" />
						</div>
						<?php print form::close(); ?>			
					</div>
				</div>
				
				<!-- offerreport-table -->
				<div class="offerreport-form">
					<?php print form::open(NULL,array('id' => 'catListing',
					 	'name' => 'catListing')); ?>
						<input type="hidden" name="action" id="offercategory_action" value="">
						<input type="hidden" name="offercategory_id" id="offercategory_id_action" value="">
						<div class="table-holder">
							<table class="table" id="offercategorySort">
								<thead>
									<tr class="nodrag">
										<th class="col-1">&nbsp;</th>
										<th class="col-2"><?php echo Kohana::lang('ui_main.offercategory');?></th>
										<th class="col-3"><?php echo Kohana::lang('ui_main.color');?></th>
										<th class="col-4"><?php echo Kohana::lang('ui_main.actions');?></th>
									</tr>
								</thead>
								<tfoot>
									<tr class="foot nodrag">
										<td colspan="4">
											<?php echo $pagination; ?>
										</td>
									</tr>
								</tfoot>
								<tbody>
									<?php
									if ($total_items == 0)
									{
									?>
										<tr class="nodrag">
											<td colspan="4" class="col" id="row1">
												<h3><?php echo Kohana::lang('ui_main.no_results');?></h3>
											</td>
										</tr>
									<?php	
									}
									foreach ($offercategories as $offercategory)
									{
										$offercategory_id = $offercategory->id;
										$parent_id = $offercategory->parent_id;
										$offercategory_title = $offercategory->offercategory_title;
										$offercategory_description = substr($offercategory->offercategory_description, 0, 150);
										$offercategory_color = $offercategory->offercategory_color;
										$offercategory_image = ($offercategory->offercategory_image != NULL) ? url::convert_uploaded_to_abs($offercategory->offercategory_image) : NULL;
										$offercategory_visible = $offercategory->offercategory_visible;
										$offercategory_trusted = $offercategory->offercategory_trusted;
										$offercategory_locals = array();
										foreach($offercategory->offercategory_lang as $offercategory_lang){
											$offercategory_locals[$offercategory_lang->locale] = $offercategory_lang->offercategory_title;
										}
										?>
										<tr id="<?php echo $offercategory_id; ?>">
											<td class="col-1 col-drag-handle">&nbsp;</td>
											<td class="col-2">
												<div class="post">
													<h4><?php echo $offercategory_title; ?></h4>
													<p><?php echo $offercategory_description; ?></p>
												</div>
											</td>
											<td class="col-3">
											<?php if (!empty($offercategory_image))
											{
												echo "<img src=\"".$offercategory_image."\">";
												echo "&nbsp;[<a href=\"javascript:catAction('i','DELETE ICON','".rawurlencode($offercategory_id)."')\">".Kohana::lang('ui_main.delete')."</a>]";
											}
											else
											{
												echo "<img src=\"".url::base()."swatch/?c=".$offercategory_color."&w=30&h=30\">";
											}
											?>
											</td>
											<td class="col-4">
												<ul>
													<li class="none-separator"><a href="#add" onClick="fillFields('<?php echo(rawurlencode($offercategory_id)); ?>','<?php echo(rawurlencode($parent_id)); ?>','<?php echo(rawurlencode($offercategory_title)); ?>','<?php echo(rawurlencode($offercategory_description)); ?>','<?php echo(rawurlencode($offercategory_color)); ?>','<?php echo(rawurlencode($offercategory_image)); ?>'<?php
													foreach($locale_array as $lang_key => $lang_name){
														echo ',';
														if(isset($offercategory_locals[$lang_key])){
															echo ' \''.rawurlencode($offercategory_locals[$lang_key]).'\'';
														}else{
															echo ' \'\'';
														}
													}
													?>)"><?php echo Kohana::lang('ui_main.edit');?></a></li>
													<li class="none-separator"><a class="status_yes" href="javascript:catAction('v','SHOW/HIDE','<?php echo(rawurlencode($offercategory_id)); ?>')"><?php if ($offercategory_visible) { echo Kohana::lang('ui_main.visible'); } else { echo Kohana::lang('ui_main.hidden'); }?></a></li>
<li><a href="javascript:catAction('d','DELETE','<?php echo(rawurlencode($offercategory_id)); ?>')" class="del"><?php echo Kohana::lang('ui_main.delete');?></a></li>
												</ul>
												
												<?php if($offercategory_trusted == 1) { ?>
												<div class="right">
													<?php if($offercategory_title == 'Trusted Offerreports') { ?>
													<a href="#" class="tooltip" title="<?php echo htmlentities(Kohana::lang('ui_admin.special_offercategory_explanation'),ENT_QUOTES);?>"><strong><?php echo Kohana::lang('ui_admin.special_offercategory');?></strong></a>
													<?php } else {?>
														<a href="#" class="tooltip" title="<?php echo htmlentities(Kohana::lang('ui_admin.orphaned_offercategory_explanation'),ENT_QUOTES); ?>"><strong><?php echo Kohana::lang('ui_admin.special_offercategory');?></strong></a>
													<?php } ?>
												</div>
												<?php } ?>
												
											</td>
										</tr>
										<?php
										
										// Get All Offercategory Children
										foreach ( $offercategory->children as $child)
										{
											$offercategory_id = $child->id;
											$parent_id = $child->parent_id;
											$offercategory_title = $child->offercategory_title;
											$offercategory_description = substr($child->offercategory_description, 0, 150);
											$offercategory_color = $child->offercategory_color;
											$offercategory_image = ($child->offercategory_image != NULL) ? url::convert_uploaded_to_abs($child->offercategory_image) : NULL;
											$offercategory_visible = $child->offercategory_visible;

											$child_offercategory_locals = array();
											foreach($child->offercategory_lang as $offercategory_lang){
												$child_offercategory_locals[$offercategory_lang->locale] = $offercategory_lang->offercategory_title;
											}

											?>
											<tr id="<?php echo $offercategory_id; ?>">
												<td class="col-1 col-drag-handle">&nbsp;</td>
												<td class="col-2_sub">
													<div class="post">
														<h4><?php echo $offercategory_title; ?></h4>
														<p><?php echo $offercategory_description; ?>...</p>
													</div>
												</td>
												<td class="col-3">
												<?php if (!empty($offercategory_image))
												{
													echo "<img src=\"".$offercategory_image."\">";
													echo "&nbsp;[<a href=\"javascript:catAction('i','DELETE ICON','".rawurlencode($offercategory_id)."')\">delete</a>]";
												}
												else
												{
													echo "<img src=\"".url::base()."swatch/?c=".$offercategory_color."&w=30&h=30\">";
												}
												?>
												</td>
												<td class="col-4">
													<ul>
														<li class="none-separator"><a href="#add" onClick="fillFields('<?php echo(rawurlencode($offercategory_id)); ?>','<?php echo(rawurlencode($parent_id)); ?>','<?php echo(rawurlencode($offercategory_title)); ?>','<?php echo(rawurlencode($offercategory_description)); ?>','<?php echo(rawurlencode($offercategory_color)); ?>','<?php echo(rawurlencode($offercategory_image)); ?>'<?php
													foreach($locale_array as $lang_key => $lang_name){
														echo ',';
														if(isset($child_offercategory_locals[$lang_key])){
															echo ' \''.rawurlencode($child_offercategory_locals[$lang_key]).'\'';
														}else{
															echo ' \'\'';
														}
													}
													?>)"><?php echo Kohana::lang('ui_main.edit');?></a></li>
														<li class="none-separator"><a class="status_yes" href="javascript:catAction('v','SHOW/HIDE','<?php echo(rawurlencode($offercategory_id)); ?>')"><?php if ($offercategory_visible) { echo Kohana::lang('ui_main.visible'); } else { echo Kohana::lang('ui_main.hidden'); }?></a></li>
	<li><a href="javascript:catAction('d','DELETE','<?php echo(rawurlencode($offercategory_id)); ?>')" class="del"><?php echo Kohana::lang('ui_main.delete');?></a></li>
													</ul>
												</td>
											</tr>
											<?php
										}										
									}
									?>
								</tbody>
							</table>
						</div>
					<?php print form::close(); ?>
				</div>

			</div>
