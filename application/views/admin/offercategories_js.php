/**
 * Offercategories js file.
 * 
 * Handles javascript stuff related to offercategory function.
 *
 * PHP version 5
 * LICENSE: This source file is subject to LGPL license 
 * that is available through the world-wide-web at the following URI:
 * http://www.gnu.org/copyleft/lesser.html
 * @author     BeeBuy Team <info@beebuy.com> 
 * @package    CrowdPesa - http://crowdpesa.com
 * @module     API Controller
 * @copyright  CrowdPesa - http://crowdpesa.com
 * @license    http://www.gnu.org/copyleft/lesser.html GNU Lesser General Public License (LGPL) 
 */

$(document).ready(function() {
    // Initialise the table
	$("#offercategorySort").tableDnD({
		dragHandle: "col-drag-handle",
		onDragClass: "col-drag",
		onDrop: function(table, row) {
			var rows = table.tBodies[0].rows;
			var offercategoriesArray = [];
			for (var i=0; i<rows.length; i++) {
				offercategoriesArray[i] = rows[i].id;
			}
			var offercategories = offercategoriesArray.join(',');
			$.post("<?php echo url::site() . 'admin/manage/offercategory_sort/' ?>", { offercategories: offercategories },
				function(data){
					if (data == "ERROR") {
						alert("Invalid Placement!!\n You cannot place a suboffercategory on top of a offercategory.");
					} else {
						$("#offercategorySort"+" tbody tr td").effect("highlight", {}, 500);
					}
			});
		}
	});
	
	$("#offercategorySort tr").hover(function() {
		$(this.cells[0]).addClass('col-show-handle');
	}, function() {
		$(this.cells[0]).removeClass('col-show-handle');
	});
});

// Offercategories JS
function fillFields(id, parent_id, offercategory_title, offercategory_description, offercategory_color, locale<?php foreach($locale_array as $lang_key => $lang_name) echo ', '.$lang_key; ?>)
{
	show_addedit();
	$("#offercategory_id").attr("value", decodeURIComponent(id));
	$("#parent_id").attr("value", decodeURIComponent(parent_id));
	$("#offercategory_title").attr("value", decodeURIComponent(offercategory_title));
	$("#offercategory_description").attr("value", decodeURIComponent(offercategory_description));
	$("#offercategory_color").attr("value", decodeURIComponent(offercategory_color));
	$("#locale").attr("value", decodeURIComponent(locale));
	<?php
		foreach($locale_array as $lang_key => $lang_name) {
			echo '$("#offercategory_title_'.$lang_key.'").attr("value", decodeURIComponent('.$lang_key.'));'."\n";
		}
	?>
}

// Ajax Submission
function catAction ( action, confirmAction, id )
{
	var statusMessage;
	var answer = confirm('<?php echo Kohana::lang('ui_admin.are_you_sure_you_want_to'); ?> ' + confirmAction + '?')
	if (answer){
		// Set Offercategory ID
		$("#offercategory_id_action").attr("value", id);
		// Set Submit Type
		$("#offercategory_action").attr("value", action);
		// Submit Form
		$("#catListing").submit();
	}
}