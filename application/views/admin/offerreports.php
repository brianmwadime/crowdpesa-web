<?php
/**
 * Offerreports view page.
 *
 * PHP version 5
 * LICENSE: This source file is subject to LGPL license
 * that is available through the world-wide-web at the following URI:
 * http://www.gnu.org/copyleft/lesser.html
 * @author     BeeBuy Team <info@beebuy.com>
 * @package    CrowdPesa - http://crowdpesa.com
 * @module     API Controller
 * @copyright  CrowdPesa - http://crowdpesa.com
 * @license    http://www.gnu.org/copyleft/lesser.html GNU Lesser General Public License (LGPL)
 */
?>
			<div class="bg">
				<h2>
					<?php admin::offerreports_subtabs("view"); ?>
				</h2>
				<!-- tabs -->
				<div class="tabs">
					<!-- tabset -->
					<ul class="tabset">
						<li>
							<a href="?status=0" <?php if ($status != 'a' AND $status !='v' AND $status != 'o') echo "class=\"active\""; ?>><?php echo Kohana::lang('ui_main.show_all');?></a>
						</li>
						<li><a href="?status=a" <?php if ($status == 'a') echo "class=\"active\""; ?>><?php echo Kohana::lang('ui_main.awaiting_approval');?></a></li>
						<li><a href="?status=o" <?php if ($status == 'o') echo "class=\"active\""; ?>><?php echo
Kohana::lang('ui_main.orphaned_offerreports'); ?></a></li>
					</ul>
					<!-- tab -->
					<div class="tab">
						<ul>
							<li><a href="#" onclick="offerreportAction('a','<?php echo strtoupper(Kohana::lang('ui_main.approve')); ?>', '');">
								<?php echo Kohana::lang('ui_main.approve');?></a>
							</li>
							<li><a href="#" onclick="offerreportAction('u','<?php echo strtoupper(Kohana::lang('ui_main.disapprove')); ?>', '');">
								<?php echo Kohana::lang('ui_main.disapprove');?></a>
							</li>
							<li><a href="#" onclick="offerreportAction('f','<?php echo strtoupper(Kohana::lang('ui_admin.feature_unfeature')); ?>', '');">
								<?php echo Kohana::lang('ui_admin.feature_unfeature');?></a>
							</li>
							<li><a href="#" onclick="offerreportAction('d','<?php echo strtoupper(Kohana::lang('ui_main.delete')); ?>', '');">
								<?php echo Kohana::lang('ui_main.delete');?></a>
							</li>
						</ul>
					</div>
				</div>
				<?php if ($form_error): ?>
					<!-- red-box -->
					<div class="red-box">
						<h3><?php echo Kohana::lang('ui_main.error');?></h3>
						<ul><?php echo Kohana::lang('ui_main.select_one');?></ul>
					</div>
				<?php endif; ?>
				
				<?php if ($form_saved): ?>
					<!-- green-box -->
					<div class="green-box" id="submitStatus">
						<h3><?php echo Kohana::lang('ui_main.offerreports');?> <?php echo $form_action; ?> <a href="#" id="hideMessage" class="hide">hide this message</a></h3>
					</div>
				<?php endif; ?>
				
				<!-- offerreport-table -->
				<?php print form::open(NULL, array('id' => 'offerreportMain', 'name' => 'offerreportMain')); ?>
					<input type="hidden" name="action" id="action" value="">
					<input type="hidden" name="offerincident_id[]" id="offerincident_single" value="">
					<div class="table-holder">
						<table class="table">
							<thead>
								<tr>
									<th class="col-1"><input id="checkallofferincidents" type="checkbox" class="check-box" onclick="CheckAll( this.id, 'offerincident_id[]' )" /></th>
									<th class="col-2"><?php echo Kohana::lang('ui_main.offerreport_details');?></th>
									<th class="col-3"><?php echo Kohana::lang('ui_main.date');?></th>
									<th class="col-4"><?php echo Kohana::lang('ui_main.actions');?></th>
								</tr>
							</thead>
							<tfoot>
								<tr class="foot">
									<td colspan="4">
										<?php echo $pagination; ?>
									</td>
								</tr>
							</tfoot>
							<tbody>
							<?php if ($total_items == 0): ?>
								<tr>
									<td colspan="4" class="col">
										<h3><?php echo Kohana::lang('ui_main.no_results');?></h3>
									</td>
								</tr>
							<?php endif; ?>
							<?php
								foreach ($offerincidents as $offerincident)
								{
									$offerincident_id = $offerincident->offerincident_id;
									$offerincident_title = strip_tags($offerincident->offerincident_title);
									$offerincident_description = text::limit_chars(strip_tags($offerincident->offerincident_description), 150, "...", true);
									$offerincident_date = $offerincident->offerincident_dateadd;
									$offerincident_date = date('Y-m-d', strtotime($offerincident->offerincident_dateadd));
									$offerincident_featured = $offerincident->offerincident_featured;
									$feature_action = ($offerincident_featured == '1')? Kohana::lang('ui_main.unfeature'):Kohana::lang('ui_main.feature');
									
									// Mode of submission... WEB/SMS/EMAIL?
									$offerincident_mode = $offerincident->offerincident_mode;
									
									// Get the offerincident ORM
									$offerincident_orm = ORM::factory('offerincident', $offerincident_id);
									
									// Get the person submitting the offerreport
									$offerincident_person = $offerincident_orm->offerincident_person;
									
									//XXX offerincident_Mode will be discontinued in favour of $service_id
									if ($offerincident_mode == 1)	// Submitted via WEB
									{
										$submit_mode = "WEB";
										// Who submitted the offerreport?
										if ($offerincident_person->loaded)
										{
											// Offerreport was submitted by a visitor
											$submit_by = $offerincident_person->person_first . " " . $offerincident_person->person_last;
										}
										else
										{
											if ($offerincident_orm->user_id)					// Offerreport Was Submitted By Administrator
											{
												$submit_by = $offerincident_orm->user->name;
											}
											else
											{
												$submit_by = Kohana::lang('ui_admin.unknown');
											}
										}
									}
									elseif ($offerincident_mode == 2) 	// Submitted via SMS
									{
										$submit_mode = "SMS";
										$submit_by = $offerincident_orm->message->message_from;
									}
									elseif ($offerincident_mode == 3) 	// Submitted via Email
									{
										$submit_mode = "EMAIL";
										$submit_by = $offerincident_orm->message->message_from;
									}
									elseif ($offerincident_mode == 4) 	// Submitted via Twitter
									{
										$submit_mode = "TWITTER";
										$submit_by = $offerincident_orm->message->message_from;
									}
									
									// Get the country name
									// $country_name = ($offerincident->country_id != 0)
									// 	? $countries[$offerincident->country_id] 
									// 	: $countries[Kohana::config('settings.default_country')]; 
									
							
									// Retrieve Offerincident Offercategories
									$offerincident_offercategory = "";
									foreach($offerincident_orm->offerincident_offercategory as $offercategory)
									{
										$offerincident_offercategory .= $offercategory->offercategory->offercategory_title ."&nbsp;&nbsp;";
									}

									// Offerincident Status
									$offerincident_approved = $offerincident->offerincident_active;
									$offerincident_verified = $offerincident->offerincident_verified;
									
									// Get Edit Log
									$edit_count = $offerincident_orm->verify->count();
									$edit_css = ($edit_count == 0) ? "post-edit-log-gray" : "post-edit-log-blue";
									
									$edit_log  = "<div class=\"".$edit_css."\">"
										. "<a href=\"javascript:showLog('edit_log_".$offerincident_id."')\">".Kohana::lang('ui_admin.edit_log').":</a> (".$edit_count.")</div>"
										. "<div id=\"edit_log_".$offerincident_id."\" class=\"post-edit-log\"><ul>";
									
									foreach ($offerincident_orm->verify as $verify)
									{
										$edit_log .= "<li>".Kohana::lang('ui_admin.edited_by')." ".$verify->user->name." : ".$verify->verified_date."</li>";
									}
									$edit_log .= "</ul></div>";

									// Get Any Translations
									$i = 1;
									$offerincident_translation  = "<div class=\"post-trans-new\">"
											. "<a href=\"" . url::base() . 'admin/offerreports/translate/?iid='.$offerincident_id."\">"
											. strtoupper(Kohana::lang('ui_main.add_translation')).":</a></div>";
											
									foreach ($offerincident_orm->offerincident_lang as $translation)
									{
										$offerincident_translation .= "<div class=\"post-trans\">"
											. Kohana::lang('ui_main.translation'). $i . ": "
											. "<a href=\"" . url::base() . 'admin/offerreports/translate/'. $translation->id .'/?iid=' . $offerincident_id . "\">"
											. text::limit_chars($translation->offerincident_title, 150, "...", TRUE)
											. "</a>"
											. "</div>";
									}
									?>
									<tr>
										<td class="col-1">
											<input name="offerincident_id[]" id="offerincident" value="<?php echo $offerincident_id; ?>" type="checkbox" class="check-box"/>
										</td>
										<td class="col-2">
											<div class="post">
												<h4>
													<a href="<?php echo url::site() . 'offer/details/' . $offerincident_id; ?>" class="more" target="_blank">
														<?php echo $offerincident_title; ?>
													</a>
												</h4>
												<p><?php echo $offerincident_description; ?>... 
													<a href="<?php echo url::base() . 'offer/details/' . $offerincident_id; ?>" class="more" target="_blank">
														<?php echo Kohana::lang('ui_main.more');?>
													</a>
												</p>
											</div>
											<ul class="info">
												<li><?php echo Kohana::lang('ui_main.submitted_by');?> 
													<strong><?php echo $submit_by; ?></strong> via <strong><?php echo $submit_mode; ?></strong>
												</li>
											</ul>
											<ul class="links">
												<li class="none-separator"><?php echo Kohana::lang('ui_main.offercategories');?>:
													<strong><?php echo $offerincident_offercategory;?></strong>
												</li>
											</ul>
											<?php
											echo $edit_log;
											
											// Action::offerreport_extra_admin - Add items to the offerreport list in admin
											Event::run('ushahidi_action.offerreport_extra_admin', $offerincident);
											?>
										</td>
										<td class="col-3"><?php echo $offerincident_date; ?></td>
										<td class="col-4">
											<ul>
												<li class="">
													<?php if ($offerincident_approved) {?>
													<a href="#" class="status_no" onclick="offerreportAction('u','UNAPPROVE', '<?php echo $offerincident_id; ?>');"><?php echo Kohana::lang('ui_admin.unapprove');?></a>
													<?php } else {?>
													<a href="#" class="status_yes" onclick="offerreportAction('a','APPROVE', '<?php echo $offerincident_id; ?>');"><?php echo Kohana::lang('ui_admin.approve');?></a>
													<?php } ?>	
												</li>
												<li><a href="#"<?php if ($offerincident_featured) echo " class=\"status_yes\"" ?> onclick="offerreportAction('f','FEATURE', '<?php echo $offerincident_id; ?>');"><?php echo $feature_action;?></a></li>
												<li><a href="#" class="del" onclick="offerreportAction('d','DELETE', '<?php echo $offerincident_id; ?>');"><?php echo Kohana::lang('ui_main.delete');?></a></li>
											</ul>
										</td>
									</tr>
									<?php
								}
								?>
							</tbody>
						</table>
					</div>
				<?php print form::close(); ?>
			</div>
