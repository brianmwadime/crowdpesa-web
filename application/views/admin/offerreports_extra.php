<?php 
/**
 * Offerreports extra view page.
 *
 * PHP version 5
 * LICENSE: This source file is subject to LGPL license 
 * that is available through the world-wide-web at the following URI:
 * http://www.gnu.org/copyleft/lesser.html
 * @author     BeeBuy Team <info@beebuy.com> 
 * @package    CrowdPesa - http://crowdpesa.com
 * @module     API Controller
 * @copyright  CrowdPesa - http://crowdpesa.com
 * @license    http://www.gnu.org/copyleft/lesser.html GNU Lesser General Public License (LGPL) 
 */
?>
			<div class="bg">
				<h2><?php echo $title; ?><span>(<?php echo $total_items; ?>)</span><a href="offerreports/edit"><?php echo Kohana::lang('ui_main.create_offerreport');?></a><a href="offerreports/extra"><?php echo Kohana::lang('ui_main.additional_offerreports');?></a></h2>
				<!-- tabs -->
				<div class="tabs">
					<!-- tabset -->
					<ul class="tabset">
						<li><a href="?status=0" <?php if ($status != 'a' && $status !='v') echo "class=\"active\""; ?>><?php echo Kohana::lang('ui_main.show_all');?></a></li>
						<li><a href="?status=a" <?php if ($status == 'a') echo "class=\"active\""; ?>><?php echo Kohana::lang('ui_main.awaiting_approval');?></a></li>
						<li><a href="?status=v" <?php if ($status == 'v') echo "class=\"active\""; ?>><?php echo Kohana::lang('ui_main.awaiting_verification');?></a></li>
					</ul>
					<!-- tab -->
					<div class="tab">
						<ul>
							<li><a href="#" onclick="offerreportAction('a','APPROVE');"><?php echo strtoupper(Kohana::lang('ui_main.appove'));?></a></li>
							<li><a href="#" onclick="offerreportAction('u','UNAPPROVE');"><?php echo strtoupper(Kohana::lang('ui_main.disapprove'));?></a></li>
							<li><a href="#" onclick="offerreportAction('v','VERIFY');"><?php echo strtoupper(Kohana::lang('ui_main.verify'));?></a></li>
							<li><a href="#" onclick="offerreportAction('d','DELETE');"><?php echo strtoupper(Kohana::lang('ui_main.delete'));?></a></li>
						</ul>
					</div>
				</div>
				<?php
				if ($form_error) {
				?>
					<!-- red-box -->
					<div class="red-box">
						<h3><?php echo Kohana::lang('ui_main.error');?></h3>
						<ul><?php echo Kohana::lang('ui_main.select_one');?></ul>
					</div>
				<?php
				}

				if ($form_saved) {
				?>
					<!-- green-box -->
					<div class="green-box" id="submitStatus">
						<h3>Offerreports <?php echo $form_action; ?> <a href="#" id="hideMessage" class="hide"><?php echo Kohana::lang('ui_main.hide_this_message');?></a></h3>
					</div>
				<?php
				}
				?>
				<!-- offerreport-table -->
				<?php print form::open(NULL, array('id' => 'offerreportMain', 'name' => 'offerreportMain')); ?>
					<input type="hidden" name="action" id="action" value="">
					<div class="table-holder">
						<table class="table">
							<thead>
								<tr>
									<th class="col-1"><input id="checkallofferincidents" type="checkbox" class="check-box" onclick="CheckAll( this.id, 'offerincident_id[]' )" /></th>
									<th class="col-2"><?php echo Kohana::lang('ui_main.offerreport_details');?></th>
									<th class="col-3"><?php echo Kohana::lang('ui_main.date');?></th>
									<th class="col-4"><?php echo Kohana::lang('ui_main.actions');?></th>
								</tr>
							</thead>
							<tfoot>
								<tr class="foot">
									<td colspan="4">
										<?php echo $pagination; ?>
									</td>
								</tr>
							</tfoot>
							<tbody>
								<?php
								if ($total_items == 0)
								{
								?>
									<tr>
										<td colspan="4" class="col">
											<h3><?php echo Kohana::lang('ui_main.no_results');?></h3>
										</td>
									</tr>
								<?php	
								}
								foreach ($offerincidents as $offerincident)
								{
									$offerincident_id = $offerincident->id;
									$offerincident_title = $offerincident->offerincident_title;
									$offerincident_description = substr($offerincident->offerincident_description, 0, 150);
									$offerincident_date = $offerincident->offerincident_date;
									$offerincident_date = date('Y-m-d', strtotime($offerincident->offerincident_date));
									$offerincident_mode = $offerincident->offerincident_mode;	// Mode of submission... WEB/SMS/EMAIL?
									
									if ($offerincident_mode == 1)
									{
										$submit_mode = "WEB";
										// Who submitted the offerreport?
										if ($offerincident->offerincident_person->id)
										{
											// Offerreport was submitted by a visitor
											$submit_by = $offerincident->offerincident_person->person_first . " " . $offerincident->offerincident_person->person_last;
										}
										else
										{
											if ($offerincident->user_id)					// Offerreport Was Submitted By Administrator
											{
												$submit_by = $offerincident->user->name;
											}
											else
											{
												$submit_by = Kohana::lang('ui_admin.unknown');
											}
										}
									}
									
									$offerincident_location = $offerincident->location->location_name;
									$offerincident_country = $offerincident->location->country->country;

									// Retrieve Offerincident Offercategories
									$offerincident_offercategory = "";
									foreach($offerincident->offerincident_offercategory as $offercategory) 
									{ 
										$offerincident_offercategory .= "<a href=\"#\">" . $offercategory->offercategory->offercategory_title . "</a>&nbsp;&nbsp;";
									}
									
									// Offerincident Status
									$offerincident_approved = $offerincident->offerincident_active;
									$offerincident_verified = $offerincident->offerincident_verified;
									?>
									<tr>
										<td class="col-1"><input name="offerincident_id[]" id="offerincident" value="<?php echo $offerincident_id; ?>" type="checkbox" class="check-box"/></td>
										<td class="col-2">
											<div class="post">
												<h4><a href="<?php echo url::site() . 'admin/offerreports/edit/' . $offerincident_id; ?>" class="more"><?php echo $offerincident_title; ?></a></h4>
												<p><?php echo $offerincident_description; ?>... <a href="<?php echo url::site() . 'admin/offerreports/edit/' . $offerincident_id; ?>" class="more"><?php echo Kohana::lang('ui_main.more');?></a></p>
											</div>
											<ul class="info">
												<li class="none-separator"><?php echo Kohana::lang('ui_main.location');?>: <strong><?php echo $offerincident_location; ?></strong>, <strong><?php echo Kohana::lang('ui_main.example_country');?></strong></li>
												<li><?php echo Kohana::lang('ui_main.submitted_by');?> <strong><?php echo $submit_by; ?></strong> via <strong><?php echo $submit_mode; ?></strong></li>
											</ul>
											<ul class="links">
												<li class="none-separator"><?php echo Kohana::lang('ui_main.offercategories');?>:<?php echo $offerincident_offercategory; ?></li>
											</ul>
										</td>
										<td class="col-3"><?php echo $offerincident_date; ?></td>
										<td class="col-4">
											<ul>
												<li class="none-separator"><a href="#"<?php if ($offerincident_approved) echo " class=\"status_yes\"" ?> onclick="offerreportAction('a','APPROVE');"><?php echo Kohana::lang('ui_main.approve');?></a></li>
												<li><a href="#"<?php if ($offerincident_verified) echo " class=\"status_yes\"" ?> onclick="offerreportAction('v','VERIFY');"><?php echo Kohana::lang('ui_main.verify');?></a></li>
												<li><a href="#" class="del" onclick="offerreportAction('d','DELETE');"><?php echo Kohana::lang('ui_main.delete');?></a></li>
											</ul>
										</td>
									</tr>
									<?php
								}
								?>
							</tbody>
						</table>
					</div>
				<?php print form::close(); ?>
			</div>