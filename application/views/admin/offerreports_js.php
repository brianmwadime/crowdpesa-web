/**
 * Main offerreports js file.
 * 
 * Handles javascript stuff related to offerreports function.
 *
 * PHP version 5
 * LICENSE: This source file is subject to LGPL license 
 * that is available through the world-wide-web at the following URI:
 * http://www.gnu.org/copyleft/lesser.html
 * @author     BeeBuy Team <info@beebuy.com> 
 * @package    CrowdPesa - http://crowdpesa.com
 * @module     API Controller
 * @copyright  CrowdPesa - http://crowdpesa.com
 * @license    http://www.gnu.org/copyleft/lesser.html GNU Lesser General Public License (LGPL) 
 */

<?php require SYSPATH.'../application/views/admin/form_utils_js.php' ?>

		// Ajax Submission
		function offerreportAction ( action, confirmAction, offerincident_id )
		{
			var statusMessage;
			if( !isChecked( "offerincident" ) && offerincident_id=='' )
			{ 
				alert('Please select at least one offerreport.');
			} else {
				var answer = confirm('<?php echo Kohana::lang('ui_admin.are_you_sure_you_want_to'); ?> ' + confirmAction + '?')
				if (answer){
					
					// Set Submit Type
					$("#action").attr("value", action);
					
					if (offerincident_id != '') 
					{
						// Submit Form For Single Item
						$("#offerincident_single").attr("value", offerincident_id);
						$("#offerreportMain").submit();
					}
					else
					{
						// Set Hidden form item to 000 so that it doesn't return server side error for blank value
						$("#offerincident_single").attr("value", "000");
						
						// Submit Form For Multiple Items
						$("#offerreportMain").submit();
					}
				
				} else {
					return false;
				}
			}
		}
		
		function showLog(id)
		{
			$('#' + id).toggle(400);
		}

