<?php 
/**
 * Categories view page.
 *
 * PHP version 5
 * LICENSE: This source file is subject to LGPL license 
 * that is available through the world-wide-web at the following URI:
 * http://www.gnu.org/copyleft/lesser.html
 * @author     Ushahidi Team <team@ushahidi.com> 
 * @package    Ushahidi - http://source.ushahididev.com
 * @module     API Controller
 * @copyright  Ushahidi - http://www.ushahidi.com
 * @license    http://www.gnu.org/copyleft/lesser.html GNU Lesser General Public License (LGPL) 
 */
?>
			<div class="bg">
				<h2>
					<?php admin::user_subtabs("packages"); ?>
				</h2>
				<?php
				if ($form_error) {
				?>
					<!-- red-box -->
					<div class="red-box">
						<h3><?php echo Kohana::lang('ui_main.error');?></h3>
						<ul>
						<?php
						foreach ($errors as $error_item => $error_description)
						{
							// print "<li>" . $error_description . "</li>";
							print (!$error_description) ? '' : "<li>" . $error_description . "</li>";
						}
						?>
						</ul>
					</div>
				<?php
				}

				if ($form_saved) {
				?>
					<!-- green-box -->
					<div class="green-box">
						<h3><?php echo Kohana::lang('ui_main.retailer_package_has_been');?> <?php echo $form_action; ?>!</h3>
					</div>
				<?php
				}
				?>
				
				<!-- tabs -->
				<div class="tabs">
					<!-- tabset -->
					<a name="add"></a>
					<ul class="tabset">
						<li><a href="#" class="active" onclick="show_addedit(true,true)"><?php echo Kohana::lang('ui_main.add_edit');?></a></li>
					</ul>
					<!-- tab -->
					
					<div class="tab" id="addedit" style="display:none">
					<?php print form::open(NULL,array('id' => 'packageMain',
					 	'name' => 'packageMain')); ?>
					<input type="hidden" name="action" id="action" value="a"/>
					<input type="hidden" id="package_id" name="package_id" value="<?php echo $form['package_id']; ?>">
					<div class="tab_form_item">
						<strong><?php echo Kohana::lang('ui_main.name');?>:</strong><br />
						<?php print form::input('package_name', '', ' class="text"'); ?>
					</div>
					<div class="tab_form_item">
						<strong><?php echo Kohana::lang('ui_main.description');?>:</strong><br />
						<?php print form::input('description', $form['description'], ' class="text long"'); ?>
					</div>
					<div class="tab_form_item">
						<strong><?php echo Kohana::lang('ui_main.email_notifications');?>:</strong><br />
						<?php print form::input('email_notifications',  $form['email_notifications'], ' class="text"'); ?>
					</div>
					<div class="tab_form_item">
						<strong><?php echo Kohana::lang('ui_main.sms_notifications');?>:</strong><br />
						<?php print form::input('sms_notifications',  $form['sms_notifications'], ' class="text"'); ?>
					</div>
					<div class="tab_form_item">
						<strong><?php echo Kohana::lang('ui_main.monthly_coupons');?>:</strong><br />
						<?php print form::input('coupons',  $form['coupons'], ' class="text"'); ?>
					</div>
					<div class="tab_form_item">
						<strong><?php echo Kohana::lang('ui_main.payment_fees');?>:</strong><br />
						<?php print form::input('payment_fees',  $form['payment_fees'], ' class="text"'); ?>
					</div>
				
					<div class="tab_form_item">
						<strong><?php echo Kohana::lang('ui_main.monthly_usd');?>:</strong><br />
						<?php print form::input('monthly_usd',  $form['monthly_usd'], ' class="text"'); ?>
					</div>
					<div class="tab_form_item">
						<strong><?php echo Kohana::lang('ui_main.quarterly_usd');?>:</strong><br />
						<?php print form::input('quarterly_usd',  $form['quarterly_usd'], ' class="text"'); ?>
					</div>
					<div class="tab_form_item">
						<strong><?php echo Kohana::lang('ui_main.bi_annual_usd');?>:</strong><br />
						<?php print form::input('bi_annual_usd',  $form['bi_annual_usd'], ' class="text"'); ?>
					</div>
					<div class="tab_form_item">

						<strong><?php echo Kohana::lang('ui_main.annual_usd');?>:</strong><br />
						<?php print form::input('annual_usd',  $form['annual_usd'], ' class="text"'); ?>
					</div>
					
					<div style="clear:both;"></div>
					<div class="tab_form_item">
					<div style="width:200px;float:left;margin-bottom:3px;">
					<?php
						echo form::checkbox('map_places', 1);
						echo "<strong>". Kohana::lang('ui_main.map_places')."</strong>";				
					?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					</div>
					<div style="width:200px;float:left;margin-bottom:3px;">
					<?php
					echo form::checkbox('map_offers',1);						
					echo "<strong>". Kohana::lang('ui_main.map_offers')."</strong>";				
					?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					</div>
					<div style="width:200px;float:left;margin-bottom:3px;">
					<?php
						echo form::checkbox('reviews',1);
						echo "<strong>". Kohana::lang('ui_main.enable_reviews')."</strong>";				
					?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					</div>
					<div style="width:200px;float:left;margin-bottom:3px;">
					<?php
						echo form::checkbox('blog',1);
						echo "<strong>". Kohana::lang('ui_main.enable_blog')."</strong>";				
					?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					</div>
					<div style="width:200px;float:left;margin-bottom:3px;">
					<?php
						echo form::checkbox('featured_offers', 1);
						echo "<strong>". Kohana::lang('ui_main.featured_offers')."</strong>";				
					?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					</div>
					<div style="width:200px;float:left;margin-bottom:3px;">
					<?php
						echo form::checkbox('ticket', 1);
						echo "<strong>". Kohana::lang('ui_main.ticket_support')."</strong>";				
					?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					</div>
					<div style="width:200px;float:left;margin-bottom:3px;">
					<?php
						echo form::checkbox('call', 1);
						echo "<strong>". Kohana::lang('ui_main.call_support')."</strong>";				
					?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					</div>
					<div style="width:200px;float:left;margin-bottom:3px;">
					<?php
						 print form::dropdown('users_analysis', $summaryfull_array,$form['users_analysis']);
						echo "<strong>". Kohana::lang('ui_main.users_anaylsis')."</strong>";				
					?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					</div>
					<div style="width:200px;float:left;margin-bottom:3px;">
					<?php
						 print form::dropdown('offers_analysis', $summaryfull_array,$form['offers_analysis']);
						echo "<strong>". Kohana::lang('ui_main.offers_anaylsis')."</strong>";				
					?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					</div>
					<div style="width:200px;float:left;margin-bottom:3px;">
					<?php
						 print form::dropdown('sales_analysis', $summaryfull_array,$form['sales_analysis']);
						echo "<strong>". Kohana::lang('ui_main.sales_anaylsis')."</strong>";				
					?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					</div>
					 
					</div>
					<div style="clear:both;"></div>

					<div style="clear:both;"></div>
					<div class="tab_form_item">
						&nbsp;<br />
						<input type="image" src="<?php echo url::file_loc('img'); ?>media/img/admin/btn-save.gif" class="save-rep-btn" />
					</div>
					<?php print form::close(); ?>			
				</div>
				</div>
				
				<!-- report-table -->
				<div class="report-form">
					<?php print form::open(NULL,array('id' => 'catListing',
					 	'name' => 'catListing')); ?>
						<input type="hidden" name="action" id="category_action" value="">
						<input type="hidden" name="package_id" id="category_id_action" value="">
						<div class="table-holder">
							<table class="table" id="categorySort">
								<thead>
									<tr class="nodrag">
										<th class="col-1">&nbsp;</th>
										<th class="col-2"><?php echo Kohana::lang('ui_main.package_types');?></th>
										<th class="col-3"><?php echo Kohana::lang('ui_main.date');?></th>
										<th class="col-4"><?php echo Kohana::lang('ui_main.actions');?></th>
									</tr>
								</thead>
								<tfoot>
									<tr class="foot nodrag">
										<td colspan="4">
											<?php echo $pagination; ?>
										</td>
									</tr>
								</tfoot>
								<tbody>
									<?php
									if ($total_items == 0)
									{
									?>
										<tr class="nodrag">
											<td colspan="4" class="col" id="row1">
												<h3><?php echo Kohana::lang('ui_main.no_results');?></h3>
											</td>
										</tr>
									<?php	
									}
									foreach ($packages as $package)
									{
										$package_id = $package->id;
										$title = $package->package_name;
										$description = substr($package->description, 0, 150);
										$places=$package->map_places;
										$offers=$package->map_offers;
										$reviews=$package->reviews;
										$sales_analysis=$package->sales_analysis;
										$offers_analysis=$package->offers_analysis;
										$users_analysis=$package->user_analysis;
										$email_notifications=$package->email_notifications;
										$sms_notifications=$package->sms_notifications;
										$blog=$package->blog;
										$ticket=$package->ticket;
										$call=$package->call;
										$featured_offers=$package->featured_offers;
										$coupons=$package->coupons;
										$payment_fees=$package->payment_fees;
										$monthly_usd=$package->monthly_usd;
										$quarterly_usd=$package->quarterly_usd;
										$bi_annual_usd=$package->bi_annual_usd;
										$annual_usd=$package->annual_usd;
										
										
										?>
										<tr id="<?php echo $package_id; ?>">
											<td class="col-1 col-drag-handle">&nbsp;</td>
											<td class="col-2">
												<div class="post">
													<h4><?php echo $title; ?></h4>
													<p><?php echo $description; ?></p>
												</div>
											</td>
											<td class="col-3">
											<?php 
												echo "";
											
											?>
											</td>
											<td class="col-4">
												<ul>
													<li class="none-separator"><a href="#add" onClick="fillFields('<?php echo(rawurlencode($package_id)); ?>','<?php echo(rawurlencode($title)); ?>','<?php echo(rawurlencode($description)); ?>','<?php echo(rawurlencode($places)); ?>','<?php echo(rawurlencode($offers)); ?>','<?php echo(rawurlencode($reviews)); ?>','<?php echo(rawurlencode($sales_analysis)); ?>','<?php echo(rawurlencode($offers_analysis)); ?>','<?php echo(rawurlencode($users_analysis)); ?>','<?php echo(rawurlencode($email_notifications)); ?>','<?php echo(rawurlencode($sms_notifications)); ?>','<?php echo(rawurlencode($blog)); ?>','<?php echo(rawurlencode($ticket)); ?>','<?php echo(rawurlencode($call)); ?>','<?php echo(rawurlencode($featured_offers)); ?>','<?php echo(rawurlencode($coupons)); ?>','<?php echo(rawurlencode($payment_fees)); ?>','<?php echo(rawurlencode($monthly_usd)); ?>','<?php echo(rawurlencode($quarterly_usd)); ?>','<?php echo(rawurlencode($bi_annual_usd)); ?>','<?php echo(rawurlencode($annual_usd)); ?>')"><?php echo Kohana::lang('ui_main.edit');?></a></li>
													<li><a href="javascript:catAction('d','DELETE','<?php echo(rawurlencode($package_id)); ?>')" class="del"><?php echo Kohana::lang('ui_main.delete');?></a></li>
												</ul>
												
												
												
											</td>
										</tr>
										<?php
										
										// Get All Category Children
										}?>
								</tbody>
							</table>
						</div>
					<?php print form::close(); ?>
				</div>

			</div>
