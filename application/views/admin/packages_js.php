/**
 * Categories js file.
 * 
 * Handles javascript stuff related to category function.
 *
 * PHP version 5
 * LICENSE: This source file is subject to LGPL license 
 * that is available through the world-wide-web at the following URI:
 * http://www.gnu.org/copyleft/lesser.html
 * @author     Ushahidi Team <team@ushahidi.com> 
 * @package    Ushahidi - http://source.ushahididev.com
 * @module     API Controller
 * @copyright  Ushahidi - http://www.ushahidi.com
 * @license    http://www.gnu.org/copyleft/lesser.html GNU Lesser General Public License (LGPL) 
 */

$(document).ready(function() {
    // Initialise the table
	$("#categorySort").tableDnD({
		dragHandle: "col-drag-handle",
		onDragClass: "col-drag",
		onDrop: function(table, row) {
			var rows = table.tBodies[0].rows;
			var categoriesArray = [];
			for (var i=0; i<rows.length; i++) {
				categoriesArray[i] = rows[i].id;
			}
			var categories = categoriesArray.join(',');
			$.post("<?php echo url::site() . 'admin/manage/category_sort/' ?>", { categories: categories },
				function(data){
					if (data == "ERROR") {
						alert("Invalid Placement!!\n You cannot place a subcategory on top of a category.");
					} else {
						$("#categorySort"+" tbody tr td").effect("highlight", {}, 500);
					}
			});
		}
	});
	
	$("#categorySort tr").hover(function() {
		$(this.cells[0]).addClass('col-show-handle');
	}, function() {
		$(this.cells[0]).removeClass('col-show-handle');
	});
});

// Categories JS
function fillFields(id, title,description,map_places,map_offers,reviews,sales_analysis,offers_analysis,
users_analysis,email_notifications,sms_notifications,blog,ticket,call,featured_offers,coupons,payment_fees,
monthly_usd,quarterly_usd,bi_annual_usd,annual_usd)
{
	show_addedit();
	console.log(decodeURIComponent(map_offers));
	$("#package_id").attr("value", decodeURIComponent(id));	
	$("#package_name").attr("value", decodeURIComponent(title));
	$("#description").attr("value", decodeURIComponent(description));
	//$("#map_places").attr("value", 1);
	$("#map_places").prop("checked", parseInt(decodeURIComponent(map_places)));
	$("#map_offers").prop("checked", parseInt(decodeURIComponent(map_offers)));
	//$("#map_offers").attr("value", 1);
	$("#reviews").prop("checked", parseInt(decodeURIComponent(reviews)));
	//$("#reviews").attr("value", 1);
	$("#sales_analysis").attr("value", decodeURIComponent(sales_analysis));
	$("#offers_analysis").attr("value", decodeURIComponent(offers_analysis));
	$("#user_analysis").attr("value", decodeURIComponent(users_analysis));
	$("#email_notifications").attr("value", decodeURIComponent(email_notifications));
	$("#sms_notifications").attr("value", decodeURIComponent(sms_notifications));
	$("#blog").prop("checked", parseInt(decodeURIComponent(blog)));
	//$("#blog").attr("value", 1);
	$("#ticket").prop("checked", parseInt(decodeURIComponent(ticket)));
	//$("#ticket").attr("value", 1);
	$("#call").prop("checked", parseInt(decodeURIComponent(call)));
	$("#call").attr("value", 1);
	$("#featured_offers").prop("checked", parseInt(decodeURIComponent(featured_offers)));
	//$("#featured_offers").attr("value", 1);
	$("#coupons").attr("value", decodeURIComponent(coupons));
	$("#payment_fees").attr("value", decodeURIComponent(payment_fees));
	$("#monthly_usd").attr("value", decodeURIComponent(monthly_usd));
	$("#quarterly_usd").attr("value", decodeURIComponent(quarterly_usd));
	$("#bi_annual_usd").attr("value", decodeURIComponent(bi_annual_usd));
	$("#annual_usd").attr("value", decodeURIComponent(annual_usd));
	
}

// Ajax Submission
function catAction ( action, confirmAction, id )
{
	var statusMessage;
	var answer = confirm('<?php echo Kohana::lang('ui_admin.are_you_sure_you_want_to'); ?> ' + confirmAction + '?')
	if (answer){
		// Set Offer Type ID
		
		$("#category_id_action").attr("value", id);
		// Set Submit Type
		$("#category_action").attr("value", action);
		// Submit Form
		$("#catListing").submit();
	}
}