<div class="bg">
				<h2>
					<?php admin::blog_subtabs("view"); ?>
				</h2>
				<!-- tabs -->
				<div class="tabs">
					<!-- tabset -->
					<ul class="tabset">
						<li>
							<a href="?status=0" <?php if ($status != 'p' AND $status !='u' AND $status !='d') echo "class=\"active\""; ?>><?php echo Kohana::lang('ui_main.show_all');?></a>
						</li>
						<li><a href="?status=p" <?php if ($status == 'p') echo "class=\"active\""; ?>><?php echo Kohana::lang('ui_main.articles_published');?></a></li>
						<li><a href="?status=u" <?php if ($status == 'u') echo "class=\"active\""; ?>><?php echo Kohana::lang('ui_main.articles_unpublished');?></a></li>
						<li><a href="?status=d" <?php if ($status == 'd') echo "class=\"active\""; ?>><?php echo Kohana::lang('ui_main.drafts');?></a></li>
					</ul>
					<!-- tab -->
					<div class="tab">
						<ul>
							<li><a href="#" onclick="blogAction('p','<?php echo strtoupper(Kohana::lang('ui_main.article_publish')); ?>', '');">
								<?php echo Kohana::lang('ui_main.article_publish');?></a>
							</li>
							<li><a href="#" onclick="blogAction('u','<?php echo strtoupper(Kohana::lang('ui_main.article_unpublish')); ?>', '');">
								<?php echo Kohana::lang('ui_main.article_unpublish');?></a>
							</li>
							<li><a href="#" onclick="blogAction('d','<?php echo strtoupper(Kohana::lang('ui_main.delete')); ?>', '');">
								<?php echo Kohana::lang('ui_main.delete');?></a>
							</li>
						</ul>
					</div>
				</div>
                
                <?php if ($form_error): ?>
					<!-- red-box -->
					<div class="red-box">
						<h3><?php echo Kohana::lang('ui_main.error');?></h3>
						<ul><?php echo Kohana::lang('ui_main.select_one');?></ul>
					</div>
				<?php endif; ?>
				
				<?php if ($form_saved): ?>
					<!-- green-box -->
					<div class="green-box" id="submitStatus">
						<h3><?php echo Kohana::lang('ui_main.articles');?> <?php echo $form_action; ?> <a href="#" id="hideMessage" class="hide">hide this message</a></h3>
					</div>
				<?php endif; ?>
                
                <!-- blog-table -->
				<?php print form::open(NULL, array('id' => 'blogMain', 'name' => 'blogMain')); ?>
					<input type="hidden" name="action" id="action" value="">
					<input type="hidden" name="article_id[]" id="article_single" value="">
					<div class="table-holder">
                <table class="table">
	                <thead>
                        <tr>
                            <th class="col-1"><input type="checkbox" onclick="CheckAll( this.id, 'offerincident_id[]' )" class="check-box" id="checkallarticles"></th>
                            <th class="col-2"><?php echo Kohana::lang('ui_main.article_details');?></th>
                            <th class="col-3"><?php echo Kohana::lang('ui_main.date');?></th>
                            <th class="col-4"><?php echo Kohana::lang('ui_main.actions');?></th>
                        </tr>
                    </thead>
                    <tbody>
					<?php if ($total_items == 0): ?>
                        <tr>
                            <td colspan="4" class="col">
                                <h3><?php echo Kohana::lang('ui_main.no_results');?></h3>
                            </td>
                        </tr>
                    <?php endif; ?>
                    <?php foreach ($articles as $article) : ?>
					<?php
						$article_title = $article->title;
						$article_intro = text::limit_chars($article->content, 120, ' ...', true);;
						$article_image_url = url::site() . 'themes/geoplaces/images/dummy_pix/' . $article->image;
						$article_url = url::site() . 'admin/blog/edit/' .  $article->id;
						$article_date =  date('Y-m-d', strtotime($article->published_date));
						$article_comment_count = (isset($comment_count[$article->id])) ? $comment_count[$article->id] : 0;
						$article_comment_count = ($article_comment_count != 1) ? $article_comment_count . ' Reviews' : $article_comment_count . ' Review';
                    ?>
                        <tr>
                            <td class="col-1">
                                <input name="article_id[]" id="article" value="<?php echo $article->id; ?>" type="checkbox" class="check-box"/>
                            </td>
                            <td class="col-2">
                                <div class="post">
                                    <h4>
                                        <a href="<?php echo $article_url; ?>" class="more">
                                            <?php echo $article_title; ?>
                                        </a>
                                    </h4>
                                    <p><?php echo $article_intro; ?>... 
                                        <a href="<?php echo $article_url ?>" class="more">
                                            <?php echo Kohana::lang('ui_main.more');?>
                                        </a>
                                    </p>
                                </div>
                            </td>
                            <td class="col-3"><?php echo $article_date; ?></td>
                            <td class="col-4">
                                <ul>
                                    <li class="none-separator">
                                        <?php if ($article->published == 1) {?>
                                        <a href="#" class="status_yes" onclick="blogAction('u','UNPUPLISH', '<?php echo $article->id; ?>');"><?php echo Kohana::lang('ui_main.article_unpublish');?></a>
                                        <?php } elseif($article->published == 0) {?>
                                        <a href="#" onclick="blogAction('p','PUBLISH', '<?php echo $article->id; ?>');"><?php echo Kohana::lang('ui_main.article_publish');?></a>
                                        <?php } else {?>
                                        <a href="<?php echo $article_url; ?>" ><?php echo Kohana::lang('ui_main.edit');?></a>
                                        <?php } ?>	
                                    </li>
                                    <li><a href="#" class="del" onclick="blogAction('d','DELETE', '<?php echo $article->id; ?>');"><?php echo Kohana::lang('ui_main.delete');?></a></li>
                                </ul>
                            </td>
                        </tr>
      				<?php endforeach; ?>
                    </tbody>
                    <tfoot>
                        <tr class="foot">
                            <td colspan="4">
                                <?php echo $pagination; ?>
                            </td>
                        </tr>
                    </tfoot>
                </table>
                </div>
                <?php print form::close(); ?>
</div>