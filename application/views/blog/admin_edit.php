<form action="" method="post" id="article-edit" enctype= "multipart/form-data">
    <input type="hidden" name="article_id" id="article_is" value="<?php echo $form['article_id'];?>"/>
    <div class="row">
        <span class="article-text-fields">
            <label for="post_title">Article Title:</label>
            <input type="text" value="<?php echo $form['post_title'] ?>" id="post_title" name="post_title" />
        </span>
    </div>
    <div class="row">
        <span class="article-text-fields">
            <label for="post_tags">Article Tags:</label>
            <input title="comma,separated,tags" type="text" value="<?php echo $form['post_tags'] ?>" id="post_tags" name="post_tags" />
    </div>
    <div class="row">
				<h4><?php echo Kohana::lang('ui_main.status');?></h4>
				<?php print form::dropdown('post_published', $published_array, $form['post_published']); ?>
			</div>
    
    <div class="row">
        <textarea id="post_content" name="post_content"><?php echo $form['post_content'] ?></textarea>
    </div>
    <div class="row">
        <span class="article-meta-keywords">
        <label for="post_meta_keywords">Meta Keywords:</label>
        <textarea id="post_meta_keywords" name="post_meta_keywords"><?php echo $form['post_meta_keywords'] ?></textarea>
        </span>
    </div>
    <div class="row">
        <span class="article-meta-description">
        <label for="post_meta_description">Meta Description:</label>
        <textarea id="post_meta_description" name="post_meta_description"><?php echo $form['post_meta_description'] ?></textarea>
        </span>
    </div>
  
    <?php Event::run('ushahidi_action.report_form_admin_after_video_link', $id); ?>

							<!-- Photo Fields -->
	<div class="row link-row">
		<h4><?php echo Kohana::lang('ui_main.reports_photos');?></h4>
		<?php								
			if ($article_media)
			{
				// Retrieve Media
				foreach($article_media as $photo) 
				{
					if ($photo->media_type == 1)
					{
						$thumb = url::convert_uploaded_to_abs($photo->media_thumb);
						$large_photo = url::convert_uploaded_to_abs($photo->media_link);
						?>
						<div class="report_thumbs" id="photo_<?php echo $photo->id; ?>">
    						<a class="photothumb" rel="lightbox-group1" href="<?php echo $large_photo; ?>">
    						<img src="<?php echo $thumb; ?>" />
    						</a>
							&nbsp;&nbsp;
							<a href="#" onClick="deletePhoto('<?php echo $photo->id; ?>', 'photo_<?php echo $photo->id; ?>'); return false;" ><?php echo Kohana::lang('ui_main.delete'); ?></a>
						</div>
						<?php
					}
				}
			}
        ?>
	</div>
    <div class="row">
        <div id="divPhoto">
            <?php
                $this_div = "divPhoto";
                $this_field = "article_photo";
                $this_startid = "photo_id";
                $this_field_type = "file";
    
                if (empty($form[$this_field]['name'][0]))
                {
                    $i = 1;
                    print "<div class=\"row link-row\">";
                    print form::upload($this_field . '[]', '', ' class="text long"');
                    print "<a href=\"#\" class=\"add\" onClick=\"addFormField('$this_div','$this_field','$this_startid','$this_field_type'); return false;\">add</a>";
                    print "</div>";
                }
                else
                {
                    $i = 0;
                    foreach ($form[$this_field]['name'] as $value) 
                    {
                        print "<div ";
                        if ($i != 0) {
                            print "class=\"row link-row second\" id=\"" . $this_field . "_" . $i . "\">\n";
                        }
                        else
                        {
                            print "class=\"row link-row\" id=\"$i\">\n";
                        }
                        // print "\"<strong>" . $value . "</strong>\"" . "<BR />";
                        print form::upload($this_field . '[]', $value, ' class="text long"');
                        print "<a href=\"#\" class=\"add\" onClick=\"addFormField('$this_div','$this_field','$this_startid','$this_field_type'); return false;\">add</a>";
                        if ($i != 0)
                        {
                            print "<a href=\"#\" class=\"rem\"  onClick='removeFormField(\"#".$this_field."_".$i."\"); return false;'>remove</a>";
                        }
                        print "</div>\n";
                        $i++;
                    }
                }
                print "<input type=\"hidden\" name=\"$this_startid\" value=\"$i\" id=\"$this_startid\">";
                ?>
            </div>
    </div>
    <input id="save-article" type="submit" value="Save Article" /><input id="save-close-article" type="submit" value="Save Article & Close" name="save_and_close" />
</form>