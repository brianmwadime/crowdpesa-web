<?php 
/**
 * Checkins view page.
 *
 * PHP version 5
 * LICENSE: This source file is subject to LGPL license 
 * that is available through the world-wide-web at the following URI:
 * http://www.gnu.org/copyleft/lesser.html
 * @author	   Ushahidi Team <team@ushahidi.com> 
 * @package	   Ushahidi - http://source.ushahididev.com
 * @module	   Checkins View
 * @copyright  Ushahidi - http://www.ushahidi.com
 * @license	   http://www.gnu.org/copyleft/lesser.html GNU Lesser General Public License (LGPL) 
 */
?>
<div class="clear" id="container">
  <div class="content left">
  <div id="loop" class="list clear work_list ">
			<div class="content-title">
  <h3><?php echo Kohana::lang('ui_admin.my_checkins'); ?></h3></div>

 <div class="cat_desc"><p>A List of All your Checkins To date </p>
 </div>
<?php
			if ($message)
            {
                ?><div class="<?php echo $message_class; ?> ui-corner-all">&#8226;&nbsp;<?php echo $message; ?></div><?php
            }
            
           

			?>

						
								<?php	
								
								foreach ($checkins as $checkin)
								{
									$checkin_id = $checkin->id;
									$incident_id = $checkin->incident_id;
									$location = $checkin->location_name;
									$latitude = $checkin->latitude;
									$longitude = $checkin->longitude;
									$description = $checkin->checkin_description;
									$checkin_thumb = url::site() . Kohana::config('upload.relative_directory') . '/' .$checkin->media_thumb;
									$checkin_date = date('Y-m-d h:i', strtotime($checkin->checkin_date));
									$auto_checkin = ($checkin->checkin_auto) ? "YES" : "NO";
									?>
									
										<!--  Post Content Condition for Post Format-->
       <div id="post_2166" class="post event type-event status-publish hentry post">
       <div class="post-content ">
	   					<a  class="post_img" href=""> <img src="<?php echo $checkin_thumb; ?>" alt="<?php echo $location; ?>" title="<?php echo $location; ?>"  /> </a>
            
            <div class="post_content">
         			<!--  Post Title Condition for Post Format-->
           <h2><a href="<?php echo url::site() . 'listing/view/' . $checkin_id; ?>">
      <?php echo $location; ?>  </a></h2>
            <!--  Post Title Condition for Post Format-->
			<div class="post_right">
					<a href="<?php echo url::site() . 'customers/checkins/delete/' . $checkin_id; ?>" class="pcomments" >Delete Checkin</a>
		    
				</div>
				<p class="timing"> <span>Date :</span> <?php echo $checkin_date;?> <span></p>
				<p class="address"><span>Location : </span> Latitude - <?php echo $latitude; ?> <br /> Longitude - <?php echo $longitude; ?></p>	<p> <?php echo $description; ?></p>					 
        </div>
		 
		 
     </div></div>
        
    <!--  Post Content Condition for Post Format-->
       
                
       <div class="hr clearfix"></div>			
													
									<?php
								}
								?>
								<div  class="pagination" ><?php echo $pagination;?></div>
						</div>
					</div>
	<div class="sidebar right right_col">
		<?php // Load listing images ?>
    <?php if (count($media)): ?>
    <script type="text/javascript">
        $(window).load(function() {
            $('#slider').nivoSlider();
        });
    </script>
    
        <h3><?php echo Kohana::lang('ui_main.photos'); ?></h3>
        <div id="slider" class="nivoSlider">
        <?php foreach ($media as $photo): ?>
            <img src="<?php echo url::site() . Kohana::config('upload.relative_directory') . '/' . $photo->media_medium ?>" alt="<?php echo $photo->media_title ?>" title="<?php echo $photo->media_title ?>" />
        <?php endforeach; ?>
        </div>
    
    <?php endif; ?>
	</div>
		
			</div>