<?php
/**
 * Invite page.
 *
 * PHP version 5
 * LICENSE: This source file is subject to LGPL license
 * that is available through the world-wide-web at the following URI:
 * http://www.gnu.org/copyleft/lesser.html
 * @author     Ushahidi Team <team@ushahidi.com>
 * @package    Ushahidi - http://source.ushahididev.com
 * @module     API Controller
 * @copyright  Ushahidi - http://www.ushahidi.com
 * @license    http://www.gnu.org/copyleft/lesser.html GNU Lesser General Public License (LGPL)
 */
?>
<div class="clear" id="container">
	<div class="content left" id="loop">
		<h3><?php echo Kohana::lang('ui_main.invite_user_msg');?></h3>
		<?php print form::open(); ?>
		<div class="report-form">
			<?php
			if ($form_error || $message):?>
		    <div class="alert alert-<?php echo $message_class ?>">
                <button data-dismiss="alert" class="close">×</button>
                <strong><?php echo ucfirst($message_class) ?>:</strong>
			<?php if ($message): ?>
				    <?php echo $message ?>
                <?php else: ?>
                <ul>
				<?php foreach ($errors as $error_item => $error_description)
				{
					print (!$error_description) ? '' : "<li>" . $error_description . "</li>";
				}
				?>
				</ul>
				<?php endif;?>
			</div>
			<?php endif; ?>
			
			<label for="emails"><?php echo Kohana::lang('ui_main.enter_emails');?></label><?php print form::textarea('emails', $form['emails'], ''); ?>
								
            <input type="submit" id="submit" name="submit" value="<?php echo Kohana::lang('ui_main.member_invite');?>" class="login_btn new_submit" />
		</div>
		<?php print form::close(); ?>
	</div>
</div>
