<?php
/**
 * Signup view page.
 *
 * PHP version 5
 * LICENSE: This source file is subject to LGPL license
 * that is available through the world-wide-web at the following URI:
 * http://www.gnu.org/copyleft/lesser.html
 * @author     Ushahidi Team <team@ushahidi.com>
 * @package    Ushahidi - http://source.ushahididev.com
 * @module     API Controller
 * @copyright  Ushahidi - http://www.ushahidi.com
 * @license    http://www.gnu.org/copyleft/lesser.html GNU Lesser General Public License (LGPL)
 */
?>
<div class="clear" id="container">
	<div class="content left" id="loop">
		<?php //if ($form['code']): ?>
		<h3><?php echo Kohana::lang('ui_main.sign_in_user_msg');?></h3>
		<?php print form::open(url::site() . 'login'); ?>
		<input type="hidden" name="action" value="signin">
		<div class="report-form">
			<?php
			if ($message)
            {
            ?>
            <div class="alert alert-<?php echo $message_class ?>">
                <button data-dismiss="alert" class="close">×</button>
                <strong><?php echo ucfirst($message_class) ?>!</strong> <?php echo $message; ?>
            </div>
            <?php
            }
            
            if ($form_error) {
			?>
				<!-- red-box -->
				<div class="red-box">
					<h3><?php echo Kohana::lang('ui_main.error');?></h3>
					<ul>
					<?php
					foreach ($errors as $error_item => $error_description)
					{
						print (!$error_description) ? '' : "<li>" . $error_description . "</li>";
					}
					?>
					</ul>
				</div>
			<?php
			}

			?>
                    
				<label for="username"><?php echo Kohana::lang('ui_main.email') . '/' . Kohana::lang('ui_main.username');?><span class="required"><?php echo Kohana::lang('ui_main.required'); ?></span></label>
				<?php print form::input('username', $form['username'], ' class="text"'); ?>
			
				<label for="password"><?php echo Kohana::lang('ui_main.password');?><span class="required"><?php echo Kohana::lang('ui_main.required'); ?></span></label>
				<?php print form::password('password', $form['password'], ' class="text"'); ?>
				
				<label for="remember"><input type="checkbox" id="remember" name="remember" value="1" checked="checked" class="checkbox" /><?php echo Kohana::lang('ui_main.password_save');?></label>
			
                <input type="submit" id="submit" name="submit" value="<?php echo Kohana::lang('ui_main.login');?>" class="login_btn new_submit" />
		</div>
		<?php print form::close(); ?>
    <?php /*else: ?>
        <h3><?php echo Kohana::lang('ui_main.not_invited');?></h3>
        <p><?php echo Kohana::lang('ui_main.no_invitation_code');?></p>
    <?php endif;*/ ?>
	</div>
</div>
