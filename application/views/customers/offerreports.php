<?php 
/**
 * Checkins view page.
 *
 * PHP version 5
 * LICENSE: This source file is subject to LGPL license 
 * that is available through the world-wide-web at the following URI:
 * http://www.gnu.org/copyleft/lesser.html
 * @author	   Ushahidi Team <team@ushahidi.com> 
 * @package	   Ushahidi - http://source.ushahididev.com
 * @module	   Checkins View
 * @copyright  Ushahidi - http://www.ushahidi.com
 * @license	   http://www.gnu.org/copyleft/lesser.html GNU Lesser General Public License (LGPL) 
 */
?>
<div class="clear" id="container">
  <div class="content left">
  <div id="loop" class="list clear work_list ">
			<div class="content-title">
				<h3><?php echo Kohana::lang('ui_admin.offerreports'); ?></h3></div>

 <div class="cat_desc"><p>A list of all the offers you have collected </p>
	 <ul class="sort_by">
   	<li class="title"> Sort by : </li>
    <li <?php if ($status != 'p' && $status !='np') echo "class=\"current\""; ?>> <a href="<?php echo url::site(); ?>customers/offerreports/myoffers/?status=a" >  All </a></li>
    <li  <?php if ($status == 'p') echo "class=\"current\""; ?>> <a href="<?php echo url::site(); ?>customers/offerreports/myoffers/?status=p"><?php echo Kohana::lang('ui_main.picked') ?></a></li>
    <li <?php if ($status == 'np') echo "class=\"current\""; ?>> <a href="<?php echo url::site(); ?>customers/offerreports/myoffers/?status=np" > <?php echo Kohana::lang('ui_main.not_picked') ?> </a></li>
    <li class="i_next"> <a class="nextpostslink" href="http://templatic.com/demos/geoplaces4/placecategory/places/page/2/">Next</a>  </li>
    <li class="i_previous"></li>
</ul>
 </div>

						
								<?php	
								
								foreach ($offerincidents as $offerincident)
								{
									$offerincident_id = $offerincident->offerincident_id;
									$offerincident_title = strip_tags($offerincident->offerincident_title);
									$offerincident_description = text::limit_chars(strip_tags($offerincident->offerincident_description), 150, "...", true);
									$picked = $offerincident->picked;
									$offer_coupon = $offerincident->offer_coupon;
									$customer_token = $offerincident->customer_token;
									$date_collected = $offerincident->date;
									$total_price = $offerincident->total_price;
									$offer_image_url = url::site() . Kohana::config('upload.relative_directory') . '/' . $offerincident->media_thumb;
									$date_collected = date('Y-m-d', strtotime($offerincident->date));
									//$offerincident_mode = $offerincident->offerincident_mode;	// Mode of submission... WEB/SMS/EMAIL?

									
									?>
									
										<!--  Post Content Condition for Post Format-->
       <div id="post_2166" class="post event type-event status-publish hentry post">
       <div class="post-content ">
	   					<a  class="post_img" href="<?php echo url::site() . 'offer/details/' . $offerincident_id; ?>"> <img src="<?php echo $offer_image_url ?>" alt="<?php echo $offerincident_title; ?>" title="<?php echo $offerincident_title; ?>"  /> </a>
            
            <div class="post_content">
         			<!--  Post Title Condition for Post Format-->
           <h2><a href="<?php echo url::site() . 'offer/details/' . $offerincident_id; ?>">
      <?php echo $offerincident_title; ?>  </a></h2>
            <!--  Post Title Condition for Post Format-->
							<p class="timing"> <span>Order Date:</span> <?php echo $date_collected;?></p>
				<div class="post_right">
				
					<a href="<?php echo url::site() . 'offer/details/' . $offerincident_id; ?>" class="pcomments" >View Offer</a>
                    
				</div>
				
				 <p><span>Paid:</span> <?php echo $total_price;?></p>
                 <p><span>Coupon:</span> <?php echo $offer_coupon;?></p>
                 <p><span>Token:</span> <?php echo $customer_token;?></p>
				<p class="timing"> <span>Status:</span> <?php echo ($picked)?'<font color="green">Picked Up</font>':'<font color="red">Not Yet Picked Up</font>';?> <span></p>				
				
				
				<p> <?php echo $offerincident_description; ?></p>					 
        </div>
		 
		 
     </div></div>
        
    <!--  Post Content Condition for Post Format-->
       
                
       <div class="hr clearfix"></div>			
													
									<?php
								}
								?>
								<div  class="pagination" ><?php echo $pagination;?></div>
						</div>
					</div>
					 
			<div class="sidebar right right_col">
  	<?php 
		
		 //get content blocks
        ob_start();
        $pattern_latest_offers = '#\<li id="block-latest_offers"\>(.*)\<\/li\>#s';
        $pattern_latest_reviews = '#\<li id="block-latest_reviews"\>(.*)\<\/li\>#s';
        $pattern_news = '#\<li id="block-news"\>(.*)\<\/li\>#s';
        blocks::render();
        $all_blocks = ob_get_contents();
        ob_end_clean();
        // get all other blocks except latest offers
        $other_blocks = preg_replace($pattern_latest_offers, '', $all_blocks);
        $other_blocks = preg_replace($pattern_latest_reviews, '', $other_blocks);
        // extract in reverse order as first pattern matches everything to the last block
        // first extract news block
        preg_match_all($pattern_news, $all_blocks, $matches);
        $news_block = $matches[0][0];
        $news_block = str_ireplace('</div></li>', '', $news_block);
        $news_block = str_ireplace('<li id="block-news"><div>', '', $news_block);
        // extract reviews block
        preg_match_all($pattern_latest_reviews, $all_blocks, $matches);
        $latest_reviews = $matches[0][0];
        $latest_reviews = preg_replace($pattern_news, '', $latest_reviews);
        $latest_reviews = str_ireplace('</div></li>', '', $latest_reviews);
        $latest_reviews = str_ireplace('<li id="block-latest_reviews"><div>', '', $latest_reviews);
        // extract latest offers by removing every other block in the matched result
        preg_match_all($pattern_latest_offers, $all_blocks, $matches);
        $latest_offers = $matches[0][0];
        $latest_offers = preg_replace($pattern_latest_reviews, '', $latest_offers);
        $latest_offers = str_ireplace('</div></li>', '', $latest_offers);
        $latest_offers = str_ireplace('<li id="block-latest_offers"><div>', '', $latest_offers);
    
		echo $latest_offers ?>
      </div>
			</div></div></div>