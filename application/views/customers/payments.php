<?php 
/**
 * Checkins view page.
 *
 * PHP version 5
 * LICENSE: This source file is subject to LGPL license 
 * that is available through the world-wide-web at the following URI:
 * http://www.gnu.org/copyleft/lesser.html
 * @author	   Ushahidi Team <team@ushahidi.com> 
 * @package	   Ushahidi - http://source.ushahididev.com
 * @module	   Checkins View
 * @copyright  Ushahidi - http://www.ushahidi.com
 * @license	   http://www.gnu.org/copyleft/lesser.html GNU Lesser General Public License (LGPL) 
 */
?>
			<div class="bg">
				<h2>
					<?php retailers::payments_subtabs("orders"); ?>
				</h2>

				<!-- tabs -->
				<div class="tabs">
					<!-- tabset -->
					<ul class="tabset">
						<li><a href="?status=0" <?php if ($status != 'a' && $status !='v') echo "class=\"active\""; ?>><?php echo Kohana::lang('ui_main.show_all');?></a></li>
						<li><a href="?status=p" <?php if ($status == 'p') echo "class=\"active\""; ?>><?php echo Kohana::lang('ui_main.pending');?></a></li>
						<li><a href="?status=f" <?php if ($status == 'f') echo "class=\"active\""; ?>><?php echo Kohana::lang('ui_main.completed');?></a></li>
					</ul>
					<!-- tab -->
					<div class="tab">
						<ul>
							<li><a href="#" onClick="checkinAction('c', 'CANCEL', '')"><?php echo strtoupper(Kohana::lang('ui_main.cancel'));?></a></li>
						</ul>
					</div>
				</div>
			
				<!-- report-table -->
				<?php print form::open(NULL, array('id' => 'orderMain', 'name' => 'orderMain')); ?>
					<input type="hidden" name="action" id="action" value="">
					<input type="hidden" name="level"  id="level"  value="">
					<input type="hidden" name="id[]" id="checkin_single" value="">
					<div class="table-holder">
						<table class="table">
							<thead>
								<tr>
									<th class="col-1"><input id="checkall" type="checkbox" class="check-box" onclick="CheckAll( this.id, 'id[]' )" /></th>
									<th class="col-2"><?php echo Kohana::lang('ui_admin.order_details');?></th>
									<th class="col-3"><?php echo Kohana::lang('ui_main.date');?></th>
									<th class="col-4"><?php echo Kohana::lang('ui_main.actions');?></th>
								</tr>
							</thead>
						
							<tbody>
								<?php
								if ($total_items == 0)
								{
								?>
									<tr>
										<td colspan="4" class="col">
											<h3><?php echo Kohana::lang('ui_main.no_results');?></h3>
										</td>
									</tr>
								<?php	
								}
								foreach ($orders as $order)
								{
									$order_id = $order->id;
									$customer_name = $order->name;
									$order_title = $order->offerincident_title;		
									$amount = $order->quantity;		
									$price = $order->amount;					
									$description = $order->offerincident_description;
									$preview = text::limit_chars(strip_tags($description), 150, "...", true);					
									$order_date = date('Y-m-d h:i', strtotime($order->date));
									
									?>
									<tr>
										<td class="col-1"><input name="order_id[]" id="checkin" value="<?php echo $order_id; ?>" type="checkbox" class="check-box"/></td>
										<td class="col-2">
											<div class="post">
												<h4><a href="<?php echo url::site() . 'retailers/payments/edit/' . $order_id; ?>" class="more"><?php echo $customer_name; ?></a></h4>
												<p><?php echo $preview; ?>... <a href="<?php echo url::base() . 'retailers/payments/edit/' . $order_id; ?>" class="more"><?php echo Kohana::lang('ui_main.more');?></a></p>
											</div>
											<ul class="info">
												<li class="none-separator"><?php echo Kohana::lang('ui_main.purchase_quantity');?>: <strong><?php echo $amount; ?></strong>,<strong></li>
												<li><?php echo Kohana::lang('ui_main.purchase_price');?> <strong><?php echo $price; ?></li>
											</ul>
											
										</td>
										<td class="col-3"><?php echo $order_date; ?></td>
										<td class="col-4">
											<ul>
												<?php
												if ((int) $order_id)
												{
													echo "<li class=\"none-separator\"><a href=\"". url::base() . 'retailers/payments/edit/' . $order_id ."\" class=\"status_yes\"><strong>".Kohana::lang('ui_admin.view_order')."</strong></a></li>";
												}
												else
												{
													echo "<li class=\"none-separator\"><a href=\"". url::base() . 'retailers/payment/edit?cid=' . $order_id ."\">".Kohana::lang('ui_admin.create_order')."?</a></li>";
												}
												?>
												<li class="none-separator"><a href="javascript:checkinAction('c','CANCEL','<?php echo(rawurlencode($order_id)); ?>')" class="del"><?php echo Kohana::lang('ui_main.cancel');?></a></li>
											</ul>
										</td>
									</tr>
									<?php
								}
								?>
							</tbody>
								<tfoot>
								<tr class="foot">
									<td colspan="4">
										<?php echo $pagination; ?>
									</td>
								</tr>
							</tfoot>
						</table>
					</div>
				<?php print form::close(); ?>
				
				
			</div>