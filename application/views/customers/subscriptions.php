<?php 
/**
 * Checkins view page.
 *
 * PHP version 5
 * LICENSE: This source file is subject to LGPL license 
 * that is available through the world-wide-web at the following URI:
 * http://www.gnu.org/copyleft/lesser.html
 * @author	   Ushahidi Team <team@ushahidi.com> 
 * @package	   Ushahidi - http://source.ushahididev.com
 * @module	   Checkins View
 * @copyright  Ushahidi - http://www.ushahidi.com
 * @license	   http://www.gnu.org/copyleft/lesser.html GNU Lesser General Public License (LGPL) 
 */
?>
<div class="clear" id="container">
  <div class="content left">
  <div id="loop" class="list clear work_list ">
			<div class="content-title">
  <h3><?php echo Kohana::lang('ui_admin.subscriptions'); ?></h3></div>

 <div class="cat_desc"><p>A List of All your Subscriptions To date </p>
 </div>

						
								<?php	
								
								foreach ($subscritpions as $subscription)
								{
									$retailer_id = $subscription->retailer_id;
									$retailer_name = $subscription->name;
									$retailer_address = $subscription->address;
									$retailer_website = $subscription->website;
									$retailer_phone = $subscription->phone;
									
									?>
									
										<!--  Post Content Condition for Post Format-->
       <div id="post_2166" class="post event type-event status-publish hentry post">
       <div class="post-content ">
	   					
         			<!--  Post Title Condition for Post Format-->
           <h2><a href="<?php echo url::site() . 'profile/user/' . $retailer_name; ?>">
      <?php echo $retailer_name; ?>  </a></h2>
            <!--  Post Title Condition for Post Format-->
			<div class="post_right">
					<a href="<?php echo url::site() . 'main/unsubscribe_retailer/' . $retailer_id; ?>" class="pcomments" >Unsubscribe</a>
		    </div>
				<span>Website :</span> <a href="<?php echo $retailer_website;?>"><?php echo $retailer_website;?> </a><span><br />
				Address - <?php echo $retailer_address; ?> <br /> Phone - <?php echo $retailer_phone; ?>					 
        
		 
		 
     </div></div>
        
    <!--  Post Content Condition for Post Format-->
       
                
       <div class="hr clearfix"></div>			
													
									<?php
								}
								?>
								<div  class="pagination" ><?php echo $pagination;?></div>
						</div>
					</div>
	
		
			</div>