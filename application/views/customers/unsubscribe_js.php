<?php
/**
 * Listing view js file based on report js
 *
 * Handles javascript stuff related to offerreports view function.
 *
 * PHP version 5
 * LICENSE: This source file is subject to LGPL license 
 * that is available through the world-wide-web at the following URI:
 * http://www.gnu.org/copyleft/lesser.html
 * @author     BeeBuy Team <info@beebuy.com> 
 * @package    CrowdPesa - http://crowdpesa.com
 * @module     Offerreports Controller
 * @copyright  CrowdPesa - http://crowdpesa.com
 * @license    http://www.gnu.org/copyleft/lesser.html GNU Lesser General Public License (LGPL) 
 */
?>
		
		function Subscribe(id){
            var url = "<?php echo url::site() ?>main/subscribe_retailer/" + id;
            window.location = url;
        }

        function Unsubscribe(id){
            var url = "<?php echo url::site() ?>main/unsubscribe_retailer/" + id;
            window.location = url;
        }
        
		
		