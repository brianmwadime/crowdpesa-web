<header>
	<hgroup>
		<ul id="header_nav_left">

			<li><span class="bignext">&raquo;</span><a href="<?php echo url::site();?>"><?php echo $site_name; ?></a></li>
			<li><span class="bignext">&raquo;</span><a href="<?php echo url::site().'login/home';?>">Home</a></li>
			<li><span class="bignext">&raquo;</span><a href="<?php echo url::site().'login/pricing';?>">Pricing</a></li>
			<li><span class="bignext">&raquo;</span><a href="<?php echo url::site().'login/support';?>">Support</a></li>

		</ul>
	</hgroup>
	<nav>
		<ul id="header_nav_right">
			<li class="header_nav_user header_nav_has_dropdown">
			<?php if($loggedin_user != FALSE){ ?>

				<a href="<?php echo url::site().$loggedin_role;?>"><span class="header_nav_label"><?php echo $loggedin_user->username; ?></span> <img src="<?php echo members::gravatar($loggedin_user->email,20); ?>" width="20" /></a>

				<ul class="header_nav_dropdown" style="display:none;">

					<li><a href="<?php echo url::site().$loggedin_role;?>/profile"><?php echo Kohana::lang('ui_main.manage_your_account'); ?></a></li>

					<li><a href="<?php echo url::site().$loggedin_role;?>"><?php echo Kohana::lang('ui_main.your_dashboard'); ?></a></li>

					<li><a href="<?php echo url::site();?>profile/user/<?php echo $loggedin_user->username; ?>"><?php echo Kohana::lang('ui_main.view_public_profile'); ?></a></li>

					<li><a href="<?php echo url::site();?>logout"><em><?php echo Kohana::lang('ui_admin.logout');?></em></a></li>

				</ul>

			<?php } else { ?>

				<a href="<?php echo url::site()."login/";?>" style="float:right;padding-top:8px;"><span class="header_nav_label"><strong><?php echo Kohana::lang('ui_main.login'); ?></strong></span></a>

				

			<?php } ?>
			</li>
		</ul>
	</nav>
</header>