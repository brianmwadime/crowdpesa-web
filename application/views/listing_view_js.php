<?php
/**
 * Listing view js file based on report js
 *
 * Handles javascript stuff related to offerreports view function.
 *
 * PHP version 5
 * LICENSE: This source file is subject to LGPL license 
 * that is available through the world-wide-web at the following URI:
 * http://www.gnu.org/copyleft/lesser.html
 * @author     BeeBuy Team <info@beebuy.com> 
 * @package    CrowdPesa - http://crowdpesa.com
 * @module     Offerreports Controller
 * @copyright  CrowdPesa - http://crowdpesa.com
 * @license    http://www.gnu.org/copyleft/lesser.html GNU Lesser General Public License (LGPL) 
 */
?>
		var map, markers;
		var myPoint;
		var selectedFeature;
		jQuery(window).load(function() {
			var moved=false;

			/*
			- Initialize Map
			- Uses Spherical Mercator Projection			
			*/
			var proj_4326 = new OpenLayers.Projection('EPSG:4326');
			var proj_900913 = new OpenLayers.Projection('EPSG:900913');
			var options = {
				units: "dd",
				numZoomLevels: 18,
				controls:[],
				projection: proj_900913,
				'displayProjection': proj_4326
				};
			map = new OpenLayers.Map('listing_map', options);
			map.addControl( new OpenLayers.Control.LoadingPanel({minSize: new OpenLayers.Size(573, 366)}) );
			
			<?php echo map::layers_js(FALSE); ?>
			map.addLayers(<?php echo map::layers_array(FALSE); ?>);
	
			map.addControl(new OpenLayers.Control.Navigation());
			var zoomBar = new OpenLayers.Control.PanZoomBar();
			zoomBar.zoomStopHeight = 5;
			map.addControl(zoomBar);
			map.addControl(new OpenLayers.Control.MousePosition(
					{ div: 	document.getElementById('mapMousePosition'), numdigits: 5 
				}));    
			//map.addControl(new OpenLayers.Control.Scale('mapScale'));
			map.addControl(new OpenLayers.Control.ScaleLine());
			map.addControl(new OpenLayers.Control.LayerSwitcher());
			
			
			// Set Feature Styles
			style1 = new OpenLayers.Style({
				pointRadius: "8",
				fillColor: "${fillcolor}",
				fillOpacity: "0.7",
				strokeColor: "${strokecolor}",
				strokeOpacity: "0.7",
				strokeWidth: "${strokewidth}",
				graphicZIndex: 1,
				externalGraphic: "${graphic}",
				graphicOpacity: 1,
				graphicWidth: "${width}",
				graphicHeight: "${height}",
				graphicXOffset: -14,
				graphicYOffset: -27
			},
			{
				context: 
				{
					graphic: function(feature)
					{
						if ( typeof(feature) != 'undefined' && 
							feature.data.id == <?php echo $incident_id; ?>)
						{
							return "<?php echo url::file_loc('img').'media/markers/' . $main_icon ;?>";
						}
						else
						{
							return "<?php echo url::file_loc('img').'media/markers/place_red.png' ;?>";
						}
					},
					width: function(feature)
                    {
                        if ( typeof(feature) != 'undefined' && 
                            feature.data.id == <?php echo $incident_id; ?>)
                        {
                            return <?php echo $main_icon_width ?>;
                        }
                        else
                        {
                            return 20;
                        }
                    },
                    height: function(feature)
                    {
                        if ( typeof(feature) != 'undefined' && 
                            feature.data.id == <?php echo $incident_id; ?>)
                        {
                            return <?php echo $main_icon_height ?>;
                        }
                        else
                        {
                            return 25;
                        }
                    },
					fillcolor: function(feature)
					{
						if ( typeof(feature.attributes.color) != 'undefined' && 
							feature.attributes.color != '' )
						{
							return "#"+feature.attributes.color;
						}
						else
						{
							return "#ffcc66";
						}
					},
					strokecolor: function(feature)
					{
						if ( typeof(feature.attributes.strokecolor) != 'undefined' && 
							feature.attributes.strokecolor != '')
						{
							return "#"+feature.attributes.strokecolor;
						}
						else
						{
							return "#CC0000";
						}
					},					
					strokewidth: function(feature)
					{
						if ( typeof(feature.attributes.strokewidth) != 'undefined' && 
							feature.attributes.strokewidth != '')
						{
							return feature.attributes.strokewidth;
						}
						else
						{
							return "3";
						}
					}
				}
			});
			style2 = new OpenLayers.Style({
				pointRadius: "8",
				fillColor: "#30E900",
				fillOpacity: "0.7",
				strokeColor: "#197700",
				strokeWidth: 3,
				graphicZIndex: 1
			});
			
			// Create the single marker layer
			<?php if ($controller_prefix == '') {
                    $json_url = url::site() . 'json/single/' . $incident_id;
                } else {
                    $json_url = url::site() . 'offerjson/places/' . $incident_id;
                }
			?>
			markers = new OpenLayers.Layer.GML("Location", "<?php echo $json_url; ?>", 
			{
				format: OpenLayers.Format.GeoJSON,
				projection: map.displayProjection,
				styleMap: new OpenLayers.StyleMap({"default":style1, "select": style1, "temporary": style2})
			});
			
			map.addLayer(markers);
			
			selectCtrl = new OpenLayers.Control.SelectFeature(markers, {
				onSelect: onFeatureSelect, 
				onUnselect: onFeatureUnselect
			});
			highlightCtrl = new OpenLayers.Control.SelectFeature(markers, {
			    hover: true,
			    highlightOnly: true,
			    renderIntent: "temporary"
			});		

			map.addControl(selectCtrl);
			map.addControl(highlightCtrl);
			selectCtrl.activate();
			//highlightCtrl.activate();

			// create a lat/lon object
			myPoint = new OpenLayers.LonLat(<?php echo $longitude; ?>, <?php echo $latitude; ?>);
			myPoint.transform(proj_4326, map.getProjectionObject());
			
			// display the map centered on a latitude and longitude (Google zoom levels)

			map.setCenter(myPoint, <?php echo ($incident_zoom) ? $incident_zoom : intval(Kohana::config('settings.default_zoom')); ?>);
			
			markers.events.on({
                    featuresadded: function(event) {
                    map.zoomToExtent(markers.getDataExtent());
                }});
            });
		
		$(document).ready(function(){
			
			/*
			Add Comments JS
			*/			
			// Ajax Validation
			$("#commentForm").validate({
				rules: {
					comment_author: {
						required: true,
						minlength: 3
					},
					comment_email: {
						required: true,
						email: true
					},
					comment_description: {
						required: true,
						minlength: 3
					},
					captcha: {
						required: true
					}
				},
				messages: {
					comment_author: {
						required: "Please enter your Name",
						minlength: "Your Name must consist of at least 3 characters"
					},
					comment_email: {
						required: "Please enter an Email Address",
						email: "Please enter a valid Email Address"
					},
					comment_description: {
						required: "Please enter a Comment",
						minlength: "Your Comment must be at least 3 characters long"
					},
					captcha: {
						required: "Please enter the Security Code"
					}
				}
			});
			
		});
		
		function Subscribe(id){
            var url = "<?php echo url::site() ?>main/subscribe_retailer/" + id;
            window.location = url;
        }

        function Unsubscribe(id){
            var url = "<?php echo url::site() ?>main/unsubscribe_retailer/" + id;
            window.location = url;
        }
        
		function onPopupClose(evt) {
            selectCtrl.unselect(selectedFeature);
			selectedFeature = '';
        }

        function onFeatureSelect(feature) {
            selectedFeature = feature;
			// Lon/Lat Spherical Mercator
			zoom_point = feature.geometry.getBounds().getCenterLonLat();
			lon = zoom_point.lon;
			lat = zoom_point.lat;
            var content = "<div class=\"infowindow\"><div class=\"infowindow_list\">"+feature.attributes.name + "</div>";
			content = content + "\n<div class=\"infowindow_meta\"><a href='javascript:zoomToSelectedFeature("+ lon + ","+ lat +", 1)'>Zoom&nbsp;In</a>&nbsp;&nbsp;|&nbsp;&nbsp;<a href='javascript:zoomToSelectedFeature("+ lon + ","+ lat +", -1)'>Zoom&nbsp;Out</a></div>";
			content = content + "</div>";
			// Since KML is user-generated, do naive protection against
            // Javascript.
            if (content.search("<script") != -1) {
                content = "Content contained Javascript! Escaped content below.<br />" + content.replace(/</g, "&lt;");
            }
            popup = new OpenLayers.Popup.FramedCloud("chicken", 
                                     feature.geometry.getBounds().getCenterLonLat(),
                                     new OpenLayers.Size(100,100),
                                     content,
                                     null, true, onPopupClose);
            feature.popup = popup;
            map.addPopup(popup);
        }

        function onFeatureUnselect(feature) {
            map.removePopup(feature.popup);
            feature.popup.destroy();
            feature.popup = null;
        }
		
		function zoomToSelectedFeature(lon, lat, zoomfactor){
			var lonlat = new OpenLayers.LonLat(lon,lat);
			map.panTo(lonlat);
			// Get Current Zoom
			currZoom = map.getZoom();
			// New Zoom
			newZoom = currZoom + zoomfactor;
			map.zoomTo(newZoom);
		}
		
		function rating(id,action,type,loader)
		{
			$('#' + loader).html('<img src="<?php echo url::file_loc('img')."media/img/loading_g.gif"; ?>">');
			$.post("<?php echo url::site().'offerreports/rating/' ?>" + id, { action: action, type: type },
				function(data){
					if (data.status == 'saved'){
						if (type == 'original') {
							$('#oup_' + id).attr("src","<?php echo url::file_loc('img').'media/img/'; ?>gray_up.png");
							$('#odown_' + id).attr("src","<?php echo url::file_loc('img').'media/img/'; ?>gray_down.png");
							$('#orating_' + id).html(data.rating);
						}
						else if (type == 'comment')
						{
							$('#cup_' + id).attr("src","<?php echo url::file_loc('img').'media/img/'; ?>gray_up.png");
							$('#cdown_' + id).attr("src","<?php echo url::file_loc('img').'media/img/'; ?>gray_down.png");
							$('#crating_' + id).html(data.rating);
						}
					} else {
						if(typeof(data.message) != 'undefined') {
							alert(data.message);
						}
					}
					$('#' + loader).html('');
			  	}, "offerjson");
		}
        
        function rateoffer(id, rating, type, loader)
		{
			return false;
            $('#' + loader).html('<img src="<?php echo url::file_loc('img')."media/img/loading_g.gif"; ?>">');
			$.post("<?php echo url::site().'offerreports/rate/' ?>" + id, { rating: rating, type: type },
				function(data){
					if (data.status == 'saved'){
						if (type == 'original') {
							//$('#oup_' + id).attr("src","<?php echo url::file_loc('img').'media/img/'; ?>gray_up.png");
							//$('#odown_' + id).attr("src","<?php echo url::file_loc('img').'media/img/'; ?>gray_down.png");
							$('#orating_' + id).html(data.rating);
						}
						else if (type == 'comment')
						{
							//$('#cup_' + id).attr("src","<?php echo url::file_loc('img').'media/img/'; ?>gray_up.png");
							//$('#cdown_' + id).attr("src","<?php echo url::file_loc('img').'media/img/'; ?>gray_down.png");
							$('#crating_' + id).html(data.rating);
						}
					} else {
						if(typeof(data.message) != 'undefined') {
							alert(data.message);
						}
					}
					$('#' + loader).html('');
			  	}, "offerjson");
		}
        
        function highlight_rating(star_no, offer_id){
            return false;
            for(i=1;i<=5;i++){
            	var star_img = document.getElementById('offer_rating_' + offer_id + '_' + i);
                var rating_url = star_img.getAttribute('src');
                if (i <= star_no ) {
                    star_img.setAttribute('src', rating_url.replace('rating_off.png', 'rating_on.png'));
                }else{
                	star_img.setAttribute('src', rating_url.replace('rating_on.png', 'rating_off.png'));
                }
            }
        }
        
        function restore_rating(x, offer_id, star_no){
        	return false;
            // only restore if mouse over star whose position is less than the current
            var rating = document.getElementById('offerrating_' + offer_id).innerHTML;
			//for(i=1;i<=5;i++){
                var rating_star = (rating >= star_no) ? 'rating_on.png' : 'rating_off.png';
                var rating_replace = (rating >= star_no) ? 'rating_off.png' : 'rating_on.png';
                var rating_url = x.getAttribute('src');
                x.setAttribute('src', rating_url.replace(rating_replace, rating_star));
            //}
        }
		
		function getFeature(feature_id) {
			var features = markers.features;
			for(var i=0; i<features.length; i++) {
				var feature = features[i];
				if(typeof(feature.attributes.feature_id) != 'undefined' && 
					feature.attributes.feature_id == feature_id)
				{
					if (typeof(selectedFeature) != 'undefined' && selectedFeature !='' ) {
						selectCtrl.unselect(selectedFeature);
					}
					selectCtrl.select(feature);
				}
			}
		}