<?php
/**
 * Offer view page.
 *
 * PHP version 5
 * LICENSE: This source file is subject to LGPL license
 * that is available through the world-wide-web at the following URI:
 * http://www.gnu.org/copyleft/lesser.html
 * @author     Ushahidi Team <team@ushahidi.com>
 * @package    Ushahidi - http://source.ushahididev.com
 * @module     API Controller
 * @copyright  Ushahidi - http://www.ushahidi.com
 * @license    http://www.gnu.org/copyleft/lesser.html GNU Lesser General Public License (LGPL)
 */
?>
			<div class="bg">
				<h2>
					<?php members::offerreports_subtabs("view"); ?>
				</h2>
				<!-- tabs -->
				<div class="tabs">
					<!-- tabset -->
					<ul class="tabset">
						<li><a href="?status=0" <?php if ($status != 'a' && $status !='v') echo "class=\"active\""; ?>><?php echo Kohana::lang('ui_main.show_all');?></a></li>
						<li><a href="?status=a" <?php if ($status == 'a') echo "class=\"active\""; ?>><?php echo Kohana::lang('ui_main.awaiting_approval');?></a></li>
						<li><a href="?status=v" <?php if ($status == 'v') echo "class=\"active\""; ?>><?php echo Kohana::lang('ui_main.awaiting_verification');?></a></li>
					</ul>
					<!-- tab -->
					<div class="tab">
						<ul>
							<li><a href="#" onclick="offerreportAction('d','DELETE', '');"><?php echo Kohana::lang('ui_main.delete');?></a></li>
						</ul>
					</div>
				</div>
				<?php
				if ($form_error) {
				?>
					<!-- red-box -->
					<div class="red-box">
						<h3><?php echo Kohana::lang('ui_main.error');?></h3>
						<ul><?php echo Kohana::lang('ui_main.select_one');?></ul>
					</div>
				<?php
				}

				if ($form_saved) {
				?>
					<!-- green-box -->
					<div class="green-box" id="submitStatus">
						<h3><?php echo Kohana::lang('ui_main.offerreports');?> <?php echo $form_action; ?> <a href="#" id="hideMessage" class="hide">hide this message</a></h3>
					</div>
				<?php
				}
				?>
				<!-- offerreport-table -->
				<?php print form::open(NULL, array('id' => 'offerreportMain', 'name' => 'offerreportMain')); ?>
					<input type="hidden" name="action" id="action" value="">
					<input type="hidden" name="offerincident_id[]" id="offerincident_single" value="">
					<div class="table-holder">
						<table class="table">
							<thead>
								<tr>
									<th class="col-1"><input id="checkallincidents" type="checkbox" class="check-box" onclick="CheckAll( this.id, 'offerincident_id[]' )" /></th>
									<th class="col-2"><?php echo Kohana::lang('ui_main.offerreport_details');?></th>
									<th class="col-3"><?php echo Kohana::lang('ui_main.date');?></th>
									<th class="col-4"><?php echo Kohana::lang('ui_main.actions');?></th>
								</tr>
							</thead>
							<tfoot>
								<tr class="foot">
									<td colspan="4">
										<?php echo $pagination; ?>
									</td>
								</tr>
							</tfoot>
							<tbody>
								<?php
								if ($total_items == 0)
								{
								?>
									<tr>
										<td colspan="4" class="col">
											<h3><?php echo Kohana::lang('ui_main.no_results');?></h3>
										</td>
									</tr>
								<?php
								}
								foreach ($offerincidents as $offerincident)
								{
									$offerincident_id = $offerincident->id;
									$offerincident_title = strip_tags($offerincident->offerincident_title);
									$offerincident_description = text::limit_chars(strip_tags($offerincident->offerincident_description), 150, "...", true);
									$offerincident_date = $offerincident->offerincident_date;
									$offerincident_date = date('Y-m-d', strtotime($offerincident->offerincident_date));
									$offerincident_mode = $offerincident->offerincident_mode;	// Mode of submission... WEB/SMS/EMAIL?

									//XXX offerincident_Mode will be discontinued in favour of $service_id
									if ($offerincident_mode == 1)	// Submitted via WEB
									{
										$submit_mode = "WEB";
										// Who submitted the offerreport?
										if ($offerincident->offerincident_person->id)
										{
											// Offerreport was submitted by a visitor
											$submit_by = $offerincident->offerincident_person->person_first . " " . $offerincident->offerincident_person->person_last;
										}
										else
										{
											if ($offerincident->user_id)					// Offer Was Submitted By Administrator
											{
												$submit_by = $offerincident->user->name;
											}
											else
											{
												$submit_by = Kohana::lang('ui_admin.unknown');
											}
										}
									}
									elseif ($offerincident_mode == 2) 	// Submitted via SMS
									{
										$submit_mode = "SMS";
										$submit_by = $offerincident->message->message_from;
									}
									elseif ($offerincident_mode == 3) 	// Submitted via Email
									{
										$submit_mode = "EMAIL";
										$submit_by = $offerincident->message->message_from;
									}
									elseif ($offerincident_mode == 4) 	// Submitted via Twitter
									{
										$submit_mode = "TWITTER";
										$submit_by = $offerincident->message->message_from;
									}

									$offerincident_location = $locations[$offerincident->location_id];
									$country_id = $country_ids[$offerincident->location_id]['country_id'];

									// Retrieve Incident Offer-categories
									$offerincident_offercategory = "";
									foreach($offerincident->offerincident_offercategory as $offercategory)
									{
										$offerincident_offercategory .= "<a href=\"#\">" . $offercategory->offercategory->offercategory_title . "</a>&nbsp;&nbsp;";
									}
									?>
									<tr>
										<td class="col-1"><input name="offerincident_id[]" id="offerincident" value="<?php echo $offerincident_id; ?>" type="checkbox" class="check-box"/></td>
										<td class="col-2">
											<div class="post">
												<h4><a href="<?php echo url::site() . 'members/offerreports/edit/' . $offerincident_id; ?>" class="more"><?php echo $offerincident_title; ?></a></h4>
												<p><?php echo $offerincident_description; ?>... <a href="<?php echo url::base() . 'members/offerreports/edit/' . $offerincident_id; ?>" class="more"><?php echo Kohana::lang('ui_main.more');?></a></p>
											</div>
											<ul class="info">
												<li class="none-separator"><?php echo Kohana::lang('ui_main.location');?>: <strong><?php echo $offerincident_location; ?></strong>,<strong><?php if ($country_id !=0) { echo $countries[$country_id];}?></strong></li>
												<li><?php echo Kohana::lang('ui_main.submitted_by');?> <strong><?php echo $submit_by; ?></strong> via <strong><?php echo $submit_mode; ?></strong></li>
											</ul>
											<ul class="links">
												<li class="none-separator"><?php echo Kohana::lang('ui_main.offercategories');?>:<?php echo $offerincident_offercategory; ?></li>
											</ul>
											<?php
											// Action::offerreport_extra_admin - Add items to the offerreport list in admin
											Event::run('ushahidi_action.offerreport_extra_members', $offerincident);
											?>
										</td>
										<td class="col-3"><?php echo $offerincident_date; ?></td>
										<td class="col-4">
											<ul>
												<li class="none-separator"><a href="#" class="del" onclick="offerreportAction('d','DELETE', '<?php echo $offerincident_id; ?>');"><?php echo Kohana::lang('ui_main.delete');?></a></li>
											</ul>
										</td>
									</tr>
									<?php
								}
								?>
							</tbody>
						</table>
					</div>
				<?php print form::close(); ?>
			</div>
