<?php
/**
 * Site view page.
 *
 * PHP version 5
 * LICENSE: This source file is subject to LGPL license
 * that is available through the world-wide-web at the following URI:
 * http://www.gnu.org/copyleft/lesser.html
 * @author     Ushahidi Team <team@ushahidi.com>
 * @package    Ushahidi - http://source.ushahididev.com
 * @module     API Controller
 * @copyright  Ushahidi - http://www.ushahidi.com
 * @license    http://www.gnu.org/copyleft/lesser.html GNU Lesser General Public License (LGPL)
 */
?>
			<div class="bg">
					<h2>
					<?php retailers::order_view_subtabs("view"); ?>
				</h2>
				<?php print form::open(); ?>
				<div class="report-form">
					<?php
					if ($form_error) {
					?>
						<!-- red-box -->
						<div class="red-box">
							<h3><?php echo Kohana::lang('ui_main.error');?></h3>
							<ul>
							<?php echo $form_error; ?>
							</ul>
						</div>
					<?php
					}

					if ($form_saved) {
					?>
						<!-- green-box -->
						<div class="green-box">
							<h3><?php echo Kohana::lang('ui_main.order_saved');?></h3>
						</div>
					<?php
					}
					?>
					<div class="head">
						<input type="image" src="<?php echo url::file_loc('img'); ?>media/img/admin/btn-save.gif" class="save-rep-btn" />
					</div>
					<!-- column -->
					<div class="sms_holder">

						<div class="row">
							<h4><?php echo Kohana::lang('ui_main.name'); ?></h4>
							<?php echo $order->name ?>
						</div>
						<div class="row">
							<h4><?php echo Kohana::lang('ui_main.offer_purchased'); ?></h4>
							<?php echo $order->offerincident_title ?>
						</div>
						<div class="row">
							<h4><?php echo Kohana::lang('ui_main.paid'); ?></h4>
							<?php echo $order->total_price ?>
						</div>
						<div class="row">
							<h4><?php echo Kohana::lang('ui_main.date'); ?></h4>
							<?php echo $order->date ?>
						</div>

						<div class="row">
							<h4><a href="#" class="tooltip" title="<?php echo Kohana::lang("tooltips.customer_token"); ?>"><?php echo Kohana::lang('ui_main.customer_token');?></a></h4>
							<?php print form::input('customer_token', $form['customer_token'], ' class="text long1"'); ?>
						</div>

					
						

					
					</div>

					<div class="simple_border"></div>

					<input type="image" src="<?php echo url::file_loc('img'); ?>media/img/admin/btn-save.gif" class="save-rep-btn" />
				</div>
				<?php print form::close(); ?>
			</div>
