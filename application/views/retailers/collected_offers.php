<?php 
/**
 * Checkins view page.
 *
 * PHP version 5
 * LICENSE: This source file is subject to LGPL license 
 * that is available through the world-wide-web at the following URI:
 * http://www.gnu.org/copyleft/lesser.html
 * @author	   Ushahidi Team <team@ushahidi.com> 
 * @package	   Ushahidi - http://source.ushahididev.com
 * @module	   Checkins View
 * @copyright  Ushahidi - http://www.ushahidi.com
 * @license	   http://www.gnu.org/copyleft/lesser.html GNU Lesser General Public License (LGPL) 
 */
?>
			<div class="bg">
				<h2>
					<?php retailers::orders_subtabs("orders"); ?>
				</h2>

				<!-- tabs -->
				<div class="tabs">
					<!-- tabset -->
					<ul class="tabset">
						<li><a href="?status=0" <?php if ($status != 'p' && $status !='np') echo "class=\"active\""; ?>><?php echo Kohana::lang('ui_main.show_all');?></a></li>
						<li><a href="?status=p" <?php if ($status == 'p') echo "class=\"active\""; ?>><?php echo Kohana::lang('ui_main.picked');?></a></li>
						<li><a href="?status=np" <?php if ($status == 'np') echo "class=\"active\""; ?>><?php echo Kohana::lang('ui_main.not_picked');?></a></li>
					</ul>
					<!-- tab -->
					<div class="tab">
						
					</div>
				</div>
			
				<!-- report-table -->
				<?php print form::open(NULL, array('id' => 'orderMain', 'name' => 'orderMain')); ?>
					<input type="hidden" name="action" id="action" value="">
					<input type="hidden" name="level"  id="level"  value="">
					<input type="hidden" name="id[]" id="checkin_single" value="">
					<div class="table-holder">
						<table class="table">
							<thead>
								<tr>
									<th class="col-1"><input id="checkall" type="checkbox" class="check-box" onclick="CheckAll( this.id, 'id[]' )" /></th>
									<th class="col-2"><?php echo Kohana::lang('ui_admin.order_details');?></th>
									<th class="col-3"><?php echo Kohana::lang('ui_main.status');?></th>
									<th class="col-4"><?php echo Kohana::lang('ui_main.actions');?></th>
								</tr>
							</thead>
						
							<tbody>
								<?php
								if ($total_items == 0)
								{
								?>
									<tr>
										<td colspan="4" class="col">
											<h3><?php echo Kohana::lang('ui_main.no_results');?></h3>
										</td>
									</tr>
								<?php	
								}
							foreach ($collected_offers as $collected)
								{
									$order_id = $collected->id;
									$customer_name = $collected->name;
									$offer_title = $collected->offerincident_title;
									$total_price = $collected->total_price;
									$coupon = $collected->offer_coupon;
									$picked = ($collected->picked == 1)?'Picked':'Not Picked';
									$order_date = $collected->date;
									$order_date = date('Y-m-d', strtotime($collected->date));
									

									
									?>
									<tr>
										<td class="col-1"><input name="order_id[]" id="checkin" value="<?php echo $order_id; ?>" type="checkbox" class="check-box"/></td>
										<td class="col-2">
											<div class="post">
											<?php if($collected->picked == '0'){ ?>
												<h4><a href="<?php echo url::site() . 'retailers/collectedoffers/edit/' . $order_id; ?>" class="more"><?php echo $customer_name; ?></a></h4>
											<?php }else{?>
											<h4><a class="more"><?php echo $customer_name; ?></a></h4>
											<?php };?>
											</div>
											<ul class="info">
												<li>Offer: <?php echo $offer_title;?></li>
												<li>Price: <?php echo $total_price;?></li>
												<li>Coupon: <?php echo $coupon;?></li>
											</ul>
											
										</td>
										<td class="col-3"><?php echo $picked; ?></td>
										<td class="col-4">
											<ul>
												<?php
												if ($collected->picked == '0')
												{
													echo "<li class=\"none-separator\"><a href=\"". url::base() . 'retailers/collectedoffers/edit/' . $order_id ."\" class=\"status_yes\"><strong>".Kohana::lang('ui_admin.view_order')."</strong></a></li>";
												}else{
													echo "<li class=\"none-separator\"><strong>".Kohana::lang('ui_admin.checked')."</strong></li>";
												}
												
												?>
											
											</ul>
										</td>
									</tr>
									<?php
								}
								?>
							</tbody>
								<tfoot>
								<tr class="foot">
									<td colspan="4">
										<?php echo $pagination; ?>
									</td>
								</tr>
							</tfoot>
						</table>
					</div>
				<?php print form::close(); ?>
				
				
			</div>
	
									