<?php 
/**
 * Comments view page.
 *
 * PHP version 5
 * LICENSE: This source file is subject to LGPL license 
 * that is available through the world-wide-web at the following URI:
 * http://www.gnu.org/copyleft/lesser.html
 * @author     Ushahidi Team <team@ushahidi.com> 
 * @package    Ushahidi - http://source.ushahididev.com
 * @module     API Controller
 * @copyright  Ushahidi - http://www.ushahidi.com
 * @license    http://www.gnu.org/copyleft/lesser.html GNU Lesser General Public License (LGPL) 
 */
?>
			<div class="bg">
				<h2>
					<?php retailers::offerreports_subtabs("offercomments"); ?>
				</h2>
				<!-- tabs -->
			
				<?php
				if ($form_error)
				{
				?>
					<!-- red-box -->
					<div class="red-box">
						<h3><?php echo Kohana::lang('ui_main.error');?></h3>
						<ul><?php echo Kohana::lang('ui_main.select_one');?></ul>
					</div>
				<?php
				}

				if ($form_saved)
				{
				?>
					<!-- green-box -->
					<div class="green-box" id="submitStatus">
						<h3><?php echo Kohana::lang('ui_admin.comments'); ?> <?php echo $form_action; ?> <a href="#" id="hideMessage" class="hide"><?php echo Kohana::lang('ui_main.hide_this_message');?></a></h3>
					</div>
				<?php
				}
				?>
				<!-- report-table -->
				<?php print form::open(NULL, array('id' => 'commentMain', 'name' => 'commentMain')); ?>
					<input type="hidden" name="action" id="action" value="">
					<input type="hidden" name="comment_id[]" id="comment_single" value="">
					<div class="table-holder">
						<table class="table">
							<thead>
								<tr>
									
									<th class="col-2"><?php echo Kohana::lang('ui_main.review_details');?></th>
									<th class="col-3"><?php echo Kohana::lang('ui_main.date');?></th>
									
								</tr>
							</thead>
							<tfoot>
								<tr class="foot">
									<td colspan="4">
										<?php echo $pagination; ?>
									</td>
								</tr>
							</tfoot>
							<tbody>
								<?php
								if ($total_items == 0)
								{
								?>
									<tr>
										<td colspan="4" class="col">
											<h3><?php echo Kohana::lang('ui_main.no_results');?></h3>
										</td>
									</tr>
								<?php	
								}
								foreach ($comments as $comment)
								{
									$comment_id = $comment->id;
									$comment_author = $comment->comment_author;
									$comment_description = $comment->comment_description;
									$comment_email = $comment->comment_email;
									$comment_ip = $comment->comment_ip;
									$comment_active = $comment->comment_active;
									$comment_spam = $comment->comment_spam;
									$comment_rating = $comment->comment_rating;
									$comment_date = date('Y-m-d H:i', strtotime($comment->comment_date));
									
									$offerincident_id = $comment->offerincident->id;
									$offerincident_title = $comment->offerincident->offerincident_title;
									?>
									<tr>
										
										<td class="col-1">
											<div class="post">
												<h4><a href="<?php echo url::base() . 'offer/details/' . $offerincident_id.'/#comment-'.$comment_id; ?>"><?php echo $comment_author; ?></a></h4>
												<?php
												if ($offerincident_title != "")
												{
													?><div class="comment_incident"><?php echo Kohana::lang('ui_main.in_response_to');?>: <strong><a href="<?php echo url::base() . 'offer/details/' . $offerincident_id.'/#comment-'.$comment_id; ?>"><?php echo $offerincident_title; ?></a></strong></div><?php
												}
												?>
												<p><?php echo $comment_description; ?></p>
											</div>
											<ul class="info">
												<li class="none-separator"><?php echo Kohana::lang('ui_main.email');?>: <strong><?php echo $comment_email; ?></strong></li>
												<li><?php echo Kohana::lang('ui_main.ip_address');?>: <strong><?php echo $comment_ip; ?></strong></li>
												<li><?php echo Kohana::lang('ui_main.comment_rating');?>: <strong><?php echo $comment_rating; ?></strong></li>
											</ul>
										</td>
										<td class="col-2"><?php echo $comment_date; ?></td>
										
									</tr>
									<?php
								}
								?>
							</tbody>
						</table>
					</div>
				<?php print form::close(); ?>
			</div>
