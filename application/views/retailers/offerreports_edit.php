<?php 
/**
 * Offer-edit view page.
 *
 * PHP version 5
 * LICENSE: This source file is subject to LGPL license 
 * that is available through the world-wide-web at the following URI:
 * http://www.gnu.org/copyleft/lesser.html
 * @author     Ushahidi Team <team@ushahidi.com> 
 * @package    Ushahidi - http://source.ushahididev.com
 * @module     API Controller
 * @copyright  Ushahidi - http://www.ushahidi.com
 * @license    http://www.gnu.org/copyleft/lesser.html GNU Lesser General Public License (LGPL) 
 */
?>
			<div class="bg">
				<h2>
					<?php retailers::offerreports_subtabs("edit"); ?>
				</h2>
				<?php print form::open(NULL, array('enctype' => 'multipart/form-data', 'id' => 'offerreportForm', 'name' => 'offerreportForm')); ?>
					<input type="hidden" name="save" id="save" value="">
					<input type="hidden" name="offer_id" id="offer_id" value="<?php print $form['offer_id']; ?>">
					<input type="hidden" name="offerincident_zoom" id="offerincident_zoom" value="<?php print $form['offerincident_zoom']; ?>">
					<input type="hidden" name="country_name" id="country_name" value="<?php echo $form['country_name'];?>" />
					<!-- offerreport-form -->
					<div class="report-form">
						<?php if ($form_error): ?>
							<!-- red-box -->
							<div class="red-box">
								<h3><?php echo Kohana::lang('ui_main.error');?></h3>
								<ul>
								<?php
								foreach ($errors as $error_item => $error_description)
								{
									print (!$error_description) ? '' : "<li>" . $error_description . "</li>";
								}
								?>
								</ul>
							</div>
						<?php endif; ?>

						<?php if ($form_saved): ?>
							<!-- green-box -->
							<div class="green-box">
								<h3><?php echo Kohana::lang('ui_main.offerreport_saved');?></h3>
							</div>
						<?php endif; ?>
						
						<div class="head">
							<h3><?php echo $id ? Kohana::lang('ui_main.edit_offerreport') : Kohana::lang('ui_main.new_offerreport'); ?></h3>
							<div class="btns" style="float:right;">
								<ul>
									<li><a href="#" class="btn_save"><?php echo strtoupper(Kohana::lang('ui_main.save_offerreport'));?></a></li>
									<li><a href="#" class="btn_save_close"><?php echo strtoupper(Kohana::lang('ui_main.save_close'));?></a></li>
									<li><a href="<?php echo url::base().'retailers/offerreports/';?>" class="btns_red"><?php echo strtoupper(Kohana::lang('ui_main.cancel'));?></a>&nbsp;&nbsp;&nbsp;</li>
									<?php if ($id): ?>
									<li><a href="<?php echo $previous_url;?>" class="btns_gray">&laquo; <?php echo strtoupper(Kohana::lang('ui_main.previous'));?></a></li>
									<li><a href="<?php echo $next_url;?>" class="btns_gray"><?php echo strtoupper(Kohana::lang('ui_main.next'));?> &raquo;</a></li>
									<?php endif; ?>
								</ul>
							</div>
						</div>
						
						<!-- f-col -->
						<div class="f-col">
							<?php
							// Action::offerreport_pre_form_admin - Runs right before offerreport form is rendered
							Event::run('ushahidi_action.offerreport_pre_form_messages', $id);
							?>
							
							<div class="row">
								<h4><?php echo Kohana::lang('ui_main.title');?><span class="required">*</span></h4>
								<?php print form::input('offerincident_title', $form['offerincident_title'], ' class="text title"'); ?>
							</div>
							<div class="row">
											<h4><a href="#" class="tooltip" title="<?php echo Kohana::lang("tooltips.offer_value"); ?>"><?php echo Kohana::lang('ui_main.offervalue');?></a> </h4>
											<?php print form::input('offerincident_price', $form['offerincident_price'], ' class="text title"'); ?>
							</div>
								<div class="row">
											<h4><a href="#" class="tooltip" title="<?php echo Kohana::lang("tooltips.offerscope"); ?>"><?php echo Kohana::lang('ui_main.offerscope');?></a></h4>
							<?php print form::dropdown('scope', $yesno_array, $form['scope']); ?>
							</div>
								<div class="row">
											<h4><a href="#" class="tooltip" title="<?php echo Kohana::lang("tooltips.offer_status"); ?>"><?php echo Kohana::lang('ui_main.offer_status');?></a></h4>
							<?php print form::dropdown('published', $status_array, $form['published']); ?>
							</div>
							<div class="row">
											<h4><a href="#" class="tooltip" title="<?php echo Kohana::lang("tooltips.maxcoupons"); ?>"><?php echo Kohana::lang('ui_main.max_coupons');?></a> </h4>
											<?php print form::input('max_coupons', $form['max_coupons'], ' class="text title"'); ?>
							</div>
							<?php if($form['prefix'] != 'hidden'):?>
							<div class="row">
											<h4><a href="#" class="tooltip" title="<?php echo Kohana::lang("tooltips.prefix"); ?>"><?php echo Kohana::lang('ui_main.coupon_prefix');?></a> </h4>
											<?php 
											
													print form::input('coupon_prefix', $form['coupon_prefix'], ' class="text long1"');
												
											 ?>
							</div>
							<?php endif; ?>
							<div class="row">
											<h4><a href="#" class="tooltip" title="<?php echo Kohana::lang("tooltips.couponcode"); ?>"><?php echo Kohana::lang('ui_main.coupon_code');?> </a></h4>
											<?php 											
											print form::input('offer_coupon', $form['offer_coupon'], ' class="text long1"'.$form['couponreadonly']); ?>
											
							</div>
							<div class="row">
								<h4><?php echo Kohana::lang('ui_main.description');?> <span><?php echo Kohana::lang('ui_main.include_detail');?>.</span><span class="required">*</span></h4>
								<?php print form::textarea('offerincident_description', $form['offerincident_description'], ' rows="12" cols="40"') ?>
							</div>

							<?php
							// Action::offerreport_form_admin - Runs just after the offerreport description
							Event::run('ushahidi_action.offerreport_form_admin', $id);
							?>

							<?php
                                if (!($id))
                                { // Use default date for new offerreport
                                        ?>
                                        <div class="row" id="startdatetime_default">
                                               <!--  <h4><a href="#" id="startdate_toggle" class="new-cat"><?php //echo Kohana::lang('ui_main.start_date');?></a><?php echo Kohana::lang('ui_main.start_date');?>: 
                                                <?php //echo Kohana::lang('ui_main.today_at').' '.$form['offerincident_start_hour']
                                                        //.":".$form['offerincident_start_minute']." ".$form['offerincident_start_ampm']; ?></h4> -->
                                        </div>
                                        <?php
                                }
                                ?>
                                <div class="row" id="startdatetime_edit">
                                        <div class="date-box">
                                                <h4><?php echo Kohana::lang('ui_main.start_date');?> <span><?php echo Kohana::lang('ui_main.start_date_format');?></span></h4>
                                                <?php print form::input('offerincident_start_date', $form['offerincident_start_date'], ' class="text"'); ?>						
                                                <?php print $start_date_picker_js; ?>				    
                                        </div>
                                        <div class="time">
                                                <h4><?php echo Kohana::lang('ui_main.start_time');?> <span>(<?php echo Kohana::lang('ui_main.approximate');?>)</span></h4>
                                                <?php
                                                print '<span class="sel-holder">' .
                                            form::dropdown('offerincident_start_hour', $hour_array,
                                                $form['offerincident_start_hour']) . '</span>';

                                                print '<span class="dots">:</span>';

                                                print '<span class="sel-holder">' .
                                                form::dropdown('offerincident_start_minute',
                                                $minute_array, $form['offerincident_start_minute']) .
                                                '</span>';
                                                print '<span class="dots">:</span>';

                                                print '<span class="sel-holder">' .
                                                form::dropdown('offerincident_start_ampm', $ampm_array,
                                                $form['offerincident_start_ampm']) . '</span>';
                                                ?>
                                        </div>
                                </div>
                        <?php
                                if (!($id))
                                { // Use default date for new offerreport
                                        ?>
                                   
                                        <?php
                                }
                                ?>
                                <div class="row" id="enddatetime_edit">
                                        <div class="date-box">
                                                <h4><?php echo Kohana::lang('ui_main.end_date');?> <span><?php echo Kohana::lang('ui_main.end_date_format');?></span></h4>
                                                <?php print form::input('offerincident_end_date', $form['offerincident_end_date'], ' class="text"'); ?>								
                                                <?php print $end_date_picker_js; ?>				    
                                        </div>
                                        <div class="time">
                                                <h4><?php echo Kohana::lang('ui_main.end_time');?> <span>(<?php echo Kohana::lang('ui_main.approximate');?>)</span></h4>
                                                <?php
                                                print '<span class="sel-holder">' .
                                            form::dropdown('offerincident_end_hour', $hour_array,
                                                $form['offerincident_end_hour']) . '</span>';

                                                print '<span class="dots">:</span>';

                                                print '<span class="sel-holder">' .
                                                form::dropdown('offerincident_end_minute',
                                                $minute_array, $form['offerincident_end_minute']) .
                                                '</span>';
                                                print '<span class="dots">:</span>';

                                                print '<span class="sel-holder">' .
                                                form::dropdown('offerincident_end_ampm', $ampm_array,
                                                $form['offerincident_end_ampm']) . '</span>';
                                                ?>
                                        </div>
                                </div>
                                <div class="row">
                                <h4><?php echo Kohana::lang('ui_main.offer_type');?></h4>
                                <?php print form::dropdown('offer_type_id', $offer_types, $form['offer_type_id']); ?>
                                </div>
							<div class="row">
								<h4><a href="#" class="tooltip" title="<?php echo Kohana::lang("tooltips.offercategory"); ?>"><?php echo Kohana::lang('ui_main.offercategories');?></a> 
								<span><?php echo Kohana::lang('ui_main.select_multiple');?>.</span></h4>
			                    <div class="offerreport_offercategory">
                        	    <?php
									$selected_offercategories = array();
									if (!empty($form['offerincident_offercategory']) && is_array($form['offerincident_offercategory'])) {
										$selected_offercategories = $form['offerincident_offercategory'];
									}
									$columns = 2;
									echo offercategory::tree($offercategories, $selected_offercategories, 'offerincident_offercategory', $columns);
								?>
             					</div>
							</div>
							
							<div id="custom_forms">
								<?php
								foreach ($disp_custom_fields as $field_id => $field_property)
								{
									echo "<div class=\"row\">";
									echo "<h4>" . $field_property['field_name'] . "</h4>";
									if ($field_property['field_type'] == 1)
									{ // Text Field
										// Is this a date field?
										if ($field_property['field_isdate'] == 1)
										{
											echo form::input('custom_field['.$field_id.']', $form['custom_field'][$field_id],
												' id="custom_field_'.$field_id.'" class="text"');
											echo '<script type="text/javascript">
													$(document).ready(function() {
													$("#custom_field_'.$field_id.'").datepicker({ 
													showOn: "both", 
													buttonImage: "'.url::file_loc('img').'media/img/icon-calendar.gif", 
													buttonImageOnly: true 
													});
													});
												</script>';
										}
										else
										{
											echo form::input('custom_field['.$field_id.']', $form['custom_field'][$field_id],
												' id="custom_field_'.$field_id.'" class="text custom_text"');
										}
									}
									elseif ($field_property['field_type'] == 2)
									{ // TextArea Field
										echo form::textarea('custom_field['.$field_id.']', $form['custom_field'][$field_id], ' class="custom_text" rows="3"');
									}
									echo "</div>";
								}
								?>
							</div>			
						</div>
						<!-- f-col-1 -->
						<div class="f-col-1">
							<h4><a href="#" class="tooltip" title="<?php echo Kohana::lang("tooltips.places"); ?>"><?php echo Kohana::lang('ui_main.places');?><span class="required">*</span> 
								<span><?php echo Kohana::lang('ui_main.select_multiple');?>.</span></a></h4>
							<?php 
							$checked = FALSE;
							$count = 1;
							foreach($incidents as $incident){
								foreach($places as $place){
								$checked = FALSE;
									if($incident->id == $place->incident_id){
										$checked = TRUE;
										break;
									}
									
								}
								if($count!=0 && $count%4==0){
									print('<br />');
									}
								$count++;
								print form::checkbox(array('name' => 'places[]', 'id' => 'places[]'),$incident->id,$checked);
								print form::label($incident->id,$incident->incident_title);
								
							}
							$ischecked = FALSE;
							
							foreach($public_places as $location){
								foreach($places as $place){
								$ischecked = FALSE;
									if($location->id == $place->incident_id){
										$ischecked = TRUE;
										print form::checkbox(array('name' => 'places[]', 'id' => 'places[]'),$location->id,$checked);
										print form::label($location->id,$location->incident_title);
										break;
									}
									
								}
								if($count!=0 && $count%4==0){
									print('<br />');
									}
								$count++;
								
								
							}
							
							?>
							<br>
							<div class="row">

								
						        <input type="search" id="city" name="city" placeholder="Add a Public Place" data-provide="typeahead" class="search-query" />
												 
							   <div id="public_places">
							   </div>
							   
							    <script type="text/javascript">
							     function parseLocation(item, val, text){
				                   var addPlace = document.getElementById('public_places');
				                   var check_place = document.createElement('input');
				                   var label_place = document.createElement('label');
				                   check_place.setAttribute('id','places[]');
				                   check_place.setAttribute('name','places[]');
				                   check_place.setAttribute('value',val);
				                   check_place.setAttribute('type','checkbox')
				                   label_place.setAttribute('for','places')
				                   label_place.appendChild(document.createTextNode(text));
				                   addPlace.appendChild(check_place);
				                   addPlace.appendChild(label_place);

				                }
						              			                
				                var locations = [<?php foreach ($public_places as $place) {echo '{id:"' . $place->id . '", name:"' . $place->incident_title . '"},';} ?>];
				                
				              
						        jQuery(document).ready(function() {
						            
						            $('#city').typeahead({source: locations,val:'id',itemSelected: parseLocation});
						        });
							    </script>
							</div>
							
							
							<!-- News Fields -->
							<div class="row link-row">
								<h4><?php echo Kohana::lang('ui_main.offerreports_news');?></h4>
							</div>
							<div id="divNews">
								<?php
								$this_div = "divNews";
								$this_field = "offerincident_news";
								$this_startid = "news_id";
								$this_field_type = "text";
					
								if (empty($form[$this_field]))
								{
									$i = 1;
									print "<div class=\"row link-row\">";
									print form::input($this_field . '[]', '', ' class="text long"');
									print "<a href=\"#\" class=\"add\" onClick=\"addFormField('$this_div','$this_field','$this_startid','$this_field_type'); return false;\">add</a>";
									print "</div>";
								}
								else
								{
									$i = 0;
									foreach ($form[$this_field] as $value) {									
										print "<div ";
										if ($i != 0) {
											print "class=\"row link-row second\" id=\"" . $this_field . "_" . $i . "\">\n";
										}
										else
										{
											print "class=\"row link-row\" id=\"$i\">\n";
										}
										print form::input($this_field . '[]', $value, ' class="text long"');
										print "<a href=\"#\" class=\"add\" onClick=\"addFormField('$this_div','$this_field','$this_startid','$this_field_type'); return false;\">add</a>";
										if ($i != 0)
										{
											print "<a href=\"#\" class=\"rem\"  onClick='removeFormField(\"#" . $this_field . "_" . $i . "\"); return false;'>remove</a>";
										}
										print "</div>\n";
										$i++;
									}
								}
								print "<input type=\"hidden\" name=\"$this_startid\" value=\"$i\" id=\"$this_startid\">";
								?>
							</div>


							<!-- Video Fields -->
							<div class="row link-row">
								<h4><?php echo Kohana::lang('ui_main.offerreports_video');?></h4>
							</div>
							<div id="divVideo">
								<?php
								$this_div = "divVideo";
								$this_field = "offerincident_video";
								$this_startid = "video_id";
								$this_field_type = "text";
					
								if (empty($form[$this_field]))
								{
									$i = 1;
									print "<div class=\"row link-row\">";
									print form::input($this_field . '[]', '', ' class="text long"');
									print "<a href=\"#\" class=\"add\" onClick=\"addFormField('$this_div','$this_field','$this_startid','$this_field_type'); return false;\">add</a>";
									print "</div>";
								}
								else
								{
									$i = 0;
									foreach ($form[$this_field] as $value) {									
										print "<div ";
										if ($i != 0) {
											print "class=\"row link-row second\" id=\"" . $this_field . "_" . $i . "\">\n";
										}
										else
										{
											print "class=\"row link-row\" id=\"$i\">\n";
										}
										print form::input($this_field . '[]', $value, ' class="text long"');
										print "<a href=\"#\" class=\"add\" onClick=\"addFormField('$this_div','$this_field','$this_startid','$this_field_type'); return false;\">add</a>";
										if ($i != 0)
										{
											print "<a href=\"#\" class=\"rem\"  onClick='removeFormField(\"#" . $this_field . "_" . $i . "\"); return false;'>remove</a>";
										}
										print "</div>\n";
										$i++;
									}
								}
								print "<input type=\"hidden\" name=\"$this_startid\" value=\"$i\" id=\"$this_startid\">";
								?>
							</div>
				
				
							<!-- Photo Fields -->
							<div class="row link-row">
								<h4><?php echo Kohana::lang('ui_main.offerreports_photos');?></h4>
								<?php								
    								if ($offerincident != "0")
                        			{
                        				// Retrieve Media
                        				foreach($offerincident->media as $photo) 
                        				{
                        					if ($photo->media_type == 1)
                        					{
                        						print "<div class=\"offerreport_thumbs\" id=\"photo_". $photo->id ."\">";

                        						$thumb = $photo->media_thumb;
                        						$photo_link = $photo->media_link;
												$prefix = url::base().Kohana::config('upload.relative_directory');
                        						print "<a class='photothumb' rel='lightbox-group1' href='$prefix/$photo_link'>";
                        						print "<img src=\"$prefix/$thumb\" >";
                        						print "</a>";

                        						print "&nbsp;&nbsp;<a href=\"#\" onClick=\"deletePhoto('".$photo->id."', 'photo_".$photo->id."'); return false;\" >".Kohana::lang('ui_main.delete')."</a>";
                        						print "</div>";
                        					}
                        				}
                        			}
			                    ?>
							</div>
							<div id="divPhoto">
								<?php
								$this_div = "divPhoto";
								$this_field = "offerincident_photo";
								$this_startid = "photo_id";
								$this_field_type = "file";
					
								if (empty($form[$this_field]['name'][0]))
								{
									$i = 1;
									print "<div class=\"row link-row\">";
									print form::upload($this_field . '[]', '', ' class="text long"');
									print "<a href=\"#\" class=\"add\" onClick=\"addFormField('$this_div','$this_field','$this_startid','$this_field_type'); return false;\">add</a>";
									print "</div>";
								}
								else
								{
									$i = 0;
									foreach ($form[$this_field]['name'] as $value) 
									{
										print "<div ";
										if ($i != 0) {
											print "class=\"row link-row second\" id=\"" . $this_field . "_" . $i . "\">\n";
										}
										else
										{
											print "class=\"row link-row\" id=\"$i\">\n";
										}
										// print "\"<strong>" . $value . "</strong>\"" . "<BR />";
										print form::upload($this_field . '[]', $value, ' class="text long"');
										print "<a href=\"#\" class=\"add\" onClick=\"addFormField('$this_div','$this_field','$this_startid','$this_field_type'); return false;\">add</a>";
										if ($i != 0)
										{
											print "<a href=\"#\" class=\"rem\"  onClick='removeFormField(\"#".$this_field."_".$i."\"); return false;'>remove</a>";
										}
										print "</div>\n";
										$i++;
									}
								}
								print "<input type=\"hidden\" name=\"$this_startid\" value=\"$i\" id=\"$this_startid\">";
								?>
							</div>
						</div>
						<!-- f-col-bottom -->
						<div class="f-col-bottom-container">
							<div class="f-col-bottom">
								<div class="row">
									<h4><?php echo Kohana::lang('ui_main.personal_information');?></span></h4>
									<label>
										<span><?php echo Kohana::lang('ui_main.first_name');?></span>
										<?php print form::input('person_first', $form['person_first'], ' class="text"'); ?>
									</label>
									<label>
										<span><?php echo Kohana::lang('ui_main.last_name');?></span>
										<?php print form::input('person_last', $form['person_last'], ' class="text"'); ?>
									</label>
								</div>
								<div class="row">
									<label>
										<span><?php echo Kohana::lang('ui_main.email_address');?></span>
										<?php print form::input('person_email', $form['person_email'], ' class="text"'); ?>
									</label>
								</div>
							</div>
							
							
							<div style="clear:both;"></div>
						</div>
						<div class="btns">
							<ul>
								<li><a href="#" class="btn_save"><?php echo strtoupper(Kohana::lang('ui_main.save_offerreport'));?></a></li>
								<li><a href="#" class="btn_save_close"><?php echo strtoupper(Kohana::lang('ui_main.save_close'));?></a></li>
								<?php if ($id): ?>
									<li><a href="#" class="btn_delete btns_red"><?php echo strtoupper(Kohana::lang('ui_main.delete_offerreport')); ?></a></li>
								<?php endif; ?>
								<li>
									<a href="<?php echo url::site().'retailers/offerreports/';?>" class="btns_red">
										<?php echo strtoupper(Kohana::lang('ui_main.cancel'));?>
									</a>
								</li>
							</ul>
						</div>						
					</div>
				<?php print form::close(); ?>
				<?php
				if($id)
				{
					// Hidden Form to Perform the Delete function
					print form::open(url::site().'retailers/offerreports/', array('id' => 'offerreportMain', 'name' => 'offerreportMain'));
					$array=array('action'=>'d','offerincident_id[]'=>$id);
					print form::hidden($array);
					print form::close();
				}
				?>
			</div>
