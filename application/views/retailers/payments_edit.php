<?php
/**
 * Edit User
 *
 * PHP version 5
 * LICENSE: This source file is subject to LGPL license
 * that is available through the world-wide-web at the following URI:
 * http://www.gnu.org/copyleft/lesser.html
 * @author     Ushahidi Team <team@ushahidi.com>
 * @package    Ushahidi - http://source.ushahididev.com
 * @module     Edit User View
 * @copyright  Ushahidi - http://www.ushahidi.com
 * @license    http://www.gnu.org/copyleft/lesser.html GNU Lesser General Public License (LGPL)
 */
?>
			<div class="bg">
				<h2>
					<?php retailers::orders_subtabs("view",$order_details->id); ?>
				</h2>
				<?php
				if ($form_error) {
				?>
					<!-- red-box -->
					<div class="red-box">
						<h3><?php echo Kohana::lang('ui_main.error');?></h3>
						<ul>
						<?php
						foreach ($errors as $error_item => $error_description)
						{
							print (!$error_description) ? '' : "<li>" . $error_description . "</li>";
						}
						?>
						</ul>
					</div>
				<?php
				}

				if ($form_saved) {
				?>
					<!-- green-box -->
					<div class="green-box">
						<h3><?php echo Kohana::lang('ui_main.order_completed');?></h3>
					</div>
				<?php
				}
				?>
				<?php print form::open(); ?>
				<div class="report-form">
					<div class="head">
						<a href="#" onClick="checkinAction('c', 'COMPLETE', '')"><?php echo strtoupper(Kohana::lang('ui_main.complete'));?></a>
					</div>
					<!-- column -->
					<div class="sms_holder">

						
							<div class="row">
								<h4><?php echo Kohana::lang('ui_main.full_name');?>:</h4><?php echo $order_details->name; ?>
							</div>

							<div class="row">
								<h4><?php echo Kohana::lang('ui_main.email');?></h4> <?php echo $order_details->email; ?>
							</div>
						
							<div class="row">
								<h4><?php echo Kohana::lang('ui_main.order_title');?> </h4><?php echo $order_details->offerincident_title; ?>
							</div>
							<div class="row">
								<h4><?php echo Kohana::lang('ui_main.purchase_quantity');?> </h4><?php echo $order_details->quantity; ?>
							</div>
							<div class="row">
								<h4><?php echo Kohana::lang('ui_main.total_price');?> </h4><?php echo $order_details->amount; ?>
							</div>
							<div class="row">
								<h4><?php echo Kohana::lang('ui_main.purchase_date');?> </h4><?php echo $order_details->date; ?>
							</div>
							<div class="row">
								<h4><?php echo Kohana::lang('ui_main.retailer_coupon');?> </h4><?php echo $order_details->retailer_coupon; ?>	
							</div>
										
							<div class="row">
								<h4><a href="#" class="tooltip" title="<?php echo Kohana::lang("tooltips.customer_coupon"); ?>"><?php echo Kohana::lang('ui_main.customer_coupon');?></a> <span class="required"><?php echo Kohana::lang('ui_main.required'); ?></span></h4><?php print form::input('customer_coupon', $form['customer_coupon'], ' class="text short3"'); ?>
								
							</div>
						

                        <?php
                        // users_form_admin - add content to users from
                        Event::run('ushahidi_action.users_form_admin', $id);
                        ?>
					</div>

					<div class="simple_border"></div>

					<a href="#" onClick="checkinAction('c', 'COMPLETE', '')"><?php echo strtoupper(Kohana::lang('ui_main.complete'));?></a>
				</div>
				<?php print form::close(); ?>
			</div>
