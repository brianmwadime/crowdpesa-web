<?php 
/**
 * Checkins view page.
 *
 * PHP version 5
 * LICENSE: This source file is subject to LGPL license 
 * that is available through the world-wide-web at the following URI:
 * http://www.gnu.org/copyleft/lesser.html
 * @author	   Ushahidi Team <team@ushahidi.com> 
 * @package	   Ushahidi - http://source.ushahididev.com
 * @module	   Checkins View
 * @copyright  Ushahidi - http://www.ushahidi.com
 * @license	   http://www.gnu.org/copyleft/lesser.html GNU Lesser General Public License (LGPL) 
 */
?>
			<div class="bg">
				<h2>
					<?php retailers::payments_subtabs("history"); ?>
				</h2>

				<!-- tabs -->
				<div class="tabs">
			
					</div>
				</div>
			
				<!-- report-table -->
				<?php print form::open(NULL, array('id' => 'transactionMain', 'name' => 'transactionMain')); ?>
					<input type="hidden" name="action" id="action" value="">
					<input type="hidden" name="level"  id="level"  value="">
					<input type="hidden" name="id[]" id="checkin_single" value="">
					<div class="table-holder">
						<table class="table">
							<thead>
								<tr>
									<th class="col-1"><input id="checkall" type="checkbox" class="check-box" onclick="CheckAll( this.id, 'id[]' )" /></th>
									<th class="col-2"><?php echo Kohana::lang('ui_admin.transaction_details');?></th>
									<th class="col-3"><?php echo Kohana::lang('ui_main.date');?></th>
									<th class="col-4"><?php echo Kohana::lang('ui_main.actions');?></th>
								</tr>
							</thead>
							<tfoot>
								<tr class="foot">
									<td colspan="4">
										<?php echo $pagination; ?>
									</td>
								</tr>
							</tfoot>
							<tbody>
								<?php
								if ($total_items == 0)
								{
								?>
									<tr>
										<td colspan="4" class="col">
											<h3><?php echo Kohana::lang('ui_main.no_results');?></h3>
										</td>
									</tr>
								<?php	
								}
								foreach ($transactions as $transaction)
								{
									$transaction_id = $transaction->id;
									$customer_name = $transaction->cust_name;
									$offer = $transaction->title;
									$amount = $transaction->quantity;
									$price = $transaction->amount;
									$retailer_coupon = $transaction->retailer_coupon;
									$description = $transaction->description;
									$preview = text::limit_chars(strip_tags($description), 150, "...", true);	
									$transaction_date = date('Y-m-d h:i', strtotime($transaction->date));
									
									?>
									<tr>
										<td class="col-1"><input name="transaction_id[]" id="checkin" value="<?php echo $transaction_id; ?>" type="checkbox" class="check-box"/></td>
										<td class="col-2">
											<div class="post">
												<h4><a href="<?php echo url::site() . 'retailers/history/view/' . $transaction_id; ?>" class="more"><?php echo $customer_name; ?></a></h4>
												<p><?php echo $preview; ?>... <a href="<?php echo url::base() . 'retailers/history/view/' . $transaction_id; ?>" class="more"><?php echo Kohana::lang('ui_main.more');?></a></p>
											</div>
											<ul class="info">
											<li class="none-separator"><?php echo Kohana::lang('ui_main.offer_title');?>: <strong><?php echo $offer; ?></strong>,<strong></li>
												<li class="none-separator"><?php echo Kohana::lang('ui_main.purchase_quantity');?>: <strong><?php echo $amount; ?></strong>,<strong></li>

												<li><?php echo Kohana::lang('ui_main.purchase_price');?> <strong><?php echo $price; ?></li>
											</ul>
											<ul class="info">
											
												<li class="none-separator"><?php echo Kohana::lang('ui_main.retailer_coupon');?>: <strong><?php echo $retailer_coupon; ?></strong>,<strong></li>
												
											</ul>
												
											</div>
											
										</td>
										<td class="col-3"><?php echo $transaction_date; ?></td>
										<td class="col-4">
											<ul>
												<?php
												if ((int) $transaction_id)
												{
													echo "<li class=\"none-separator\"><a href=\"". url::base() . 'retailers/history/view/' . $transaction_id ."\" class=\"status_yes\"><strong>".Kohana::lang('ui_admin.view_transaction')."</strong></a></li>";
												}
												?>
											</ul>
										</td>
									</tr>
									<?php
								}
								?>
							</tbody>
						</table>
					</div>
				<?php print form::close(); ?>
				
				
			</div>