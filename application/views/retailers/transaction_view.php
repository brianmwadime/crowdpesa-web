<?php
/**
 * Edit User
 *
 * PHP version 5
 * LICENSE: This source file is subject to LGPL license
 * that is available through the world-wide-web at the following URI:
 * http://www.gnu.org/copyleft/lesser.html
 * @author     Ushahidi Team <team@ushahidi.com>
 * @package    Ushahidi - http://source.ushahididev.com
 * @module     Edit User View
 * @copyright  Ushahidi - http://www.ushahidi.com
 * @license    http://www.gnu.org/copyleft/lesser.html GNU Lesser General Public License (LGPL)
 */
?>
			<div class="bg">
				<h2>
					<?php retailers::transaction_subtabs("view",$transaction->id); ?>
				</h2>
				<?php
				if ($form_error) {
				?>
					<!-- red-box -->
					<div class="red-box">
						<h3><?php echo Kohana::lang('ui_main.error');?></h3>
						<ul>
						<?php
						foreach ($errors as $error_item => $error_description)
						{
							print (!$error_description) ? '' : "<li>" . $error_description . "</li>";
						}
						?>
						</ul>
					</div>
				<?php
				}

				if ($form_saved) {
				?>
					<!-- green-box -->
					<div class="green-box">
						<h3><?php echo Kohana::lang('ui_main.order_completed');?></h3>
					</div>
				<?php
				}
				?>
				<?php print form::open(); ?>
				<div class="report-form">
					<div class="head">
						
					</div>
					<!-- column -->
					<div class="sms_holder">

						
							<div class="row">
								<h4><?php echo Kohana::lang('ui_main.customer_name');?>:</h4><?php echo $transaction->customer_name; ?>
							</div>

							<div class="row">
								<h4><?php echo Kohana::lang('ui_main.email');?></h4> <?php echo $transaction->email; ?>
							</div>
						
							<div class="row">
								<h4><?php echo Kohana::lang('ui_main.order_title');?> </h4><?php echo $transaction->title; ?>
							</div>
							<div class="row">
								<h4><?php echo Kohana::lang('ui_main.purchase_quantity');?> </h4><?php echo $transaction->quantity; ?>
							</div>
							<div class="row">
								<h4><?php echo Kohana::lang('ui_main.total_price');?> </h4><?php echo $transaction->amount; ?>
							</div>
							<div class="row">
								<h4><?php echo Kohana::lang('ui_main.purchase_date');?> </h4><?php echo $transaction->date; ?>
							</div>
							<div class="row">
								<h4><?php echo Kohana::lang('ui_main.retailer_coupon');?> </h4><?php echo $transaction->retailer_coupon; ?>	
							</div>
										
							
						

                        <?php
                        // users_form_admin - add content to users from
                        Event::run('ushahidi_action.users_form_admin', $id);
                        ?>
					</div>

					<div class="simple_border"></div>

					
				</div>
				<?php print form::close(); ?>
			</div>
