<?php if (isset($center) && is_array($center) && isset($markers) && is_array($markers) && count($markers) > 0) : ?>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
        <title>CrowdPesa.com - What's Hot Near You?</title>
        <style type="text/css">
            #map-container {border: 1px solid #999; width: <?php echo $size[0] ?>px}
            .nearest-listings, #map-container {float:left}
            ul{padding:0}
            .sitesListing {
                margin: 4px 10px;
                padding: 0;
                font-family:helvetica, verdana;
            }
            ul.sitesListing li{
                list-style-type:none;
                background:none;
            }
            ul.sitesListing li h5{
                font-size:16px;
                border-bottom: 1px solid #ccc;
                margin:3px 0 7px 0; 
                padding: 0 0 5px 0;
            }
            ul.sitesListing li li{
                font-size:13px;
                background:none;
                list-style-type:none;
                padding:0 0 4px 0;
                margin:0;
            }       
            ul.sitesListing li li .address{
                list-style:none;
                font-size:13px;
                color:#666;
                text-decoration:none;
            }
        </style>
        <script src="http://maps.google.com/maps/api/js?sensor=false" type="text/javascript"></script>
        <script type="text/javascript" src="http://google-maps-utility-library-v3.googlecode.com/svn/trunk/markerclusterer/src/markerclusterer_compiled.js"></script>
    </head>
    <body>
        <div id="map-container">
            <div id="map" style="width: <?php echo ($size[0]-10) ?>px; height: <?php echo $size[1] ?>px; border: 5px solid #fff"></div>
        </div>
        <div class="nearest-listings">
            <ul class="sitesListing">
                <li>
                    <h5>Nearest Outlets</h5>
                    <ul>
                        <?php foreach($markers as $name => $props): ?>
                        <li><span><?php echo $name ?></span>
                            <div class="address"><?php echo $props[1] ?></div>    
                        </li>
                        <?php endforeach; ?>
                    </ul>
                </li>
            </ul>
        </div>

        <script type="text/javascript">
            var locations = [
            <?php $counter = 1; ?>
            <?php foreach($markers as $name => $props): ?>
                ['<?php echo $name ?>', <?php echo $props[0] ?>]<?php echo ($counter < count($markers)) ? ',' : '' ?>
                <?php $counter++; ?>
            <?php endforeach; ?>
            ];

            var map = new google.maps.Map(document.getElementById('map'), {
                zoom : 10,
                center : new google.maps.LatLng(<?php echo $center[0] ?>, <?php echo $center[1] ?>),
                mapTypeId : google.maps.MapTypeId.ROADMAP
            });

            var infowindow = new google.maps.InfoWindow();
            var marker, i;
            var latlngbounds = new google.maps.LatLngBounds();
            var markers = [];

            for ( i = 0; i < locations.length; i++) {
                marker = new google.maps.Marker({
                    position : new google.maps.LatLng(locations[i][1], locations[i][2]),
                    map : map
                });
                markers.push(marker);
                latlngbounds.extend(marker.position);

                google.maps.event.addListener(marker, 'click', (function(marker, i) {
                    return function() {
                        infowindow.setContent(locations[i][0]);
                        infowindow.open(map, marker);
                    }
                })(marker, i));
            }
            var markerCluster = new MarkerClusterer(map, markers);
            map.fitBounds(latlngbounds);
        </script>
    </body>
</html>
<?php endif; ?>