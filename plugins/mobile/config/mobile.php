<?php defined('SYSPATH') or die('No direct script access.');

/**
* MOBILE CONFIGURATION
*/

/**
 * Set single marker radius
 * Values from 1 to 10
 * Default: 4
 */
$config['items_per_page'] = "4";
$config['show_maps'] = TRUE;
$config['allow_checkins'] = TRUE;