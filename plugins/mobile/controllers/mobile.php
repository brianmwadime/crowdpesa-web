<?php defined('SYSPATH') or die('No direct script access.');
/**
 * Mobile Controller
 *
 * PHP version 5
 * LICENSE: This source file is subject to LGPL license 
 * that is available through the world-wide-web at the following URI:
 * http://www.gnu.org/copyleft/lesser.html
 * @author     BeeBuy Team <info@beebuy.com> 
 * @package    CrowdPesa - http://crowdpesa.com
 * @module     Offerreports Controller
 * @copyright  CrowdPesa - http://crowdpesa.com
 * @license    http://www.gnu.org/copyleft/lesser.html GNU Lesser General Public License (LGPL) 
 */

class Mobile_Controller extends Template_Controller {
	
	public $auto_render = TRUE;
	public $mobile = TRUE;
	
	// Cacheable Controller
	public $is_cachable = TRUE;
	
	// Main template
    public $template = 'mobile/layout';

	// Table Prefix
	protected $table_prefix;

    public function __construct()
    {
		parent::__construct();
		$this->session = Session::instance();
		$this->db = new Database();
		// Set Table Prefix
		$this->table_prefix = Kohana::config('database.default.table_prefix');
		
		// Load Header & Footer
		$this->template->header  = new View('mobile/header');
		$this->template->footer  = new View('mobile/footer');

		$this->template->header->site_name = Kohana::config('settings.site_name');
		$this->template->header->site_tagline = Kohana::config('settings.site_tagline');						
		$this->template->header->show_map = TRUE;
		$this->template->header->js = "";
		$this->template->header->breadcrumbs = "";
		
		// Google Analytics
		$google_analytics = Kohana::config('settings.google_analytics');
		$this->template->footer->google_analytics = $this->_google_analytics($google_analytics);
				
		if (isset($_GET['lat']) && isset($_GET['lon']) && (!empty($_GET['lat'])) && (!empty($_GET['lon']))){
			$this->session->set('user_map_center', 2);
			$this->session->set('map_center_lat', $_GET['lat']);
			$this->session->set('map_center_lng', $_GET['lon']);
		}elseif($this->session->get('user_map_center') != 1 && $this->session->get('user_map_center') != 2){
		    $this->session->set('user_map_center', 0);
		    $this->session->set('map_center_lat', Kohana::config('settings.default_lat'));
		    $this->session->set('map_center_lng', Kohana::config('settings.default_lon'));
		    $this->session->set('map_zoom', Kohana::config('settings.default_zoom'));
		}
		$this->session->set('map_radius', 5);
    	}
	
	public function index()
	{
		$this->template->content  = new View('mobile/main');
		
		// Get Most Recent offers
		$max_radius = $this->session->get('map_radius'); // in km
		//$max_radius = 5.015;
		$lat = $this->session->get('map_center_lat');
		$lng = $this->session->get('map_center_lng');
		$this->template->content->incidents = $this->db->query("SELECT DISTINCT incident.* ,latitude, longitude,
			( 6371 * acos( cos( radians( $lat ) ) * cos( radians( latitude ) ) * cos( radians( longitude ) - radians( $lng ) ) + sin( radians( $lat ) ) * sin( radians( latitude ) ) ) ) AS distance
			FROM `incident` INNER JOIN location ON incident.location_id = location.id WHERE longitude BETWEEN ( $lng -20 / abs( cos( radians( $lat ) ) *69 ) ) AND ( $lng +20 / abs( cos( radians( $lat ) ) *69 ) )
			AND latitude BETWEEN ( $lat - ( $max_radius / 69 ) ) AND ( $lat + ( $max_radius / 69 ) ) AND incident_active = 1
			HAVING distance < $max_radius ORDER BY distance LIMIT 10");
		
		// Get all active top level categories
        $parent_categories = array();
        foreach (ORM::factory('category')
				->where('category_visible', '1')
				->where('parent_id', '0')
				->orderby('category_title')
				->find_all() as $category)
        {
            // Get The Children
			$children = array();
			foreach ($category->children as $child)
			{
				$children[$child->id] = array(
					$child->category_title,
					$child->category_color,
					$child->category_image,
					$this->_category_count($child->id)
				);
			}

			// Put it all together
            $parent_categories[$category->id] = array(
				$category->category_title,
				$category->category_color,
				$category->category_image,
				$this->_category_count($category->id),
				$children
			);
			
			if ($category->category_trusted)
			{ // Get Trusted Category Count
				$trusted = ORM::factory("incident")
					->join("incident_category","incident.id","incident_category.incident_id")
					->where("category_id",$category->id);
				if ( ! $trusted->count_all())
				{
					unset($parent_categories[$category->id]);
				}
			}
        }
        $this->template->content->categories = $parent_categories;

		// Get RSS News Feeds
		$this->template->content->feeds = ORM::factory('feed_item')
			->limit('10')
            ->orderby('item_date', 'desc')
            ->find_all();
	}
	
	private function _category_count($category_id = false)
	{
		if ($category_id)
		{
			return ORM::factory('incident_category')
				->join('incident', 'incident_category.incident_id', 'incident.id')
				->where('category_id', $category_id)
				->where('incident_active', '1')
				->count_all();			
		}
		else
		{
			return 0;
		}
	}
	
	/*
	* Google Analytics
	* @param text mixed  Input google analytics web property ID.
    * @return mixed  Return google analytics HTML code.
	*/
	private function _google_analytics($google_analytics = false)
	{
		$html = "";
		if (!empty($google_analytics)) {
			$html = "<script type=\"text/javascript\">
				var gaJsHost = ((\"https:\" == document.location.protocol) ? \"https://ssl.\" : \"http://www.\");
				document.write(unescape(\"%3Cscript src='\" + gaJsHost + \"google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E\"));
				</script>
				<script type=\"text/javascript\">
				var pageTracker = _gat._getTracker(\"" . $google_analytics . "\");
				pageTracker._trackPageview();
				</script>";
		}
		return $html;
	}	
}