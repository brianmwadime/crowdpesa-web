<?php
defined('SYSPATH') or die('No direct script access.');
/**
 * PHP version 5
 * LICENSE: This source file is subject to LGPL license 
 * that is available through the world-wide-web at the following URI:
 * http://www.gnu.org/copyleft/lesser.html
 * @author     BeeBuy Team <info@beebuy.com> 
 * @package    CrowdPesa - http://crowdpesa.com
 * @module     Offerreports Controller
 * @copyright  CrowdPesa - http://crowdpesa.com
 * @license    http://www.gnu.org/copyleft/lesser.html GNU Lesser General Public License (LGPL) 
 */

class Accounts_Controller extends Mobile_Controller {
	
	protected $user;
	
	public function __construct()
	{
		parent::__construct();
		
		$this->auth = new Auth();
		$this->session = Session::instance();
		$this->auth->auto_login();
		
		if ( !$this->auth->logged_in())
		{
			url::redirect('mobileci/login');
				
		}
	}

	public function index($user_id = False)
	{
		
		if($user_id){
		$checkins_total = ORM::factory('checkin')->where('user_id',$user_id)->count_all();
		$this->user = ORM::factory('user', $user_id)->where('public_profile',1);
		}else{
		$id = $_SESSION['auth_user']->id;
		$this->user = ORM::factory('user', $id);
		$checkins_total = ORM::factory('checkin')->where('user_id',$id)->count_all();
		}
		
		$this->template->content = new View('mobile/accounts');
		$this->template->content->citotal = $checkins_total;
		$this->template->content->user = $this->user;
		
	}
	
	
	
	
	
	
	
}