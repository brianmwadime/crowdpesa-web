<?php
defined('SYSPATH') or die('No direct script access.');
/**
 * PHP version 5
 * LICENSE: This source file is subject to LGPL license 
 * that is available through the world-wide-web at the following URI:
 * http://www.gnu.org/copyleft/lesser.html
 * @author     BeeBuy Team <info@beebuy.com> 
 * @package    CrowdPesa - http://crowdpesa.com
 * @module     Offerreports Controller
 * @copyright  CrowdPesa - http://crowdpesa.com
 * @license    http://www.gnu.org/copyleft/lesser.html GNU Lesser General Public License (LGPL) 
 */

class Checkins_Controller extends Mobile_Controller {
	
	public function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
		$this->template->content = new View('mobile/checkins');
		$max_radius = $this->session->get('map_radius'); // in km
		//$max_radius = 0.015;
		$lat = $this->session->get('map_center_lat');
		$lng = $this->session->get('map_center_lng');
		
		if(isset($_POST) AND !empty($_POST) ){
		 $cpage = $_POST["paged"];
		}else{
		 $cpage = 1;
		}
		
		$db= New Database();
		
		$total_items = $db->query("SELECT checkin.* , ( 6371 * acos( cos( radians( $lat ) ) * cos( radians( latitude ) ) * cos( radians( longitude ) - radians( $lng ) ) + sin( radians( $lat ) ) * sin( radians( latitude ) ) ) ) AS distance
							FROM `checkin`
							INNER JOIN location ON checkin.location_id = location.id
							INNER JOIN users ON checkin.user_id = users.id
							WHERE longitude BETWEEN ( $lng -20 / abs( cos( radians( $lat ) ) *69 ) ) AND ( $lng +20 / abs( cos( radians( $lat ) ) *69 ) )
							AND latitude BETWEEN ( $lat - ( $max_radius / 69 ) ) AND ( $lat + ( $max_radius / 69 ) )
							HAVING distance < $max_radius")->count();
						
		$total_pages = (int) ceil( $total_items / (int) Kohana::config('mobile.items_per_page'));
		
		$current_page = ( $cpage > 1 ) ? $cpage : 1;
		
		$limit = (int) Kohana::config('mobile.items_per_page');
		
		$offset = (int)($current_page - 1 ) * (int) Kohana::config('mobile.items_per_page');
		
		// pull nearest checkins within max_radius ordered by distance
		$query = "SELECT DISTINCT checkin.id, users.name, users.username as user_username, users.color as user_color, checkin.user_id, checkin_description, checkin.location_id, checkin_date, location_name, latitude, longitude,
			( 6371 * acos( cos( radians( $lat ) ) * cos( radians( latitude ) ) * cos( radians( longitude ) - radians( $lng ) ) + sin( radians( $lat ) ) * sin( radians( latitude ) ) ) ) AS distance
			FROM `checkin`
			INNER JOIN location ON checkin.location_id = location.id
			INNER JOIN users ON checkin.user_id = users.id
			WHERE longitude BETWEEN ( $lng -20 / abs( cos( radians( $lat ) ) *69 ) ) AND ( $lng +20 / abs( cos( radians( $lat ) ) *69 ) )
			AND latitude BETWEEN ( $lat - ( $max_radius / 69 ) ) AND ( $lat + ( $max_radius / 69 ) )
			HAVING distance < $max_radius ORDER BY checkin.checkin_date DESC LIMIT $offset , $limit";
		
		$checkins = $db->query($query);           
		$nextpage = min( $total_pages, $current_page+1 );
		$jsvars = array(
			'startPage' => $current_page,
			'maxPages' => $total_pages,
			'nextLink' => url::site()."mobile/checkins/?paged=".$nextpage
			);
		//change php array into js array and inject into header of checkins view
		$jsvars = "var checkin = ".json_encode($jsvars).";";
		
		$this->template->header->js = $jsvars;
		$this->template->content->checkins = $checkins;
    }
    
    /**
    * Displays a checkin.
    * @param boolean $id If id is supplied, a checkin with that id will be
    * retrieved.
    */
    public function view($id = false)
    {
    	$this->template->header->show_map = Kohana::config('mobile.show_maps');
    	$this->template->header->js = new View('mobile/checkins_view_js');
    	$this->template->content = new View('mobile/checkins_view');
    
    	if ( ! $id )
    	{
    		url::redirect('mobile');
    	}
    	else
    	{
		$db = new Database;
    		$checkin = ORM::factory('checkin', $id);
    		if ( !$checkin)
    		{
    			url::redirect('mobile/checkins');
    		}
    			
    		$this->template->content->checkin = $checkin;
    		$this->template->header->js->latitude = $checkin->location->latitude;
    		$this->template->header->js->longitude = $checkin->location->longitude;
    			
    	}
    }
    
}