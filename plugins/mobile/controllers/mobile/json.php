<?php
defined('SYSPATH') or die('No direct script access.');
/**
 * PHP version 5
 * LICENSE: This source file is subject to LGPL license 
 * that is available through the world-wide-web at the following URI:
 * http://www.gnu.org/copyleft/lesser.html
 * @author     BeeBuy Team <info@beebuy.com> 
 * @package    CrowdPesa - http://crowdpesa.com
 * @module     Offerreports Controller
 * @copyright  CrowdPesa - http://crowdpesa.com
 * @license    http://www.gnu.org/copyleft/lesser.html GNU Lesser General Public License (LGPL) 
 */

class Json_Controller extends Template_Controller {
	
	public $template = 'mobile/json';
	
	public function __construct()
	{
		parent::__construct();
		//header('Content-type: application/json; charset=utf-8');
		$this->table_prefix = Kohana::config('database.default.table_prefix');
		
		
	}

	public function index($currentcount = 0)
	{
		$this->template = new View("mobile/json");
		
		$db= New Database();
		
		$newcount = $db->query("SELECT DISTINCT c.* FROM `".$this->table_prefix."checkin` AS c JOIN `".$this->table_prefix."users` AS u ON (c.`user_id` = u.`id`)")->count();
		//echo $newcount;
		$diff =  $newcount - $currentcount;
		//echo $diff; exit;
		if( $diff > 0 ){
			$checkins = $db->query("SELECT DISTINCT c.*, u.* FROM `".$this->table_prefix."checkin` AS c JOIN `".$this->table_prefix."users` AS u ON (c.`user_id` = u.`id`)ORDER BY c.checkin_date DESC LIMIT ".$diff." OFFSET 0");
			
		// Get 10 Most Recent checkins
		$results = array();
		foreach($checkins as $checkin){
		
			$results[] = $checkin;
		}
		
		}else{
			
			$results ="";	
		}
		//exit;
		//var_dump(json_encode($results));
		
		header('Content-type: application/json');
		$this->template->results = json_encode($results);
	
    }
    
}