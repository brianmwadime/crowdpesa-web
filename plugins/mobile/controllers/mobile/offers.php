<?php
defined('SYSPATH') or die('No direct script access.');
/**
 * PHP version 5
 * LICENSE: This source file is subject to LGPL license 
 * that is available through the world-wide-web at the following URI:
 * http://www.gnu.org/copyleft/lesser.html
 * @author     BeeBuy Team <info@beebuy.com> 
 * @package    CrowdPesa - http://crowdpesa.com
 * @module     Offerreports Controller
 * @copyright  CrowdPesa - http://crowdpesa.com
 * @license    http://www.gnu.org/copyleft/lesser.html GNU Lesser General Public License (LGPL) 
 */

class Offers_Controller extends Mobile_Controller {
	
	public function __construct()
	{
		parent::__construct();
		
	}

	public function index($id = False)
	{
		$this->template->content = new View('mobile/offers');
		
		$db = new Database;
		
		// Pagination
		$pagination = new Pagination(array(
						'style' => 'mobile',
						'query_string' => 'page',
						'items_per_page' => (int) Kohana::config('mobile.items_per_page'),
						'total_items' => $db->query("SELECT DISTINCT o.* FROM `".$this->table_prefix."offerincident` AS o JOIN `".$this->table_prefix."location` AS l ON (o.`location_id` = l.`id`)")->count()
		));
		$this->template->content->pagination = $pagination;
		$offers = $db->query("SELECT DISTINCT o.*, l.location_name FROM `".$this->table_prefix."offerincident` AS o JOIN `".$this->table_prefix."location` AS l ON (o.`location_id` = l.`id`) ORDER BY o.offerincident_date DESC LIMIT ". (int) Kohana::config('mobile.items_per_page') . " OFFSET ".$pagination->sql_offset);
		$this->template->content->offers = $offers;
		
    }
    
   /**
    * Displays an offer.
    * @param boolean $id If id is supplied, a report with that id will be
    * retrieved.
    */
    public function view($id = false)
    {
    	$this->template->header->show_map = Kohana::config('mobile.show_maps');
    	$this->template->header->js = new View('mobile/offers_view_js');
    	$this->template->content = new View('mobile/offers_view');
	
	$customer = Auth::instance()->get_user();
        $customer_id = 0;
        if ($customer){
            $customer_id = $customer->id;
        }

    	if ( !$id )
    	{
    		url::redirect('mobile/offers');
    	}
    	else
    	{
    		$db = new Database;
		$query ="SELECT offerincident.*, location.location_name, location.latitude, location.longitude, offerincident_description as description FROM offerincident  
			INNER JOIN offer_incidents oi ON oi.offerincident_id = offerincident.id
			INNER JOIN incident ON incident.id = oi.incident_id 
			INNER JOIN location ON incident.location_id = location.id WHERE offerincident.`id` =".$id."";
		
    		$offer = $db->query($query);
    		if ( !count($offer))
    		{
    			url::redirect('mobile');
    		}
            //var_dump($offer[0]);exit;
    		
		$average_rating = $db->query("SELECT AVG(rating) 'ave' FROM rating WHERE offerincident_id = $id");	
    		
		$order = Database::instance()->query("SELECT customer_token FROM orders WHERE offerincident_id = $id AND customer_id = $customer_id");
		if (count($order))
                Session::instance()->set('offer_' . $offer[0]->id . '_token', $order[0]->customer_token);
		$total_reviews = ($this->_get_reviews($offer[0]->id,"offerincident_id")) ? $this->_get_reviews($offer[0]->id,"offerincident_id") : 0 ;
    		$this->template->content->total_reviews = $total_reviews;
		$this->template->content->offer = $offer;
		$this->template->content->offer_thumb = ($this->_get_thumb($offer[0]->id)) ? $this->_get_thumb($offer[0]->id) : "blank.gif" ;
    		$this->template->content->offer_rating = round($average_rating[0]->ave);
    		$this->template->header->js->latitude = $offer[0]->latitude;
    		$this->template->header->js->longitude = $offer[0]->longitude;
    	}
    }
    
    private function _generate_customer_token () {
        $chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";  
        $length = 5;
        $str = substr(str_shuffle($chars), 0, $length);
        return $str;
    }
    
    public function mcoupon($incident_id){
        header("Content-type: image/png");
        //$orange = imagecolorallocate($im, 220, 210, 60);
        $offer = ORM::factory('offerincident', $incident_id);
        if ($offer->loaded){
            $im     = imagecreatefrompng(MEDIAPATH."coupon_bg.png");
            $white = imagecolorallocate($im, 0xFF, 0xFF, 0xFF);
            $px     = (imagesx($im) - 7.5 * strlen($offer->offer_coupon)) / 2;
            $font = imageloadfont(MEDIAPATH.'trebuchetms.gdf');
            imagestring($im, $font, $px-14, 6, $offer->offer_coupon, $white);
	    
		imagepng($im);
	  	imagedestroy($im);
        }
    }
    
    public function mcollect($incident_id){
        if(!(intval($incident_id) > 0)) url::redirect($_SERVER['HTTP_REFERER']);
        
        if(!Auth::instance()->logged_in()) url::redirect('login');
        $user = Auth::instance()->get_user();
        $user_id = $user->id;
        
        $offer = ORM::factory('offerincident', $incident_id);
        if ($offer->max_coupons !== 0){
            $order = new Order_Model();
            $order->customer_id = $user_id;
            $order->offerincident_id = $incident_id;
            $order->date = date('Y-m-d H:i:s');
            $order->total_price = $offer->offerincident_price;
            $order->customer_token = $this->_generate_customer_token();
            if($order->save() && $offer->max_coupons !== null){
                $offer->max_coupons = $offer->max_coupons - 1;
                $offer->save();
            }
            Session::instance()->set('offer_' . $incident_id . '_token', $order->customer_token);
        }else{
            // someone already beat you to the last coupon
        }
        url::redirect($_SERVER['HTTP_REFERER']);
    }
    
    
    public function comments($id = false)
    {
    	$this->template->header->js = "";
    	$this->template->content = new View('mobile/comments');
	
	       	$db = new Database;
    		$comments = $db->query("SELECT c.* , u.username FROM `".$this->table_prefix."comment` AS c LEFT JOIN `".$this->table_prefix."users` AS u ON (c.`user_id` = u.`id`) WHERE c.`offerincident_id` =".$id."");
    		$this->template->content->comments = $comments;
    }
	
    public function submit($saved = false)
    {
    	// Cacheable Controller
    	$this->is_cachable = FALSE;
    
    	$this->template->header->show_map = TRUE;
    	$this->template->content  = new View('mobile/reports_submit');
    
    	// First, are we allowed to submit new reports?
    	if ( ! Kohana::config('settings.allow_reports'))
    	{
    		url::redirect(url::site().'main');
    	}
    
    	// setup and initialize form field names
    	$form = array
    	(
    			'incident_title' => '',
    			'incident_description' => '',
    			'incident_month' => '',
    			'incident_day' => '',
    			'incident_year' => '',
    			'incident_hour' => '',
    			'incident_minute' => '',
    			'incident_ampm' => '',
    			'latitude' => '',
    			'longitude' => '',
    			'location_name' => '',
    			'country_id' => '',
    			'incident_category' => array(),
    	);
    	//	copy the form as errors, so the errors will be stored with keys corresponding to the form field names
    	$errors = $form;
    	$form_error = FALSE;
    
    	if ($saved == 'saved')
    	{
    		$form_saved = TRUE;
    	}
    	else
    	{
    		$form_saved = FALSE;
    	}
    
    
    	// Initialize Default Values
    	$form['incident_month'] = date('m');
    	$form['incident_day'] = date('d');
    	$form['incident_year'] = date('Y');
    	$form['incident_hour'] = "12";
    	$form['incident_minute'] = "00";
    	$form['incident_ampm'] = "pm";
    	// initialize custom field array
    	// $form['custom_field'] = $this->_get_custom_form_fields($id,'',true);
    	//GET custom forms
    	//$forms = array();
    	//foreach (ORM::factory('form')->find_all() as $custom_forms)
    	//{
    	//	$forms[$custom_forms->id] = $custom_forms->form_title;
    		//}
    		//$this->template->content->forms = $forms;
    
    
    		// check, has the form been submitted, if so, setup validation
    		if ($_POST)
    		{
    			// Instantiate Validation, use $post, so we don't overwrite $_POST fields with our own things
    			$post = Validation::factory(array_merge($_POST,$_FILES));
    
    			//  Add some filters
    			$post->pre_filter('trim', TRUE);
    
    			// Add some rules, the input field, followed by a list of checks, carried out in order
    			$post->add_rules('incident_title', 'required', 'length[3,200]');
    			$post->add_rules('incident_description', 'required');
    			$post->add_rules('incident_month', 'required', 'numeric', 'between[1,12]');
    			$post->add_rules('incident_day', 'required', 'numeric', 'between[1,31]');
    			$post->add_rules('incident_year', 'required', 'numeric', 'length[4,4]');
    				
    			if ( ! checkdate($_POST['incident_month'], $_POST['incident_day'], $_POST['incident_year']) )
    			{
    				$post->add_error('incident_date','date_mmddyyyy');
    			}
    				
    			$post->add_rules('incident_hour', 'required', 'between[1,12]');
    			$post->add_rules('incident_minute', 'required', 'between[0,59]');
    
    			if ($_POST['incident_ampm'] != "am" && $_POST['incident_ampm'] != "pm")
    			{
    				$post->add_error('incident_ampm','values');
    			}
    
    			// Validate for maximum and minimum latitude values
    			$post->add_rules('latitude', 'between[-90,90]');
    			$post->add_rules('longitude', 'between[-180,180]');
    			$post->add_rules('location_name', 'required', 'length[3,200]');
    
    			//XXX: Hack to validate for no checkboxes checked
    			if (!isset($_POST['incident_category'])) {
    				$post->incident_category = "";
    				$post->add_error('incident_category', 'required');
    			}
    			else
    			{
    				$post->add_rules('incident_category.*', 'required', 'numeric');
    			}
    				
    			// Geocode Location
    			if ( empty($_POST['latitude']) AND empty($_POST['longitude'])
    			AND ! empty($_POST['location_name']) )
    			{
    				$default_country = Kohana::config('settings.default_country');
    				$country_name = "";
    				if ($default_country)
    				{
    					$country = ORM::factory('country', $default_country);
    					if ($country->loaded)
    					{
    						$country_name = $country->country;
    					}
    				}
    
    				$geocode = mobile_geocoder::geocode($_POST['location_name'].", ".$country_name);
    				if ($geocode)
    				{
    					$post->latitude = $geocode['lat'];
    					$post->longitude = $geocode['lon'];
    				}
    				else
    				{
    					$post->add_error('location_name', 'geocode');
    				}
    			}
    
    			// Test to see if things passed the rule checks
    			if ($post->validate())
    			{
    				if ($post->latitude AND $post->longitude)
    				{
    					// STEP 1: SAVE LOCATION
    					$location = new Location_Model();
    					$location->location_name = $post->location_name;
    					$location->latitude = $post->latitude;
    					$location->longitude = $post->longitude;
    					$location->location_date = date("Y-m-d H:i:s",time());
    					$location->save();
    				}
    
    				// STEP 2: SAVE INCIDENT
    				$incident = new Incident_Model();
    				if (isset($location) AND $location->loaded)
    				{
    					$incident->location_id = $location->id;
    				}
    				$incident->user_id = 0;
    				$incident->incident_title = $post->incident_title;
    				$incident->incident_description = $post->incident_description;
    
    				$incident_date = $post->incident_year."-".$post->incident_month."-".$post->incident_day;
    				$incident_time = $post->incident_hour
    				.":".$post->incident_minute
    				.":00 ".$post->incident_ampm;
    				$incident->incident_date = date( "Y-m-d H:i:s", strtotime($incident_date . " " . $incident_time) );
    				$incident->incident_dateadd = date("Y-m-d H:i:s",time());
    				$incident->save();
    
    				// STEP 3: SAVE CATEGORIES
    				foreach($post->incident_category as $item)
    				{
    					$incident_category = new Incident_Category_Model();
    					$incident_category->incident_id = $incident->id;
    					$incident_category->category_id = $item;
    					$incident_category->save();
    				}
    
    				url::redirect('reports/thanks');
    
    			}
    			// No! We have validation errors, we need to show the form again, with the errors
    			else
    			{
    				// repopulate the form fields
    				$form = arr::overwrite($form, $post->as_array());
    
    				// populate the error fields, if any
    				$errors = arr::overwrite($errors, $post->errors('mobile_report'));
    				$form_error = TRUE;
    			}
    				
    		}
        
    		$this->template->content->form = $form;
    		$this->template->content->errors = $errors;
    		$this->template->content->form_error = $form_error;
    		$this->template->content->categories = $this->_get_categories($form['incident_category']);
    
    		$this->template->content->cities = $this->_get_cities();
    
    		$this->template->header->js = new View('mobile/reports_submit_js');
    		if (!$form['latitude'] || !$form['latitude'])
    		{
    			$this->template->header->js->latitude = Kohana::config('settings.default_lat');
    			$this->template->header->js->longitude = Kohana::config('settings.default_lon');
    		}else{
    			$this->template->header->js->latitude = $form['latitude'];
    			$this->template->header->js->longitude = $form['longitude'];
    		}
    	}
	
	private function _get_reviews($id,$type)
    	{
		$total_comments = ORM::factory('comment')->where($type, $id)->count_all();
    	   	
		return $total_comments;
    	}
	private function _get_thumb($id)
    	{
		$media = ORM::factory('offerincident', $id)->media;
		
			if ($media->count())
			{
				foreach ($media as $photo)
				{
					if ($photo->media_type == 1)
					{
						if ($photo->media_thumb)
						{ 
							// Get the first thumb
							$prefix = url::base().Kohana::config('upload.relative_directory');
							$thumb = $prefix."/".$photo->media_thumb;
							break;
						}
					}
				}
			
		}
		return $thumb;
			
    	}
	
	public function nearby()
    	{
		$this->template->content  = new View('mobile/offers_nearby');
		$max_radius = $this->session->get('map_radius');
		$lat = $this->session->get('map_center_lat');
		$lng = $this->session->get('map_center_lng');
		//Nearest Offers
		$db = Database::instance();
		
		$query = "SELECT offerincident.*, location.location_name, offerincident_description as description, ( 6371 * acos( cos( radians( $lat ) ) * cos( radians( latitude ) ) * cos( radians( longitude ) - radians( $lng ) ) + sin( radians( $lat ) ) * sin( radians( latitude ) ) ) ) AS distance FROM offerincident  
INNER JOIN offer_incidents oi ON oi.offerincident_id = offerincident.id
INNER JOIN incident ON incident.id = oi.incident_id 
INNER JOIN location ON incident.location_id = location.id WHERE offerincident_start_date < NOW() AND offerincident_end_date > NOW() AND offerincident_active = 1 AND longitude BETWEEN ( $lng -20 / abs( cos( radians( $lat ) ) *69 ) ) AND ( $lng +20 / abs( cos( radians( $lat ) ) *69 ) )
                    AND latitude BETWEEN ( $lat - ( $max_radius / 69 ) ) AND ( $lat + ( $max_radius / 69 ) ) GROUP BY offerincident.id HAVING distance < $max_radius ORDER BY distance DESC";

            // Pagination

            $pagination = new Pagination( array('style' => 'mobile', 'query_string' => 'page', 'items_per_page' => (int)Kohana::config('mobile.items_per_page'), 'total_items' => count($db->query($query))));
            $this->template->content->pagination = $pagination;
            $this->template->content->nearby = $db->query($query . ' LIMIT ' . $pagination->sql_offset . ','. 	    (int)Kohana::config('mobile.items_per_page'));
    	}
	
	public function feature()
    	{
		$this->template->content  = new View('mobile/offers_featured');
		$max_radius = $this->session->get('map_radius');
		$lat = $this->session->get('map_center_lat');
		$lng = $this->session->get('map_center_lng');
		//Featured Offers
		$db = Database::instance();
		
		$query = "SELECT offerincident.*, location.location_name, offerincident_description as description, ( 6371 * acos( cos( radians( $lat ) ) * cos( radians( latitude ) ) * cos( radians( longitude ) - radians( $lng ) ) + sin( radians( $lat ) ) * sin( radians( latitude ) ) ) ) AS distance FROM offerincident  
			INNER JOIN offer_incidents oi ON oi.offerincident_id = offerincident.id
			INNER JOIN incident ON incident.id = oi.incident_id 
			INNER JOIN location ON incident.location_id = location.id WHERE offerincident_start_date < NOW() AND offerincident_end_date > NOW() AND offerincident_active = 1 AND longitude BETWEEN ( $lng -20 / abs( cos( radians( $lat ) ) *69 ) ) AND ( $lng +20 / abs( cos( radians( $lat ) ) *69 ) )
                         AND latitude BETWEEN ( $lat - ( $max_radius / 69 ) ) AND ( $lat + ( $max_radius / 69 ) ) AND offerincident_featured = 1  GROUP BY offerincident.id HAVING distance < $max_radius ORDER BY distance DESC";

            // Pagination

            $pagination = new Pagination( array('style' => 'mobile', 'query_string' => 'page', 'items_per_page' => (int)Kohana::config('mobile.items_per_page'), 'total_items' => count($db->query($query))));
            $this->template->content->pagination = $pagination;
            $this->template->content->featured = $db->query($query . ' LIMIT ' . $pagination->sql_offset . ','. 	    (int)Kohana::config('mobile.items_per_page'));
    	}
	
	public function recent()
    	{
		$this->template->content  = new View('mobile/offers_recent');
		$max_radius = $this->session->get('map_radius');
		$lat = $this->session->get('map_center_lat');
		$lng = $this->session->get('map_center_lng');
		//Recent Offers
		$db = Database::instance();
		
		$query = "SELECT offerincident.*, location.location_name, offerincident_description as description, ( 6371 * acos( cos( radians( $lat ) ) * cos( radians( latitude ) ) * cos( radians( longitude ) - radians( $lng ) ) + sin( radians( $lat ) ) * sin( radians( latitude ) ) ) ) AS distance FROM offerincident  
			INNER JOIN offer_incidents oi ON oi.offerincident_id = offerincident.id	INNER JOIN incident ON incident.id = oi.incident_id 
			INNER JOIN location ON incident.location_id = location.id WHERE offerincident_dateadd BETWEEN DATE_SUB(CURDATE(), INTERVAL 7 DAY) AND CURDATE() AND offerincident_start_date < NOW() AND offerincident_end_date > NOW() AND offerincident_active = 1 AND longitude BETWEEN ( $lng -20 / abs( cos( radians( $lat ) ) *69 ) ) AND ( $lng +20 / abs( cos( radians( $lat ) ) *69 ) )
                         AND latitude BETWEEN ( $lat - ( $max_radius / 69 ) ) AND ( $lat + ( $max_radius / 69 ) ) AND offerincident_featured = 1  GROUP BY offerincident.id HAVING distance < $max_radius ORDER BY distance DESC";
		
            // Pagination

            $pagination = new Pagination( array('style' => 'mobile', 'query_string' => 'page', 'items_per_page' => (int)Kohana::config('mobile.items_per_page'), 'total_items' => count($db->query($query))));
            $this->template->content->pagination = $pagination;
            $this->template->content->recent = $db->query($query . ' LIMIT ' . $pagination->sql_offset . ','.(int)Kohana::config('mobile.items_per_page'));
    	}
	
	
 
}