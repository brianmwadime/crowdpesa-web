<?php defined('SYSPATH') or die('No direct script access.');
/**
* PHP version 5
 * LICENSE: This source file is subject to LGPL license 
 * that is available through the world-wide-web at the following URI:
 * http://www.gnu.org/copyleft/lesser.html
 * @author     BeeBuy Team <info@beebuy.com> 
 * @package    CrowdPesa - http://crowdpesa.com
 * @module     Offerreports Controller
 * @copyright  CrowdPesa - http://crowdpesa.com
 * @license    http://www.gnu.org/copyleft/lesser.html GNU Lesser General Public License (LGPL) 
 */

class Search_Controller extends Mobile_Controller {
	
	public function __construct()
    {
		parent::__construct();
		
	}
	
	public function index($page=1)
	{
		$this->template->content  = new View('mobile/search');
		
		$search_query = "";
		$keyword_string = "";
		$where_string = "";
		$keyword_string2 = "";
		$where_string2 = "";
		$plus = "";
		$or = "";
		$search_info = "";
		$html = "";
		$groupmenu = "";
		$pagination = "";
		
		// Stop words that we won't search for
		// Add words as needed!!
		$stop_words = array('the', 'and', 'a', 'to', 'of', 'in', 'i', 'is', 'that', 'it',
					'on', 'you', 'this', 'for', 'but', 'with', 'are', 'have', 'be',
					'at', 'or', 'as', 'was', 'so', 'if', 'out', 'not'
		);
		
		if ($_GET)
		{
			/**
			 * NOTES: 15/10/2010 - Emmanuel Kala <emmanuel@ushahidi.com>
			 *
			 * The search string undergoes a 3-phase sanitization process. This is not optimal
			 * but it works for now. The Kohana provided XSS cleaning mechanism does not expel
			 * content contained in between HTML tags this the "bruteforce" input sanitization.
			 *
			 * However, XSS is attempted using Javascript tags, Kohana's routing mechanism strips
			 * the "<script>" tags from the URL variables and passes inline text as part of the URL
			 * variable - This has to be fixed
			 */

			// Phase 1 - Fetch the search string and perform initial sanitization
			$keyword_raw = (isset($_GET['s']))? preg_replace('#/\w+/#', '', $_GET['s']) : "";

			// Phase 2 - Strip the search string of any HTML and PHP tags that may be present for additional safety
			$keyword_raw = strip_tags($keyword_raw);
			//echo $keyword_raw; exit;
			// Phase 3 - Apply Kohana's XSS cleaning mechanism
			$keyword_raw = $this->input->xss_clean($keyword_raw);
		}
		else
		{
			$keyword_raw = "";
		}

		// Database instance
		$db = new Database();
		
		$keywords = explode(' ', $keyword_raw);
		if (is_array($keywords) AND ! empty($keywords))
		{
			array_change_key_case($keywords, CASE_LOWER);
			$i = 0;
			
			foreach($keywords as $value)
			{
				if ( ! in_array($value,$stop_words) AND ! empty($value))
				{
					// Escape the string for query safety
					$chunk = $db->escape_str($value);
			
					if ($i > 0)
					{
						$plus = ' + ';
						$or = ' OR ';
					}
					//Sarching begins here, trying to find an optimized way of searching through two tables
					// Give relevancy weighting
					// Title weight = 2
					// Description weight = 1
					//For incidents
					$keyword_string = $keyword_string.$plus."(CASE WHEN incident_title LIKE '%$chunk%' THEN 2 ELSE 0 END) + "
					. "(CASE WHEN incident_description LIKE '%$chunk%' THEN 1 ELSE 0 END) ";
			
					$where_string = $where_string.$or."(incident_title LIKE '%$chunk%' OR incident_description LIKE '%$chunk%')";
								
					//For  Offers
					$keyword_string2 = $keyword_string2.$plus."(CASE WHEN offerincident_title LIKE '%$chunk%' THEN 2 ELSE 0 END) + "
					. "(CASE WHEN offerincident_description LIKE '%$chunk%' THEN 1 ELSE 0 END) ";
										
					$where_string2 = $where_string2.$or."(offerincident_title LIKE '%$chunk%' OR offerincident_description LIKE '%$chunk%')";
					$i++;
					
				}
				
				
			}
			if ( (! empty($keyword_string) AND !empty($where_string)) OR (! empty($keyword_string2) AND !empty($where_string2)))
			{
				// Limit the result set to only those reports that have been approved
				$where_string = '(' . $where_string . ') AND incident_active = 1 ';
				$incidents_query = "SELECT id, incident_title AS title, incident_description AS description, incident_date AS item_date, (CASE WHEN incident_active = 1 THEN 'reports' ELSE '' END) AS page, (".$keyword_string.") AS relevance FROM "
								. $this->table_prefix."incident "
								. "WHERE ".$where_string.""
								. "ORDER BY relevance DESC ";
				
				// Limit the result set to only those offers that are active
				$where_string2 = '(' . $where_string2 . ') AND offerincident_active = 1 ';
				$offers_query = "SELECT id, offerincident_title AS title, offerincident_description AS description, offerincident_date AS item_date, (CASE WHEN offerincident_active = 1 THEN 'offers' ELSE '' END) AS page, (".$keyword_string2.") AS relevance FROM "
								 . $this->table_prefix."offerincident "
								 . "WHERE ".$where_string2.""
								 . "GROUP BY offerincident_active, relevance ";
			}
		}
		
		if ( ! empty($incidents_query) OR ! empty($offers_query) )
		{
			
			$querys[] = $db->query($incidents_query);
				
			$querys[] = $db->query($offers_query);
			
			//merge the two queries
			$results = array();
			if (isset($_GET["action"]) AND $_GET['action'] == "all"){
			foreach ($querys as $query) {
				$results = arr::merge($results,$query);
			}
			}elseif (isset($_GET["action"])AND $_GET["action"] == "places"){
				$results = arr::merge($querys[0]);
			}elseif (isset($_GET["action"])AND $_GET["action"] == "offers"){
				$results = arr::merge($querys[1]);
			}
			// Pagination
			$pagination = new Pagination(array(
									'query_string' => 'page',
									'items_per_page' => (int) Kohana::config('settings.items_per_page'),
									'total_items' => count($results)
			));
				
			if ((int) Kohana::config('settings.items_per_page') > 0) {
				$result_items = array_splice($results, $pagination->sql_offset, (int) Kohana::config('settings.items_per_page'));
			} else {
				$result_items = $results;
			}
			// Results Bar
			if ($pagination->total_items != 0)
			{
				$search_info .= "<div class=\"search_info\">"
							. Kohana::lang('ui_admin.showing_results')
							. ' '. ( $pagination->sql_offset + 1 )
							. ' '.Kohana::lang('ui_admin.to')
							//Set to show the exact number of items remaining in the pagination queue
							. ' '.( ( $pagination->sql_offset) + count($result_items) )
							. ' '.Kohana::lang('ui_admin.of').' '. $pagination->total_items
							. ' '.Kohana::lang('ui_admin.searching_for').' <strong>'. $keyword_raw . "</strong>"
							. "</div>";
			}
			else
			{
				$search_info .= "<div class=\"search_info\">0 ".Kohana::lang('ui_admin.results')."</div>";

				$html .= "<div class=\"search_result\">";
				$html .= "<h3>".Kohana::lang('ui_admin.your_search_for')."<strong> ".$keyword_raw."</strong> ".Kohana::lang('ui_admin.match_no_documents')."</h3>";
				$html .= "</div>";

				$pagination = "";
			}
				
				//For dispalying both incidents and offer search results
		foreach ($result_items as $search)
			{
				//sorting through search results
				$item_id = $search->id;
				$item_title = $search->title;
				$highlight_title = "";
				$item_title_arr = explode(' ', $item_title);

				foreach($item_title_arr as $value)
				{
					if (in_array(strtolower($value),$keywords) AND !in_array(strtolower($value),$stop_words))
					{
						$highlight_title .= "<span class=\"search_highlight\">" . $value . "</span> ";
					}
					else
					{
						$highlight_title .= $value . " ";
					}
				}

				$item_description = $search->description;

				// Remove any markup, otherwise trimming below will mess things up
				$item_description = strip_tags($item_description);
				
				// Trim to 180 characters without cutting words
				if ((strlen($item_description) > 180) AND (strlen($item_description) > 1))
				{
					$whitespaceposition = strpos($item_description," ",175)-1;
					$item_description = substr($item_description, 0, $whitespaceposition);
				}

				$highlight_description = "";
				$item_description_arr = explode(' ', $item_description);

				foreach($item_description_arr as $value)
				{
					if (in_array(strtolower($value),$keywords) && !in_array(strtolower($value),$stop_words))
					{
						$highlight_description .= "<span class=\"search_highlight\">" . $value . "</span> ";
					}
					else
					{
						$highlight_description .= $value . " ";
					}
				}

				$item_date = date('D M j Y g:i:s a', strtotime($search->item_date));

				$html .= "<div class=\"search_result\">";
				//sets the url using the page column of the table
				$html .= "<h3><a href=\"" . url::base() . "mobile/".$search->page."/view/" . $item_id . "\">" . $highlight_title . "</a></h3>";
				//$html .= "(".$search->page.")<br/>";
				$html .= $highlight_description . " ...";
				$html .= "<div class=\"search_date\">" . $item_date . " | ".Kohana::lang('ui_admin.relevance').": <strong>+" . $search->relevance . "</strong></div>";
				$html .= "</div>";
			}

		}
		else
		{
			// Results Bar
			$search_info .= "<div class=\"search_info\">0 ".Kohana::lang('ui_admin.results')."</div>";

			$html .= "<div class=\"search_result\">";
			$html .= "<h3>".Kohana::lang('ui_admin.your_search_for')."<strong>&nbsp;' ".$keyword_raw." '</strong> ".Kohana::lang('ui_admin.match_no_documents')."</h3>";
			$html .= "</div>";
		}

		$html .= $pagination;
			
			$groupmenu .="<ul id=\"search_navigation\">";
			$groupmenu .="<span class=\"search_groupby\"><small>Group by</small></span>";
			$search_term = $_GET['s'];
			if($_GET['action'] =="all"){
				$groupmenu .="<li class=\"active\" ><a  href=\"".url::base()."mobile/search/?s=".$search_term."&action=all\">All</a></li>";
				$groupmenu .="<li><a href=\"".url::base(). "mobile/search/?s=".$search_term."&action=offers\">Offers</a></li>";
				$groupmenu .="<li><a href=\"".url::base()."mobile/search/?s=".$search_term."&action=places\">Places</a></li>";
			}elseif($_GET['action'] =="offers"){
				$groupmenu .="<li><a href=\"".url::base()."mobile/search/?s=".$search_term."&action=all\">All</a></li>";
				$groupmenu .="<li class=\"active\"><a  href=\"".url::base()."mobile/search/?s=".$search_term."&action=offers\">Offers</a></li>";
				$groupmenu .="<li><a href=\"".url::base()."mobile/search/?s=".$search_term."&action=places\">Places</a></li>";
			}elseif($_GET['action'] =="places"){
				$groupmenu .="<li><a href=\"".url::base()."mobile/search/?s=".$search_term."&action=all\">All</a></li>";
				$groupmenu .="<li><a href=\"".url::base()."mobile/search/?s=".$search_term."&action=offers\">Offers</a></li>";
 				$groupmenu .="<li class=\"active\"><a  href=\"".url::base()."mobile/search/?s=".$search_term."&action=places\">Places</a></li>";
			}
			$groupmenu .="</ul>";		
	  $this->template->content->search_menu = $groupmenu;
	   $this->template->content->search_info = $search_info;
	  $this->template->content->search_results = $html;
		
	}
	
}