<?php
/**
 * Mobile Time helper
 * 
 * @package    Mobile
 * @author     Beebuy Team
 * @copyright  (c) 2008 crowdpesa.om
 */
class mobile_time_Core {
    
 public static function format_interval($interval, $granularity = 2) {
  $units = array(
    '1 year|@count years' => 31536000,
    '1 month|@count months' => 2592000,
    '1 week|@count weeks' => 604800,
    '1 day|@count days' => 86400,
    '1 hour|@count hours' => 3600,
    '1 min|@count min' => 60,
    '1 sec|@count sec' => 1
  );
  $output = '';
  foreach ($units as $key => $value) {
    $key = explode('|', $key);
    if ($interval >= $value) {
      $output .= ($output ? ' ' : '') . mobile_time::format_plural(floor($interval / $value), $key[0], $key[1], array());
      $interval %= $value;
      $granularity--;
    }

    if ($granularity == 0) {
      break;
    }
  }
  return $output ? $output : '0 sec';
}
    
 public static function format_plural($count, $singular, $plural, array $args = array()) {
            $args['@count'] = $count;
                if ($count == 1) {
                  return strtr($singular, $args);
                }
          
            // Get the plural index through the gettext formula.
            $index = -1;
            // Backwards compatibility.
                if ($index < 0) {
                  return strtr($plural, $args);
                }
                else {
                  switch ($index) {
                    case "0":
                      return strtr($singular, $args);
                    case "1":
                      return strtr($plural, $args);
                    default:
                      unset($args['@count']);
                      $args['@count[' . $index . ']'] = $count;
                      return strtr($plural, array('@count' => '@count[' . $index . ']'));
                      
                  }
        }
    }
    
}