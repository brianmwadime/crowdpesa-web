jQuery(document).ready(function($) {

	// The number of the next page to load (/page/x/).
	var pageNum = parseInt(checkin.startPage) + 1;
	//alert(pageNum);
	// The maximum number of pages the current query can return.
	var max = parseInt(checkin.maxPages);
	
	// The link of the next page of posts.
	var nextLink = checkin.nextLink;
	

	/**
	 * Replace the traditional navigation with our own,
	 * but only if there is at least one page of new posts to load.
	 */
	if(pageNum <= max) {
		// Insert the "More Posts" link.
		$('#cilist')
			.append('<div class="checkin-placeholder-'+ pageNum +'"></div>')
			.append('<p id="load-checkins"><strong class="title"><a href="#">Load More Checkins</a></strong></p>')
			.append('');
		// Remove the traditional navigation.
	}
	
	
	/**
	 * Load new posts when the link is clicked.
	 */
	$('#load-checkins a').click(function() {
	
		// Are there more posts to load?
		if(pageNum <= max) {
		
			// Show that we're working.
			$(this).text('Loading checkins...');
				$('.checkin-placeholder-'+ pageNum).load(nextLink + ' .checkin',{ "paged":pageNum },function() {
					// Update page number and nextLink.
					pageNum++;
                                        //alert(pageNum);
					//nextLink = nextLink.replace(/[0-9]?/,pageNum);
					// Add a new placeholder, for when user clicks again.
					$('#load-checkins')
						.before('<div class="checkin-placeholder-'+ pageNum +'"></div>')
					
					// Update the button message.
					if(pageNum <= max) {
						$('#load-checkins a').text('Load More Checkins');
					} else {
						$('#load-checkins a').text('No more Checkins to load.');
					}
				}
			);
		} else {
			$('#load-checkins a').append('.');
		}	
		
		return false;
	});
});