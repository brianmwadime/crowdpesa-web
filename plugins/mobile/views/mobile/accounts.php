<?php $session = Session::instance();  ?>
<div class="list">
			<?php if($user->public_profile == 1){   ?>
			<ul class="details">
				<?php
						echo "<li><strong>Full name : </strong>".$user->name."</li>";
						echo "<li><strong>User name : </strong>".$user->username."</li>";
						
						if($session->get('auth_user')->id == $user->id){						
						echo "<li><strong>E-mail : </strong>".$user->email."</li>";
						}
						$notify= ($user->notify) ? 'Yes':'No';
						echo "<li><strong>Notifications : </strong>".$notify."</li>";
						echo "<li><strong>Total Checkins : </strong>&nbsp;&nbsp;&nbsp;".$citotal."</li>";
						if($session->get('auth_user')->id != $user->id){						
						echo "<li><strong>Last login : </strong>".mobile_time::format_interval(floor((time() - $user->last_login)))."&nbsp;ago</li>";
						}
				?>
			</ul>
			<?php }else{  ?>
			  <div>Sorry&nbsp;<?php echo $user->username ?>&nbsp;doesn't have a Public profile.</div>
			
			<?php } ?>
			<small>&nbsp;&nbsp;<a href="<?php echo url::site()?>mobileci/logout">Logout</a></small>
</div>