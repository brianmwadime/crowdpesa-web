<div id="cilist">
<?php 
if ($checkins->count()) {
  foreach ($checkins as $checkin) {
	    echo "<div class='checkin'>";
	    echo "<div style='border: medium none;' id=checkin-".$checkin->id." class='checkin-".$checkin->id."'><a name='ci_id_6'></a></div>";
 	    echo "<div style='background-color: rgb(255, 0, 0);' class='colorblock shorterblock'><div class='colorfade'></div></div>";
 	    echo  "<div style=\"float: right; width: 24px; height: 24px; margin-right: 10px;\"><a href=\"".url::site()."mobile/checkins/view/".$checkin->id."\"><img height=\"24\" width=\"24\" src=\"".url::site()."plugins/mobile/views/images/earth_trans.png\"></a></div>";
 	    echo  "<span class='title' style='font-size: 14px; width: 100%; padding-top: 0px;'>".$checkin->name."</span>";
 	    echo  "<div class='cimsg'>".$checkin->checkin_description."<br>";
	    //calculate the difference in time btwn today and the checkin_date in seconds and round off to the nearest day
 	    echo  "<small><em>".mobile_time::format_interval(floor((time() - strtotime($checkin->checkin_date))))."&nbsp;ago</em></small></div><div style='clear: both;'></div>";
	    echo "</div>";
  }

}else{
	echo "<div style='background-color: rgb(255, 0, 0); width: 100%; height: 24px; padding-left: 10px;'>No checkins Found</div>";
}
 
?>
</div>
<div class="block-submit">
<span class="submit"><strong class="checkin-submit"><a class="checkin-link" href="<?php echo url::site()."mobileci/"?>">Check-In</a></strong></span>
</div>
<script type="text/javascript">
  jQuery(document).ready(function($) {
	// The number of the next page to load (/page/x/).
	var pageNum = parseInt(checkin.startPage) + 1;
	//alert(pageNum);
	// The maximum number of pages the current query can return.
	var max = parseInt(checkin.maxPages);
	// The link of the next page of chekins.
	var nextLink = checkin.nextLink;
	/**
	 * Replace the traditional navigation with our own,
	 * but only if there is at least one page of new chekins to load.
	 */
	if(pageNum <= max) {
		// Insert the "Load More Checkins" link.
		$('#cilist')
			.append('<div class="checkin-placeholder-'+ pageNum +'"></div>')
			.append('<p id="load-checkins"><strong class="title"><a href="#">Load More Checkins</a></strong></p>')
			.append('');
		// Remove the traditional navigation.
	}
	/**
	 * Load new checkins when the link is clicked.
	 */
	$('#load-checkins a').click(function() {
		// Are there more checkins to load?
		if(pageNum <= max) {
			// Show that we're working.
			$(this).text('Loading checkins...');
				$('.checkin-placeholder-'+ pageNum).load(nextLink + ' .checkin',{ "paged":pageNum },function() {
					// Update page number and nextLink.
					pageNum++;
                                        // Add a new placeholder, for when user clicks again.
					$('#load-checkins')
						.before('<div class="checkin-placeholder-'+ pageNum +'"></div>')
					// Update the button message.
					if(pageNum <= max) {
						$('#load-checkins a').text('Load More Checkins');
					} else {
						$('#load-checkins a').text('No more Checkins to load.');
					}
				}
			);
		} else {
			$('#load-checkins a').append('.');
		}	
		return false;
	});
});
</script>