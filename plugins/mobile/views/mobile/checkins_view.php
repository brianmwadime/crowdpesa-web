<div class="checkin_info">
	<h2><a href="<?php echo url::site().'mobile/accounts/index/'.$checkin->user->id ?>"><?php echo $checkin->user->name; ?></a></h2>
	<ul class="details">
		<li>
			<small>Location</small>: 
			<?php echo $checkin->location->location_name; ?>
		</li>
		<li>
			<small>checked in</small>: 
			<?php echo mobile_time::format_interval(floor((time() - strtotime($checkin->checkin_date))))."&nbsp;ago"; ?>
		</li>
		<li>
			<small>Description</small>: <br />
			<?php echo $checkin->checkin_description; ?>
		</li>
	</ul>
</div>
<div style="clear:both;"></div>
<div id="map_canvas"></div>