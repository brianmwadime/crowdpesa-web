<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="Content-type" content="text/html; charset=utf-8"/>
<meta name="HandheldFriendly" content="True"/>
<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0"/>
<title>
<?php echo $site_name; ?></title>
<?php
echo html::script('plugins/mobile/views/js/jquery', true);
echo html::script('plugins/mobile/views/js/jquery.treview', true);
echo html::script('plugins/mobile/views/js/expand', true);
echo html::stylesheet('plugins/mobile/views/css/styles','screen, handheld');
echo html::stylesheet('plugins/mobile/views/css/jquery.treeview','screen, handheld');
if ($show_map === TRUE)
{
	echo "\n<script type=\"text/javascript\" src=\"http://maps.google.com/maps/api/js?sensor=false\"></script>\n";
}
?>
<script type="text/javascript">
<!--//--><![CDATA[//><!--
$(function() {
	$("span.expand").toggler({speed: "fast"});
	 var locCallback = function(results, status) {
			if (status == google.maps.GeocoderStatus.OK) {
				var mylocation ="";
				mylocation = results[0].formatted_address;
				//alert(mylocation);
				var map_center = <?php echo $this->session->get('user_map_center') ?>;
				if(map_center == 0){
					$("#location_status").html('Sorry your device does not support geolocation');
				}else{
					$("#location_status").html('You are Here: ' + mylocation);
				}
				
				
			}
	}
	var coder = new google.maps.Geocoder();
	var latLng = new google.maps.LatLng(<?php echo $this->session->get('map_center_lat') ?>, <?php echo $this->session->get('map_center_lng') ?>);
	coder.geocode({ 'latLng': latLng }, locCallback);
});
//--><!]]>
</script>
<script type="text/javascript">
<?php echo $js; ?>
</script>
</head>
<body <?php
if(URI::method(FALSE)== "view"){
	if ($show_map === TRUE) {
		echo " onload=\"initialize()\"";
	}
}
?>>
<div id="Top"></div>
	<div id="container">
		<div id="header">
		<table id="top">
    <tbody><tr>
      <td class="left">
       <a href="<?php echo url::site() ?>mobile" class="brandmark">
         <img class="logo" src="<?php echo url::site() ?>plugins/mobile/views/images/logo.png"/>
         <span class="logo"><?php echo $site_name; ?></span>
        </a>
      </td>
      <td class="right">
        </td>
    </tr>
  </tbody>
		</table>
		</div>
		<div style="width:100%;">
		<table id="navigation">
			<tbody>
				<tr id="nav">
					<td class="home"><a class="" href="<?php echo url::site()."mobile"; ?>">Home</a></td>
					<td class="checkins"><a class="" href="<?php echo url::site()."mobile/checkins"; ?>">Timeline</a></td>
					<?php if ( !Auth::instance()->logged_in())
					{
					// If we aren't logged in, redirect to login page
					echo "<td class=\"profile\"><a class=\"\" href=\"".url::site()."mobileci/login\">Sign In</a></td>";
					}else{
						//If logged in show profile page
					echo "<td class=\"profile\"><a class=\"\" href=\"".url::site()."mobile/accounts\">My Profile</a></td>";
					} ?>
				</tr>
			</tbody>
		</table>
		</div>
		
	<div id="page">
		<div id="location_status"></div>