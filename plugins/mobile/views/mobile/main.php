<?php
include "location.php";
$location="?lat=$lat_you&lon=$lon_you";
?>
<div class="search-fields">
<div class="title">Search for a place or offer below</div>
<form class="search" method="get" action="<?php echo url::site()."mobile/search/" ?>">
<input type="hidden" name="action" value="all"></td>
<table><tbody><tr>
       	<td id="search" class="value"><div><input value="" name="s" class="search_txt" type="text"></div></td>
  		<td class="button"><input type="submit" name="submit" class="search" value=""></td>
 </tr>
     </tbody>
      </table>
      </form>
      </div>
<div class="block">
<div class="block-featured">
    <span id="featured" class="expand">Offers</span>
    <div class="collapse">
    <h2 class="other"><a href="<?php echo url::site()."mobile/offers/nearby" ?>">View Nearby Offers</a></h2>
    <h2 class="other"><a href="<?php echo url::site()."mobile/offers/feature" ?>">View Featured Offers</a></h2>
    <h2 class="other"><a href="<?php echo url::site()."mobile/offers/recent" ?>">View Recently Added Offers</a></h2>
    </div>
  </div>
<div class="block">
<span id="category" class="expand">Nearest Places By Category</span>
<div class="collapse">
	<?php
	foreach ($categories as $category => $category_info)
	{
		$category_title = $category_info[0];
		$category_color = $category_info[1];
		$category_image = '';
		$category_count = $category_info[3];
		$color_css = 'class="swatch" style="background-color:#'.$category_color.'"';
		if (count($category_info[4]) == 0)
		{
			echo '<h2 class="other"><a href="'.url::site().'mobile/reports/index/'.$category.$location.'"><div '.$color_css.'>'.$category_image.'</div>'.$category_title.'</a><span>'.$category_count.'</span></h2>';
		}
		else
		{
			echo '<h2 class="expand"><div '.$color_css.'>'.$category_image.'</div>'.$category_title.'</h2>';
		}
		// Get Children
		echo '<div class="collapse">';
		foreach ($category_info[4] as $child => $child_info)
		{
			$child_title = $child_info[0];
			$child_color = $child_info[1];
			$child_image = '';
			$child_count = $child_info[3];
			$color_css = 'class="swatch" style="background-color:#'.$child_color.'"';
			echo '<h2 class="other"><a href="'.url::site().'mobile/reports/index/'.$child.$location.'"><div '.$color_css.'>'.$child_image.'</div>'.$child_title.'</a><span>'.$child_count.'</span></h2>';
		}
		echo '</div>';
	}
	?>				
</div>
</div>
<div class="block-submit">
<span class="submit"><strong class="report-submit"><a class="report-link" href="<?php echo url::site()."mobile/reports/submit/" ?>">Submit A Place</a></strong></span>
</div>
</div>
</div>