<div class="search-fields">
<div class="title">Please Enter an offer to search for</div>
<form class="search" method="get" action="<?php echo url::site()."mobile/search/" ?>">
<input type="hidden" name="action" value="offers"></td>
<table><tbody><tr>
      	      	<td id="search" class="value"><div><input value="" name=s class="search_txt" type="text"></div></td>

  		<td class="button"><input type="submit" name="submit" class="search" value=""></td>
 </tr>
     </tbody>
      </table>
      </form>
      </div>
      
<div class="report_list">
	<div class="block-offers">
		<span class="other"><a href="#">Offers</a></span>
		<div class="list">
			<ul>
				<?php
				if ($offers->count())
				{
					$page_no = (isset($_GET['page'])) ? $_GET['page'] : "";
					foreach ($offers as $offer)
					{
						
						//$offer_date = $offer->offer_date;
						$offer_date = date('M j Y', strtotime($offer->offerincident_dateadd));
						$location_name = $offer->location_name;
						echo "<li><strong><a href=\"".url::site()."mobile/offers/view/".$offer->id."?&p=".$page_no."\">".$offer->offerincident_title."</a></strong>";
						echo "&nbsp;&nbsp;<i>$offer_date</i>";
						echo "<BR /><span class=\"location_name\">".$offer->location_name."</span></li>";
					}
				}
				else
				{
					echo "<li>No offers Found</li>";
				}
				?>
			</ul>
		</div>
		<?php echo $pagination; ?>
	</div>
</div>