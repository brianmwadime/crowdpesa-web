<?php $session = Session::instance(); 
$collect_url = Auth::instance()->logged_in() ? url::site() . 'mobile/offers/mcollect/' . $offer[0]->id : url::site() . 'mobileci/login?redir';
        $collect_js = Auth::instance()->logged_in() ? '' : 'showLogin();return false;';
	$button = Auth::instance()->logged_in() ? 'collect_offer.png' : 'login_collect.png';
	?>
<div class="report_info">
	<h2><?php echo $offer[0]->offerincident_title; ?></h2>
	<img style="margin:0 auto;display: block;" width="128px" src="<?php echo $offer_thumb ?>">
	<ul class="details">
		<li>
			<small>Location</small>: 
			<?php echo $offer[0]->location_name; ?>
		</li>
		
		<li>
			<small>Date</small>: 
			<?php  echo date('M j Y', strtotime($offer[0]->offerincident_end_date)); ?>
		</li>
		<li>
			<small>Description</small>: <br />
			<?php echo $offer[0]->description; ?>
		</li>
		<li>
			<small>Rating</small>&nbsp;&nbsp;&nbsp;
		<span class="rating"><?php
				 for ($i = 1; $i <= 5; $i++)
				 {
					
					 if ($offer_rating >= $i){
						
						echo "<img alt=\"* \" src=\"".url::site()."themes/pesatheme/images/dummy_pix/rating_on.png\" />";
					}else{
						echo "<img alt=\"* \" src=\"".url::site()."themes/pesatheme/images/dummy_pix/rating_off.png\" />";
					}
				 } ?>
				 </span>
		</li>
		<li>
		<?php if($total_reviews >= 1) : ?>
			<?php echo $total_reviews; ?>&nbsp;<a href="<?php echo url::site() ?>mobile/offers/comments/<?php echo $offer[0]->id; ?>"><strong style="font-weight:bold;">Reviews</strong></a>
			<?php else : ?>
			<strong style="font-weight:bold;">No comments</strong>
			<?php endif; ?>
		</li>
		<li>
			<p><strong>Offer Value:</strong>&nbsp;<?php echo $offer[0]->offerincident_price ?></p>
			<p><strong>Validity:</strong>&nbsp;<?php echo date('j M g:m a', strtotime($offer[0]->offerincident_start_date)) . ' - ' . date('j M g:m a', strtotime($offer[0]->offerincident_end_date)) ?></p>
			<p><strong>Coupons Available:</strong>&nbsp;<?php echo ($offer[0]->max_coupons) ? $offer[0]->max_coupons : 'unlimited' ?></p>
		<?php if ($offer[0]->scope == 0): ?>
			<p><strong>Coupon:</strong>&nbsp;<br /><img alt="<?php echo $offer[0]->offerincident_title ?> coupon" src="<?php echo url::site() . 'mobile/offers/mcoupon/' . $offer[0]->id ?>" /></p>
		<?php else: ?>
		<?php if ($session->get('offer_' . $offer[0]->id . '_token')): ?>
			<p>Your verification token is <strong>"<?php echo $session->get('offer_' . $offer[0]->id . '_token')?>"</strong> Present it to one of the <?php echo $offer[0]->user_id ?> stores highlighted on the map to claim your offer along with the following coupon:<br /><img alt="<?php echo $offer[0]->offerincident_title ?> coupon" src="<?php echo url::site() . 'mobile/offers/mcoupon/' . $offer[0]->id ?>" /></p>
		<?php else: ?>
		<p><a href="<?php echo $collect_url ?>" onclick="<?php echo $collect_js ?>"><img alt="Collect Offer" src="<?php echo url::site() . 'themes/pesatheme/images/' . $button ?>" /></a></p>
		<?php endif; ?>    
		<?php endif; ?>
		</li>
	</ul>
</div>
<div style="clear:both;"></div>
<div id="map_canvas"></div>
<script type="text/javascript">
function showLogin(){
	window.location = "<?php echo url::site(); ?>mobileci/login?redir";
	return false;
}
</script>