<div class="search-fields">
<div class="title">Please Enter a place to search for</div>
<form class="search" method="get" action="<?php echo url::site()."mobile/search/" ?>">
<input type="hidden" name="action" value="places"></td>
<table><tbody><tr>
      	      	<td id="search" class="value"><div><input value="" name=s class="search_txt" type="text"></div></td>

  		<td class="button"><input type="submit" name="submit" class="search" value=""></td>
 </tr>
     </tbody>
      </table>
      </form>
      </div>
<div class="report_list">
	<div class="block">
		<?php
		if ($category AND $category->loaded)
		{
			$category_id = $category->id;
			$color_css = 'class="swatch" style="background-color:#'.$category->category_color.'"';
			echo '<h2 class="other"><a href="#"><div '.$color_css.'></div>'.$category->category_title.'</a></h2>';
		}
		else
		{
			$category_id = "";
		}
		?>
		<div class="list">
			<ul>
				<?php
				if ($incidents->count())
				{
					$page_no = (isset($_GET['page'])) ? $_GET['page'] : "";
					foreach ($incidents as $incident)
					{
						$incident_date = $incident->incident_date;
						$incident_date = date('M j Y', strtotime($incident->incident_date));
						$location_name = $incident->location_name;
						echo "<li><strong><a href=\"".url::site()."mobile/reports/view/".$incident->id."?c=".$category_id."&p=".$page_no."\">".$incident->incident_title."</a></strong>";
						echo "&nbsp;&nbsp;<i>$incident_date</i>";
						echo "<BR /><span class=\"location_name\">".$location_name."</span></li>";
					}
				}
				else
				{
					echo "<li>No Reports Found</li>";
				}
				?>
			</ul>
		</div>
		<?php echo $pagination; ?>
	</div>
</div>