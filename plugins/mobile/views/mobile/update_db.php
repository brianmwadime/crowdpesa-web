
//You can periodically check if the latest record ID matches the last ID pulled by your script, and if not, pull the new data. Example:

function updateView(id) {
	$.get("foo.php", {lastId: id }, function(response) {
			if(response != lastId) {

				// new entry in DB, do something special
				// and set lastId to the newly fetched ID
				lastId = id;
			}
		});
}

var i = setInterval(function() {
	updateView(id) }, 10000);
