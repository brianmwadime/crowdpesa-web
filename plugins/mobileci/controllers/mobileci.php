<?php defined('SYSPATH') or die('No direct script access.');
/**
 * PHP version 5
 * LICENSE: This source file is subject to LGPL license 
 * that is available through the world-wide-web at the following URI:
 * http://www.gnu.org/copyleft/lesser.html
 * @author     BeeBuy Team <info@beebuy.com> 
 * @package    CrowdPesa - http://crowdpesa.com
 * @module     Offerreports Controller
 * @copyright  CrowdPesa - http://crowdpesa.com
 * @license    http://www.gnu.org/copyleft/lesser.html GNU Lesser General Public License (LGPL) 
 */

class Mobileci_Controller extends Template_Controller {
	
	public $auto_render = TRUE;
	public $mobile = TRUE;
	public $user_name ='';
	// Cacheable Controller
	public $is_cachable = TRUE;
	protected $user;
	// Main template
    public $template = 'mobileci/layout';
    
	// Table Prefix
	protected $table_prefix;

    public function __construct()
    {
		parent::__construct();
		/***************************************************************/
		// Load database
		$this->db = new Database();
		
		$this->auth = new Auth();
		$this->session = Session::instance();
		$this->auth->auto_login();
		
		// Set Table Prefix
		$this->table_prefix = Kohana::config('database.default.table_prefix');
		
		// Set Table Prefix
		$this->table_prefix = Kohana::config('database.default.table_prefix');
		// Load Header & Footer
		$this->template->header  = new View('mobileci/header');
		$this->template->footer  = new View('mobileci/footer');

		$this->template->header->js  = "";
		$this->template->header->site_name = Kohana::config('settings.site_name');
		$this->template->header->site_tagline = Kohana::config('settings.site_tagline');
		
		// Google Analytics
		$google_analytics = Kohana::config('settings.google_analytics');
		$this->template->footer->google_analytics = $this->_google_analytics($google_analytics);
	}
	
	public function index()
	{
		
		$this->auth->auto_login();
		$form_error = FALSE;
		
		if ( !$this->auth->logged_in())
		{
			// If we aren't logged in, do some awesome login action
				
			url::redirect('mobileci/login');
				
		}else{
				
			// If we are logged in, show the checkin form
			
		$this->user = new User_Model($this->session->get('auth_user')->id);
		$this->template->content = new View('mobileci/main');
		$this->template->content->locations = $this->_get_locations();
		$this->template->content->form_error = $form_error;
		$this->template->content->user = $this->user;
		}
		
		
		

	}
	
	public function ci()
	{
		$this->template->content = new View('mobileci/main');
		
		// First, are we allowed to submit new checkins?
		if ( !Kohana::config('mobile.allow_checkins'))
		{
			url::redirect(url::site().'mobile');
		}
		
		$form = array
		(
			'latitude'	=> '',
			'longitude'	=> '',
			'location_id'   => '',
			'location_name'   => '',
			'message'   => '',
			
		);
		$errors = $form;
		$form_error = FALSE;
		
		if($_POST)
		{
			$post = Validation::factory($_POST)
			->pre_filter('trim', TRUE)
			->add_rules('latitude', 'between[-90,90]')
			->add_rules('location_id','numeric')
			->add_rules('longitude', 'between[-180,180]')
			->add_rules('location_name', 'length[3,200]')
			->add_rules('message', 'required');
			
			if(empty($_POST['message'])){
					$post->add_error('message','required');
				}
				
				/**if((!empty($_POST['location_id'])) AND !empty($_POST['location_name'])){
					$post->add_error('location_id', 'multiple');	
				}**/
				
			if ($post->validate())
			{
				$postdata = $post->safe_array();
				$lat = $postdata['latitude'];
				$lon = $postdata['longitude'];
				$location_id = $postdata['location_id'];
				$location_name = $postdata['location_name'];
				$message = $post['message'];
				$user_id = $this->auth->get_user()->id;
				
				// save the location
				if($location_id){
					
					$checkin = ORM::factory('checkin');
					$checkin->user_id = $user_id;
					$checkin->location_id = $location_id;
					$checkin->checkin_description = $message;
					
					$checkin->checkin_date = date("Y-m-d H:i:s",time());
					$checkin_id = $checkin->save();
					url::redirect('mobile/checkins');
					
				}elseif((($lat) AND ($lat)) AND $location_name){
					//FIRST, save location	
					$location = new Location_Model();
					$location->location_name = $location_name;
					$location->latitude = $lat;
					$location->longitude = $lon;
					$location->country_id = 115;
					$location->location_date = date("Y-m-d H:i:s",time());
					$newlocation_id = $location->save();
					
					// save checkin incident pending verification
					$checkinincident = new Incident_Model();
					
					$checkinincident->user_id = $user_id;
					$checkinincident->incident_title = $location_name;
					$checkinincident->incident_description = $message;
					$checkinincident->incident_active = 1;
					$checkinincident->location_id = $newlocation_id;
					$checkinincident->incident_zoom = Kohana::config('settings.default_zoom');
					$checkinincident->incident_date = date("Y-m-d H:i:s",time());
					$checkinincident->incident_dateadd = date("Y-m-d H:i:s",time());
					$checkinincident->save();
				
					// save the checkin
					$checkin = ORM::factory('checkin');
					$checkin->user_id = $user_id;
					$checkin->incident_id = $checkinincident->id;
					$checkin->location_id = $newlocation_id;
					$checkin->checkin_type = 0;
					$checkin->checkin_description = $message;
					$checkin->checkin_date = date("Y-m-d H:i:s",time());
					$checkin_id = $checkin->save();
					url::redirect('mobile/checkins');
					
					//save the category
					$category = ORM::factory('incident_category');
					$category->incident_id = $checkinincident->id;
					$category->save();
				}
				// No! We have validation errors, we need to show the form again, with the errors	
				}else{
				
				$form = arr::overwrite($form, $post->as_array());
				$errors = arr::overwrite($errors, $post->errors('mobile_checkin'));
				$form_error = TRUE;
			}
					
		}
			$this->user = new User_Model($this->session->get('auth_user')->id);
			$this->template->content->form = $form;
			$this->template->content->errors = $errors;
			$this->template->content->locations = $this->_get_locations();
			$this->template->content->form_error = $form_error;
			$this->template->content->user = $this->user;
		
	
	}
	
public function createaccount()
	{
		$form = array
		(
				'name'	=> '',
				'email'	=> '',
				'password'	=> '',
		);
		$errors = $form;
		$form_error =FALSE;
		if ($_POST AND isset($_POST["action"])
			AND $_POST["action"] == "createnew")
		{
			$post = Validation::factory($_POST)
					->pre_filter('trim')
					->add_rules('name', 'required')
					->add_rules('email', 'required', 'email')
					->add_rules('password', 'required');
			
			if ($post->validate())
			{
				$postdata_array = $post->safe_array();
								
				$email = $post['email'];
				if (valid::email($email) == false)
				{
					// Invalid Email, tell user
					$post->add_error('email', 'login error');
					$form_error = TRUE;
					$errors = arr::overwrite($errors, $post->errors('auth'));
					var_dump($errors);
				
				}else{
					
				try{
				$name = $postdata_array['name'];
				$email = $postdata_array['email'];
				$password = $postdata_array['password'];
				$user = ORM::factory('user');
				$user->name = $name;
				$user->email = $email;
				$user->username = $email;
				$user->password = $password;
				$user->add(ORM::factory('role', 'login'));
				$user_id = $user->save();
					    
				url::redirect('mobileci');
			}catch(Exception $e){

	            	$error_message = $e->getMessage();
	            	$post->add_error('password', 'User already exists');
	            	// repopulate the form fields
	            	$form = arr::overwrite($form, $post->as_array());
	            	
	            	// populate the error fields, if any
	            	// We need to already have created an error message file, for Kohana to use
	            	// Pass the error message file name to the errors() method
	            	$errors = arr::overwrite($errors, $post->errors('auth'));
	            	
	            	$form_error = TRUE;
	            
		}
				}
				}else{
				$form = arr::overwrite($form, $post->as_array());
				$errors = arr::overwrite($errors, $post->errors('auth'));
				$form_error = TRUE;
				
			}
								
			}

			$this->template->content  = new View('mobileci/createaccount');
			$this->template->content->errors = $errors;
			$this->template->content->form = $form;
			$this->template->content->form_error = $form_error;
		
	}
	
	public function logout(){
		$auth = new Auth;
		$auth->logout(TRUE);
		url::redirect("mobileci/login");
		
	}
	
	public function resetpassword(){
		$auth = Auth::instance();
		$form = array
		(
				'resetemail'	=> ''
		);		
		$errors = $form;
		$form_error =FALSE;
		
		if ($_POST AND isset($_POST["action"])AND $_POST["action"] == "forgot")
		{
		
			// START: Forgot Password Process
		
			$post = Validation::factory($_POST);
		
			//	Add some filters
			$post->pre_filter('trim', TRUE);
			$post->add_callbacks('resetemail', array($this,'email_exists_chk'));
				
			if ($post->validate())
			{   
				$user = ORM::factory('user',$post->resetemail);
		
				// Existing User??
				if ($user->loaded==true)
				{
		
					// Determine which reset method to use. The options are to use the RiverID server
					//   or to use the normal method which just resets the password locally.
					if (kohana::config('riverid.enable') == TRUE AND ! empty($user->riverid))
					{
						// Reset on RiverID Server
		
						$secret_link = url::site('login/index/'.$user->id.'/%token%?reset');
						$message = $this->_email_resetlink_message($user->name, $secret_link);
		
						$riverid = new RiverID;
						$riverid->email = $post->resetemail;
						$riverid->requestpassword($message);
					}
					else
					{
						// Reset locally
		
						// Secret consists of email and the last_login field.
						// So as soon as the user logs in again,
						// the reset link expires automatically.
						$secret = $auth->hash_password($user->email.$user->last_login);
						$secret_link = url::site('login/index/'.$user->id.'/'.$secret.'?reset');
						$this->_email_resetlink($post->resetemail,$user->name,$secret_link);
					}
		
					// Send Confirmation email
					$email_sent = $this->_send_email_confirmation($user);
		
					if ($email_sent == true)
					{
						url::redirect('/mobileci');
					}
					else
					{
						url::redirect('/mobileci');
					}
		
					$success = TRUE;
					$action = "";
				}
			}
			else
			{
				// repopulate the form fields
				$form = arr::overwrite($form, $post->as_array());
				
				// populate the error fields, if any
				$errors = arr::overwrite($errors, $post->errors('auth'));
				$form_error = TRUE;
			}
		
			// END: Forgot Password Process
		
		}
		$this->template->content  = new View('mobileci/reset_password');
		$this->template->content->errors = $errors;
		$this->template->content->form = $form;
		$this->template->content->form_error = $form_error;
	}
	
	/**
	* Checks if email address is associated with an account.
	* @param Validation $post $_POST variable with validation rules
	*/
	public function email_exists_chk( Validation $post )
	{
		$users = ORM::factory('user');
		if ($post->action == "new")
		{
			if (array_key_exists('email',$post->errors()))
			return;
	
			if ($users->email_exists( $post->email ) )
			$post->add_error('email','exists');
		}
		elseif($post->action == "forgot")
		{
			if (array_key_exists('resetemail',$post->errors()))
			return;
	
			if ( ! $users->email_exists( $post->resetemail ) )
			$post->add_error('resetemail','invalid');
		}
	}
	
	/**
	* Sends an email confirmation
	*/
	private function _send_email_confirmation($user)
	{
		$settings = kohana::config('settings');
	
		// Check if we require users to go through this process
		if ($settings['require_email_confirmation'] == 0)
		{
			return false;
		}
	
		$email = $user->email;
		$code = text::random('alnum', 20);
		$user->code = $code;
		$user->save();
	
		$url = url::site()."login/verify/?c=$code&e=$email";
	
		$to = $email;
		$from = array($settings['site_email'], $settings['site_name']);
		$subject = $settings['site_name'].' '.Kohana::lang('ui_main.login_signup_confirmation_subject');
		$message = Kohana::lang('ui_main.login_signup_confirmation_message',
		array($settings['site_name'], $url));
	
		email::send($to, $from, $subject, $message, FALSE);
	
		return true;
	}
	
	/**
	 * Email reset link to the user.
	 *
	 * @param the email address of the user requesting a password reset.
	 * @param the username of the user requesting a password reset.
	 * @param the new generated password.
	 *
	 * @return void.
	 */
	private function _email_resetlink( $email, $name, $secret_url )
	{
		$to = $email;
		$from = Kohana::lang('ui_admin.password_reset_from');
		$subject = Kohana::lang('ui_admin.password_reset_subject');
		$message = $this->_email_resetlink_message($name, $secret_url);
	
		//email details
		if( email::send( $to, $from, $subject, $message, FALSE ) == 1 )
		{
			return TRUE;
		}
		else
		{
			return FALSE;
		}
	
	}
	
	/**
	 * Generate the email message body that goes out to the user when a password is reset
	 *
	 * @param the username of the user requesting a password reset.
	 * @param the new generated password.
	 *
	 * @return void.
	 */
	private function _email_resetlink_message( $name, $secret_url )
	{
		$message = Kohana::lang('ui_admin.password_reset_message_line_1').' '.$name.",\n";
		$message .= Kohana::lang('ui_admin.password_reset_message_line_2').' '.$name.". ";
		$message .= Kohana::lang('ui_admin.password_reset_message_line_3')."\n\n";
		$message .= $secret_url."\n\n";
	
		return $message;
	
	}
	
	/*
    	* Retrieves locations
    	*/
	private function _get_locations()
    	{
		$limit = 15;
		$offset = 0;
		
		$lat =$this->session->get('map_center_lat');
		$lon =$this->session->get('map_center_lng');
		if(empty($lat) AND empty($lon)){
			$lat = Kohana::config('settings.default_lat');
		        $lon = Kohana::config('settings.default_lon');
		}
		//$max_radius = $this->session->get('map_radius');
		$max_radius = 0.005;
		$query = "SELECT id, location_name, latitude, longitude, ( 6371 * acos( cos( radians( $lat ) ) * cos( radians( latitude ) ) * cos( radians( longitude ) - radians( $lon ) ) + sin( radians( $lat ) ) * sin( radians( latitude ) ) ) ) AS distance FROM location WHERE longitude BETWEEN ( $lon -20 / abs( cos( radians( $lat ) ) *69 ) ) AND ( $lon +20 / abs( cos( radians( $lat ) ) *69 ) )
			AND latitude BETWEEN ( $lat - ( $max_radius / 69 ) ) AND ( $lat + ( $max_radius / 69 ) ) HAVING distance < $max_radius ORDER BY distance
			LIMIT $offset , $limit";
			//echo $query;exit;
			$locations = $this->db->query($query);
			$location_select = array();
    	                $location_select = array('' => 'Location');
			//var_dump($locations);
    			foreach ($locations as $location)
				{
					$location_select[$location->id] = $location->location_name;
				}
				$location_select[] = 'Let me specify';
	    	return $location_select;
    	}
	
	/*
	* Google Analytics
	* @param text mixed  Input google analytics web property ID.
    * @return mixed  Return google analytics HTML code.
	*/
	private function _google_analytics($google_analytics = false)
	{
		$html = "";
		if (!empty($google_analytics)) {
			$html = "<script type=\"text/javascript\">
				var gaJsHost = ((\"https:\" == document.location.protocol) ? \"https://ssl.\" : \"http://www.\");
				document.write(unescape(\"%3Cscript src='\" + gaJsHost + \"google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E\"));
				</script>
				<script type=\"text/javascript\">
				var pageTracker = _gat._getTracker(\"" . $google_analytics . "\");
				pageTracker._trackPageview();
				</script>";
		}
		return $html;
	}	
}