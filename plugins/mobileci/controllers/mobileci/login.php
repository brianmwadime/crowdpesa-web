<?php defined('SYSPATH') or die('No direct script access.');
/**
 * PHP version 5
 * LICENSE: This source file is subject to LGPL license 
 * that is available through the world-wide-web at the following URI:
 * http://www.gnu.org/copyleft/lesser.html
 * @author     BeeBuy Team <info@beebuy.com> 
 * @package    CrowdPesa - http://crowdpesa.com
 * @module     Offerreports Controller
 * @copyright  CrowdPesa - http://crowdpesa.com
 * @license    http://www.gnu.org/copyleft/lesser.html GNU Lesser General Public License (LGPL) 
 */

class Login_Controller extends Template_Controller {
	
	protected $user;
	// Main template
    public $template = 'mobileci/layout';
    
	// Table Prefix
	protected $table_prefix;

    public function __construct()
    {
		parent::__construct();
		/***************************************************************/
		// Load session
		$this->session = Session::instance();
		// Load Header & Footer
		$this->template->header  = new View('mobileci/header');
		$this->template->footer  = new View('mobileci/footer');
		
		$this->template->header->js  = "";
		$this->template->header->site_name = Kohana::config('settings.site_name');
		$this->template->header->site_tagline = Kohana::config('settings.site_tagline');
		
		// Google Analytics
		$google_analytics = Kohana::config('settings.google_analytics');
		$this->template->footer->google_analytics = $this->_google_analytics($google_analytics);
		}
	
	public function index()
	{
		if ((isset($_GET['redir']) || isset($_POST['redir'])) && !$this->session->get('login_redirect'))
		$this->session->set('login_redirect', $_SERVER['HTTP_REFERER']);
		if ( $this->auth->logged_in())
		{
			$this->user = new User_Model($_SESSION['auth_user']->id);
		}
		
		$auth = Auth::instance();
		// setup and initialize form field names
		$form = array
		(
							'username'	=> '',
							'password'	=> '',
		);
		$errors = $form;
		$success = FALSE;
		$form_error = FALSE;
		//	copy the form as errors, so the errors will be stored with keys corresponding to the form field names
				
		// Set messages to display on the login page for the user
		$message = FALSE;
		$message_class = 'login_error';
		
		if ($_POST AND isset($_POST["action"]) AND $_POST["action"] == "signin")
		{
			$post = Validation::factory($_POST);
			$post->pre_filter('trim');
			$post->add_rules('username', 'required');
			$post->add_rules('password', 'required');
				
			if ($post->validate())
			{
		
				$postdata_array = $post->safe_array();
				$valid_login = true;
				$post = Validation::factory($_POST);
				$name = $post['username'];
				$password = trim($post['password']);
		
				// Load the user
				$user = ORM::factory('user', $postdata_array['username']);
		
				$remember = (isset($post->remember))? TRUE : FALSE;
		
				// Allow a login with username or email address, but we need to figure out which is
				//   which so we can pass the appropriate variable on login. Mostly used for RiverID
		
				$email = $postdata_array['username'];
				if (valid::email($email) == false)
				{
					// Invalid Email, we need to grab it from the user account instead
		
					$email = $user->email;
					if (valid::email($email) == false AND kohana::config('riverid.enable') == true)
					{
						// We don't have any valid email for this user.
						// Only skip login if we are authenticating with RiverID.
						$valid_login = false;
					}
				}
						
				try{
						
					$login = $auth->login($user, $postdata_array['password'], $remember, $email);
															
					// Attempt a login
					if ($login && $valid_login)
					{
						// Action::user_login - User Logged In
						//url::redirect to page that broght you here in the first place;
						url::redirect($this->session->get_once('login_redirect', 'mobile'));
						
					}
					else
					{
						$post->add_error('password', 'login error');
						$form_error = TRUE;
						$errors = arr::overwrite($errors, $post->errors('auth'));
					}
				}catch(Exception $e){
						
					$error_message = $e->getMessage();
					$post->add_error('password', $error_message);
					// repopulate the form fields
					$form = arr::overwrite($form, $post->as_array());
						
					// populate the error fields, if any
					// We need to already have created an error message file, for Kohana to use
					// Pass the error message file name to the errors() method
					$errors = arr::overwrite($errors, $post->errors('auth'));
						
					$form_error = TRUE;
				}
			}else{
				$form = arr::overwrite($form, $post->as_array());
				$errors = arr::overwrite($errors, $post->errors('auth'));
				$form_error = TRUE;
				
			}
		}
			$this->template->content  = new View('mobileci/login');
			$this->template->content->errors = $errors;
			$this->template->content->form = $form;
			$this->template->content->form_error = $form_error;
	}
	
		/*
	* Google Analytics
	* @param text mixed  Input google analytics web property ID.
    * @return mixed  Return google analytics HTML code.
	*/
	private function _google_analytics($google_analytics = false)
	{
		$html = "";
		if (!empty($google_analytics)) {
			$html = "<script type=\"text/javascript\">
				var gaJsHost = ((\"https:\" == document.location.protocol) ? \"https://ssl.\" : \"http://www.\");
				document.write(unescape(\"%3Cscript src='\" + gaJsHost + \"google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E\"));
				</script>
				<script type=\"text/javascript\">
				var pageTracker = _gat._getTracker(\"" . $google_analytics . "\");
				pageTracker._trackPageview();
				</script>";
		}
		return $html;
	}	
}