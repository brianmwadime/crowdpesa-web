<?php

$lang = array
(
        'location_id' => array
	(
		'multiple'		=> 'Please either select a location from the dropdown or enter a location.'
        ),
	
	'checkin_error' => array
	(
		'required'		=> 'Checkin error please check your details.'
        ),
	
	'message' => array
	(
		'required'		=> 'The message field is required.'
	),
        
	'latitude' => array
	(
		'required'		=> 'The latitude field is required. Please click on the map to pinpoint a location.',
		'between' => 'The latitude field does not appear to contain a valid latitude?'
	),
	
	'longitude' => array
	(
		'required'		=> 'The longitude field is required. Please click on the map to pinpoint a location.',
		'between' => 'The longitude field does not appear to contain a valid longitude?'
	),
	
	'location_name' => array
	(
		'required'		=> 'The location name field is required.',
		'length'		=> 'The location name field must be at least 3 and no more 200 characters long.',
		'geocode'		=> 'The location cannot be found. Please add more details like city/town, country.'
	),
			
	'checkin_active' => array
	(
		'required'		=> 'Please enter a valid value for Approve This checkin',
		'between'		=> 'Please enter a valid value for Approve This checkin'
	),
	
	'checkin_verified' => array
	(
		'required'		=> 'Please enter a valid value for Verify This checkin',
		'between'		=> 'Please enter a valid value for Verify This checkin'
	)
);
?>