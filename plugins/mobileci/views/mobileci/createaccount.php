<?php			
if ($form_error) {
	 echo "<div class=\"login_error\">";
	foreach ($errors as $error_item => $error_description)
	{   
		if ($error_description)
		{
			echo '&#8226;&nbsp;'.$error_description.'<br />';
		}
	}
	echo "</div>";
}
	?>
	
<div class="signup-page">
  <div class="header">
    <h2>Create an account.</h2>
    <div class="hint">
     Already have an account? <a href="<?php echo url::site()?>mobileci/login">Sign in</a>
    </div>
  </div>
  <div class="body">
<form action="<?php echo url::site() ?>mobileci/createaccount" method="POST" name="frm_createaccount">
		<input type="hidden" name="action" value="createnew">
      <div style="margin:0;padding:0;display:inline"></div>
      <fieldset>
      
	 <div class="email ">
          <label><?php echo Kohana::lang('ui_main.name');?></label>
          <div class="input-wrapper">
            <input type="text" name="name" id="name" class="text-input" />
          </div>
        </div>
        <div class="email ">
          <label><?php echo Kohana::lang('ui_main.email');?></label>
          <div class="input-wrapper">
            <input type="text" name="email" id="email" class="text-input" />
          </div>
        </div>
        <div class="password ">
          <label><?php echo Kohana::lang('ui_main.password');?></label>
          <div class="input-wrapper">
            <input class="text-input" name="password" type="password" id="password" size="20" />
          </div>
         </fieldset>
      <span class="login-button"><input type="submit" id="submit" name="submit" value="Create Account" /></span>
    </form>
  </div>
</div>