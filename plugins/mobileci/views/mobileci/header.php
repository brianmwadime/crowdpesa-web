<!DOCTYPE html>
<html lang="en">
<html>
<head>
<meta http-equiv="Content-type" content="text/html; charset=utf-8"/>
<meta name="HandheldFriendly" content="True"/>
<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0"/>
<title><?php echo $site_name; ?></title>
<?php
echo html::script('plugins/mobileci/views/js/jquery', true);
echo html::script('plugins/mobileci/views/js/modernizr', true);
echo html::stylesheet('plugins/mobileci/views/css/style','screen');
?>
<script type="text/javascript">
<?php  echo $js; ?>
</script>
<script type="text/javascript">
$(function() {
	// Fill Latitude/Longitude with selected location
	$("#select_location").click(function() {
	  	//check if location is set and allow user to specify new location
		if($("#select_location option:selected").text() == "Let me specify" && $("#latitude").val() && $("#longitude").val() ){
		  $("#location_id").attr("value", "");
		  $("#ci-location").attr("style","display:block");
		}else{
		  var id = $(this).val();  
		  $("#location_id").attr("value", id);
		  $("#ci-location").attr("style","display:none");
		}
	});
		
});
</script>
</head>
<body>
<div id="Top"></div>
	<div id="container">
		<div id="header">
		<table id="top">
    <tbody><tr>
      <td class="left">
        <a href="<?php echo url::site() ?>mobile" class="brandmark">
         <img class="logo" src="<?php echo url::site() ?>plugins/mobile/views/images/logo.png"/><span class="logo"><?php echo $site_name; ?></span><br/>
        </a>
      </td>
      <td class="right">
        </td>
    </tr>
  </tbody>
		</table>
		</div>
		<div style="width:100%;">
		<table id="navigation">
			<tbody>
				<tr>
					<td class="home"><a class="" href="<?php echo url::site()."mobile"; ?>">Home</a></td>
					<td class="checkins"><a class="" href="<?php echo url::site()."mobile/checkins"; ?>">Timeline</a></td>
					<?php if ( !Auth::instance()->logged_in()) : ?>					
					<!-- If we aren't logged in, redirect to login page -->
					<td class="profile"><a class="" href="<?php echo url::site()."mobileci/login"; ?>">Sign In</a></td>
					<?php else : ?>
						<!--If logged in show profile page-->
					<td class="profile"><a class="" href="<?php echo url::site()."mobile/accounts"; ?>">My Profile</a></td>
					<?php endif; ?>
				</tr>
			</tbody>
			
		</table>
		</div>
	<div id="page">