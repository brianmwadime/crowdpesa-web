<?php			
if ($form_error) {
	echo "<div class=\"login_error\">";
	foreach ($errors as $error_item => $error_description)
	{
		if ($error_description)
		{
			echo '&#8226;&nbsp;'.$error_description.'<br />';
		}
	}
	echo "</div>";
}
?>

<div class="signup-page">
  <div class="header">
    <h2>Sign in</h2>
    <div class="hint">
     Dont have an account? <a href="<?php echo url::site()?>mobileci/createaccount">Sign up</a>
    </div>
  </div>
  <div class="body">
    <form action="<?php echo url::site() ?>mobileci/login" method="POST" name="frm_login" id="frm_login">
		<input type="hidden" name="action" value="signin">
      <div style="margin:0;padding:0;display:inline"></div>
      <fieldset>
        <div class="email ">
          <label><?php echo Kohana::lang('ui_main.email');?></label>
          <div class="input-wrapper">
            <input type="text" name="username" id="username" class="text-input" />
          </div>
        </div>
        <div class="password ">
          <label><?php echo Kohana::lang('ui_main.password');?></label>
          <div class="input-wrapper">
            <input class="text-input" name="password" type="password" id="password" size="20" />
          </div>
          <div class="remember"><input type="checkbox" id="remember" name="remember" value="1" checked="checked" /><br/>
			<?php echo Kohana::lang('ui_main.password_save');?></div>
        </div>
        </fieldset>
      <span class="login-button"><input type="submit" id="submit" name="submit" value="Log In" /></span>
    </form>
  </div>
</div>
