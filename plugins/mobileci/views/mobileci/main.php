<?php			
if ($form_error) {
	echo "<div class=\"login_error\">";
	foreach ($errors as $error_item => $error_description)
	{
		if ($error_description)
		{
			echo '&#8226;&nbsp;'.$error_description.'<br />';
		}
	}
	echo "</div>";
}
?>
<div>
	<!------CHANGING STUFF------------------------>
	<div class="signup-page">
  <div class="header">
    <h2>Checkin</h2>
    <div>Hello, <strong><?php echo $user->name; ?>!</strong></div>
	<div id="location_status">...</div>
  </div>
  <div class="body">
    <form action="<?php echo url::site() ?>mobileci/ci" method="post" id="checkinform">
		<input type="hidden" name="latitude" id="latitude" value="" />
		<input type="hidden" name="longitude" id="longitude" value="" />
		<input type="hidden" name="location_id" id="location_id" value="" />
      <div style="margin:0;padding:0;display:inline"></div>
      <fieldset>
        <div class="email ">
          <label><strong class="title">Select Your Location</strong></label>
          <div class="input-wrapper">
           	<?php print form::dropdown('select_location',$locations,'', ' class="text-input" '); ?>
          </div>
        </div>
        <div class="location" id="ci-location">
	<label><strong class="title">Please specify your location below.</strong></label>
        <div class="input-wrapper">
            <input type="text" name="location_name" id="location_name" value="" class="text-input" />
          </div>
	  </div>
	  <div class="password ">
	  <label><strong class="title"><?php echo Kohana::lang('ui_main.message');?> / Description&nbsp;&nbsp;&nbsp;*</strong></label>
          <div class="input-wrapper">
	  <input type="text" name="message" id="message" value="" class="text-input" /></div>
        </div>
        </fieldset>
      <span class="login-button"><input type="submit" id="submit" value="Check In" /></span>
      <span class="logout-button"><a href="<?php echo url::site() ?>mobileci/logout">Logout</span>
    </form>
  </div>
</div>
<?php echo html::script('plugins/mobileci/views/js/gears_init', true); ?>
<?php echo html::script('plugins/mobileci/views/js/geo', true); ?>
<script type="text/javascript">
	function geo_success(loc) {
		$("#location_status").html('Location Found: '+loc.coords.latitude+','+loc.coords.longitude);
		$("#citable").css('display','inline');
		$("#latitude").val(loc.coords.latitude);
		$("#longitude").val(loc.coords.longitude);
		//$("#ci-location").css('display','block');	
	}
	function geo_error() {
		$("#location_status").html('Sorry, location search failed. Please try again.');
		$("#ci-location").attr("style","display:none");
		disable_user();
	}
	function disable_user(){
		$("#select_location option").each(function(i){
			 if( $("#select_location option[value="+i+"]").text()=="Let me specify") $("#select_location option[value="+i+"]").remove();
		});
	}
	if (geo_position_js.init()) {
		$("#location_status").html('Please wait while we locate your device.');
		geo_position_js.getCurrentPosition(geo_success, geo_error);
		$("#ci-location").attr("style","display:none");
		
	}else{
		$("#location_status").html('Sorry, your browser does not support geolocation.');
		$("#ci-location").attr("style","display:none");
		disable_user();
	}
</script>