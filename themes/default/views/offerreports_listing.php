<?php
/**
 * View file for updating the offerreports display
 *
 * PHP version 5
 * LICENSE: This source file is subject to LGPL license 
 * that is available through the world-wide-web at the following URI:
 * http://www.gnu.org/copyleft/lesser.html
 * @author     Ushahidi Team - http://www.ushahidi.com
 * @package    CrowdPesa - http://crowdpesa.com
 * @copyright  CrowdPesa - http://crowdpesa.com
 * @license    http://www.gnu.org/copyleft/lesser.html GNU Lesser General Public License (LGPL) 
 */
?>
		<!-- Top offerreportbox section-->
		<div class="rb_nav-controls r-5">
			<table border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td>
						<ul class="link-toggle offerreport-list-toggle lt-icons-and-text">
							<li class="active"><a href="#rb_list-view" class="list"><?php echo Kohana::lang('ui_main.list'); ?></a></li>
							<li><a href="#rb_map-view" class="map"><?php echo Kohana::lang('ui_main.map'); ?></a></li>
						</ul>
					</td>
					<td><?php echo $pagination; ?></td>
					<td><?php echo $stats_breadcrumb; ?></td>
					<td class="last">
						<ul class="link-toggle lt-icons-only">
							<?php //@todo Toggle the status of these links depending on the current page ?>
							<li><a href="#" class="prev" id="page_<?php echo $previous_page; ?>"><?php echo Kohana::lang('ui_main.previous'); ?></a></li>
							<li><a href="#" class="next" id="page_<?php echo $next_page; ?>"><?php echo Kohana::lang('ui_main.next'); ?></a></li>
						</ul>
					</td>
				</tr>
			</table>
		</div>
		<!-- /Top offerreportbox section-->
		
		<!-- Offerreport listing -->
		<div class="r_cat_tooltip"><a href="#" class="r-3"></a></div>
		<div class="rb_list-and-map-box">
			<div id="rb_list-view">
			<?php
				foreach ($offerincidents as $offerincident)
				{
					$offerincident = ORM::factory('offerincident', $offerincident->offerincident_id);
					$offerincident_id = $offerincident->id;
					$offerincident_title = $offerincident->offerincident_title;
					$offerincident_description = $offerincident->offerincident_description;
					//$offerincident_offercategory = $offerincident->offerincident_offercategory;
					// Trim to 150 characters without cutting words
					// XXX: Perhaps delcare 150 as constant

					$offerincident_description = text::limit_chars(strip_tags($offerincident_description), 140, "...", true);
					$offerincident_date = date('H:i M d, Y', strtotime($offerincident->offerincident_date));
					//$offerincident_time = date('H:i', strtotime($offerincident->offerincident_date));
					$location_id = $offerincident->location_id;
					$location_name = $offerincident->location->location_name;
					$offerincident_verified = $offerincident->offerincident_verified;

					if ($offerincident_verified)
					{
						$offerincident_verified = '<span class="r_verified">'.Kohana::lang('ui_main.verified').'</span>';
						$offerincident_verified_class = "verified";
					}
					else
					{
						$offerincident_verified = '<span class="r_unverified">'.Kohana::lang('ui_main.unverified').'</span>';
						$offerincident_verified_class = "unverified";
					}

					$comment_count = $offerincident->comment->count();

					$offerincident_thumb = url::file_loc('img')."media/img/offerreport-thumb-default.jpg";
					$media = $offerincident->media;
					if ($media->count())
					{
						foreach ($media as $photo)
						{
							if ($photo->media_thumb)
							{ // Get the first thumb
								$offerincident_thumb = url::convert_uploaded_to_abs($photo->media_thumb);
								break;
							}
						}
					}
				?>
				<div id="<?php echo $offerincident_id ?>" class="rb_offerreport <?php echo $offerincident_verified_class; ?>">
					<div class="r_media">
						<p class="r_photo" style="text-align:center;"> <a href="<?php echo url::site(); ?>offerreports/view/<?php echo $offerincident_id; ?>">
							<img src="<?php echo $offerincident_thumb; ?>" style="max-width:89px;max-height:59px;" /> </a>
						</p>

						<!-- Only show this if the offerreport has a video -->
						<p class="r_video" style="display:none;"><a href="#"><?php echo Kohana::lang('ui_main.video'); ?></a></p>

						<!-- Offercategory Selector -->
						<div class="r_categories">
							<h4><?php echo Kohana::lang('ui_main.offercategories'); ?></h4>
							<?php foreach ($offerincident->offercategory as $offercategory): ?>
								
								<?php // Don't show hidden offercategories ?>
								<?php if($offercategory->offercategory_visible == 0) continue; ?>
						
								<?php if ($offercategory->offercategory_image_thumb): ?>
									<?php $offercategory_image = url::base().Kohana::config('upload.relative_directory')."/".$offercategory->offercategory_image_thumb; ?>
									<a class="r_category" href="<?php echo url::site(); ?>offerreports/?c=<?php echo $offercategory->id; ?>">
										<span class="r_cat-box"><img src="<?php echo $offercategory_image; ?>" height="16" width="16" /></span> 
										<span class="r_cat-desc"><?php echo $localized_offercategories[(string)$offercategory->offercategory_title];?></span>
									</a>
								<?php else:	?>
									<a class="r_category" href="<?php echo url::site(); ?>offerreports/?c=<?php echo $offercategory->id; ?>">
										<span class="r_cat-box" style="background-color:#<?php echo $offercategory->offercategory_color;?>;"></span> 
										<span class="r_cat-desc"><?php echo $localized_offercategories[(string)$offercategory->offercategory_title];?></span>
									</a>
								<?php endif; ?>
							<?php endforeach; ?>
						</div>
						<?php
						// Action::offerreport_extra_media - Add items to the offerreport list in the media section
						Event::run('ushahidi_action.offerreport_extra_media', $offerincident_id);
						?>
					</div>

					<div class="r_details">
						<h3><a class="r_title" href="<?php echo url::site(); ?>offerreports/view/<?php echo $offerincident_id; ?>">
								<?php echo $offerincident_title; ?>
							</a>
							<a href="<?php echo url::site(); ?>offerreports/view/<?php echo $offerincident_id; ?>#discussion" class="r_comments">
								<?php echo $comment_count; ?></a> 
								<?php echo $offerincident_verified; ?>
							</h3>
						<p class="r_date r-3 bottom-cap"><?php echo $offerincident_date; ?></p>
						<div class="r_description"> <?php echo $offerincident_description; ?>  
						  <a class="btn-show btn-more" href="#<?php echo $offerincident_id ?>"><?php echo Kohana::lang('ui_main.more_information'); ?> &raquo;</a> 
						  <a class="btn-show btn-less" href="#<?php echo $offerincident_id ?>">&laquo; <?php echo Kohana::lang('ui_main.less_information'); ?></a> 
						</div>
						<p class="r_location"><a href="<?php echo url::site(); ?>offerreports/?l=<?php echo $location_id; ?>"><?php echo $location_name; ?></a></p>
						<?php
						// Action::offerreport_extra_details - Add items to the offerreport list details section
						Event::run('ushahidi_action.offerreport_extra_details', $offerincident_id);
						?>
					</div>
				</div>
			<?php } ?>
			</div>
			<div id="rb_map-view" style="display:none; width: 590px; height: 384px; border:1px solid #CCCCCC; margin: 3px auto;">
			</div>
		</div>
		<!-- /Offerreport listing -->
		
		<!-- Bottom paginator -->
		<div class="rb_nav-controls r-5">
			<table border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td>
						<ul class="link-toggle offerreport-list-toggle lt-icons-and-text">
							<li class="active"><a href="#rb_list-view" class="list"><?php echo Kohana::lang('ui_main.list'); ?></a></li>
							<li><a href="#rb_map-view" class="map"><?php echo Kohana::lang('ui_main.map'); ?></a></li>
						</ul>
					</td>
					<td><?php echo $pagination; ?></td>
					<td><?php echo $stats_breadcrumb; ?></td>
					<td class="last">
						<ul class="link-toggle lt-icons-only">
							<?php //@todo Toggle the status of these links depending on the current page ?>
							<li><a href="#" class="prev" id="page_<?php echo $previous_page; ?>"><?php echo Kohana::lang('ui_main.previous'); ?></a></li>
							<li><a href="#" class="next" id="page_<?php echo $next_page; ?>"><?php echo Kohana::lang('ui_main.next'); ?></a></li>
						</ul>
					</td>
				</tr>
			</table>
		</div>
		<!-- /Bottom paginator -->
	        