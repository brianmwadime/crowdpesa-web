<?php blocks::open("news");?>
<?php //blocks::title(Kohana::lang('ui_main.news'));?>
      <h3><span><?php echo Kohana::lang('ui_main.news') ?></span><a class="more" href="<?php echo url::site() . 'blog'?>">View All</a></h3>
      <?php foreach ($articles as $article) : ?>
      <?php
	  	$article_title = $article->title;
		$article_intro = text::limit_chars($article->content, 150, ' ...', true);
        $article->image = ($article->image) ? $article->image : 'no_image_t.png';
		$article_image_url = url::site() . Kohana::config('upload.relative_directory') . '/' . $article->image;
		$article_url = url::site() . 'blog/article/' .  $article->id;
		$comments_url = $article_url . '/#commentarea';
		$article_comment_count = (isset($comment_count[$article->id])) ? $comment_count[$article->id] : 0;
		$article_comment_count = ($article_comment_count != 1) ? $article_comment_count . ' Reviews' : $article_comment_count . ' Review';
	  ?>
      <div class="post-<?php echo $article->id ?> post id="post_<?php echo $article->id ?>">
        <div class="post-content">
        <a href="<?php echo $article_url ?>" class="post_img">
        <img title="<?php echo $article_title ?>" alt="<?php echo $article_title ?>" src="<?php echo $article_image_url ?>" />
        </a>
          <div class="post_content">
            <h2><a href="<?php echo $article_url ?>" class="widget-title"><?php echo $article_title ?></a></h2>
            <div class="post_right">
            	<a class="pcomments" href="<?php echo $comments_url ?>"><?php echo $article_comment_count ?></a>
                <div id="fb-root"></div>
                <div style="width: 37%; margin-bottom: 4px; display: inline-block;">
                	<span class="sharetweet" id="tweet_offer_<?php echo $article->id ?>">
                    	<a href="https://twitter.com/share" class="twitter-share-button" data-text="<?php echo $article_title; ?>" data-url="<?php echo $article_url ?>" data-via="crowdpesa" data-count="none"></a>
                    </span>
                </div>
                <div style="width:28%;">
                	<span class="shareInterest" id="pinit_offer_<?php echo $article->id ?>">
                    	<a href="http://pinterest.com/pin/create/button/?url=<?php echo $article_url ?>" class="pin-it-button" count-layout="horizontal"><img border="0" src="//assets.pinterest.com/images/PinExt.png" title="Pin It" /></a>
                    </span>
                </div>
                <div style="width: 28%; display: inline-block; margin-top: 4px;">
                	<span class="sharelike" id="like_offer_<?php echo $article->id ?>">
                    	<div style="float: left;" class="fb-like" data-href="<?php echo $article_url ?>" data-layout="button_count" data-send="false" data-width="50" data-show-faces="false"></div>
                    </span>
                </div>
            </div>
            <p class="address"></p>
            <p><?php echo $article_intro ?><a class="read_more" href="<?php echo $article_url ?>">Read more</a></p>
          </div>
        </div>
      </div>
      <?php endforeach; ?>
<?php blocks::close();?>