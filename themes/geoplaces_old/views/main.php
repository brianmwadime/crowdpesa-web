<div class="top_banner_section">
  <div class="top_banner_section_in clearfix">
    <div id="timelineWrapper">
    <div id="timelineScrollUp" onmouseover="scroll_up();" onmouseout="stop_scroll();"></div>
    <div id="timelinecontainer">
    <div id="timelinescroller"><div id="timelinefeed"></div></div>
    </div>
    <div id="timelineScrollDown" onmouseover="scroll_down();" onmouseout="stop_scroll();"></div>
    </div>
    <div onclick="toggle();" class="toggleoff" id="toggle" title="Click to hide timeline" rel="tooltip"></div>
    <!-- Other content -->
	<?php								
	// Map and Timeline Blocks
	echo $div_map;
	echo $div_timeline;
	?>
    <script type="text/javascript">
		$(document).ready(function(){	
			setupTimelineMap();
			updateTimeline('');
			});
	</script>
  </div>
</div>
<div class="clear" id="container">
  <?php //get content blocks
        /* TODO:
         * 1) Get blocks setting to determine block order
         * 2) Strip the last _block from each saved item
         * 3) Prepend "block-" to each item
         * 4) Create a blocks array with the items
         * 5) Loop through each creating a $$ preg pattern of the form #\<li id="{{item}}"\>(.*)\<\/li\>#s and
         *    extracting it from all_blocks and previous_block (previously the block processed before this one)
         *    Example:
         *    
              $active_blocks = array_filter(explode("|", $active_blocks));
              $blocks = array();
              foreach($active_blocks as $block){
                    $item = str_ireplace('_block', '', $block);
                    $blocks[$block] = array('pattern' => '#\<li id="' . 'block-' . $item' . "\>(.*)\<\/li\>#s', 'position' => 'right', 'content' => '');  
              }
              
              
              
              preg_match_all($pattern_news, $all_blocks, $matches);
              $news_block = $matches[0][0];
              $news_block = str_ireplace('</div></li>', '', $news_block);
              $news_block = str_ireplace('<li id="block-news"><div>', '', $news_block);
         * 
         * 
         */
         
        ob_start();
        $pattern_latest_offers = '#\<li id="block-latest_offers"\>(.*)\<\/li\>#s';
        $pattern_latest_reviews = '#\<li id="block-latest_reviews"\>(.*)\<\/li\>#s';
        $pattern_news = '#\<li id="block-news"\>(.*)\<\/li\>#s';
        blocks::render();
        $all_blocks = ob_get_contents();
        ob_end_clean();
        
        // Get Active Blocks and extract based on that
        $settings = ORM::factory('settings', 1);
        $active_blocks = $settings->blocks;
        $active_blocks = array_filter(explode("|", $active_blocks));
        
        // get all other blocks except latest offers
        $other_blocks = preg_replace($pattern_latest_offers, '', $all_blocks);
        $other_blocks = preg_replace($pattern_latest_reviews, '', $other_blocks);
        // extract in reverse order as first pattern matches everything to the last block
        // first extract news block
        preg_match_all($pattern_news, $all_blocks, $matches);
        $news_block = $matches[0][0];
        $news_block = str_ireplace('</div></li>', '', $news_block);
        $news_block = str_ireplace('<li id="block-news"><div>', '', $news_block);
        // extract reviews block
        preg_match_all($pattern_latest_reviews, $all_blocks, $matches);
        $latest_reviews = $matches[0][0];
        $latest_reviews = preg_replace($pattern_news, '', $latest_reviews);
        $latest_reviews = str_ireplace('</div></li>', '', $latest_reviews);
        $latest_reviews = str_ireplace('<li id="block-latest_reviews"><div>', '', $latest_reviews);
        // extract latest offers by removing every other blog in the matched result
        preg_match_all($pattern_latest_offers, $all_blocks, $matches);
        $latest_offers = $matches[0][0];
        $latest_offers = preg_replace($pattern_latest_reviews, '', $latest_offers);
        $latest_offers = str_ireplace('</div></li>', '', $latest_offers);
        $latest_offers = str_ireplace('<li id="block-latest_offers"><div>', '', $latest_offers);
    ?>
  
  <div class="content left">
  <?php if($context == 'all'): ?>
    <!-- content blocks -->
	<div class="content-blocks clearingfix">
		<ul>
			<?php echo $other_blocks ?>
		</ul>
	</div>
	<!-- /content blocks -->
    
    <div class="list clear" id="loop">
      		<?php echo $news_block ?>
    </div>
    
    <!--  CONTENT AREA END --> 
  
  <?php else:?>
    <div class="list clear" id="loop">
          <?php echo $listings;?>
          <div class="pagination" ><?php echo $pagination;?></div>
    </div>
  <?php endif;?>
  </div>
  <div class="sidebar right right_col">
  	<?php echo $latest_offers ?>
    
    <div class="widget recent_comments_section">
      <?php echo $latest_reviews ?>
    </div>
    
    <?php /*<div class="widget advt_widget"> <a href="#"><img alt="" src="<?php echo url::site()?>themes/geoplaces/images/dummy_pix/advt300x250px.jpg"></a> </div>*/?>
  </div>
</div>
