<div class="clear" id="container">
<div class="content left">
			<div class="content-title" id="loop">
				<h3><?php echo ucfirst($user->name).' Profile Page' ; ?></h3></div>

 <div class="cat_desc">
	 <ul class="sort_by">
   
    <li <?php if ($status == 'p') echo "class=\"current\""; ?>> <a href="<?php echo url::site().'profile/user/'.$user->username.'?status=p' ?>" ><?php echo Kohana::lang('ui_main.places') ?></a></li>
    <li  <?php if ($status == 'o') echo "class=\"current\""; ?>> <a href="<?php echo url::site().'profile/user/'.$user->username.'?status=o'?>"><?php echo Kohana::lang('ui_main.offers') ?></a></li>
    
</ul>
 </div>

						<div class="list clear work_list ">
								<?php	
							if($status == 'o'){	
								foreach ($offers as $offer)
								{
									$offerincident_id = $offer->id;
									$offerincident_title = strip_tags($offer->offerincident_title);
									$offerincident_description = text::limit_chars(strip_tags($offer->offerincident_description), 150, "...", true);
									$offer_coupon = $offer->offer_coupon;
									$date_added = $offer->offerincident_dateadd;
									$total_price = $offer->offerincident_price;
                                    $offer->media_thumb = ($offer->media_thumb) ? $offer->media_thumb : 'no_image_t.png';
									$offer_image_url = url::site() . Kohana::config('upload.relative_directory') . '/' . $offer->media_thumb;
									
			
									?>
									
										<!--  Post Content Condition for Post Format-->
       <div id="post_2166" class="post event type-event status-publish hentry post">
       <div class="post-content ">
	   					<a  class="post_img" href="<?php echo url::site() . 'offer/details/' . $offerincident_id; ?>"> <img src="<?php echo $offer_image_url ?>" alt="<?php echo $offerincident_title; ?>" title="<?php echo $offerincident_title; ?>"  /> </a>
            
            <div class="post_content">
         			<!--  Post Title Condition for Post Format-->
           <h2><a href="<?php echo url::site() . 'offer/details/' . $offerincident_id; ?>">
      <?php echo $offerincident_title; ?>  </a></h2>
            <!--  Post Title Condition for Post Format-->
							<p class="timing"> <span>Order Date:</span> <?php echo $date_added;?></p>
				<div class="post_right">
				
					<a href="<?php echo url::site() . 'offer/details/' . $offerincident_id; ?>" class="pcomments" >View Offer</a>
                    
				</div>
				
				 <p><span>Value:</span> <?php echo $total_price;?></p>
                 <p><span>Coupon:</span> <?php echo $offer_coupon;?></p>
       
				<p> <?php echo $offerincident_description; ?></p>					 
        </div>
		 
		 
     </div></div>
 		
													
			<?php
                                }?>
                                <div  class="pagination" ><?php echo $offers_pagination;?></div>        
       <div class="hr clearfix"></div>  
            <?php }else if($status == 'p'){
				foreach ($incidents as $incident)
				{
					$incident_id = $incident->id;
					$incident_title = strip_tags($incident->incident_title);
					$incident_description = text::limit_chars(strip_tags($incident->incident_description), 150, "...", true);
					$date_added = $incident->incident_dateadd;
                    $incident->media_thumb = ($incident->media_thumb) ? $incident->media_thumb : 'no_image_t.png';
					$incident_image_url = url::site() . Kohana::config('upload.relative_directory') . '/' . $incident->media_thumb;

			?>
	     <div id="post_2166" class="post event type-event status-publish hentry post">
					<div class="post-content ">
							<a  class="post_img" href="<?php echo url::site() . 'listing/details/' . $incident_id; ?>"> <img src="<?php echo $incident_image_url ?>" alt="<?php echo $incident_title; ?>" title="<?php echo $incident_title; ?>"  /> </a>
							<div class="post_content">
										<h2><a href="<?php echo url::site() . 'listing/details/' . $incident_id; ?>"><?php echo $incident_title; ?>  </a></h2>
										<p class="timing"> <span>Added On:</span> <?php echo $date_added;?></p>
										<div class="post_right">				
											<a href="<?php echo url::site() . 'listing/details/' . $incident_id; ?>" class="pcomments" >View Place</a>
										</div>				
										<p><span>Description:</span><br /><?php echo $incident_description; ?></p>					 
								</div>
					</div>
			 </div>
									<?php
								}?>
								<div class="hr clearfix"></div>         
            <div  class="pagination" ><?php echo $incidents_pagination;?></div>
								<?php }
								?>
								
						</div>
					</div>
  <div class="sidebar right right_col">
    <?php // Load map here ?>
    <div class="company_info"> <a class="i_claim" title="Samsung" href="<?php echo url::site() . 'profile/user/' . $user->username ?>"><?php echo $user->name; ?></a>
      <p> <span class="i_location">Address:</span> <?php echo $retailer_info->address ?></p>
      <p> <span class="i_website"><a target="blank" href="<?php echo $retailer_info->website ?>"><strong>Website</strong></a> </span> </p>
      <p> <span class="i_time"> Time: </span> <?php echo $retailer_info->opening_hours ?></p>
      <p> <span class="i_contact">Phone: </span> <?php echo $retailer_info->phone ?></p>
			<p> <span class="i_contact">Email: </span> <?php echo $user->email ?></p>
    </div>
  
  </div>
</div>
