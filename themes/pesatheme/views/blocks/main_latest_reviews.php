<?php blocks::open("latest_reviews");?>
<?php //blocks::title(Kohana::lang('ui_main.reviews_latest'));?>
<?php if (count($reviews)): ?>
  <div class="widget recent_comments_section">
  <h3><?php echo Kohana::lang('ui_main.reviews_latest') ?></h3>
  <ul class="recent_comments">
    <?php foreach ($reviews as $review)
    {
        $offer_title = text::limit_chars($review->comment_title, 40, '...', true);
        $comment_date = 'reviewed on ' . date('j M Y \a\t g:m a', strtotime($review->comment_date));
		$comment_author = $review->comment_author;
        // TODO: Check if user wants their profile to be publicly visible before generating the profile url
		$author_profile = ($review->user_id != 0) ? url::site() . 'profile/user/' . $review->comment_author : '#'; 
		$comment_rating = $review->comment_rating;
		$comment = $review->comment_description;
		$comment_url = url::site() . 'offer/details/' .  $review->offer_id . '/#comment-' . $review->comment_id;
    ?>
    <li class="clearfix">
      <p>
      <span> <a href="<?php echo $comment_url ?>" title="<?php echo $comment_author ?> | <?php echo $offer_title ?>"><?php echo $offer_title ?></a> by <a href="<?php echo $author_profile ?>" title="<?php echo $comment_author ?>"><?php echo $comment_author ?></a></span> 
		<span class="rating"><?php echo $comment_date; ?>
		<?php for ($i = 1; $i <= 5; $i++):?>
            <?php if ($comment_rating >= $i): ?>
            <img alt="* " src="<?php echo url::site()?>themes/pesatheme/images/dummy_pix/rating_on.png" />
            <?php else: ?>
            <img alt="" src="<?php echo url::site()?>themes/pesatheme/images/dummy_pix/rating_off.png" />
            <?php endif; ?>
        <?php endfor; ?>
        </span>
      </p>
      <p><?php echo $comment ?></p>
    </li>
    <?php 
	}
	?>
  </ul>
  </div>
<?php endif; ?>
<?php blocks::close();?>
