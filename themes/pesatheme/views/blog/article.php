<div class="clear" id="container">
  <div class="content left">
    <div class="blog-article list clear" id="loop">
      <?php
	  	$article_title = $article->title;
		$article_author = 'written by <strong>' . $article_author . '</strong>';
		$article_published = 'on ' . date('j M Y \a\t g:m a', strtotime($article->published_date));
		$article_intro = text::limit_chars($article->content, 130, ' ...', true);
		$article_body = $article->content;
		$article_image_url = url::site() . Kohana::config('upload.relative_directory') . '/' . $article->image;
		$article_url = url::site() . 'blog/article/' .  $article->id;
		$comments_url = $article_url . '/#commentarea';
		$tags = array();
		foreach ($article_tags as $tag)
			$tags[] = ($tag != 'None') ?'<a href="' . url::site() . 'blog/tag/' . $tag . '">' . $tag . '</a>' : $tag;
		$article_comment_count = (isset($comment_count[$article->id])) ? $comment_count[$article->id] : 0;
		$article_comment_count = ($article_comment_count != 1) ? $article_comment_count . ' Reviews' : $article_comment_count . ' Review';
	  ?>
      <h3><span><?php echo $article_title ?></span></h3>
      <div class="blog-share">
        <div style="width: 37%; margin-bottom: 4px; display: inline-block;"> <span class="sharetweet" id="tweet_offer_<?php echo $article->id ?>"> <a href="https://twitter.com/share" class="twitter-share-button" data-text="<?php echo $article_title; ?>" data-url="<?php echo $article_url ?>" data-via="crowdpesa" data-count="none"></a> </span> </div>
        <div style="width:28%;"> <span class="shareInterest" id="pinit_offer_<?php echo $article->id ?>"> <a href="http://pinterest.com/pin/create/button/?url=<?php echo $article_url ?>" class="pin-it-button" count-layout="horizontal"><img border="0" src="//assets.pinterest.com/images/PinExt.png" title="Pin It" /></a> </span> </div>
        <div style="width: 28%; display: inline-block; margin-top: 4px;"> <span class="sharelike" id="like_offer_<?php echo $article->id ?>">
          <div style="float: left;" class="fb-like" data-href="<?php echo $article_url ?>" data-layout="button_count" data-send="false" data-width="50" data-show-faces="false"></div>
          </span> </div>
      </div>
      <div class="post-<?php echo $article->id ?> article-view-post post" id="post_<?php echo $article->id ?>">
        <?php /*?>Article metadata section<?php */?>
        <div class="post-content"> <a href="<?php echo $article_url ?>" class="post_img"> <img title="<?php echo $article_title ?>" alt="<?php echo $article_title ?>" src="<?php echo $article_image_url ?>" /> </a>
          <div class="post_content">
            <p class="article-date-author"><?php echo $article_author ?> <?php echo $article_published ?></p>
            <p><strong>Average user rating:</strong>
                <span class="rating">
                <?php for ($i = 1; $i <= 5; $i++):?>
                    <?php if ($article_rating >= $i): ?>
                    <img alt="* " src="<?php echo url::site()?>themes/pesatheme/images/dummy_pix/rating_on.png" />
                    <?php else: ?>
                    <img alt="" src="<?php echo url::site()?>themes/pesatheme/images/dummy_pix/rating_off.png" />
                    <?php endif; ?>
                <?php endfor; ?>
                </span>
            </p>
            <p><strong>Tags:</strong>&nbsp;<?php echo implode(', ', $tags) ?></p>
          </div>
        </div>
        
      </div>
      <?php /*?>Article content section<?php */?>
      <div class="post-content">
            <?php echo $article_body ?>
       </div>
       <div class="article-comments">
       	 <?php echo $comments; ?>
         <?php echo $comments_form; ?>
       </div>
    </div>
  </div>
  <div class="sidebar right right_col">
    <?php /* <div class="widget advt_widget">
    	<a href="#"><img style="background-color: #fff; padding:8px; border: 1px solid #ccc;" alt="" src="<?php //echo url::site()?>http://www.dummyimage.com/290x240/dddddd/ffffff.gif&text=Advertise+here"></a>
  	</div> */ ?>
  	<?php blocks::render_position('right', $this_page) ?>
    <?php if (count($similar_articles)): ?>
    <h3><?php echo Kohana::lang('ui_main.similar_articles'); ?></h3>
    <ul>
    	<?php foreach ($similar_articles as $article): ?>
        <li><a href="<?php echo url::site() . '/blog/article/' . $article->article_id ?>"><?php echo $article->title ?></a></li>
        <?php endforeach; ?>
    </ul>
    <?php endif; ?>
  </div>
</div>
