<!-- Continuation of wrapper div -->    
    <div class="bottom">
    	<div class="footer tour_8">
    		<div class="footer_in">
    			<p class="copyright">
    				&copy; <?php echo date("Y") ?>
    				<a href="<?php echo url::site(); ?>"> CrowdPesa.com</a>. All rights reserved. <a href="http://www.pesapay.com/" class="footer-link">PesaPay Inc</a> <a href="#" class="footer-link">Terms & Conditions</a> | <a href="#" class="footer-link">Privacy</a><br /><b>Integrations:</b> <a href="http://www.ushahidi.com/" class="footer-link">Ushahidi</a> | <a href="http://www.whive.com/" class="footer-link">Whive</a> <b>Payments:</b> <a href="http://www.PesaPay.com/" class="footer-link">Pesapay</a> | <a href="http://www.stripe.com/" class="footer-link">Stripe</a> | <a href="http://www.paypal.com/" class="footer-link">Paypal</a> | <a href="http://www.2checkout.com/" class="footer-link">2CO</a>
    			</p>
    		</div>
    	</div>
    </div>
</div><!-- End of wrapper div -->
<script type="text/javascript">var _kiq = _kiq || [];</script>
<script type="text/javascript" src="//s3.amazonaws.com/ki.js/39940/7Ht.js" async="true"></script>
<?php if (Auth::instance()->logged_in()): ?>
<script type="text/javascript" charset="utf-8">
_kiq.push(['identify', '<?php echo Auth::instance()->get_user()->username ?>']);
</script>
<?php endif; ?>

<div id="fb-root"></div>
<script type="text/javascript">
    window.fbAsyncInit = function() {
        FB.init({
            appId: '289525864457161',
            status: true,
            cookie: true,
            xfbml: true
        });
    };
    (function() {
        var e = document.createElement('script'); e.async = true;
        e.src = document.location.protocol +
            '//connect.facebook.net/en_US/all.js';
        document.getElementById('fb-root').appendChild(e);
    }());
</script>
<script type="text/javascript" src="//assets.pinterest.com/js/pinit.js"></script>
<script>
    !function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0]; 
    if(!d.getElementById(id)) {
            js=d.createElement(s); js.id=id; 
            js.src="//platform.twitter.com/widgets.js";
            fjs.parentNode.insertBefore(js,fjs);}
    }
    (document,"script", "twitter-wjs");
</script>
</body>

