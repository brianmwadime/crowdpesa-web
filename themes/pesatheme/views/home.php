<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title><?php echo Kohana::lang('ui_main.pricing');?></title>
<?php
// echo html::stylesheet(url::file_loc('css').'media/css/jquery-ui-themeroller', '', true);
echo html::stylesheet(url::file_loc('css').'media/css/home.min.css', '', true);
echo html::stylesheet(url::file_loc('css').'media/css/pricing_style.css', '', true);
//echo html::stylesheet(url::file_loc('css').'media/css/global', '', true);
//echo html::script(url::file_loc('js').'media/js/jquery', true);
// echo html::script(url::file_loc('js').'media/js/openid/openid-jquery', true);
// echo html::script(url::file_loc('js').'media/js/openid/openid-jquery-en', true);
// echo html::script(url::file_loc('js').'media/js/global', true);
?>

</head>

<body>

<?php echo $header_nav; ?>

<div id="wrapper">
<div class="ribbon icons top">
	<div class="container">
		<div class="row">
			<div id="home-headers" class="span9" style="margin-top:-8px;">
				<h1 id="logo-large" style="margin-top: .15em;">Geoloqi</h1>
				<h2 class="hide">crowdpesa.com , Whats hot near you?</h2>

        <div class="partner center-text clearfix well" style="margin-top:4px; position:relative; padding-top:20px; padding-bottom:10px;">
          <h2 style="text-shadow:#fff;position:relative; font-size:27px; z-index:1; margin-bottom:10px;">
            <a style="color:#006699;" href="#">crowdpesa.com is Here!</a>
          </h2>
            <img src="<?php echo url::site().'media/images/merchants_banner.png'?>" alt="" style="margin-top:-60px; border-radius:2px;" width="486" height="auto">
          
          <a href="#" class="btn primary" id="learn-more" style="position:absolute;bottom:30px;">Learn More</a>
        </div>
			</div>
      <style type="text/css">
      #home-audiences article.dev {
        padding-bottom:20px;
      }
        #home-audiences h2 {
          font-size:24px;
          line-height:1.5em; 
        }
      </style>
			<div class="span6 offset1 push-up">
				<div id="home-audiences" class="well article-list">
					<div class="dev clearfix">
						<h2 class="compress"><a href="#">Merchants</a></h2>
						<p>Lorem ipsum.</p>
					</div>
					<div class="partner clearfix">
						<h2 class="compress"><a href="#">Pricing</a></h2>
						<p>Lorem Ipsum.</p>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!--END SLIDER-->
<!--LEADING TEXT-->
  <div class="container">
    <h1 class="leading center-text">A powerful platform for CrowdSourcing.</h1>
  </div>
<!---->
<!--Call to Action-->
<div class="MainAction_call"> <p class="SevenColumns">Reach a larger audience through crowdsourcing.</p> 
		  
            <div class="FiveColumns  Right"><p class="LearnMore"> <a href="#" class="MainSignup_button" title="Sign Up now">Sign Up Now</a><span class="Normal">or </span> <a href="#" class="LearnMore_home">Learn More</a></p></div>         
		 <div class="clear"></div> </div>
<!---->
<!--INNER DETAILS-->
<div id="inner-details">
    <!--ODD-->
<div class="ribbon light">
  <div class="container">
    <div class="row">

      <div class="span8">
        <header>
          <h2><a href="#">Real-time Location Tracking</a></h2>
        </header>
        <p>Whether you're making an application for tracking assets, finding friends nearby, or setting up a location-based game, Geoloqi makes it simple to implement full-featured tracking systems with minimal time and effort.</p>
        <h6>Location Tracking Features</h6>
        <ul class="push-down">
        <li>Track Location in Real-time</li>
        <li>Battery Safe Tracking </li>
        <li>Customizable Tracking Rates</li>
        <li>Works Indoors and Outdoors </li>
        <li>SDKs for iOS, Android, Javascript, Ruby, PHP and more</li>
        </ul>
        <a href="#" class="btn primary large">Pricing/Sign Up</a>
        <a href="#" class="btn info large">Learn More</a>
      </div>

      <span class="span8">
        <img src="<?php echo url::site().'media/images/message.png'?>" alt="crowdpesa" width="400" height="400" style="margin:5px 0 5px 40px;">
      </span>

    </div>
  </div>
</div>
    <!-- ODD  -->
<div class="ribbon bg light">
  <div class="container">
    <div class="row">
      <span class="span8">
        <img src="<?php echo url::site().'media/images/message.png'?>" alt="Messaging Diagram" width="400" height="400" style="margin:5px 0 5px;">
      </span>

      <div class="span8">
        <header>
          <h2><a href="#">Location-Based Messaging</a></h2>
        </header>
        <p>Send messages to your customers across a variety of channels with Geoloqi's location-based messaging tools. Messages can be sent upon arriving, dwelling or leaving a place, as well as time or day or a user's speed.</p>
        <h6>Messaging Features</h6>
        <ul class="push-down">
        <li>Carrier Agnostic - Works with any carrier</li>
        <li>Battery Safe - minimal effect on battery life</li>
        <li>Push Notifications for iOS and Android, SMS and Email for all devices</li>
        </ul>
        <a href="#" class="btn primary large">Pricing/Sign Up</a>
        <a href="#" class="btn info large">Learn More</a>
      </div>
    </div>
  </div>
</div>
<!--END INNER SECTION-->
<!--Call to Action-->
<div class="MainAction_call"> <p class="SevenColumns">Reach a larger audience through crowdsourcing.</p> 
		  
            <div class="FiveColumns  Right"><p class="LearnMore"> <a href="#" class="MainSignup_button" title="Sign Up now">Sign Up Now</a><span class="Normal">or </span> <a href="#" class="LearnMore_home">Learn More</a></p></div>         
		  <div class="clear"></div></div>
<!---->
    </div>
    </div>
</div>
    
</body>
</html>