<?php if ($context == 'all'): ?>

<div class="category_legend" style="position: absolute;" left:10px; top: 100px;>
    <div style="font-weight: bold"></div>
    <img class="cat_color animated" id="cat-color-you" data-cat="You" src="<?php echo url::site() ?>media/markers/location.png" />
    <?php foreach ($categories as $id => $attribs): ?>
    <img class="cat_color animated" id="cat-color-<?php echo strtolower($attribs[0]) ?>" data-cat="<?php echo $attribs[0] ?>" src="<?php echo "http://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=%E2%80%A2|" . $attribs[1] ?>" />
    <?php endforeach; ?>
    <div class="cat_desc animated right in" style="display: none"><div class="tooltip-arrow"></div><div class="tooltip-inner"></div></div>
</div>

<?php endif; ?>

<div class="top_banner_section">
  <?php if ($context == 'all'): ?>
  <div class="top_banner_section_in clearfix" align="center" style="border: medium none; background: none; margin-top: -20px; font-size: 28px;">Discover, explore and locate hot offers near you today!</div>
  <?php else: ?>
  <div class="top_banner_section_in clearfix" align="center" style="border: medium none; background: none; margin-top: -20px; font-size: 28px;">Discover, explore and locate <?php echo ucwords(strtolower(($category_title))); ?> <?php echo Inflector::singular(Inflector::plural(Kohana::lang('ui_main.'. $title))) ?> near you.</div>
  <?php endif; ?>

  <div class="top_banner_section_in clearfix">
    <div id="timelineWrapper">
	    <div id="timelineScrollUp" onmouseover="scroll_up();" onmouseout="stop_scroll();"></div>
	    <div id="timelinecontainer" class="tour_5">
	    	<div id="timelinescroller"><div id="timelinefeed"></div></div>
	    </div>
	    <div id="timelineScrollDown" onmouseover="scroll_down();" onmouseout="stop_scroll();"></div>
    </div>

    <div onclick="toggle();" class="toggleoff" id="toggle" title="Click to hide timeline" rel="tooltip"></div>
    <?php                               

    // Map and Timeline Blocks
    echo $div_map;
    echo $div_timeline;
    ?>

    <script type="text/javascript">
        $(document).ready(function(){   
            setupTimelineMap();
            updateTimeline('');
            });
    </script>
  </div>

</div>

<div class="clear" id="container">
    <div class="content left tour_6">
    <?php if($context == 'all'): ?>
            <?php blocks::render_position('body', $this_page) ?>
    <?php else:?>
        <div class="list clear" id="loop">
            <?php echo $listings;?>
            <div class="pagination" ><?php echo $pagination;?></div>
        </div>
    <?php endif;?>
    </div>

    <div class="sidebar right right_col tour_7">
        <?php blocks::render_position('right', $this_page) ?>
    </div>
</div>