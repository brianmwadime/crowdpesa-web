<div class="clear" id="container">
    <div class="content" style="width:100%">
		<div id="main" class="report_detail">

			<div class="left-col">

			  <?php
				  if ($offerincident_verified)
					{
						echo '<p class="r_verified">'.Kohana::lang('ui_main.verified').'</p>';
					}
					else
					{
						echo '<p class="r_unverified">'.Kohana::lang('ui_main.unverified').'</p>';
					}
			  ?>

				<h1 class="report-title"><?php
					echo $offerincident_title;

					// If Admin is Logged In - Allow For Edit Link
					if ($logged_in)
					{
						echo " [&nbsp;<a href=\"".url::site()."admin/offerreports/edit/".$offerincident_id."\">".Kohana::lang('ui_main.edit')."</a>&nbsp;]";
					}
				?></h1>

				<p class="report-when-where">
					<span class="r_date"><?php echo $offerincident_time.' '.$offerincident_date; ?> </span>
					<span class="r_location"><?php echo $offerincident_location; ?></span>
					<?php Event::run('ushahidi_action.offerreport_meta_after_time', $offerincident_id); ?>
				</p>

				<div class="report-category-list">
				<p>
					<?php
						foreach($offerincident_offercategory as $offercategory)
						{

							// don't show hidden categoies
							if($offercategory->offercategory->offercategory_visible == 0)
							{
								continue;
							}

						  if ($offercategory->offercategory->offercategory_image_thumb)
							{
							?>
							<a href="<?php echo url::site()."offerreports/?c=".$offercategory->offercategory->id; ?>"><span class="r_cat-box" style="background:transparent url(<?php echo url::base().Kohana::config('upload.relative_directory')."/".$offercategory->offercategory->offercategory_image_thumb; ?>) 0 0 no-repeat;">&nbsp;</span> <?php echo $offercategory->offercategory->offercategory_title; ?></a>

							<?php
							}
							else
							{
							?>
							  <a href="<?php echo url::site()."offerreports/?c=".$offercategory->offercategory->id; ?>"><span class="r_cat-box" style="background-color:#<?php echo $offercategory->offercategory->offercategory_color; ?>">&nbsp;</span> <?php echo $offercategory->offercategory->offercategory_title; ?></a>
						  <?php
						  }
						}
					?>
					</p>
					<?php
					// Action::offerreport_meta - Add Items to the Offerreport Meta (Location/Date/Time etc.)
					Event::run('ushahidi_action.offerreport_meta', $offerincident_id);
					?>
				</div>

				<?php
				// Action::offerreport_display_media - Add content just above media section
				Event::run('ushahidi_action.offerreport_display_media', $offerincident_id);
				?>

				<!-- start offerreport media -->
				<div class="<?php if( count($offerincident_photos) > 0 || count($offerincident_videos) > 0){ echo "offerreport-media";}?>">
				<?php
				// if there are images, show them
				if( count($offerincident_photos) > 0 )
				{
					echo '<div id="offerreport-images">';
					foreach ($offerincident_photos as $photo)
					{
						echo '<a class="photothumb" rel="lightbox-group1" href="'.$photo['large'].'"><img src="'.$photo['thumb'].'"/></a> ';
					};
					echo '</div>';
				}

				// if there are videos, show those too
				if( count($offerincident_videos) > 0 )
				{
				  echo '<div id="offerreport-video"><ol>';

				  // embed the video codes
				  foreach( $offerincident_videos as $offerincident_video)
				  {
					echo '<li>';
					$videos_embed->embed($offerincident_video,'');
					echo '</li>';
				  };
					echo '</ol></div>';

				}
				?>
				</div>

				<!-- start offerreport description -->
				<div class="report-description-text">
					<h5><?php echo Kohana::lang('ui_main.offerreports_description');?></h5>
					<?php echo $offerincident_description; ?>
					<br/>


					<!-- start news source link -->
					<?php if( count($offerincident_news) > 0 ) { ?>
					<div class="credibility">
					<h5><?php echo Kohana::lang('ui_main.offerreports_news');?></h5>
							<?php
								foreach( $offerincident_news as $offerincident_new)
								{
									?>
									<a href="<?php echo $offerincident_new; ?> " target="_blank"><?php
									echo $offerincident_new;?></a>
									<br/>
									<?php
								}
					?>
					</div>
					<?php } ?>
					<!-- end news source link -->

					<!-- start additional fields -->
					<?php if(strlen($custom_forms) > 0) { ?>
					<div class="credibility">
					<h5><?php echo Kohana::lang('ui_main.additional_data');?></h5>
					<?php

						echo $custom_forms;

					?>
					<br/>
					</div>
					<?php } ?>
					<!-- end additional fields -->

					<?php if ($features_count)
					{
						?>
						<br /><br /><h5><?php echo Kohana::lang('ui_main.offerreports_features');?></h5>
						<?php
						foreach ($features as $feature)
						{
							echo ($feature->geometry_label) ?
								"<div class=\"feature_label\"><a href=\"javascript:getFeature($feature->id)\">$feature->geometry_label</a></div>" : "";
							echo ($feature->geometry_comment) ?
								"<div class=\"feature_comment\">$feature->geometry_comment</div>" : "";
						}
					}?>

					<div class="credibility">
						<table class="rating-table" cellspacing="0" cellpadding="0" border="0">
                  <tr>
					<td><h5><?php echo Kohana::lang('ui_main.offer_average_rating');?>:</h5></td>
                    <td class="average-rating-box">
					<?php for ($i = 1; $i <= 5; $i++):?>
						<?php if ($offerincident_rating >= $i): ?>
                        <img id="offer_rating_<?php echo $i; ?>" alt="* " src="<?php echo url::site()?>themes/geoplaces/images/dummy_pix/rating_on.png" />
                        <?php else: ?>
                        <img id="offer_rating_<?php echo $i; ?>" alt="" src="<?php echo url::site()?>themes/geoplaces/images/dummy_pix/rating_off.png" />
                        <?php endif; ?>
                    <?php endfor; ?>
                    <span class="average-rating-value">(<span id="offerrating_<?php echo $offerincident_id; ?>"><?php echo $offerincident_rating; ?></span> out of 5)</span>
                    </td>
				  </tr>
				</table>
					</div>
				</div>

				<?php
					// Action::offerreport_extra - Allows you to target an individual offerreport right after the description
					Event::run('ushahidi_action.offerreport_extra', $offerincident_id);

					// Filter::comments_block - The block that contains posted comments
					Event::run('ushahidi_filter.comment_block', $comments);
					echo $comments;
				?>

				<?php
					// Filter::comments_form_block - The block that contains the comments form
					Event::run('ushahidi_filter.comment_form_block', $comments_form);
					echo $comments_form;
				?>

			</div>

			<div class="right-col">

				<div class="report-media-box-content">

					<div id="report-map" class="report-map">
						<div class="map-holder" id="map"></div>
				<ul class="map-toggles">
				  <li><a href="#" class="smaller-map">Smaller map</a></li>
				  <li style="display:block;"><a href="#" class="wider-map">Wider map</a></li>
				  <li><a href="#" class="taller-map">Taller map</a></li>
				  <li><a href="#" class="shorter-map">Shorter Map</a></li>
				</ul>
				<div style="clear:both"></div>
					</div>
				</div>

				<?php
					// Action::offerreport_view_sidebar - This gives plugins the ability to insert into the sidebar (below the map and above additional offerreports)
					Event::run('ushahidi_action.offerreport_view_sidebar', $offerincident_id);
				?>

				<div class="report-additional-reports">
					<h4><?php echo Kohana::lang('ui_main.additional_offerreports');?></h4>
					<?php foreach($offerincident_neighbors as $neighbor) { ?>
					  <div class="rb_report">
					  <h5><a href="<?php echo url::site(); ?>offerreports/view/<?php echo $neighbor->id; ?>"><?php echo $neighbor->offerincident_title; ?></a></h5>
					  <p class="r_date r-3 bottom-cap"><?php echo date('H:i M d, Y', strtotime($neighbor->offerincident_date)); ?></p>
					  <p class="r_location"><?php echo $neighbor->location_name.", ".round($neighbor->distance, 2); ?> Kms</p>
					</div>
			  <?php } ?>
				</div>

			</div>

			<div style="clear:both;"></div>



		</div>
	</div>
</div>
