<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title><?php echo Kohana::lang('ui_main.pricing');?></title>
<?php
// echo html::stylesheet(url::file_loc('css').'media/css/jquery-ui-themeroller', '', true);
echo html::stylesheet(url::file_loc('css').'media/css/home.min.css', '', true);
echo html::stylesheet(url::file_loc('css').'media/css/pricing_style.css', '', true);
//echo html::stylesheet(url::file_loc('css').'media/css/global', '', true);
//echo html::script(url::file_loc('js').'media/js/jquery', true);
// echo html::script(url::file_loc('js').'media/js/openid/openid-jquery', true);
// echo html::script(url::file_loc('js').'media/js/openid/openid-jquery-en', true);
// echo html::script(url::file_loc('js').'media/js/global', true);
?>

</head>

<body>

<?php echo $header_nav; ?>

<div id="wrapper">
<div class="container">
	<h1 class="pricing_header">Platform Pricing</h1>
	</div>
	<div class="container">
	<!--BRONZE-->
	    <div class="PricingMenu">
		<!--<span class="best_value"></span>-->
	    <div class="PricingHeader">
	    <h3>BRONZE</h3>
	    </div>
	    <div class="Amount">Free</div>
	    <ul class="PricingList">
		    <li>Mapping Places</li>
		    <li class="altRow">Mapping Offers</li>
		    <li>Reviews /Ratings </li>
		    <li class="altRow">Sales Analysis </li>
		    <li>Offers Analysis</li>
		    <li>User Analysis</li>
		    <li>User SMS Notifications</li>
		    <li>Newsletter/Blog </li>
		    <li>Ticket Support</li>
		    <li>Call Support</li>
		    <li>Featured Offers</li>
		    <li>Monthly Coupons</li>
		    <li>Payment Fees</li>
		    <li>MONTHLY USD</li>
		    <li>QUARTERLY USD</li>
		    <li>BI-ANNUAL USD</li>
		    <li>ANNUAL USD</li>		    
	    </ul>
	    </div>
	<!--END BRONZE-->
	<!--SILVER-->
	    <div class="PricingMenu_BestValue">
		<span class="best_value"></span>
	    <div class="PricingHeader">
	    <h3>SILVER</h3>
	    </div>
	    <div class="Amount">Free</div>
	    <ul class="PricingList_BestValue">
		    <li>Mapping Places</li>
		    <li class="altRow">Mapping Offers</li>
		    <li>Reviews /Ratings </li>
		    <li class="altRow">Sales Analysis </li>
		    <li>Offers Analysis</li>
		    <li>User Analysis</li>
		    <li>User SMS Notifications</li>
		    <li>Newsletter/Blog </li>
		    <li>Ticket Support</li>
		    <li>Call Support</li>
		    <li>Featured Offers</li>
		    <li>Monthly Coupons</li>
		    <li>Payment Fees</li>
		    <li>MONTHLY USD</li>
		    <li>QUARTERLY USD</li>
		    <li>BI-ANNUAL USD</li>
		    <li>ANNUAL USD</li>		    
	    </ul>
	    </div>
	<!--END SILVER-->
	<!--GOLD-->
	    <div class="PricingMenu_last">
		<!--<span class="best_value"></span>-->
	    <div class="PricingHeader">
	    <h3>GOLD</h3>
	    </div>
	    <div class="Amount">Free</div>
	    <ul class="PricingList_last">
		    <li>Mapping Places</li>
		    <li class="altRow">Mapping Offers</li>
		    <li>Reviews /Ratings </li>
		    <li class="altRow">Sales Analysis </li>
		    <li>Offers Analysis</li>
		    <li>User Analysis</li>
		    <li>User SMS Notifications</li>
		    <li>Newsletter/Blog </li>
		    <li>Ticket Support</li>
		    <li>Call Support</li>
		    <li>Featured Offers</li>
		    <li>Monthly Coupons</li>
		    <li>Payment Fees</li>
		    <li>MONTHLY USD</li>
		    <li>QUARTERLY USD</li>
		    <li>BI-ANNUAL USD</li>
		    <li>ANNUAL USD</li>		    
	    </ul>
	    </div>
	<!--END BRONZE-->
	</div>
	<!--Call to Action-->
<div class="MainAction_call"> <p class="SevenColumns">Reach a larger audience through crowdsourcing.</p> 
		  
            <div class="FiveColumns  Right"><p class="LearnMore"> <a href="#" class="MainSignup_button" title="Sign Up now">Sign Up Now</a><span class="Normal">or </span> <a href="#" class="LearnMore_home">Learn More</a></p></div>         
		 <div class="clear"></div> </div>
<!---->
    </div>
    </div></div>
</body>
</html>