<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />

<?php
// echo html::stylesheet(url::file_loc('css').'media/css/jquery-ui-themeroller', '', true);
echo html::stylesheet(url::file_loc('css').'media/css/home.min.css', '', true);
echo html::stylesheet(url::file_loc('css').'media/css/pricing_style.css', '', true);
//echo html::stylesheet(url::file_loc('css').'media/css/global', '', true);
//echo html::script(url::file_loc('js').'media/js/jquery', true);
// echo html::script(url::file_loc('js').'media/js/openid/openid-jquery', true);
// echo html::script(url::file_loc('js').'media/js/openid/openid-jquery-en', true);
// echo html::script(url::file_loc('js').'media/js/global', true);
?>

</head>

<body>

<?php echo $header_nav; ?>

<div id="wrapper">
    <div id="main">
<!--	HEADER-->
       
<!--	END HEADER-->

<!--CONTENT-->
<div class="container">

  <div class="row">
    <div class="span16">
      <h1 class="leading center-text push-down-half push-up-half">Support</h1>
    </div>
  </div>
  
  <div class="row">
    <div class="span-one-third">
      <h3>Ticket Support</h3>
      <p>Check out the crowdpesa faq for answers to your questions.</p>
      <ul class="unstyled">
        <li>
          <h5><a href="#">Get Ticket</a></h5>
        </li>
      </ul>
    </div>
    
    <div class="span-one-third">
      <h3>IRC</h3>
      <p>Hop onto the <code>#crowdpesa</code> channel on freenode to ask a questions. Generally crowdpesa team members are available for short chats.</p>
      <p><code>#crowdpesa</code> on <a href="#">irc.freenode.net</a></p>
      <p><a href="http://webchat.freenode.net" class="btn small">Freenode Webchat</a></p>
    </div>

    <div class="span-one-third">
      <h3>Email / Phone Support</h3>
      <p>Email and phone support are available for <a href="#">Enterprise</a> customers.</p>
    </div>

  </div>
</div>
<!--END CONTENT-->
    </div>
    
    </div>
</body>
</html>