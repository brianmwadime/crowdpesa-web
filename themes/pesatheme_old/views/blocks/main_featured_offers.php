<?php blocks::open("featured_offers");?>
<?php //blocks::title(Kohana::lang('ui_main.offers_featured'));?>
<?php if (count($offers)): ?>
<div id="featured_offers_carousel" class="carousel slide">
    <div class="grid clear carousel-inner" id="loop">
          <h3><span><?php echo Kohana::lang('ui_main.offers_featured') ?></span> <a class="more" href="<?php echo url::site()?>?context=offers">View All</a> </h3>
          <?php $counter = 1; ?>
          <?php foreach ($offers as $offer)
    		{
    			$offer_id = $offer->id;
                $offer_thumb = 'no_image_t.png';
                foreach($offer->media as $m){
                        if ($m->media_thumb && url::exists(url::site() . Kohana::config('upload.relative_directory') . '/' .$m->media_thumb) && $offer_thumb == 'no_image_t.png')
                            $offer_thumb = $m->media_thumb;
                }
                $offer_image_url = url::site() . Kohana::config('upload.relative_directory') . '/' . $offer_thumb;
    			$offer_title = text::limit_chars($offer->offerincident_title, 40, '...', true);
    			$offer_intro = text::limit_chars($offer->offerincident_description, 120, '...', true);
    			$offer_date = $offer->offerincident_start_date;
    			$offer_date = date('M j Y', strtotime($offer->offerincident_start_date));
    			$offer_end_date = $offer->offerincident_end_date;
    			$offer_end_date = date('M j Y', strtotime($offer->offerincident_end_date));
    			$offer_start_date = $offer->offerincident_start_date;
    			$offer_start_date = date('M j Y', strtotime($offer->offerincident_start_date));
    			$offer_location = $offer->location_id;
    			$offer_comments = $comment_count[$offer->id];
    			$offer_rating = $average_rating[$offer->id];
                $offer_url = url::site() . 'offer/details/' . $offer_id;
    		?>
            <?php if ($counter % 3 == 1): ?><div class="item <?php echo ($counter == 1) ? 'active' : '' ?>"><?php endif; ?>
              <div class="post-<?php echo $offer_id ?> place type-place status-publish hentry post featured_post" id="post_<?php echo $offer_id ?>">
                <div class="post-content"> <span class="featured_img">featured</span>
                	<a href="<?php echo $offer_url ?>" class="post_img"><img title="<?php echo $offer_title ?>" alt="<?php echo $offer_title ?>" src="<?php echo $offer_image_url ?>">
                    </a>
                    <div class="post_content">
                        <h2><a href="<?php echo $offer_url ?>" class="widget-title"><?php echo $offer_title ?></a></h2>
                        <span class="rating">
                            <?php for ($i = 1; $i <= 5; $i++):?>
                            <?php if ($offer_rating >= $i): ?>
                            <img alt="* " src="<?php echo url::site()?>themes/pesatheme/images/dummy_pix/rating_on.png" />
                            <?php else: ?>
                            <img alt="" src="<?php echo url::site()?>themes/pesatheme/images/dummy_pix/rating_off.png" />
                            <?php endif; ?>
        					<?php endfor; ?>
        					<a href="<?php echo $offer_url ?>"><img alt="<?php echo Kohana::lang('ui_main.offer_collect_this')?>" src="<?php echo url::site()?>themes/pesatheme/images/collect_offer_small.png" /></a>
                        </span>
                        <p><?php echo $offer_intro ?><a class="read_more" href="<?php echo url::site() ?>offerreports/view/<?php echo $offer_id ?>/">Read more</a> </p>
                        <span><?php //echo 'KES ' . floor(rand(5,20)) * 100 ?></span>
                        <!--	Addition of Social Sharing capabilities	-->
                        <div id="fb-root"></div>
                        <div style="float: left; width:37%;">
                        	<span class="sharetweet" id="tweet_offer_<?php echo $offer_id ?>">
                            	<a href="https://twitter.com/share" class="twitter-share-button" data-text="<?php echo $offer_title; ?>" data-url="<?php echo url::site() ?>offerreports/view/<?php echo $offer_id ?>" data-via="crowdpesa" data-count="none"></a>
                            </span>
                        </div>
                        <div style="float: left; width:28%;">
                        	<span class="shareInterest" id="pinit_offer_<?php echo $offer_id ?>">
                            	<a href="http://pinterest.com/pin/create/button/?url=<?php echo url::site() ?>offerreports/view/<?php echo $offer_id ?>" class="pin-it-button" count-layout="horizontal"><img border="0" src="//assets.pinterest.com/images/PinExt.png" title="Pin It" /></a>
                            </span>
                        </div>
                        <div style="float: left; width:28%">
                        	<span class="sharelike" id="like_offer_<?php echo $offer_id ?>">
                            	<div style="float: left;" class="fb-like" data-href="<?php echo url::site() ?>offerreports/view/<?php echo $offer_id ?>" data-layout="button_count" data-send="false" data-width="50" data-show-faces="false"></div>
                            </span>
                        </div>
                        <div style="clear:both"></div>
                        <!--	End of Addition of Social Sharing capabilities	-->        
        		<p class="review clearfix" style="border-bottom: none">
                            <?php /*<a class="pcomments" href="<?php echo url::site() ?>offer/details/<?php echo $offer_id ?>/#commentarea"><?php echo $offer_comments ?></a>
                            <span class="readmore">
                                <a href="<?php echo $offer_url ?>">Read More →</a>
                            </span> */ ?>
                        </p>
                    </div>
                </div>
              </div>
          <?php if ($counter % 3 == 0 || $counter == count($offers)): ?></div><?php endif; ?>
          <?php $counter++ ?>
    		<?php }?>
          <?php // <div class="hr clearfix"></div>?>
    </div>
    <a class="carousel-control left" href="#featured_offers_carousel" data-slide="prev" style="margin-left: -30px">&lsaquo;</a>
    <a class="carousel-control right" href="#featured_offers_carousel" data-slide="next">&rsaquo;</a>
</div>
<script type="text/javascript">
    $(function () {
        $('#featured_offers_carousel').carousel({interval: 2000});                       
    });
</script>
<div style="clear:left;"></div>
<?php endif; ?>
<?php blocks::close();?>