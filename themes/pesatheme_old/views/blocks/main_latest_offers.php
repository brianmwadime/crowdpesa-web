<?php blocks::open("latest_offers");?>
<?php //blocks::title(Kohana::lang('ui_main.offers_latest'));?>
<?php if (count($offers)): ?>
<h3><?php echo Kohana::lang('ui_main.offers_latest') ?></h3>
    <ul>
	<?php foreach ($offers as $offer)
    {
        $offer_id = $offer->id;
        $offer_title = text::limit_chars($offer->offerincident_title, 40, '...', true);
        $offer_date = 'valid until ' . date('j M Y \a\t g:m a', strtotime($offer->offerincident_end_date));
    ?>
      <li class="clearfix">
      <a href="<?php echo url::site() ?>offer/details/<?php echo $offer_id ?>"><?php echo $offer_title ?></a> <br>
      <span class="date"><?php echo $offer_date ?></span>
      </li>
	<?php 
	}
	?>
    </ul>
<?php endif; ?>
<?php blocks::close();?>