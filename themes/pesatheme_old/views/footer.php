    <div class="bottom">
    	<div class="footer tour_8">
    		<div class="footer_in">
    			<p class="copyright">
    				&copy; <?php echo date("Y") ?>
    				<a href="<?php echo url::site(); ?>"> CrowdPesa.com</a>. All rights reserved. <a href="#" class="footer-link">Privacy Policy</a> | <a href="#" class="footer-link">Terms of Service</a><br />Powered by <a href="http://www.pesapay.com/" class="footer-link">PesaPay</a> | <a href="http://www.ushahidi.com/" class="footer-link">Ushahidi</a> | <a href="http://www.whive.com/" class="footer-link">Whive SMS</a>
    			</p>
    		</div>
    	</div>
    </div>
</div>

