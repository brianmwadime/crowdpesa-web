<?php /*<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> */?>
<!DOCTYPE HTML>
<html<?php /*< xmlns="http://www.w3.org/1999/xhtml"*/?>>
<head>
<meta http-equiv="X-UA-Compatible" content="IE=8">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title><?php echo $page_title.$site_name; ?></title>
<?php /* Load all template javascript to prevent UI from blocking on ushahidi errors */ ?>
<script src="<?php echo url::site() ?>media/js/jquery.js" type="text/javascript"></script>

<script type="text/javascript">

var scroller  = null;

var scrollbar = null;

function scroll_down(){

    if (scroller)scroller.startScroll(0, -5, $('div#timelinefeed').height() - 400);

}

function scroll_up(){

    if (scroller)scroller.startScroll(0, 5);

}

function stop_scroll(){

    if (scroller)scroller.stopScroll();

}

function animate(element_ID, animation) {
    if (!$(element_ID).hasClass('animated')) {
        $(element_ID).addClass('animated');
    }
    $(element_ID).addClass(animation);
    var wait = window.setTimeout( function(){
        $(element_ID).removeClass(animation)}, 5000
    );
}

jQuery(document).ready(function($){

    jQuery('.mega-menu').dcMegaMenu({

        rowItems: '1',

        speed: 'fast'

    });
    
    
    jQuery("*[rel=tooltip]").tooltip();

    jQuery("label[for=geolocate]").popover({placement:'bottom'});
    

    /*jQuery("*[rel=popover]").popover({

        offset: 10,

        trigger: 'manual',

        animate: false,

        html: true,

        placement: 'bottom',

        //template: '<div class="popover" onmouseover="$(this).mouseleave(function() {$(this).hide(); });"><div class="arrow"></div><div class="popover-inner"><h3 class="popover-title"></h3><div class="popover-content"><p></p></div></div></div>'



    }).click(function(e) {

        jQuery(this).popover('show');

        e.preventDefault() ;

    }).mouseenter(function(e) {

        jQuery(this).popover('show');

    });*/

    
    jQuery(document).click(function() { jQuery("*[rel=popover]").popover('hide');});

    jQuery(document).click(function() { jQuery("*[rel=tooltip]").tooltip('hide');});

    <?php if (uri::instance()->controller(false) == 'main'): ?>

        scroller  = new jsScroller(document.getElementById("timelinescroller"), 240);

    <?php endif; ?>

});

</script>



<?php

	$dont_show_css = Kohana::config("settings.site_style_css");

	$dont_show_css = $dont_show_css[0];

	//echo str_ireplace('<link rel="stylesheet" type="text/css" href="'.$dont_show_css.'" />','',$header_block);

	// Remove jquery lib as it's already been loaded

	echo str_ireplace('<script type="text/javascript" src="' . url::site() . 'media/js/jquery.js"></script>','',$header_block);

	//echo $header_block;

?>

<?php

// Action::header_scripts - Additional Inline Scripts from Plugins

Event::run('ushahidi_action.header_scripts');

?>

<link rel="profile" href="http://gmpg.org/xfn/11" />

<link href="<?php echo url::site(); ?>themes/pesatheme/skins/1-default.css" rel="stylesheet" type="text/css" />

<link rel="stylesheet" type="text/css" href="<?php echo url::site(); ?>themes/pesatheme/library/css/print.css" media="print" />

<link rel="stylesheet" type="text/css" href="<?php echo url::site(); ?>themes/pesatheme/library/css/basic.css" media="all" />

<script type="text/javascript" src="//assets.pinterest.com/js/pinit.js"></script>

<script>(function(d, s, id) {

    var js, fjs = d.getElementsByTagName(s)[0];

    if (d.getElementById(id)) return;

    js = d.createElement(s); js.id = id;

    js.src = "//connect.facebook.net/en_US/all.js#xfbml=1&appId=289525864457161";

    fjs.parentNode.insertBefore(js, fjs);

}(document, 'script', 'facebook-jssdk'));

</script>

<script>

    !function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0]; 

    if(!d.getElementById(id)) {

            js=d.createElement(s); js.id=id; 

            js.src="//platform.twitter.com/widgets.js";

            fjs.parentNode.insertBefore(js,fjs);}

    }

    (document,"script", "twitter-wjs");

</script>

<script type="text/javascript">

function showLogin(){

	$(".LoginPopup").fadeTo(0, 400, function () {

			$(this).slideDown(600);

		});

	return false;

}

function hideLogin(){

	$(".LoginPopup").fadeTo(400, 0, function () {

			$(this).slideUp(600);

		});

	return false;

}

function toggle(){

	var div1 = document.getElementById('timelineWrapper');

	if (div1.style.display == 'none') {

		div1.style.display = 'block';

	} else {

		div1.style.display = 'none';

	}

	if($('#toggle').attr('class') == 'toggleoff'){

		$('#toggle').attr('class','toggleon');

		$('#toggle').attr('title','Click to show timeline');

		$('#toggle').attr('data-original-title', 'Click to show timeline');

	} else {

		$('#toggle').attr('class','toggleoff');

		$('#toggle').attr('title','Click to hide timeline');  

        $('#toggle').attr('data-original-title', 'Click to hide timeline');		

	}

}



function set_selected_city(){

    document.multicity_dl_frm_name.submit();

}



function geolocate_error(error){  

    switch(error.code)  

    {  

        case error.PERMISSION_DENIED: alert("You did not share your geolocation data.");  

        break;  



        case error.POSITION_UNAVAILABLE: alert("Unable to detect your current position.");  

        break;  



        case error.TIMEOUT: alert("The process timed out, please try again.");  

        break;  



        default: alert("Oops! Something went wrong. Please try again.");  

        break;  

    }

    $('input#geolocate').attr('checked', false);

    $('select#multi_city').removeAttr('disabled');

}



function toggleMultiCity(disable){

    if (disable) {

        if (navigator.geolocation){

            $('select#multi_city').attr('disabled', 'disabled');

            navigator.geolocation.getCurrentPosition(function(position){

                $('input#geolocate').attr('value', position.coords.latitude + ',' + position.coords.longitude);

                set_selected_city();

            },

            geolocate_error,

            {

                maximumAge: 0//1800000 //cache last geolocated position for 30 min

            }

            );

        }else{

            alert("Sorry, your browser does not support this feature.");

            $('input#geolocate').attr('checked', false);

            $('select#multi_city').removeAttr('disabled');

        }

        setTimeout(function(){

                if ($('input#geolocate').attr('value') == ''){

                    alert("Sorry, your browser cannot determine your location or you took too long to respond.");

                    $('input#geolocate').attr('checked', false);

                    $('select#multi_city').removeAttr('disabled');

                }

            }, 5000);

    }else{

        $('select#multi_city').removeAttr('disabled');

    }

}



var nofeeds = true;

var drawn_feeds = 0;

function highlightLocations(layer, marker_ids, style){

    if (!marker_ids || typeof(map) == 'undefined') return false;

    placeLayer = map.getLayersByName(layer)[0];

    var success = false;

    var style = (style) ? style: 'default'; 

    $.each(marker_ids, function(i, id){

        if (feature = placeLayer.getFeatureByFid(id)){

           if (feature.renderIntent != style){

                //placeLayer.drawFeature(feature, style);

                if (!feature.onScreen()){

                    map.panTo(new OpenLayers.LonLat(feature.attributes.lon, feature.attributes.lat).transform(

                            new OpenLayers.Projection("EPSG:4326"),

                            new OpenLayers.Projection("EPSG:900913")

                        )

                    );

                }

            }

           success = true;

        }

    });

    return success;

}



jQuery(document).ready(function($){
    
    if (typeof(map) !== 'undefined'){
        map.getLayersByName("Places")[0].events.on({
    
            featuresadded: function(event) {
    
                if (nofeeds || drawn_feeds < 2) // no point in zooming to just one marker
    
                    map.zoomToExtent(map.getLayersByName("Places")[0].getDataExtent());
    
            },
    
            loadend: function(event) {
    
                if(event.object.features.length === 0){
    
                    // alert user of no data
    
                    setTimeout(function(){$('#subscribe_alerts').modal('show');}, 5000);
    
                }
    
            }
    
        });
    }
});

function updateTimeline(lastDate) {

    <?php $context = (isset($context)) ? "&context=$context":''; ?>

    jsonurl = '<?php echo url::site()."api/?task=timeline$context&sincetime="; ?>' + lastDate

    var nodatahtml = '<div style="padding:10px;">No feeds found within a <?php echo Session::instance()->get('map_radius') ?>km radius of your location.</div>';

    var passed_date = lastDate;

    $.getJSON(jsonurl, function(data) {

        if (data.payload.success == 'true'){

            //timeline_response = data;

            if ($('div#timelinefeed').html() == nodatahtml)

                $('div#timelinefeed').html('');

            var user_colors = new Array();

            var feed_type_styles = new Array();

            feed_type_styles['ofr'] = 'offer_retail';

            feed_type_styles['ofr_ftrd'] = 'offer_featured';

            feed_type_styles['ofr_usr'] = 'offer_user';

            feed_type_styles['ci'] = 'checkin';

            feed_type_styles['place'] = 'place';

            // Get colors

            $.each(data.payload.users, function(i, payl) {

                user_colors[payl.id] = payl.color;

            });

            

            $.each(data.payload.feeds, function(i,item){

                nofeeds = false;

                if (lastDate === '' || lastDate < i){

                    lastDate = i;

                }

                // generate feed item data

                var html = '';

                var layer = (item.draw_marker == 0) ? "Places" : "Timeline"; 

                var layer_id = (item.type == "ci") ? new Array(item.id) : item.incident_ids;

                html += '<div onmouseover="highlightLocations(\''+layer+'\',['+layer_id+'], \''+item.type+'\')" onmouseout="highlightLocations(\''+layer+'\',['+layer_id+'], false)" class="feed" id="'+item.type+'_id_'+item.id+'"><a name="'+item.type+'_id_'+item.id+'" />';

                

                html += "<div class=\"colorblock shorterblock\" style=\"background-color:#"+user_colors[item.user]+";\"><div class=\"colorfade\"></div></div>";

                <?php /*if(item.media === undefined)

                {

                    // Tint the color a bit

                    html += "<div class=\"colorblock shorterblock\" style=\"background-color:#"+user_colors[item.user]+";\"><div class=\"colorfade\"></div></div>";

                <?php /*}else{

                    // Show image

                    html += "<div class=\"colorblock tallerblock\" style=\"background-color:#"+user_colors[item.user]+";\"><div class=\"imgblock\"><a href=\""+item.media[0].link+"\" rel=\"lightbox-group1\" title=\""+item.msg+"\"><img src=\""+item.media[0].thumb+"\" width=\"100\" /></a></div></div>";

                }*/?>

                var tooltip = (item.type == 'ci') ? 'Zoom map to this checkin' : 'Zoom map to nearest offer location';

                html += "<div style=\"float:right;margin-right:6px;\"><a class=\"moredetails\" reportid=\""+item.id+"\" href=\"javascript:externalZeroIn("+item.lon+","+item.lat+",16,"+item.id+");\"><img title=\""+ tooltip + "\"rel=\"tooltip\" class=\"tl_show_on_map\" src=\"<?php echo url::site(); ?>/themes/pesatheme/images/earth_trans_white.png\" /></a></div>";

                

                $.each(data.payload.users, function(j,useritem){

                    if(useritem.id == item.user){

                        <?php //html += "<h3 class="+feed_type_styles[item.type]+">"+useritem.name+"</h3>"; ?>

                        html += "<h3 class="+feed_type_styles[item.type]+">";

                        html += (useritem.profile == 1) ? "<a href=\"<?php echo url::site() . 'profile/user/'; ?>" + useritem.username + "\">"+useritem.name+"</a>" : useritem.name;

                        html += "</h3>";

                    }

                });

                

                var utcDate = item.date.replace(" ","T")+"Z";

                var url = '<?php echo url::site() ?>';

                url += (item.type == 'ci') ? 'listing/details/' : 'offer/details/';

                url += item.id;

                if (item.draw_marker == 0)

                    html += '<a target="_blank" href="' + url + '">';

                <?php /*if(item.msg == "")

                {

                    html += "<div class=\"cimsg\"><small><em>"+$.timeago(utcDate)+"</em></small></div>";

                }else{

                    html += "<span class=\"cimsg\">"+item.msg+"<br/><small><em>"+$.timeago(utcDate)+"</em></small></span>";

                }*/ ?>

                html += "<span class=\"cimsg\">"+item.msg+"<br/><small><em>"+$.timeago(utcDate)+"</em></small></span>";

                

                if (item.draw_marker == 0)

                    html += '</a>';

                

                if(item.comments !== undefined)

                {

                    var user_link = '';

                    var comment_utcDate = '';

                    $.each(item.comments, function(j,comment){

                        comment_utcDate = comment.date.replace(" ","T")+"Z";

                        if(item.user_id != 0){

                            user_link = '<a href=\"<?php echo url::site(); ?>profile/user/'+comment.username+'\">'+comment.author+'</a>';

                        }else{

                            user_link = ''+comment.author+'';

                        }

                       html += "<div style=\"clear:both\"></div>"+user_link+": "+comment.description+" <small>(<em>"+$.timeago(comment_utcDate)+"</em>)</small></div>";

                    });

                }

                

                html += "<div style=\"clear:both\"></div></div>";

                $('div#tempfeed').append(html);

                

                if (item.draw_marker == 1){

                    drawn_feeds += 1;

                    // generate feed item marker

                    var feedpoint = new OpenLayers.Geometry.Point(parseFloat(item.lon), parseFloat(item.lat));

                    feedpoint.transform(proj_4326, proj_900913);

    

                    var media_link = '';

                    var media_medium = '';

                    var media_thumb = '';

    

                    if(item.media === undefined)

                    {

                        // No image

                    }

                    else

                    {

                        // Image!

                        media_link = item.media[0].link;

                        media_medium = item.media[0].medium;

                        media_thumb = item.media[0].thumb;

                    }

    

                    var feedPoint = new OpenLayers.Feature.Vector(feedpoint, {

                        feed_id: item.id,

                        feed_type: item.type,

                        feed_msg: item.msg,

                        feed_media_link: media_link,

                        feed_media_medium: media_medium,

                        feed_media_thumb: media_thumb,

                        thumb: media_thumb,

                        highlighticon: 'media/markers/place_red_highlight.gif',

                        highlightwidth: 20,

                        highlightheight: 57,

                        hglticonoffsetY: -57,

                        name: '<a href="'+ item.link + '">' + item.msg + '</a>',

                        link: item.link,

                        lon: item.lon,

                        lat: item.lat

                    });

                    feedPoint.id = "feed_" + item.id;

                    feedPoint.fid = item.id;

                    <?php /*$("#"+feedPoint.id).popover({

                        offset: 10,

                        trigger: 'manual',

                        animate: false,

                        html: true,

                        placement: 'top',

                        //template: '<div class="popover" onmouseover="$(this).mouseleave(function() {$(this).hide(); });"><div class="arrow"></div><div class="popover-inner"><h3 class="popover-title"></h3><div class="popover-content"><p></p></div></div></div>'

        

                    }).click(function(e) {

                        $(this).popover('show');

                        e.preventDefault() ;

                    }).mouseenter(function(e) {

                        $(this).popover('show');

                    });

                    $("#"+feedPoint.id).tooltip();*/ ?>

                    timelineLayer.addFeatures(feedPoint);

                    //map.getLayersByName("Places")[0].addFeatures(feedPoint);

                }else if(item.draw_marker == 0 && passed_date != ''){

                    console.log('add new markers to map');

                    //map.getLayersByName("Places")[0].addFeatures(feedPoint);

                }

            });

            // update UI

            $('div#timelinefeed').prepend($('div#tempfeed').html());

            if (!nofeeds && drawn_feeds > 0){

                map.zoomToExtent(timelineLayer.getDataExtent());

                timelineLayer.redraw();

            }

            $('div#tempfeed').html("");

            jQuery("img[class=tl_show_on_map]").tooltip();

         }else{

            nofeeds = true;

            if ($('div#timelinefeed').html() == '') {

                $('div#timelinefeed').html(nodatahtml);

                setTimeout(function(){toggle();}, 10000);

            }

         }

         <?php //jQuery("image[r=1]").popover({placement:'bottom'}); ?>

    }).complete(function(){

        if ($('div#timelinefeed').html() == '') {

            $('div#timelinefeed').html(nodatahtml);

            setTimeout(function(){toggle();$('#toggle').tooltip('show');}, 10000);

        }

        setTimeout(function(){updateTimeline(lastDate);}, 30000);

    });

}

</script>

</head>

<?php

// Add a class to the body tag according to the page URI

$body_class = "home blog";

?>



<body class="<?php echo $body_class; ?>"><div id="tempfeed" style="display: none"></div>

<div class="wrapper">

<div class="header">

  <div class="header_in">

    <div class="logo"><a href="<?php echo url::site(); ?>"><span class="sitename"><img alt="<?php echo Kohana::config('settings.site_name')?>" src="<?php echo url::site(); ?>themes/pesatheme/images/logo.png"></span></a><span class="tagline"><?php echo Kohana::config('settings.site_tagline') ?></span>

  </div>

  <?php if (!Auth::instance()->logged_in()): ?>

  <div class="pinboard">

    <div class="inner tour_1">

      <div>

          <p><strong>Looking for great Offers?</strong><br>Get an invitation here</p>       

          <div class="bootstrap">

              <a class="btn btn-warning" href="<?php echo url::site() . 'customers/join' ?>">Request an Invite</a>

              <a class="btn btn-primary" href="#loginbox" data-toggle="modal">Sign In</a>

          </div>

      </div>

    </div>

    <?php /*<div class="Sheet2 Sheet"></div>

    <div class="Sheet3 Sheet"></div> */?>

  </div>

  <?php endif; ?>

    <div class="header_right">

          <div class="widget"><?php $logged_in =  Auth::instance()->logged_in(); $user = Auth::instance()->get_user();?>

            <ul class="member_link">

                  <li>Welcome <?php echo ($logged_in) ? $user->username .',' : 'guest'; ?></li>              

                  <?php if ($logged_in): ?><li><a <?php echo ($logged_in) ? '' : 'data-toggle="modal" '; ?>href="<?php echo ($logged_in) ? url::site() . 'logout/front' : '#loginbox'?>"<?php //echo ($logged_in) ? '' : ' onclick="showLogin();return false;"'; ?>><?php echo ($logged_in) ? 'Sign Out' : 'Sign In'?></a></li>

                  <?php endif; ?>

                </ul>

          </div>

          <!-- searchform --> 

          <?php echo $search; ?> 

          <!-- / searchform --> 

        </div>

	</div>

  <!-- header inner #end --> 

</div>



<!-- mainmenu -->

<div class="main_nav">

    <div class="main_nav_in ">

        <div class="widget">

            <div id="dc_jqmegamenu" class="mega-menu">

            <ul class="mega" id="menu-menu">

                 <?php 

				if(!Auth::instance()->logged_in()){

					nav::main_tabs($this_page, array('reports_submit', 'alerts', 'account'));

				}else{

				nav::main_tabs($this_page, array('reports_submit', 'alerts'));} ?>

            </ul>

            </div>

         </div>

         <div class="widget multi_city tour_2">
         <?php 
		 $cities = array(

		 				//Kohana::config('settings.default_lat') . ',' . Kohana::config('settings.default_lon') . ',' . Kohana::config('settings.default_zoom') => 'Select City',
						"-1.2920659,36.82194619999996" => 'Nairobi',
						"-0.0917016,34.76795679999998" => 'Kisumu',
						"-4.0434771,39.6682065" => 'Mombasa',
						"-0.3030988,36.080025999999975" => 'Nakuru',
                        "0.51524,35.26636899999994" => 'Eldoret',
                        "-0.678306,34.77196600000002" => 'Kisii',
                        "0.2827307,34.7518631" => 'Kakamega',
                        "0.0514721,37.64560419999998" => 'Meru',
                        "-1.0387569,37.08337529999994" => 'Thika',
                        "-1.1748105,36.83041019999996" => 'Kiambu',
                        "-0.7202360999999999,36.42853059999993" => 'Naivasha',
		 				);

		$session = Session::instance();

		$selected_city = ($session->get('user_map_center') === 1) ? $session->get('map_center_lat') . ',' . $session->get('map_center_lng') . ',' . $session->get('map_zoom') : Kohana::config('settings.default_lat') . ',' . Kohana::config('settings.default_lon') . ',' . Kohana::config('settings.default_zoom');

		 ?>

          <form method="post" action="" name="multicity_dl_frm_name" id="multicity_dl_frm_id">

            <label for="geolocate" rel="popover" data-content="Requires a browser & device that support geolocation." title="Detect my location"><?php echo form::checkbox('geolocate', '', ($session->get('user_map_center') === 2), 'onClick="toggleMultiCity(this.checked)"') ?>&nbsp;Use my location</label>

            <?php /*if ($session->get('user_map_center') === 2): ?>

            <?php echo form::dropdown(array('name' => 'multi_city', 'id' => 'multi_city', 'onchange' => 'set_selected_city()', 'disabled' => ''),$cities, $selected_city); ?>

            <?php else: ?>

            <?php echo form::dropdown(array('name' => 'multi_city', 'id' => 'multi_city', 'onchange' => 'set_selected_city()'),$cities, $selected_city); ?>

            <?php endif;*/ ?>
            <span class="bootstrap"><input type="text" class="location span2" data-items="6" placeholder="or enter your location.."></span>
            <input type="hidden" id="multi_city" name="multi_city" value="" />
          </form>
        <script type="text/javascript">
            $(function () {
                var geocode = false;
                var geocoder = new google.maps.Geocoder();
                
                var cities = [<?php foreach ($cities as $coords => $name) {echo '{coords:"' . $coords . '", name:"' . $name . '"},';} ?>];
                
                function geocodeLocation(address, success, failure) {
                    geocoder.geocode( { 'address': address}, function(results, status) {
                        if (status == google.maps.GeocoderStatus.OK) {
                            if (typeof(success) === 'function')
                                success.call(this, results[0].geometry.location);
                        } else {
                            if (typeof(failure) === 'function')
                                failure.call(this, status);
                        }
                    });
                }
                
                function parseLocation(item, val, text){
                    $('input#multi_city').val(val);
                    set_selected_city();
                }
                
                function isPredefinedLocation(location){
                    var isPredefined = false;
                    for (var i = 0, len = cities.length; i < len; i++){
                        if (cities[i]['name'] === location){
                            isPredefined = true;
                            break;
                        }
                    }
                    return isPredefined;
                }
                
                var geocode = function(e){
                    var charCode = (e.keyCode ? e.keyCode : e.which);
                    if (charCode == 13 && !isPredefinedLocation(e.target.value)){
                        $("#geocoding-status").removeClass('none');
                        geocodeLocation(
                            e.target.value, 
                            function(location){
                                parseLocation(null, location.lat() + ',' + location.lng(), e.target.value);
                                $("#geocoding-status").addClass('none');
                            },
                            function(status){
                                alert("Oops! Seems there was a problem, please try again.");
                            }
                        );
                    }
                }
                
                $('.location').typeahead({
                    source: cities,
                    val: 'coords',
                    itemSelected: parseLocation
                });
                
                $('.location').keyup(geocode);
                
                $("#multicity_dl_frm_id").submit(function(e){
                    if ($('input#multi_city').val() == '')
                        e.preventDefault();
                });
                
            });
        </script>
        </div>
    <img id="geocoding-status" src="<?php echo url::site() . 'themes/pesatheme/images/preloader_yellow.gif' ?>" style="position: absolute; right: -30px; top: 8px;" class="none">
     </div>
     
</div>

<?php if (!$logged_in): ?>

<div class="bootstrap">

    <div id="loginbox" class="modal hide fade" style="display: none;">

        <div class="modal-header">

          <a class="close" data-dismiss="modal">×</a>

          <h3><?php if (Kohana::config('settings.allow_social_login')): ?>
              Sign in to your account or using your preferred social network
              <?php else: ?>
              Sign in to your CrowdPesa account below
              <?php endif; ?>
          </h3>

        </div>

        <div class="modal-body">

            <div class="well">

            <form method="post" action="<?php echo url::site() . 'login' ;?>">

                <label>Username:</label>

                <input type="text" class="span3" name="username" placeholder="Enter your username...">

                <span class="help-block">Your username must be unique and contain no spaces</span>

                <label>Password:</label>

                <input type="password" size="20" name="password" class="span3" placeholder="Enter your password...">

                <span class="help-block">A good password has symbols, text and numbers</span>

                <!--<a style="float: right; margin-right: 48%" href="javascript:toggle('signin_forgot');">I forgot my password</a>-->

                <label class="checkbox" style="width:50%"><input type="checkbox" checked="checked" value="1" name="remember" id="remember">Remember Me</label>

                

                <button type="submit" class="btn btn-primary" name="submit">Sign In</button>

                <?php /* <a href="<?php echo url::site() . 'customers/join' ?>" class="btn btn-primary">Sign Up</a>

                <a href="#" class="btn" data-dismiss="modal">Close</a> */ ?>

                <input type="hidden" value="signin" name="action">

                <input type="hidden" name="redir" />

            </form>

            </div>

        </div>
        <?php if (Kohana::config('settings.allow_social_login')): ?>
        <div class="modal-footer" style="text-align: center">

          <a href="<?php echo url::site(); ?>login/twitter"><img src="<?php echo url::site(); ?>themes/pesatheme/images/signin-twitter-darker.png" alt="Twitter Login" title="Login with your Twitter account" /></a>

            <a href="<?php echo url::site(); ?>login/facebook"><img src="<?php echo url::site(); ?>themes/pesatheme/images/signin-facebook.png" alt="Facebook Login" title="Login with your Facebook account" /></a>

            <a href="<?php echo url::site(); ?>login/google"><img src="<?php echo url::site(); ?>themes/pesatheme/images/signin-google.png" alt="Google Login" title="Login with your Google account" /></a>

        </div>
        <?php endif; ?>
    </div>

</div>

<?php endif; ?>

<div class="bootstrap">
    <div id="subscribe_alerts" class="modal hide fade" style="display: none;">
        <div class="modal-header">
          <a class="close" data-dismiss="modal">×</a>
          <h3>Subscribe to alerts of new places in your area</h3>
        </div>
        <form method="post" action="<?php echo url::site() . 'alerts' ;?>">
        <div class="modal-body">
            <div class="well" style="margin-bottom: 0">
                <p>Unfortunately, we do not have any listings of places in your area. Subscribe using the form below and we will notify you when we do.</p>

                <label>Email:</label>
                <input type="text" class="span3" name="alert_email" placeholder="Enter your email address...">
                <span class="help-block">Relax, we will not disclose your email address to anyone</span>               
                <input type="hidden" id="alert_lat" name="alert_lat" value="<?php echo $session->get('map_center_lat'); ?>">
                <input type="hidden" id="alert_lon" name="alert_lon" value="<?php echo $session->get('map_center_lng'); ?>">
                <input type="hidden" id="alert_radius" name="alert_radius" value="<?php echo $session->get('map_radius'); ?>">
            </div>
        </div>

        <div class="modal-footer" style="text-align: left;padding-left: 34px;">
          <button type="submit" class="btn btn-primary" name="submit">Subscribe</button>
          <a href="#" class="btn" data-dismiss="modal">No thanks!</a>
        </div>
        </form>

    </div>

</div>