<?php if(count($incident_comments) > 0): ?>
<a name="commentarea" id="commentarea"></a>
<div class="report-comments">
  <h5><?php echo Kohana::lang('ui_main.reviews'); ?></h5>
  <?php foreach($incident_comments as $comment): ?>
  <div class="report-comment-box">
  	<a name="comment-<?php echo $comment->id; ?>" id="comment-<?php echo $comment->id; ?>"></a>
    <div class="comment-separator">&nbsp;</div>
    <div class="comment-title">reviewed by&nbsp;<strong><?php echo $comment->comment_author; ?></strong>&nbsp;on&nbsp;<?php echo date('j M Y \a\t g:m a', strtotime($comment->comment_date)); ?>
    <span class="comment-rating-box" id="crating_<?php echo $comment->id; ?>">
    <?php for ($i = 1; $i <= 5; $i++):?>
		<?php if ($comment->comment_rating >= $i): ?>
        <img onclick="javascript:rateoffer()" onmouseover="highlight_rating(<?php echo $i; ?>, <?php echo $comment->id; ?>)" onmouseout="javascript:restore_rating(this, '<?php echo $comment->id; ?>', '<?php echo $i ?>')" id="offer_rating_<?php echo $comment->id . '_' . $i; ?>" alt="* " src="<?php echo url::site()?>themes/geoplaces/images/dummy_pix/rating_on.png" />
        <?php else: ?>
        <img onclick="javascript:rateoffer()" onmouseover="highlight_rating(<?php echo $i; ?>, <?php echo $comment->id; ?>)" onmouseout="javascript:restore_rating(this, '<?php echo $comment->id; ?>', '<?php echo $i ?>')" id="offer_rating_<?php echo $comment->id . '_' . $i; ?>" alt="" src="<?php echo url::site()?>themes/geoplaces/images/dummy_pix/rating_off.png" />
        <?php endif; ?>
    <?php endfor; ?>
    </span>
    </div>
    <div><?php echo $comment->comment_description; ?></div>
  </div>
  <?php endforeach; ?>
</div>
<?php endif; ?>
