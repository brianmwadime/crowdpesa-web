      <h3><span><?php echo Kohana::lang('ui_main.'. $title) ?></span></h3>
      <?php if (count($items)): ?>
      <?php foreach ($items as $item) : ?>
      <?php
	  	$item_title = $item->title;
		$item_intro = text::limit_chars($item->description, 180, ' ...', true);
        $item->image = ($thumbnails[$item->id]) ? $thumbnails[$item->id] : 'no_image_t.png';
        $item_image_url = url::site() . Kohana::config('upload.relative_directory') . '/' . $item->image;
        $url_controller = ($context == 'places') ? 'listing' : 'offer';
		$item_url = url::site() . $url_controller . '/details/' .  $item->id;
		$comments_url = $item_url . '/#commentarea';
		$item_comment_count = (isset($comment_count[$item->id])) ? $comment_count[$item->id] : 0;
		$item_comment_count = ($item_comment_count != 1) ? $item_comment_count . ' Reviews' : $item_comment_count . ' Review';
	  ?>
      <div class="post-<?php echo $item->id ?> <?php echo ($item->featured == 1) ? 'featured_post' : '' ?> post" id="post_<?php echo $item->id ?>">
        <div class="post-content"><?php echo ($item->featured == 1) ? '<span class="featured_img">featured</span>' : '' ?>
        <a href="<?php echo $item_url ?>" class="post_img">
        <img title="<?php echo $item_title ?>" alt="<?php echo $item_title ?>" src="<?php echo $item_image_url ?>" />
        </a>
          <div class="post_content">
            <h2><a href="<?php echo $item_url ?>" class="widget-title"><?php echo $item_title ?></a></h2>
            <div class="post_right">
            	<a class="pcomments" href="<?php echo $comments_url ?>"><?php echo $item_comment_count ?></a>
                <div id="fb-root"></div>
                <div style="width: 37%; margin-bottom: 4px; display: inline-block;">
                	<span class="sharetweet" id="tweet_offer_<?php echo $item->id ?>">
                    	<a href="https://twitter.com/share" class="twitter-share-button" data-text="<?php echo $item_title; ?>" data-url="<?php echo $item_url ?>" data-via="crowdpesa" data-count="none"></a>
                    </span>
                </div>
                <div style="width:28%;">
                	<span class="shareInterest" id="pinit_offer_<?php echo $item->id ?>">
                    	<a href="http://pinterest.com/pin/create/button/?url=<?php echo $item_url ?>" class="pin-it-button" count-layout="horizontal"><img border="0" src="//assets.pinterest.com/images/PinExt.png" title="Pin It" /></a>
                    </span>
                </div>
                <div style="width: 28%; display: inline-block; margin-top: 4px;">
                	<span class="sharelike" id="like_offer_<?php echo $item->id ?>">
                    	<div style="float: left;" class="fb-like" data-href="<?php echo $item_url ?>" data-layout="button_count" data-send="false" data-width="50" data-show-faces="false"></div>
                    </span>
                </div>
            </div>
            <p class="address"></p>
            <p><?php echo $item_intro ?><a class="read_more" href="<?php echo $item_url ?>">Read more</a></p>
          </div>
        </div>
      </div>
      <?php endforeach; ?>
      <?php else: ?>
          <p>Sorry, no <?php echo Inflector::singular(Inflector::plural(Kohana::lang('ui_main.'. $title))) ?> were found in this category.</p>
      <?php endif; ?>