<div class="top_banner_section">
  <div class="top_banner_section_in clearfix">
    <div id="timelineWrapper">
    <div id="timelineScrollUp" onmouseover="scroll_up();" onmouseout="stop_scroll();"></div>
    <div id="timelinecontainer" class="tour_5">
    <div id="timelinescroller"><div id="timelinefeed"></div></div>
    </div>
    <div id="timelineScrollDown" onmouseover="scroll_down();" onmouseout="stop_scroll();"></div>
    </div>
    <div onclick="toggle();" class="toggleoff" id="toggle" title="Click to hide timeline" rel="tooltip"></div>
    <?php                               
    // Map and Timeline Blocks
    echo $div_map;
    echo $div_timeline;
    ?>
    <script type="text/javascript">
        $(document).ready(function(){   
            setupTimelineMap();
            updateTimeline('');
            });
    </script>
  </div>
</div>
<div class="clear" id="container">
    <div class="content left tour_6">
    <?php if($context == 'all'): ?>
            <?php blocks::render_position('body', $this_page) ?>
    <?php else:?>
        <div class="list clear" id="loop">
            <?php echo $listings;?>
            <div class="pagination" ><?php echo $pagination;?></div>
        </div>
    <?php endif;?>
    </div>
    <div class="sidebar right right_col tour_7">
        <?php blocks::render_position('right', $this_page) ?>
    </div>
</div>