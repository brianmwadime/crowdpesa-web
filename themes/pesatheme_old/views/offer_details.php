<div class="clear" id="container">
  <div class="content left">
    <div class="blog-article list clear" id="loop">
      <?php
        $session = Session::instance();
	  	$incident_title = $incident->offerincident_title;
		$incident_owner = $incident_user->name;
        $retailer_id = $incident_user->id;
		$address = $incident_user->retailer_info->address;
		$phone = $incident_user->retailer_info->phone;
		$website = $incident_user->retailer_info->website;
		$opening_hours = $incident_user->retailer_info->opening_hours;
        $button = Auth::instance()->logged_in() ? 'collect_offer.png' : 'login_collect.png';
        $collect_url = Auth::instance()->logged_in() ? url::site() . 'offer/collect/' . $incident->id : '#loginbox';//url::site() . 'login?redir';
        $collect_js = Auth::instance()->logged_in() ? '' : 'showLogin();return false;';
        $collect_attrib = (Auth::instance()->logged_in()) ? '' : 'data-toggle="modal" ';
        $price = 'KES ' . $incident->offerincident_price;
		//$incident_published = 'on ' . date('j M Y \a\t g:m a', strtotime($incident->offerincident_dateadd));
		$incident_intro = text::limit_chars($incident->offerincident_description, 130, ' ...', true);
		$incident_body = $incident->offerincident_description;
        $incident_thumb = (count($incident->media)) ? $incident->media[0]->media_thumb : 'no_image_t.png';
		$incident_image_url = url::site() . Kohana::config('upload.relative_directory') . '/' . $incident_thumb;
        $offer_type_url = ($incident->offer_type->icon) ? url::site() . Kohana::config('upload.relative_directory') . '/' . $incident->offer_type->icon : null;
		$incident_url = url::site() . 'offer/details/' .  $incident->id;
        $subscribe_msg = ($user_subscribed) ? 'Unsubscribe from our future updates' : 'Subscribe to our future updates';
        $subscribe_link = ($user_subscribed) ? "Unsubscribe($retailer_id)" : "Subscribe($retailer_id)";
		//$comments_url = $incident_url . '/#commentarea';
		$incident_comment_count = (isset($comment_count[$incident->id])) ? $comment_count[$incident->id] : 0;
		$incident_comment_count = ($incident_comment_count != 1) ? $incident_comment_count . ' Reviews' : $incident_comment_count . ' Review';
        $media = array();
        foreach ($incident->media as $m){
            if ($m->media_medium && url::exists(url::site() . Kohana::config('upload.relative_directory') . '/' .$m->media_medium))
                $media[] = $m;
        }
	  ?>
      <h3><span><?php echo $incident_title ?></span>
          <div class="blog-share" style="text-align: right">
            <div id="fb-root"></div>
            <div style="width: 29%; display: inline-block; margin-bottom: 0px;"> <span class="sharetweet" id="tweet_offer_<?php echo $incident->id ?>"> <a href="https://twitter.com/share" class="twitter-share-button" data-text="<?php echo $incident_title; ?>" data-url="<?php echo $incident_url ?>" data-via="crowdpesa" data-count="none"></a> </span> </div>
            <div style="width: 22%; display: inline-block; margin-bottom: -5px;"> <span class="shareInterest" id="pinit_offer_<?php echo $incident->id ?>"> <a href="http://pinterest.com/pin/create/button/?url=<?php echo $incident_url ?>" class="pin-it-button" count-layout="horizontal"><img border="0" src="//assets.pinterest.com/images/PinExt.png" title="Pin It" /></a> </span> </div>
            <div style="width: 28%; display: inline-block; margin-top: 0;"> <span class="sharelike" id="like_offer_<?php echo $incident->id ?>">
              <div style="float: left;" class="fb-like" data-href="<?php echo $incident_url ?>" data-layout="button_count" data-send="false" data-width="50" data-show-faces="false"></div>
              </span> </div>
          </div>
      </h3>
      
      <div class="post-<?php echo $incident->id ?> article-view-post post" id="post_<?php echo $incident->id ?>">
        <?php /*?>Listing details section<?php */?>
        <div class="post-content"><div class="map-holder" id="listing_map"></div>
          <div class="listing_content">
            <p style="font-style: italic"><?php if ($offer_type_url): ?><img class="offer-icon" src="<?php echo $offer_type_url; ?>" /><?php endif; ?><?php echo $incident_body ?></p>
            <p><strong>Offer rating:</strong>
                <span class="rating">
                <?php for ($i = 1; $i <= 5; $i++):?>
                    <?php if ($incident_rating >= $i): ?>
                    <img alt="* " src="<?php echo url::site()?>themes/pesatheme/images/dummy_pix/rating_on.png" />
                    <?php else: ?>
                    <img alt="" src="<?php echo url::site()?>themes/pesatheme/images/dummy_pix/rating_off.png" />
                    <?php endif; ?>
                <?php endfor; ?>
                </span>
            </p>
            <p><strong>Offer Value:</strong>&nbsp;<?php echo $price ?></p>
            <p><strong>Validity:</strong>&nbsp;<?php echo date('j M g:m a', strtotime($incident->offerincident_start_date)) . ' - ' . date('j M g:m a', strtotime($incident->offerincident_end_date)) ?></p>
            <p><strong>Coupons Available:</strong>&nbsp;<?php echo ($incident->max_coupons) ? $incident->max_coupons : 'unlimited' ?></p>
            <?php if ($incident->scope == 0): ?>
            <p><strong>Coupon:</strong>&nbsp;<br /><img alt="<?php echo $incident_title ?> coupon" src="<?php echo url::site() . 'offer/coupon/' . $incident->id ?>" /></p>
            <?php else:?>
            <?php if ($session->get('offer_' . $incident->id . '_token')): ?>
                <p>Your verification token is <strong>"<?php echo $session->get('offer_' . $incident->id . '_token')?>"</strong> Present it to one of the <?php echo $incident_owner ?> stores highlighted on the map to claim your offer along with the following coupon:<br /><img alt="<?php echo $incident_title ?> coupon" src="<?php echo url::site() . 'offer/coupon/' . $incident->id ?>" /></p>
            <?php else: ?>
            <p><a href="<?php echo $collect_url ?>" <?php echo $collect_attrib ?>><img alt="Collect Offer" src="<?php echo url::site() . 'themes/pesatheme/images/' . $button ?>" /></a></p>
            <?php endif; ?>    
            <?php endif; ?>
          </div>
        </div>
        
      </div>
       <ul class="sort_by">
          <li class="<?php echo ($tab == 'rr') ? 'current' : '' ?>"> <a href="<?php echo $incident_url ?>?t=rr" onclick="<?php echo ($tab_count['rr'] == 0) ? 'return false;' : '' ?>">  Reviews (<?php echo $tab_count['rr'] ?>) </a></li>
          <li class="<?php echo ($tab == 'so') ? 'current' : '' ?>"> <a href="<?php echo $incident_url ?>?t=so" onclick="<?php echo ($tab_count['so'] == 0) ? 'return false;' : '' ?>">  Similar Offers (<?php echo $tab_count['so'] ?>) </a></li>
          <?php /*<li class="<?php echo ($tab == 'no') ? 'current' : '' ?>"> <a href="<?php echo $incident_url ?>?t=no">  Nearby Offers (<?php echo $tab_count['no'] ?>)  </a></li> */?>
       </ul>
       <?php if ($tab == 'rr'):?>
       <div class="article-comments">
         <?php echo $comments; ?>
         <?php echo $comments_form; ?>
       </div>
       <?php elseif ($tab == 'so'):?>
           <ul class="content-tab">
           <?php foreach ($similar_incidents as $i): ?>
                <li><a href="<?php echo url::site() . '/offer/details/' . $i->id ?>"><?php echo $i->title ?></a> [<?php echo 'valid until ' . date('j M Y \a\t g:m a', strtotime($i->end_date));?>]
                   <span class="single_rating">
                    <?php for ($n = 1; $n <= 5; $n++):?>
                        <?php if ($i->rating >= $n): ?>
                        <img alt="* " src="<?php echo url::site()?>themes/pesatheme/images/dummy_pix/rating_on.png" />
                        <?php else: ?>
                        <img alt="" src="<?php echo url::site()?>themes/pesatheme/images/dummy_pix/rating_off.png" />
                        <?php endif; ?>
                    <?php endfor; ?>
                  </span>
                <br /><?php echo $i->description ?>
                </li>
           <?php endforeach; ?>
           </ul>
       <?php elseif ($tab == 'no'):?>
           
       <?php endif; ?>
    </div>
  </div>
  <div class="sidebar right right_col">
    <?php // Load brand info here ?>
    <div class="company_info"> <a class="i_claim" title="<?php echo $incident_owner ?>" href="<?php echo url::site() . 'profile/user/' . $incident_user->username ?>"><?php echo $incident_owner ?>&nbsp;</a>
      <p> <span class="i_location">Address:</span> <?php echo $address ?></p>
      <p> <span class="i_website"><a target="blank" href="<?php echo $website ?>"><strong>Website</strong></a> </span> </p>
      <p> <span class="i_time"> Time: </span> <?php echo $opening_hours ?></p>
      <p> <span class="i_contact">Phone: </span> <?php echo $phone ?></p>
      <p> <span class="fav"><a onclick="<?php echo $subscribe_link ?>" class="addtofav" href="javascript:void(0);"><?php echo $subscribe_msg ?></a></span> </p>
      <p> <span class="i_rating">Brand Rating  :</span>
          <span class="single_rating">
            <?php for ($i = 1; $i <= 5; $i++):?>
                <?php if ($brand_rating >= $i): ?>
                <img alt="* " src="<?php echo url::site()?>themes/pesatheme/images/dummy_pix/rating_on.png" />
                <?php else: ?>
                <img alt="" src="<?php echo url::site()?>themes/pesatheme/images/dummy_pix/rating_off.png" />
                <?php endif; ?>
            <?php endfor; ?>
          </span>
      </p>
    </div>
    <?php // Load listing images ?>
    <?php if (count($media)): ?>
    <script type="text/javascript">
        $(window).load(function() {
            $('#slider').nivoSlider();
        });
    </script>
    <div class="widget recent_comments_section">
        <h3><?php echo Kohana::lang('ui_main.photos'); ?></h3>
        <div id="slider" class="nivoSlider">
        <?php foreach ($media as $m): ?>
            <img src="<?php echo url::site() . Kohana::config('upload.relative_directory') . '/' . $m->media_medium ?>" alt="<?php echo $m->media_title ?>" title="<?php echo $m->media_title ?>" />
        <?php endforeach; ?>
        </div>
    </div>
    <?php endif; ?>
    
  </div>
</div>
